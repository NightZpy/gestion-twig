<?php 
class GestionFirkSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $acciones = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'activo' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $asociado_clientes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'asociado_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'cliente_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $auditorias = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'modelo' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'registro_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'campo' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'valor_anterior' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'valor_nuevo' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'fecha' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'usuario_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'observacion' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $bancos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'INDEX-ACTIVO' => array('column' => 'activo', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $ciudades = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 80, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'estado_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'activo' => array('type' => 'boolean', 'null' => true, 'default' => '1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $clasificaciones = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'relacion_cliente' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'relacion_concepto' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $cobranza_facturas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'cobranza_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'factura_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'abono_monto_base' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'abono_monto_islr' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'abono_monto_iva' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'recibo_id_2' => array('column' => array('cobranza_id', 'factura_id'), 'unique' => 1),
			'factura_id' => array('column' => 'factura_id', 'unique' => 0),
			'recibo_id' => array('column' => 'cobranza_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $cobranzas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'numero' => array('type' => 'integer', 'null' => false, 'default' => null),
		'fecha' => array('type' => 'date', 'null' => false, 'default' => null),
		'asociado_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'cliente_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'observaciones' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'estatus_instrumento' => array('type' => 'integer', 'null' => true, 'default' => null),
		'estatus_monto' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'asociado_id' => array('column' => 'asociado_id', 'unique' => 0),
			'cliente_id' => array('column' => 'cliente_id', 'unique' => 0),
			'proveedor_id' => array('column' => 'proveedor_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $concepto_proveedores = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'concepto_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $conceptos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'porcentaje_islr' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '9,2'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'impuesto_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'impuesto_id' => array('column' => 'impuesto_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $cuentas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'persona_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index'),
		'banco_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'tipo_cuenta_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'numero' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'INDEX' => array('column' => array('persona_id', 'banco_id', 'tipo_cuenta_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $descripciones = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'concepto_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'descripcion' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1000, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'proveedor_id' => array('column' => array('proveedor_id', 'concepto_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $detalle_facturas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'factura_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'concepto_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'descripcion' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'cantidad' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'precio_unitario' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_base' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_iva' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_islr' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_total' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_iva' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_islr' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'impuesto_islr_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'impuesto_iva_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_detalle_facturas_1' => array('column' => 'factura_id', 'unique' => 0),
			'impuesto_islr_id' => array('column' => array('impuesto_islr_id', 'impuesto_iva_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $estados = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 80, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $estatus_workflows = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'predeterminado' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $factura_recibos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'recibo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'factura_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'abono_monto_base' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'abono_monto_islr' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'abono_monto_iva' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '11,2'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'recibo_id_2' => array('column' => array('recibo_id', 'factura_id'), 'unique' => 1),
			'factura_id' => array('column' => 'factura_id', 'unique' => 0),
			'recibo_id' => array('column' => 'recibo_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);
	public $facturas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'requerimiento_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'razon_social' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'fecha_requerida' => array('type' => 'date', 'null' => true, 'default' => null),
		'fecha_facturacion' => array('type' => 'date', 'null' => true, 'default' => null),
		'numero_factura' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'numero_control' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'monto_base' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_iva' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_total' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_retencion_iva' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_retencion_iva' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_retencion_islr' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'pagado' => array('type' => 'float', 'null' => false, 'default' => null),
		'monto_costo_venta' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_venta_secundario' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_pago_proveedor' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_administrativo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_gestion' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_operativo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_maximo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'concepto_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'observaciones' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_facturas_1' => array('column' => 'requerimiento_id', 'unique' => 0),
			'concepto_id' => array('column' => 'concepto_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);
	public $impuestos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'valor' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '9,2'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'predeterminado' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'es_iva' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'es_retencion_islr' => array('type' => 'integer', 'null' => true, 'default' => null),
		'es_retencion_iva' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $instruccion_pagos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'requerimiento_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'monto' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '11,2'),
		'fecha' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'tipo_instrumento_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'banco_origen_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'numero_referencia' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'banco_destino_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'cuenta_destino_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'estatus_instruccion_pago_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);
	public $instrumentos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'cliente_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'tipo_instrumento_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'nro_referencia' => array('type' => 'integer', 'null' => true, 'default' => null),
		'banco_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'cuenta_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'monto' => array('type' => 'float', 'null' => false, 'default' => null),
		'fecha_carga' => array('type' => 'date', 'null' => false, 'default' => null),
		'asociado_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'estatus_instrumento_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'cobranza_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'cliente_id' => array('column' => 'cliente_id', 'unique' => 0),
			'proveedor_id' => array('column' => 'proveedor_id', 'unique' => 0),
			'tipo_instrumento_id' => array('column' => 'tipo_instrumento_id', 'unique' => 0),
			'banco_origen_id' => array('column' => 'banco_id', 'unique' => 0),
			'cuenta_id' => array('column' => 'cuenta_id', 'unique' => 0),
			'asociado_id' => array('column' => 'asociado_id', 'unique' => 0),
			'estatus_instrumento_id' => array('column' => 'estatus_instrumento_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $persona_personas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'persona_padre_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'persona_hijo_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tipo_relacion' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);
	public $personas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'codigo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nombre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 80, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'apellido' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 80, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'razon_social' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tipo_persona_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'clasificacion_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'telefono' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 12, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'direccion' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tipo_documento_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'documento' => array('type' => 'integer', 'null' => false, 'default' => null),
		'contribuyente_especial' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'porcentaje_retencion_iva' => array('type' => 'integer', 'null' => true, 'default' => null),
		'porcentaje_costo_gestion' => array('type' => 'float', 'null' => true, 'default' => null),
		'porcentaje_costo_operativo' => array('type' => 'float', 'null' => true, 'default' => null),
		'correo_electronico' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 180, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'usuario' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'clave' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'es_usuario' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'estado_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'ciudad_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'pagina_web' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 180, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'monto_costo_venta' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_venta_secundario' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'monto_costo_administrativo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_costo_venta' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_costo_venta_secundario' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_costo_administrativo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'validado' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $prioridades = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'predeterminado' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $recibos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'numero' => array('type' => 'integer', 'null' => false, 'default' => null),
		'fecha' => array('type' => 'date', 'null' => false, 'default' => null),
		'asociado_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'cliente_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'proveedor_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'observaciones' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'estatus_instrumento' => array('type' => 'integer', 'null' => true, 'default' => null),
		'estatus_monto' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'asociado_id' => array('column' => 'asociado_id', 'unique' => 0),
			'cliente_id' => array('column' => 'cliente_id', 'unique' => 0),
			'proveedor_id' => array('column' => 'proveedor_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);
	public $requerimientos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'asociado_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'cliente_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'estatus_workflow_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'prioridad_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'porcentaje_costo_gestion' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'porcentaje_costo_operativo' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_costo_gestion' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_pago_proveedor' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_costo_operativo' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_costo_venta' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_costo_venta_secundario' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_costo_administrativo' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_base' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_iva' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_total' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_retencion_iva' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'monto_retencion_islr' => array('type' => 'float', 'null' => true, 'default' => '0.00', 'length' => '11,2'),
		'creado' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'creado_por' => array('type' => 'integer', 'null' => true, 'default' => null),
		'modificado' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modificado_por' => array('type' => 'integer', 'null' => true, 'default' => null),
		'porcentaje_costo_venta' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_costo_venta_secundario' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'porcentaje_costo_administrativo' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '11,2'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'InnoDB')
	);
	public $tipo_cuentas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'INDEX-ACTIVO' => array('column' => 'activo', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $tipo_documentos = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'key' => 'primary', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nombre_corto' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $tipo_instrumentos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 80, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'grupo_tipo_instrumento_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'requiere_banco' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'requiere_cuenta' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'requiere_referencia' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'grupo_tipo_instrumento_id' => array('column' => 'grupo_tipo_instrumento_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);
	public $tipo_personas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nombre_corto' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
	public $transiciones = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'estatus_inicial_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'estatus_final_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'accion_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'usuario_grupo_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'activo' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'es_modificable' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);
}

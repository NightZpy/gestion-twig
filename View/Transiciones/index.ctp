    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('id');?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('estatus_inicial_id');?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('estatus_final_id');?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('accion_id');?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('rol_id');?></th>
            <th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php
        foreach ($transiciones as $transicion): 
        ?>
        <tr class="registros">
            <td></td>
            <td><?php echo h($transicion['Transicion']['id']); ?></td>
            <td><?php echo h($transicion['EstatusInicial']['nombre']); ?></td>
            <td><?php echo h($transicion['EstatusFinal']['nombre']); ?></td>
            <td><?php echo h($transicion['Accion']['nombre']); ?></td>
            <td><?php echo h($transicion['Rol']['nombre']); ?></td>
            <td class="options-width">
                <?php echo $this->Html->link('', array('action' => 'view', $transicion['Transicion']['id']), array('class' => 'icon-5 info-tooltip', 'title'=>'Ver')); ?>
                <?php echo $this->Html->link('', array('action' => 'edit', $transicion['Transicion']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Editar')); ?>
                <?php echo $this->Form->postLink('', array('action' => 'delete', $transicion['Transicion']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $transicion['Transicion']['id'])); ?>
                
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
<?php echo $this->Form->create('Transicion'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Transicion'); ?></legend>
	<?php
		echo $this->Form->input('estatus_inicial_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('estatus_final_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('accion_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('rol_id', array('empty' => '-- seleccione --'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

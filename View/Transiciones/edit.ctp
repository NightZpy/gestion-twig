<div class="Transicion form">
    <?php echo $this->Form->create('Transicion'); ?>
	<fieldset>
		<legend><?php echo __('Editar Transicion'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('estatus_inicial_id');
        echo $this->Form->input('estatus_final_id');
        echo $this->Form->input('accion_id');
        echo $this->Form->input('rol_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
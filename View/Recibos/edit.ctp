<?php
$this->Html->script('recibosEdit.js', array('inline' => false));
?>

<div>
	<fieldset>
		<?php echo $this->Form->create(); ?>
		<div class="seccion-izquierda">
			<?php
			echo $this->Form->input('numero', array(
				'class' => 'numero',
				'label' => 'Número')
			);
			echo $this->Form->input('fechafld', array(
				'type' => 'text',
				'class' => 'fecha',
				'label' => 'Fecha')
			);
			echo $this->Form->input('fecha', array('type' => 'hidden'));
			if (!$this->UserAuth->isAsociado()) {
				echo $this->Form->input('asociado_id', array('empty' => 'Seleccione...'));
			} else {
				echo $this->Form->input('asociado_id', array('type' => 'hidden'));
			}
			echo $this->Form->input('cliente_id');
			echo $this->Form->input('proveedor_id');
			?>
		</div>
		<div class="seccion-derecha">
			<label>Observaciones</label>
			<?php echo $this->Form->textarea('observaciones', array('class' => 'observaciones')) ?>
		</div>
		<input type="hidden" name="accion" value="recibo">
		<?php echo $this->Form->end('submit'); ?>
	</fieldset>
</div>

<div style="margin-top: 20px;">
	<fieldset>
		<legend>Facturas</legend>
		<div>
			<a class="btn1" id="agregarFactura" href="#">Agregar Factura Pendiente</a>
		</div>


		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<thead>
				<tr>
					<th class="table-header-check">&nbsp;</th>
					<th class="table-header-repeat fecha"><a>Fecha Factura</a></th>
					<th class="table-header-repeat numero"><a>Número</a></th>
					
					<th class="table-header-repeat numero"><a>Monto Factura</a></th>
                                        <th class="table-header-repeat numero"><a>Saldo Pendiente</a></th>
					
					<th class="table-header-repeat numero"><a>Monto Abonado</a></th>
					
					<th class="table-header-repeat numero"><a>Monto Ab. Ret. ISLR</a></th>
					
					<th class="table-header-repeat numero"><a>Monto Ab. Ret. IVA</a></th>
					<th class="table-header-options"><a>Acciones</a></th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach($facturaRecibos as $facturaRecibo): ?>
					<tr>
						<td><?php echo $facturaRecibo['fr']['id']?></td>
						<td><?php echo AppHelper::fechaNormal($facturaRecibo['f']['fecha_facturacion'])?></td>
						<td><?php echo $facturaRecibo['f']['numero_factura']?><input type="hidden" id="fac<?php echo $facturaRecibo['fr']['id']?>" value="<?php echo $facturaRecibo['f']['id']?>"></td>
						<td class="td-number"><?php echo CakeNumber::currency($facturaRecibo['f']['monto_total'], 'VEF')?></td>						
						<td class="td-number"><?php echo CakeNumber::currency($facturaRecibo['f']['monto_maximo'], 'VEF')?></td>
						<td class="td-number editable" id="mba<?php echo $facturaRecibo['fr']['id']?>">
							<?php echo CakeNumber::currency($facturaRecibo['fr']['abono_monto_base'], 'VEF')?>
							<input type="hidden" value="<?php echo $facturaRecibo['fr']['abono_monto_base']?>">
						</td>
						
						<td class="td-number editable" id="mis<?php echo $facturaRecibo['fr']['id']?>">
							<?php echo CakeNumber::currency($facturaRecibo['fr']['abono_monto_islr'], 'VEF')?>
							<input type="hidden" value="<?php echo $facturaRecibo['fr']['abono_monto_islr']?>">
						</td>
						
						<td class="td-number editable" id="miv<?php echo $facturaRecibo['fr']['id']?>">
							<?php echo CakeNumber::currency($facturaRecibo['fr']['abono_monto_iva'], 'VEF')?>
							<input type="hidden" value="<?php echo $facturaRecibo['fr']['abono_monto_iva']?>">
						</td>
						<td><?php echo $this->Form->postLink('', array('action' => 'deleteFactura', $facturaRecibo['fr']['id']), array('class' => 'icon-17 info-tooltip', 'title' => 'Eliminar'), 'Esta seguro de eliminar este registro?'); ?></td>
						
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</fieldset>
</div>

<div style="margin-top: 20px;">
	<fieldset>
		<legend>Instrumentos de Pago</legend>
		<div>
			<a class="btn1" id="agregarInstrumento" href="#">Agregar Instrumento de Pago</a>
		</div>

		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<thead>
				<tr>
					<th class="table-header-check"><a>Id</a></th>
					<th class="table-header-repeat minwidth-10"><a>Tipo</a></th>
					<th class="table-header-repeat minwidth-10"><a>Referencia</a></th>
					<th class="table-header-repeat minwidth-10"><a>Banco Origen</a></th>
					<th class="table-header-repeat minwidth-1"><a>Cuenta</a></th>
					<th class="table-header-repeat minwidth-1"><a>Monto</a></th>
					<th class="table-header-repeat minwidth-1"><a>Estatus</a></th>
					<th class="table-header-options"><a>Acciones</a></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($instrumentos as $instrumento):
					$instrumentoId = $instrumento['i']['id'];
					?>
					<tr>
						<td><?php echo $instrumento['i']['id'] ?> <input type="hidden" id="proveedorId<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['proveedor_id'] ?>"> </td>
						<td><?php echo $instrumento['ti']['nombre'] ?><input type="hidden" id="tipoInstrumentoId<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['tipo_instrumento_id'] ?>"></td>
						<td><?php echo $instrumento['i']['nro_referencia'] ?><input type="hidden" id="nroReferencia<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['nro_referencia'] ?>"></td>
						<td><?php echo $instrumento['b']['nombre'] ?><input type="hidden" id="bancoId<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['banco_id'] ?>"></td>
						<td><?php echo $instrumento['cue']['numero'] ?><input type="hidden" id="cuentaId<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['cuenta_id'] ?>"></td>
						<td class="td-number"><?php echo CakeNumber::currency($instrumento['i']['monto'], 'VEF') ?><input type="hidden" id="monto<?php echo $instrumentoId ?>" value="<?php echo $instrumento['i']['monto'] ?>"></td>
						<td><?php echo $instrumento['ei']['nombre'] ?> </td>
						<td style="width:110px;">
							<?php if ($instrumento['i']['estatus_instrumento_id'] == 1){?>
							<?php echo $this->Form->postLink('', array('action' => 'aprobarInstrumento', $instrumento['i']['id'], $this->request->data['Recibo']['id']), array('class' => 'icon-14 info-tooltip', 'title' => 'Eliminar'), 'Esta seguro de aprobar este instrumento?', $instrumento['i']['id']); ?>
							<a class="icon-13" onclick="javascript:editarInstrumento(<?php echo $instrumentoId ?>);return false" href="#"></a>
							<?php }?>
	<?php echo $this->Form->postLink('', array('action' => 'deleteInstrumento', $instrumento['i']['id'], $this->request->data['Recibo']['id']), array('class' => 'icon-17 info-tooltip', 'title' => 'Eliminar'), 'Esta seguro de eliminar este registro?', $instrumento['i']['id']); ?>
						</td>
					</tr>
<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

	<div class="resumen">
		<table>
			<thead>
				<tr><th>&nbsp;</th><th>Recibido</th><th>Disponible</th></tr>
			</thead>
<?php 
	if (isset($totalesRecibo))
		foreach ($totalesRecibo as $key => $row): ?>
				<tr>
					<td><?php echo $row['nombre'] ?></td>
					<td align="right"><?php echo CakeNumber::currency($row['recibido'], 'VEF') ?></td>
					<td align="right"><?php echo CakeNumber::currency($row['disponible'], 'VEF') ?><input type="hidden" id="flddisponible<?php echo $key?>" value="<?php echo $row['disponible']?>"></td>
				</tr>
<?php endforeach; ?>
		</table>
		<?php if (!$totalesRecibo){?>
		<input type="hidden" id="flddisponible1" value="0">
		<input type="hidden" id="flddisponible2" value="0">
		<input type="hidden" id="flddisponible3" value="0">
		<?php }?>
		
	</div>

</div>

<div style = "display:none;" id = "dialogForm">
	<div clas = "seccion-izquierda">
		<?php
		echo $this->Form->create('Instrumento');
		echo $this->Form->input('tipo_instrumento_id');
		echo $this->Form->input('nro_referencia', array('class' => 'numero', 'div' => array('id' => 'fldReferencia')));
		echo $this->Form->input('banco_id', array('empty' => '', 'div' => array('id' => 'fldBanco')));
		echo $this->Form->input('cuenta_id', array('empty' => '', 'div' => array('id' => 'fldCuenta')));
		echo $this->Form->input('monto', array('class' => 'numero'));
		echo $this->Form->input('fecha_carga', array('type' => 'hidden'));
		echo $this->Form->input('asociado_id', array('type' => 'hidden'));
		echo $this->Form->input('cliente_id', array('type' => 'hidden'));
		echo $this->Form->input('proveedor_id', array('type' => 'hidden'));
		?>
		<input type="hidden" name="data[Instrumento][estatus_instrumento_id]" value="1">
		<input type="hidden" id="InstrumentoId" name="data[Instrumento][id]" value="">
		<input type="hidden" name="data[Instrumento][recibo_id]" value="<?php echo $this->request->data['Recibo']['id'] ?>">
		<input type="hidden" id ="accion" name="accion" value="detalle">
		</form>
	</div>
	
	<div style="margin-top: 10px;" id="dataInstrumentosConciliados">
		<?php if (count($instrumentosConciliados) > 0){ ?>
		<fieldset>
			<legend>Instrumentos Conciliados</legend>
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr><td>&nbsp;</td><td>Instrumento</td><td>Ref.</td><td>Banco</td><td>Cuenta</td><td>Monto</td></tr>
				<?php foreach ($instrumentosConciliados as $conciliado):?>
				<tr id="inst<?php echo $conciliado['i']['id'];?>">
					<td><a href="#" id="fact<?php echo $conciliado['i']['id'];?>" class="icon-14" onclick="javascript:agregarInstrumentoAprobado(<?php echo $conciliado['i']['id'];?>);return false;"></a></td>
					<td><?php echo $conciliado['ti']['nombre']?></td>
					<td><?php echo $conciliado['i']['nro_referencia']?></td>
					<td><?php echo $conciliado['b']['nombre']?></td>
					<td><?php echo $conciliado['c']['numero']?></td>
					<td><?php echo CakeNumber::currency($conciliado['i']['monto'],'VEF')?></td>
				</tr>
				<?php endforeach;?>
			</table>
		</fieldset>
		<?php }?>
	</div>
	
</div>


<div id="facturas" style="display:none;">
	
	<form id="frmbuscarfacturas">
		<div class="input number">
			<label for="nrorequerimiento">Requerimiento Nro.</label>
			<input id="nrorequerimiento" name="data[Factura][requerimiento_id]" type="text" class="numero">
		</div>
		<div class="input number">
			<label for="nrofactura">Factura Nro.</label>
			<input id="nrofactura" name="data[Factura][numero_factura]" type="text" class="numero">
		</div>
		
		<div class="input number">
			<label for="fechadesde">Fecha</label>
			<input id="fechadesde" class="fechas numero" name="data[Factura][fecha_facturacion][]" type="text" class="numero" style="margin-right: 15px;">
			<input id="fechahasta"  class="fechas numero" name="data[Factura][fecha_facturacion][]" type="text" class="numero">
		</div>
		
		<button class="btn1" id="btnBuscarFacturas" type="button">Buscar</button>
	</form>
	
	<div id="detalleFacturas">
		
	</div>
</div>

<div id="formMonto" style="display:none;">
	<form name="frmMonto">
		<input name="monto">
	</form>
</div>

<script>
	
	var cuenta = <?php echo count($this->validationErrors['Instrumento']) ?>;
	var reciboId = <?php echo $this->request->data['Recibo']['id'] ?>;
	var td ="";
	var instAgregado=0;
	//var disponible = <?php echo $totalesRecibo[1]['disponible'] + $totalesRecibo[2]['disponible'] + $totalesRecibo[3]['disponible'] ?>;
    var disponible = <?php echo $totalesRecibo[1]['recibido'] + $totalesRecibo[2]['recibido'] + $totalesRecibo[3]['recibido'] ?>;
	var recibido = <?php echo $totalesRecibo[1]['recibido'] + $totalesRecibo[2]['recibido'] + $totalesRecibo[3]['recibido'] ?>;
	
	activarEdicion();
	
	$('#btnBuscarFacturas').bind('click', function(e){
		e.preventDefault();
		$.ajax({
			url:'/recibos/getFacturasPendientes/page:0',
			type:'post',
			data:$('#frmbuscarfacturas').serialize()+'&recibo_id='+reciboId+'&data[Factura][recibo_id]='+reciboId+'&data[Factura][cliente_id]='+$('#ReciboClienteId').val()
		}).done(function(data){
			$('#detalleFacturas').html(data);
		});
	})
	
	function activarEdicion(){
		$('.editable').bind('click',function(){
			td = $(this).attr('id');
			var facturaReciboId = td.substring(3,10);
			var campo = td.substring(0,3);
			var facturaid = $('#fac'+facturaReciboId).val();
			var valor = $('#'+td+' > input' ).val();
			$('#'+td).html('<form id="formedit" onsubmit="javascript:return prueba(\''+campo+'\');" name="formedit" method="post" action="/recibos/actualizaMonto"><input type="hidden" name="factura_id" value="'+facturaid+'"><input type="hidden" name="campo" value="'+campo+'"><input type="hidden" name="factura_recibo_id" value="'+facturaReciboId+'"><input type="hidden" name="recibo_id" value="'+reciboId+'"><input class="td" id="fldmonto" name="monto" value="'+valor+'"><input type="submit" style="display:none;"></form>');
			$('#fldmonto').focus().select();

			$('.editable').unbind('click');
			
			$('#fldmonto').keyup(function(e){
				if (e.keyCode == 27){
					location.reload();
				}
				
			})
			
		})
	}
	
	function agregarInstrumentoAprobado(instrumentoId){
		$.ajax({
			url:'/recibos/agregarInstrumentoAprobado',
			type:'post',
			data:'instrumento_id='+instrumentoId+'&recibo_id='+reciboId
		}).done(function(data){
			if(data=="OK"){
				instAgregado++;
				$('#inst'+instrumentoId).hide();
			}
			
		});
	}
	
	function prueba(campo){
		if (campo=='mba'){
			if (Number($('#fldmonto').val()) > Math.round(Number($('#flddisponible1').val())*100)/100){
				alert('El monto ingresado es mayor que el disponible');
				return false;
			}
		}
		
		if (campo=='mis'){
			if (Number($('#fldmonto').val()) > Math.round(Number($('#flddisponible2').val())*100)/100){
				alert('El monto ingresado es mayor que el disponible');
				return false;
			}
		}
		
		if (campo=='miv'){
			if (Number($('#fldmonto').val()) > Math.round(Number($('#flddisponible3').val())*100)/100){
				alert('El monto ingresado es mayor que el disponible!!!');
				return false;
			}
		}
		
	}

</script>
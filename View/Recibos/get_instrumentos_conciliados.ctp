<?php if (count($instrumentosConciliados) > 0) { ?>
	<fieldset>
		<legend>Instrumentos Conciliados</legend>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<tr><td>&nbsp;</td><td>Instrumento</td><td>Ref.</td><td>Banco</td><td>Cuenta</td><td>Monto</td></tr>
			<?php foreach ($instrumentosConciliados as $conciliado): ?>
				<tr id="inst<?php echo $conciliado['i']['id']; ?>">
					<td><a href="#" id="fact<?php echo $conciliado['i']['id']; ?>" class="icon-14" onclick="javascript:agregarInstrumentoAprobado(<?php echo $conciliado['i']['id']; ?>);return false;"></a></td>
					<td><?php echo $conciliado['ti']['nombre'] ?></td>
					<td><?php echo $conciliado['i']['nro_referencia'] ?></td>
					<td><?php echo $conciliado['b']['nombre'] ?></td>
					<td><?php echo $conciliado['c']['numero'] ?></td>
					<td><?php echo CakeNumber::currency($conciliado['i']['monto'], 'VEF') ?></td>
				</tr>
				<?php endforeach; ?>
		</table>
	</fieldset>
<?php
}?>
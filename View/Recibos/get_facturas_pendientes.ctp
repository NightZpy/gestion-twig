<div style="color:red;" id="msgError"></div>
<div>
    <form>
        <label>Distribucion automática</label>
        <input id="automatica" type="checkbox" />
            
    </form>
</div>
<div style="height:400px;overflow: scroll;clear: both;">
<table border="0" width="95%" cellpadding="0" cellspacing="0" id="product-table" style="margin-top: 10px;">
	<thead>
		<tr>
			<th class="table-header-check">&nbsp;</th>
			<th  class="table-header-repeat"><a>Factura nro.</a></th>
			<th  class="table-header-repeat"><a>Monto Total.</a></th>
			<th  class="table-header-repeat"><a>Pendiente Dinero.</a></th>
			<th  class="table-header-repeat"><a>Ret ISLR.</a></th>
			<th  class="table-header-repeat"><a>Ret IVA.</a></th>
			
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($facturas['data'] as $factura):?>
		<tr>
			<?php if ($factura['factura_recibos']['factura_id'] != $factura['facturas']['id']){?>
			<td><a href="#" id="fact<?php echo $factura['facturas']['id'];?>" class="icon-14" onclick="javascript:agregarFactura(<?php echo $factura['facturas']['id'];?>);return false;"></a></td>
			<?php }else{?>
			<td><a href="#" id="fact<?php echo $factura['facturas']['id'];?>" class="checked" onclick="javascript:eliminarFactura(<?php echo $factura['factura_recibos']['id'];?>);return false;"></a></td>
			<?php }?>
			<td><?php echo $factura['facturas']['numero_factura'];?></td>
			<td class="td-number"><?php echo CakeNumber::currency($factura['facturas']['monto_total'],'VEF');?></td>
			<td class="td-number"><?php echo CakeNumber::currency($factura['facturas']['monto_base']-$factura['facturas']['monto_retencion_islr']+$factura['facturas']['monto_iva']-$factura['facturas']['monto_retencion_iva'], 'VEF');?></td>
			<td class="td-number"><?php echo CakeNumber::currency($factura['facturas']['monto_retencion_islr'], 'VEF');?></td>
			<td class="td-number"><?php echo CakeNumber::currency($factura['facturas']['monto_retencion_iva'], 'VEF');?></td>
			
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
	<div class="paging">
		<span class="prev"><?php echo $this->Html->link('Anterior', array('action'=>'getFacturasPendientes/'), array('id'=>'anterior'))?></span>
		<span class="next"><?php echo $this->Html->link('Siguiente', array('action'=>'getFacturasPendientes/'), array('id'=>'siguiente'))?></span>
	</div>
	
</div>

<div class="resumen">
    <table >
        <thead>
            <tr><td>&nbsp;</td><td>Disponible</td></tr>
        </thead>
        <?php foreach ($totales as $total):?>    
            <tr><td><?php echo $total['nombre']?></td><td class="td-number"><?php echo CakeNumber::currency($total['disponible'],'VEF')?></td></tr>
        <?php endforeach;?>
    </table>
</div>
<script>
	var page = <?php echo $page ?>;
	var max = Math.ceil(<?php echo isset($facturas['paginado'][0][0]['cuenta'])?$facturas['paginado'][0][0]['cuenta']:0; ?>);
	$('#anterior').bind('click',function(e){
		e.preventDefault();
		if (page>0)
		$.ajax({
			url:'/recibos/getFacturasPendientes/page:<?php echo $page-1?>',
			type:'post',
			data:'recibo_id='+reciboId+'&data[Factura][recibo_id]='+reciboId+'&data[Factura][cliente_id]='+$('#ReciboClienteId').val()+'&'+$('#frmbuscarfacturas').serialize()
		}).done(function(data){
			$('#detalleFacturas').html(data);
		});
	})
	
	$('#siguiente').bind('click',function(e){
		e.preventDefault();
		if (page < max)
		$.ajax({
			url:'/recibos/getFacturasPendientes/page:<?php echo $page+1?>',
			type:'post',
			data:'recibo_id='+reciboId+'&data[Factura][recibo_id]='+reciboId+'&data[Factura][cliente_id]='+$('#ReciboClienteId').val()+'&'+$('#frmbuscarfacturas').serialize()
		}).done(function(data){
			$('#detalleFacturas').html(data);
		});
	})
	
	function agregarFactura(id){
        var ch = $('#automatica').is(':checked');
		$.ajax({
			url:'recibos/agregaFactura/',
			data:'data[FacturaRecibo][factura_id]=' + id + '&data[FacturaRecibo][recibo_id]=' + reciboId+'&automatico='+ch,
			type:'post'
		}).done(function(data){
			if (data == "OK"){
				refrescar();
				//$('#fact'+id).attr('class','checked');
			}else{
				$('#msgError').html('No se pudo agregar la factura ya que no tiene disponibilidad en instrumentos').slideDown(300).delay(3000).slideUp(300);
			}
		})
	}
	
	function eliminarFactura(id){
		$.ajax({
			url:'recibos/deleteFactura/'+id,
			data:'via=ajax&data[FacturaRecibo][factura_id]=' + id + '&data[FacturaRecibo][recibo_id]=' + reciboId,
			type:'post'
		}).done(function(data){
			if (data == "OK"){
				refrescar();
				//$('#fact'+id).attr('class','icon-14');
			}else{
				$('#msgError').html('No se pudo agregar la factura ya que no tiene disponibilidad en instrumentos').slideDown(300).delay(3000).slideUp(300);
			}
		})
	}
	
	function refrescar(){
		$.ajax({
			url:'/recibos/getFacturasPendientes/page:<?php echo $page?>',
			type:'post',
			data:'recibo_id='+reciboId+'&data[Factura][recibo_id]='+reciboId+'&data[Factura][cliente_id]='+$('#ReciboClienteId').val()+'&'+$('#frmbuscarfacturas').serialize()
		}).done(function(data){
			$('#detalleFacturas').html(data);
		});
	}
</script>
<?php 
	$this->Html->script('recibosAdd.js', array('inline' => false));
?>

<div>
	<?php echo $this->Form->create('Recibo'); ?>
	<div class="seccion-izquierda">
		<?php
		echo $this->Form->input('numero', array(
			'class' => 'numero',
			'label' => 'Número')
		);
		echo $this->Form->input('fechafld', array(
			'type' => 'text',
			'class' => 'fecha',
			'label' => 'Fecha')
		);
		echo $this->Form->input('fecha', array('type' => 'hidden'));
		
		if (!$this->UserAuth->isAsociado()){
			echo $this->Form->input('asociado_id', array('empty'=>'Seleccione...'));
		}else{
			echo $this->Form->input('asociado_id', array('type'=>'hidden'));
		}
		
		echo $this->Form->input('cliente_id');
		echo $this->Form->input('proveedor_id');
		?>
	</div>
	<div class="seccion-derecha">
		<label>Observaciones</label>
		<?php echo $this->Form->textarea('observaciones', array('class'=>'observaciones')) ?>
	</div>
	<?php echo $this->Form->end('submit'); ?>
</div>

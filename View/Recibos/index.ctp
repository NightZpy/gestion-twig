<?php
$this->Html->script('recibosIndex.js', array('inline' => false));
?>

<fieldset>
	<legend>Buscar Recibos Cargados</legend>
	<?php
	echo $this->Form->create('Recibo');

	echo $this->Form->input('numero', array('class'=>'numero'));
	
	if (!$this->UserAuth->isAsociado()){
		echo $this->Form->input('asociado_id', array('empty'=>'Todos'));
	}else{
		echo $this->Form->input('asociado_id', array('type'=>'hidden'));
	}
	//echo $this->Form->input('asociado_id', array('empty' => 'Todos', 'selected' => isset($asociados_seleccionado[0]) ? $asociados_seleccionado[0] : null));
	echo $this->Form->input('cliente_id', array('empty' => 'Todos'));

	echo $this->Form->end(array('label' => 'Agregar', 'class' => 'form-buscar'));
	?>

	<div style="clear:both;">

	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" style="margin-top: 10px;">
		<thead>
			<tr>
				<th class="table-header-check">Id</th>
				<th class="table-header-repeat line-left numero"><?php echo $this->Paginator->sort('Recibo.numero', 'Nro. Recibo'); ?></th>
				<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('Asociado.razon_social', 'Asociado'); ?></th>
				<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('Cliente.razon_social', 'Cliente'); ?></th>
				<th class="table-header-options"><a>Acciones</a></th>
			</tr>
		</thead>

		<tbody>
<?php foreach ($recibos as $recibo) : ?>
				<tr>
					<td><?php echo $recibo['Recibo']['id']; ?></td>
					<td><?php echo $recibo['Recibo']['numero']; ?></td>
					<td><?php echo $recibo['Asociado']['razon_social']; ?></td>
					<td><?php echo $recibo['Cliente']['razon_social']; ?></td>
					<td class="actions">
	<?php echo $this->Html->link('', array('action' => 'edit', $recibo['Recibo']['id']), array('class' => 'icon-13')); ?>
				<?php //echo $this->Form->postLink('', array('action' => 'delete', $instrumento['Instrumento']['id']), array('class' => 'icon-17'), __('Are you sure you want to delete # %s?', $instrumento['Instrumento']['id']));  ?>
					</td>
				</tr>
<?php endforeach; ?>

		</tbody>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
		));
		?>	
    </p>

	<div class="paging">
		<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>	

</fieldset>

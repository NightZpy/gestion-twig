<h1>Ciudades</h1>
<p><a href="ciudades/agregar" class="btn">Agregar Registro</a></p>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre'); ?></th>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('estado_id'); ?></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php foreach ($ciudades as $ciudad):  ?>
    <tr class="registros">
        <td nowrap><?= $ciudad['Ciudad']['nombre']; ?></td>
        <td nowrap><?= $ciudad['Estado']['nombre']; ?></td>
        <td class="options-width">
            <?php echo $this->Html->link('', array('action' => 'editar', $ciudad['Ciudad']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
            <?php echo $this->Form->postLink('', array('action' => 'eliminar', $ciudad['Ciudad']['id']), array('class' => 'icon-17 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $ciudad['Ciudad']['id'])); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?= $this->element('paginate'); ?>

<script>
	$(function(){
		$('.btn').button();
	})
</script>
<div class="ciudades form">
<?php echo $this->Form->create('Ciudad'); ?>
	<fieldset>
		<legend>Ciudad</legend>
	<?php
        echo $this->Form->input('id');
        echo $this->Form->input('nombre');
        echo $this->Form->input('estado_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('capital');
        echo $this->Form->input('activo');
	?>
	</fieldset>
<?php echo $this->Form->end('Guardar'); ?>
</div>
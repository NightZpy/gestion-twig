<h1><?php  echo __('Ver Prioridad'); ?></h2>
<dl>
    <dt><?php echo __('Nombre'); ?></dt>
    <dd>
        <?php echo h($prioridad['Prioridad']['nombre']); ?>
        &nbsp;
    </dd>
    <dt><?php echo __('Predeterminado?'); ?></dt>
    <dd>
        <?php echo $this->Html->mostrarBoolean($prioridad['Prioridad']['predeterminado']); ?>
        &nbsp;
    </dd>
    <dt><?php echo __('Activo?'); ?></dt>
    <dd>
        <?php echo $this->Html->mostrarBoolean($prioridad['Prioridad']['activo']); ?>
        &nbsp;
    </dd>
</dl>
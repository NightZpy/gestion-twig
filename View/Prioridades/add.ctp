<?php echo $this->Form->create('Prioridad'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Prioridad'); ?></legend>
	<?php
		echo $this->Form->input('nombre');
        echo $this->Form->input('predeterminado');
        echo $this->Form->input('activo');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

    <?php echo $this->Form->create('Prioridad', array('action'=>'index','id'=>'filters')); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <fieldset>
            <legend>Buscar Prioridades</legend>
                <?php 
                echo $this->Form->input('nombre', array('class' => 'Search'));
                ?>
                <?php echo $this->Form->end(array('label'=>'Filtrar',  'class' => 'form-buscar')); ?>
        </fieldset>
        <tr>
            <th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('predeterminado'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('activo'); ?></th>
            <th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php
        foreach ($prioridades as $prioridad): 
        ?>
        <tr class="registros">
            <td></td>
            <td><?php echo h($prioridad['Prioridad']['nombre']); ?></td>
            <td><?php echo $this->Html->mostrarBoolean($prioridad['Prioridad']['predeterminado']); ?></td>
            <td><?php echo $this->Html->mostrarBoolean($prioridad['Prioridad']['activo']); ?></td>
            <td class="options-width">
                <?php echo $this->Html->link('', array('action' => 'edit', $prioridad['Prioridad']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
                <?php echo $this->Form->postLink('', array('action' => 'delete', $prioridad['Prioridad']['id']), array('class' => 'icon-17 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $prioridad['Prioridad']['id'])); ?>
                
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
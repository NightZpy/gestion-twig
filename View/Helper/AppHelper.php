<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
    
    /**
     * 
     * Mostrar Boolean
     * 
     * Permite mostrar el texto SI o NO según el valor del campo Tinyint
     * 
     * @param Tinyint $valor
     * @return String
     */
    public function mostrarBoolean($valor) {
        return $valor ? 'Si' : 'No';
    }
	
	static function fechaNormal($fecha){
		$return=NULL;
		if ($fecha!=""){
			$str =  explode("-", $fecha);
			$return = $str[2]."-".$str[1]."-".$str[0];
		}
		return $return;
		
	}
    
    static function Mostrar($groupId, $controller, $action)
    {
        $Pintar = ClassRegistry::init('Factura'); 
        $probar = $Pintar->getPermisos($groupId, $controller, $action);
        return $probar;
    }
    
}

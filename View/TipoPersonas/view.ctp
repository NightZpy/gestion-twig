<h1><?php  echo __('Tipo Persona'); ?></h1>
	<dl>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($tipoPersona['TipoPersona']['nombre']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Nombre Corto'); ?></dt>
		<dd>
			<?php echo h($tipoPersona['TipoPersona']['nombre_corto']); ?>
			&nbsp;
		</dd>
	</dl>
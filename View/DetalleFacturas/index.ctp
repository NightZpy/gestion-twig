<?php
if (isset($factura_id)) {
?>
<fieldset>
    <legend><?php echo 'Detalle de Facturas'; ?></legend>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Id'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Cant'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Concepto'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Descripcion'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Tasa IVA'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Tasa ISLR'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Precio Unitario'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Sub Total Base'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Sub Total IVA'); ?></a></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php
    foreach ($detallefacturas as $detallefactura): 
    ?>
    <tr class="registros">
        <td></td>
        <td><?php echo h($detallefactura['DetalleFactura']['id']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['cantidad']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['concepto_id']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['descripcion']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['porcentaje_iva']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['porcentaje_islr']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['sub_total_iva']); ?></td>
        <td class="options-width">
            <?php echo $this->Html->link('', array('controller' => 'facturas', 'action' => 'edit', $factura_id, $detallefactura['DetalleFactura']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Editar')); ?>
            <?php echo $this->Form->postLink('', array('action' => 'delete', $detallefactura['DetalleFactura']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Eliminar'), 'Esta seguro de eliminar este registro?', $detallefactura['DetalleFactura']['id']); ?>
        </td>
    </tr>
    <?php 
    endforeach; 
    ?>
</table>
</fieldset>
<?php
}
?>
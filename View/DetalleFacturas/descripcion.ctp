<h2>Descripciones</h2>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-repeat line-left minwidth-1"></th>
            <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'descripcion'; ?></a></th>
        </tr>
        <?php
        foreach ($descripciones as $descripcion): 
        ?>
        <tr class="registros">
            <td>
                <?php echo $this->Html->link('', 'javascript: fSelecciona(\''.$descripcion['DetalleFactura']['descripcion'].'\');', array('class' => 'icon-14 info-tooltip', 'title'=>'Ver')); ?>
            </td>
            <td><?php echo h($descripcion['DetalleFactura']['descripcion']); ?></td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
<script>
function fSelecciona (valores)
{
    var cadena = valores;
  
    parent.parent.document.getElementById("DetalleFacturaDescripcion").value = cadena;
    parent.parent.GB_CURRENT.hide();
}
</script>
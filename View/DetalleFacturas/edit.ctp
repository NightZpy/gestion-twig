<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <?php echo $this->Form->create('Detalle_Factura'); ?>
    <tr>
        <td><?php echo $this->Form->input('factura_id', array('type' => 'hidden', 'value' => $id)); ?></td>
        <td><?php echo $this->Form->input('cantidad', array('class' => 'Porcentajes', 'label' => '')); ?></td>
        <td><?php echo $this->Form->input('concepto_id', array('label' => '')); ?></td>
        <td><?php echo $this->Form->input('descripcion', array('class' => 'Montos', 'label' => '')); ?></td>
        <td><?php echo $this->Form->input('porcentaje_iva', array('class' => 'Porcentajes', 'label' => '')); ?></td>
        <td><?php echo $this->Form->input('porcentaje_islr', array('class' => 'Porcentajes', 'label' => '')); ?></td>
        <td><?php echo $this->Form->input('precio_unitario', array('class' => 'Montos', 'label' => '')); ?></td>
        <td></td>
        <td></td>
        <td class="options-width">
            <?php echo $this->Form->end('Submit'); ?>
        </td>
    </tr>
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-10"><a href=""><?php echo __('Cant'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Concepto'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Descripcion'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Tasa IVA'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Tasa ISLR'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Precio Unitario'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Sub Total Base'); ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo __('Sub Total IVA'); ?></a></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php
    foreach ($detallefacturas as $detallefactura): 
    ?>
    <tr class="registros">
        <td></td>
        <td><?php echo h($detallefactura['DetalleFactura']['cantidad']); ?></td>
        <td><?php echo h($detallefactura['Concepto']['nombre']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['descripcion']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['porcentaje_iva']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['porcentaje_islr']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['precio_unitario']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['sub_total_base']); ?></td>
        <td><?php echo h($detallefactura['DetalleFactura']['sub_total_iva']); ?></td>
        <td class="options-width">
            <?php echo $this->Html->link('', array('controller' => 'facturas', 'action' => 'edit', $id, $detallefactura['DetalleFactura']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Editar')); ?>
            <?php echo $this->Form->postLink('', array('action' => 'delete', $detallefactura['DetalleFactura']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Eliminar'), 'Esta seguro de eliminar este registro?', $detallefactura['DetalleFactura']['id']); ?>
        </td>
    </tr>
    <?php 
    endforeach; 
    ?>
</table>
<script>
    $(function() {
        $( "#fecha_requerida" ).datepicker();
        $( "#fecha_requerida" ).datepicker('option', {dateFormat: 'yy/mm/dd'});
        $( "#fecha_facturacion" ).datepicker();
        $( "#fecha_facturacion" ).datepicker('option', {dateFormat: 'yy/mm/dd'});
    });
</script>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
}
</style>
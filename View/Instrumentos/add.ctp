<div class="instrumentos form">
	<?php echo $this->Form->create('Instrumento'); ?>

	<fieldset>
		<legend><?php echo __('Agregar Instrumento de Pago'); ?></legend>
		<input type="hidden" name="data[Instrumento][estatus_instrumento_id]" id="InstrumentoEstatusInstrumentoId" value="1"/>
		<?php if ($esAsociado){ ?>
			<input type="hidden" name="data[Instrumento][asociado_id]" id="InstrumentoAsociadoId" value="<?php echo $asociadoId; ?>"/>
		<?php
		}else{
			echo $this->Form->input('asociado_id', array('div'=>array('class'=>'divhorizontal')));
		}
		echo $this->Form->input('cliente_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('proveedor_id', array('div'=>array('class'=>'divhorizontal maxsize')));
		echo $this->Form->input('tipo_instrumento_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('banco_id', array('div'=>array('class'=>'divhorizontal maxsize')));
		echo $this->Form->input('nro_referencia', array('div'=>array('class'=>'divhorizontal')));
		echo $this->Form->input('cuenta_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('monto', array('div'=>array('class'=>'divhorizontal')));
		echo $this->Form->input('fecha_carga');
		?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script>
	$(function(){
		$('#InstrumentoAsociadoId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getClientes",
				data: $('#InstrumentoAddForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoClienteId').html(data);
					$('#InstrumentoProveedorId').html('');
					$('#InstrumentoCuentaId').html('');
				}
			});
		})
		
		$('#InstrumentoClienteId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getProveedores",
				data: $('#InstrumentoAddForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoProveedorId').html(data);
					$('#InstrumentoCuentaId').html('');
				}
			});
		})
		
		$('#InstrumentoProveedorId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getCuentasBancarias",
				data: $('#InstrumentoAddForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoCuentaId').html(data);
					if (data==""){
						alert("El proveedor no tiene cuenta bancaria asociada")
					}
				}
			});
		})
	})
</script>
<div class="instrumentos form">
<?php echo $this->Form->create('Instrumento'); ?>
	<fieldset>
		<legend><?php echo __('Editar Instrumento de Pago'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('estatus_instrumento_id',array('type'=>'hidden'));
		if (!$this->UserAuth->isAsociado()){
			echo $this->Form->input('asociado_id', array('div'=>array('class'=>'divhorizontal')));
		}else{
			echo $this->Form->input('asociado_id',array('type' => 'hidden'));
		}
		
		echo $this->Form->input('cliente_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('proveedor_id', array('div'=>array('class'=>'divhorizontal maxsize')));
		echo $this->Form->input('tipo_instrumento_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('banco_id', array('div'=>array('class'=>'divhorizontal maxsize')));
		echo $this->Form->input('nro_referencia', array('div'=>array('class'=>'divhorizontal')));
		echo $this->Form->input('cuenta_id', array('div'=>array('class'=>'divhorizontaln maxsize','style'=>'width:435px;')));
		echo $this->Form->input('monto', array('div'=>array('class'=>'divhorizontal')));
		echo $this->Form->input('fecha_carga');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<?php
	if(isset($transiciones))
	foreach ($transiciones as $transicion):
?>
<div style="width:100px;float: left">
	<form name="InstrumentoEditForm" action="/instrumentos/cambiarestatus/<?php echo $this->request->data['Instrumento']['id']?>" method="post">
		<input type="hidden" name="data[Instrumento][estatus_instrumento_id]" value="<?php echo $transicion['ti']['id']?>"/>
		<button class="btn1" type="submit"><?php echo $transicion['a']['nombre']?></button>
	</form>
</div>
<?php
endforeach;
?>

<script>
	$(function(){
		$('.btn1').button();
		
		$('#InstrumentoAsociadoId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getClientes",
				data: $('#InstrumentoEditForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoClienteId').html(data);
					$('#InstrumentoProveedorId').html('');
					$('#InstrumentoCuentaId').html('');
				}
			});
		})
		
		$('#InstrumentoClienteId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getProveedores",
				data: $('#InstrumentoEditForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoProveedorId').html(data);
					$('#InstrumentoCuentaId').html('');
				}
			});
		})
		
		$('#InstrumentoProveedorId').bind('change', function(){
			$.ajax({
				url: "/instrumentos/getCuentasBancarias",
				data: $('#InstrumentoEditForm').serialize(),
				type: 'post',
				success: function(data){
					$('#InstrumentoCuentaId').html(data);
					if (data==""){
						alert("El proveedor no tiene cuenta bancaria asociada")
					}
				}
			});
		})
	})
</script>

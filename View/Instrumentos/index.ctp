<div class="instrumentos index">
	<h2><?php echo __('Instrumentos'); ?></h2>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<thead>
	<tr>
			<th class="table-header-check"><?php echo $this->Paginator->sort('id'); ?></th>
			<?php if (!$this->UserAuth->isAsociado()):?>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort(__('Asociado.razon_social'),'Asociado'); ?></th>
			<? endif;?>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort(__('cliente_id')); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('proveedor_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('tipo_instrumento_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('nro_referencia'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('banco_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('cuenta_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('monto'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('fecha_carga'); ?></th>
			<th class="table-header-options line-left"><a><?php echo __('Actions'); ?></a></th>
	</tr>
	</thead>
	<?php
	foreach ($instrumentos as $instrumento): ?>
	<tr>
		<td><?php echo h($instrumento['Instrumento']['id']); ?>&nbsp;</td>
		<?php if (!$this->UserAuth->isAsociado()):?>
		<td>
			<?php echo $this->Html->link($instrumento['Asociado']['razon_social'], array('controller' => 'personas', 'action' => 'view', $instrumento['Cliente']['id'])); ?>
		</td>
		<? endif;?>
		<td>
			<?php echo $this->Html->link($instrumento['Cliente']['codigo'], array('controller' => 'personas', 'action' => 'view', $instrumento['Cliente']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($instrumento['Proveedor']['codigo'], array('controller' => 'personas', 'action' => 'view', $instrumento['Proveedor']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($instrumento['TipoInstrumento']['nombre'], array('controller' => 'tipo_instrumentos', 'action' => 'view', $instrumento['TipoInstrumento']['id'])); ?>
		</td>
		<td align="center"><?php echo h($instrumento['Instrumento']['nro_referencia']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($instrumento['Banco']['nombre'], array('controller' => 'bancos', 'action' => 'view', $instrumento['Banco']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($instrumento['Cuenta']['numero'], array('controller' => 'cuentas', 'action' => 'view', $instrumento['Cuenta']['id'])); ?>
		</td>
		<td align="right"><?php echo h(CakeNumber::currency($instrumento['Instrumento']['monto'], 'VEF')); ?>&nbsp;</td>
		<td><?php echo h(AppHelper::fechaNormal($instrumento['Instrumento']['fecha_carga'])); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('', array('action' => 'edit', $instrumento['Instrumento']['id']), array('class'=>'icon-13')); ?>
			<?php echo $this->Form->postLink('', array('action' => 'delete', $instrumento['Instrumento']['id']), array('class'=>'icon-17'), __('Are you sure you want to delete # %s?', $instrumento['Instrumento']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

<div class="instrumentos view">
<h2><?php  echo __('Instrumento Cargado'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($instrumento['Instrumento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo h($instrumento['Cliente']['razon_social']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proveedor'); ?></dt>
		<dd>
			<?php echo h($instrumento['Proveedor']['razon_social']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo Instrumento'); ?></dt>
		<dd>
			<?php echo h($instrumento['TipoInstrumento']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nro Referencia'); ?></dt>
		<dd>
			<?php echo h($instrumento['Instrumento']['nro_referencia']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Banco'); ?></dt>
		<dd>
			<?php echo h($instrumento['Banco']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cuenta'); ?></dt>
		<dd>
			<?php echo h($instrumento['Cuenta']['numero']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h( CakeNumber::currency($instrumento['Instrumento']['monto'], 'VEF') ); ?>
			
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Carga'); ?></dt>
		<dd>
			<?php echo h($instrumento['Instrumento']['fecha_carga']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<fieldset>
    <legend>Buscar Personas</legend>
    {{ form.create('Persona', {'url': {'action': 'index'}}) }}
    {{ form.input('codigo', {'class': 'Search'}) }}
    {{ form.input('razon_social', {'class': 'Search'}) }}
    {{ form.input('tipo_documento_id', {'empty': '(Todos)'}) }}
    {{ form.input('documento', {'class': 'Search', 'label': 'Documento Fiscal'}) }}         
    {{ form.input('clasificacion_id', {'empty': '(Todos)'}) }}      
    {{ form.submit('Filtrar', {'div': false, 'class': 'form-buscar'}) }}    
    {{ form.end }}   

</fieldset>
<br />

{% if Html.Mostrar(UserAuth.getGroupId, 'Personas', 'agregar')|length %}
<p><a href="/personas/agregar/" class="btn">Agregar Persona</a></p>
{% endif %}

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-1">{{ paginator.sort('codigo') }}</th>
        <th nowrap class="table-header-repeat line-left">{{ paginator.sort('razon_social') }}</th>
        <th nowrap class="table-header-repeat line-left">{{ paginator.sort('tipo_persona_id') }}</th>
        <th class="table-header-repeat line-left">{{ paginator.sort('clasificacion_id') }}</th>
        <th class="table-header-repeat line-left">{{ paginator.sort('documento','Documento Fiscal') }}</th>
        <th class="table-header-repeat line-left">{{ paginator.sort('contribuyente_especial') }}</th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    {% for persona in personas %}    
    {% set tipoPersona = persona.TipoPersona %}
    {% set clasificacion = persona.Clasificacion %}
    {% set tipoDocumento = persona.TipoDocumento %}    
    {% set persona = persona.Persona %}
        <tr class="registros">
            <td></td>
            <td>{{ persona.codigo }}</td>
            <td nowrap>{{ persona.razon_social }}</td>
            <td>{{ tipoPersona.nombre }}</td>
            <td>{{ clasificacion.nombre }}</td>
            <td>{{ tipoDocumento.id ~ '-' ~ persona.documento }}</td>
            <td>{{ html.mostrarboolean(persona.contribuyente_especial) }}</td>
            <td class="options-width">

                {% if Html.Mostrar(UserAuth.getGroupId, 'Personas', 'editar')|length %}
                    {{ Html.link('',[{'action': 'editar'}, persona.id], {'class': 'icon-13 info-tooltip', 'title': 'Editar'}) }}
                {% endif %}

                {% if Html.Mostrar(UserAuth.getGroupId, 'Personas', 'eliminar')|length %}                    
                    {{ Html.postLink('',[{'action': 'eliminar'}, persona.id], {'class': 'icon-17 info-tooltip', 'title': 'Eliminar'}, ('Esta Seguro De Eliminar este registro?' ~ persona.nombre)| trans) }}
                {% endif %}

                {% if clasificacion.relacion_cliente == 1 %}
                    {% if Html.Mostrar(UserAuth.getGroupId, 'Personas', 'relacionCliente')|length %}
                        {{ Html.link('',[{'action': 'relacionCliente'}, persona.id, '1'], {'class': 'icon-3 info-tooltip', 'title': 'Relacionar Clientes'}) }}
                    {% endif %}
                {% endif %}

                {% if clasificacion.relacion_concepto == 1 %}
                    {% if Html.Mostrar(UserAuth.getGroupId, 'Personas', 'relacionConcepto')|length %}
                        {{ Html.link('',[{'action': 'relacionConcepto'}, persona.id], {'class': 'icon-12 info-tooltip', 'title': 'Relacionar Conceptos'}) }}
                    {% endif %}
                {% endif %}
            </td>
        </tr>
    {% endfor %}
</table>
 {{ view.element('paginate') }}
<script>
	$(function(){
		$('.btn').button();
	})
</script>
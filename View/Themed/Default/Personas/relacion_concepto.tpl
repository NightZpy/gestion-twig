<p><a href="/personas/" class="btn"><< Regresar al Listado de Personas</a></p><br />
<h1>Relación Proveedor - Conceptos</h1>
<dl>
    <dt>Razon Social</dt>
    <dd><?= $persona['Persona']['razon_social']; ?></dd>
    <dt>Clasificación</dt>
    <dd><?= $persona['Clasificacion']['nombre']; ?></dd>
    <dt>Documento</dt>
    <dd><?= $persona['TipoDocumento']['nombre_corto']. '-'. $persona['Persona']['documento']; ?></dd>
</dl>

<fieldset>
    <legend>Conceptos Disponibles</legend>
        <?= $this->Form->create('Concepto', array(array('action'=>'relationConcepto',$persona['Persona']['id']),'id'=>'filters')); ?>
            <?= $this->Form->input('nombre', array('class' => 'Search')); ?>
        <?= $this->Form->end(array('label' => 'Filtrar', 'class' => 'form-buscar')); ?>
	<div style="clear: both;"></div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" style="margin-top: 10px;">
		<tr>
			<th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><a href="#">Nombre</a></th>
			<th class="table-header-options line-left"><a href="#">Opciones</a></th>
		</tr>
    <?php if (count($conceptos_disponibles)>= 1): ?>
        <?php foreach ($conceptos_disponibles as $conceptoDisponible): ?>
		<tr class="registros">
				<td></td>
				<td><?= $conceptoDisponible['Concepto']['nombre']; ?></td>
				<td><?= $this->Form->create('ConceptoProveedor', array('action' => 'asociarConcepto')); ?>
						<?= $this->Form->input('proveedor_id', array('type' => 'hidden','value' => $persona['Persona']['id']));?>
						<?= $this->Form->input('concepto_id', array('type' => 'hidden','value' => $conceptoDisponible['Concepto']['id']));?>
					<?= $this->Form->button(' Relacionar ', array('type' => 'submit', 'title' => ' Relacionar ' )); ?>
				</td>
			</form>
		</tr>
        <?php endforeach; ?>
        <?php else: ?>
        <tr class="registros">
            <td colspan="3" align="center">No existen conceptos disponibles</td>
        </tr>
    <?php endif; ?>
	</table>
</fieldset>
<br>
<fieldset>
    <legend>Conceptos Relacionados</legend>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><a href="#">Nombre</a></th>
			<th class="table-header-options line-left"><a href="#">Opciones</a></th>
		</tr>
        <?php if (count($conceptos_asociados)>= 1): ?>
            <?php foreach ($conceptos_asociados as $conceptoRelacionado): ?>
		<tr class="registros">
			<td></td>
			<td><?= $conceptoRelacionado['Concepto']['nombre']; ?></td>
			<td><?= $this->Form->postLink(''); ?>
			<?= $this->Form->postLink('', array(
													'controller' => 'ConceptoProveedores', 
													'action' => 'removerConcepto', 
													$conceptoRelacionado['ConceptoProveedor']['id'], 
													$conceptoRelacionado['ConceptoProveedor']['proveedor_id']
												), 
												array( 
													'class' => 'icon-2 info-tooltip', 
													'title' => 'Remover'), 
													'Esta seguro de remover este registro?'
			);?>
			</td>
		</tr>
            <?php endforeach; ?>
        <?php else: ?>
        <tr class="registros">
            <td colspan="3" align="center">No existen conceptos relacionados</td>
        </tr>
        <?php endif; ?>
	</table>
</fieldset>
<?php 
$this->Html->script('cargaCiudades.js', array('inline' => false));
?>
    <?php echo $this->Form->create('Persona', array('action'=>'find','id'=>'filters')); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <fieldset>
            <legend>Buscar Proveedores</legend>
                <?php 
                echo $this->Form->input('codigo', array('class' => 'Search'));
                echo $this->Form->input('nombre', array('class' => 'Search'));
                echo $this->Form->input('razon_social', array('class' => 'Search'));
                echo $this->Form->input('documento', array('class' => 'Search', 'label'=>'Documento Fiscal'));
                //echo $this->Form->input('clasificacion_id', array('empty' => 'Todos'));
                echo $this->Form->input('estado_id', array('empty' => 'Todos'));
                echo $this->Form->input('ciudad_id', array('empty' => 'Todos'));
                echo $this->Form->input('concepto_id', array('empty' => 'Todos'));
                ?>
                <?php echo $this->Form->end(array('label' => 'Filtrar', 'class' => 'form-buscar')); ?>
        </fieldset>
        <tr>
            <th class="table-header-repeat line-left minwidth-1"></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('codigo'); ?></th>
			<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('apellido'); ?></th>
			<th nowrap class="table-header-repeat line-left"><?php echo $this->Paginator->sort('razon_social'); ?></th>
			<th nowrap class="table-header-repeat line-left"><?php echo $this->Paginator->sort('tipo_persona_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('clasificacion_id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('documento','Documento Fiscal'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('contribuyente_especial'); ?></th>
        </tr>
        <?php
        foreach ($personas as $persona): 
        ?>
        <tr class="registros">
            <td>
                <?php echo $this->Html->link('', 'javascript: fSelecciona(\''.$persona['Persona']['id'].'|'.$persona['Persona']['razon_social'].'\');', array('class' => 'icon-14 info-tooltip', 'title'=>'Ver')); ?>
            </td>
            <td><?php echo h($persona['Persona']['codigo']); ?></td>
            <td><?php echo h($persona['Persona']['nombre']); ?></td>
            <td><?php echo h($persona['Persona']['apellido']); ?></td>
            <td nowrap><?php echo h($persona['Persona']['razon_social']); ?></td>
            <td><?php echo h($persona['TipoPersona']['nombre']); ?></td>
            <td><?php echo h($persona['Clasificacion']['nombre']); ?></td>
            <td><?php echo h($persona['TipoDocumento']['nombre_corto']. '-' . $persona['Persona']['documento']); ?></td>
            <td><?php echo $this->Html->mostrarBoolean($persona['Persona']['contribuyente_especial']);?></td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
<script>
function fSelecciona (valores)
{
    var cadena = valores;
    var proveedor = cadena.split("|");
    var proveedor_id = proveedor[0];
    
    $("#FacturaProveedorId option[value="+proveedor_id+"]", window.parent.parent.document).attr("selected",true);
    parent.parent.location.reload();
    parent.parent.GB_CURRENT.hide();
}
</script>
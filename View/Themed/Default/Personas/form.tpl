{{ Html.script('cargaCiudades', {'inline': false}) }}
{{ Html.script('datosFiscales', {'inline': false}) }}
{{ Html.script('jquery.validate', {'inline': false}) }}
{{ Html.script('utilidades', {'inline': false}) }}
{{ Html.script('tooltip_cuentas', {'inline': false}) }}
{{ Html.script('tooltip_attach', {'inline': false}) }}
{{ Html.script('cargarBancos', {'inline': false}) }}
{{ Html.script('cargarAdjuntos', {'inline': false}) }}
{{ Html.script('cuentas', {'inline': false}) }}


<script>
    $(function() {
        $( "#tabs" ).tabs();

        $("#PersonaContribuyenteEspecial").click(function() {  
                    if($("#PersonaContribuyenteEspecial").is(':checked')) {  
                        $("#PersonaPorcentajeRetencionIva").val('75');
                    } else {  
                        $("#PersonaPorcentajeRetencionIva").val('0');
                    }  
                }); 

        $("#PersonaAgregarform").submit(function(){
            return true;          
        });
    }); 
</script>
<div id="tabs">
    
    <ul>
        <li class="tab"><a href='{{ here }}#info_basica'>Información Básica</a></li>
        <li class="tab"><a href='{{ here }}#info_adicional'>Información Adicional</a></li>
    </ul>
    {{ form.create('Persona', {'type': 'file'}) }}
    <div id="info_basica">        
        <fieldset>
            <legend>Información Básica</legend>            
            {{ form.input('id') }}
            {{ form.input('codigo') }}            
            {{ form.input('tipo_persona_id', {'empty': '-- seleccione --'}) }}
            {{ form.input('clasificacion_id', {'empty': '-- seleccione --'}) }}
            {{ form.input('rubro_id', {'empty': '-- seleccione --'}) }}
            {{ form.input('grupo_id', {'empty': '-- seleccione --', 'label': 'Agrupación'}) }}

            {{ form.input('documento', {
                'label': 'Documento Fiscal',
                'type': 'text',
                'between': form.input('tipo_documento_id', {
                                'div'  : false,
                                'label': false,
                                'empty': '---',
                                'error': false,
                            }),
                'class': 'Search'
            }) }}

            {{ form.input('razon_social') }}
            {{ form.input('direccion') }}
            {{ form.input('estado_id', {'empty': '-- seleccione --'}) }}
            {{ form.input('ciudad_id', {'empty': '-- seleccione --'}) }}            
            {{ form.input('telefono') }}            
            {{ form.input('correo_electronico') }}
            {{ form.input('pagina_web',  {'label': 'Dirección Web'}) }}            
            {{ form.input('observaciones') }}       

        </fieldset>
    </div>

    <div id="info_adicional">
        <fieldset>
            <legend>Datos Fiscales</legend>
            {{ form.input('contribuyente_especial') }}
            {{ form.input('porcentaje_retencion_iva', {'label': 'Porcentaje de Retención IVA',  'type': 'text', 'class': 'Montos'}) }}
        </fieldset>
        
        <fieldset>
            <legend>Porcentajes de Comisión</legend>
                {{ form.input('porcentaje_costo_gestion', {'type': 'text', 'class': 'Montos'}) }}
                {{ form.input('porcentaje_costo_operativo', {'type': 'text', 'class': 'Montos'}) }}
                {{ form.input('porcentaje_costo_venta', {'type': 'text', 'class': 'Montos'}) }}
                {{ form.input('porcentaje_costo_venta_secundario', {'type': 'text', 'class': 'Montos'}) }}
                {{ form.input('porcentaje_costo_administrativo', {'type': 'text', 'class': 'Montos'}) }}
        </fieldset>  

       <div id="account-container" title="Cuentas Bancarias">
            <fieldset>
                <legend>Cuentas Bancarias</legend>
                <button id="create-account" type="button">Agregar</button>                
                <br>
                <div class="clear"></div>
                <table id="cuentas-table" border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="table-header-repeat line-left minwidth-1">Banco</th>
                        <th class="table-header-repeat line-left minwidth-1">Tipo</th>
                        <th class="table-header-repeat line-left minwidth-1">Número</th>
                        <th class="table-header-repeat line-left minwidth-1">Opciones</th>
                    </tr> 
                    {% if cuentas is defined and cuentas|length > 0 %}
                    {% for cuenta in cuentas %}
                    <tr class="registro">
                        <td nowrap>
                            {{ cuenta.Banco.nombre }}
                            {{ form.input("Cuenta.id", {'type': 'hidden', 'value': cuenta.id, 'name': "data[Cuenta][" ~loop.index~ "][id]" }) }}
                            {{ form.input("Cuenta.banco_id", {'type': 'hidden', 'value': cuenta.banco_id, 'name': "data[Cuenta][" ~loop.index~ "][banco_id]"}) }}
                        </td>
                        <td nowrap>
                            {{ cuenta.TipoCuenta.nombre }}
                            {{ form.input("Cuenta.tipo_cuenta_id", {'type': 'hidden', 'value': cuenta.tipo_cuenta_id, 'name': "data[Cuenta][" ~loop.index~ "][tipo_cuenta_id]"}) }}
                        </td>
                        <td nowrap>
                            {{ cuenta.numero }}
                            {{ form.input("Cuenta.numero", {'type': 'hidden', 'value': cuenta.numero, 'name': "data[Cuenta][" ~loop.index~ "][numero]"}) }}                          
                        </td>
                        <td class="options-width">
                            <a href="{{ here }}#" id="editar_{{ cuenta.id }}" title="Editar" class = 'icon-13 info-tooltip' title = 'Editar'></a>
                            <a href="{{ here }}#" id="eliminar_{{ cuenta.id }}" onclick="return confirm('Esta seguro de eliminar este registro?');" title="Eliminar" class = 'icon-17 info-tooltip' title = 'Eliminar'></a>'
                        </td>
                    </tr>
                    {% endfor %}
                    {% endif %}
                </table>
            </fieldset>
        </div>
        
        <div id="attach-container" title="Adjuntos">
            <fieldset>
                <legend>Adjuntos</legend>
                <button id="create-attach" type="button">Agregar</button>

                <div class="clear"></div>                
                <br>
                {{ form.input('tipo_adjunto_id', {'label': 'Tipo de Adjunto', 'empty': '-- seleccione --'}) }}
                <div class="clear"></div>
                <br>
                <table id="adjuntos-table" border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="table-header-repeat line-left minwidth-1">Imagen</th>
                        <th class="table-header-repeat line-left minwidth-1">Tipo</th>
                        <th class="table-header-repeat line-left minwidth-1">Opciones</th>
                    </tr>
                    {% if adjuntos is defined and adjuntos|length > 0 %}
                    {% for adjunto in adjuntos %} 
                    {% set tipoAdjunto = adjunto.TipoAdjunto %}                   
                    <tr>                        
                        <td nowrap>{{ adjunto.nombre }}</td>
                        <td nowrap>{{ tipoAdjunto.nombre }}</td>
                        <td class="options-width">
                            {{ Html.link('', [{'controller': 'Adjuntos', 'action': 'editar'}, adjunto.id], {'class': 'icon-13 info-tooltip', 'title': 'Editar'}) }}
                            {{ form.postLink('', [{'controller': 'Adjuntos', 'action': 'eliminar'}, adjunto.id], {'class': 'icon-17 info-tooltip', 'title': 'Eliminar'}, 'Esta seguro de eliminar este registro?', adjunto.id) }}
                        </td>                        
                    </tr>
                    {% endfor %}
                    {% endif %}                 
                </table>
            </fieldset>
        </div>
    </div>           
    <input type="submit" id="enviar" value="Guardar">
    <div class="clear"></div>    
</div>

<div id="account-form" title="Administrar Cuentas Bancarias">
  <p class="validateTips">All form fields are required.</p>

  <fieldset>
    {{ form.input('Cuenta.banco_id', {'empty': '-- seleccione --', 'class': 'input select required'}) }}
    {{ form.input('Cuenta.tipo_cuenta_id', {'label': 'Tipo de Cuenta', 'empty': '-- seleccione --', 'class': 'input select required'}) }}
    {{ form.input('Cuenta.numero', {'label': 'Número de Cuenta', 'class': 'input text required'}) }}
  </fieldset>
</div>

<!--<div id="attach-form" title="Administrar Adjuntos">
  <p class="validateTips">All form fields are required.</p> 
  <fieldset>
    <?php //form.input('tipo_adjunto_id', ['label': 'Tipo de Adjunto', 'empty': '-- seleccione --']); ?>
    <?php //form.file('ruta'); ?>
  </fieldset>
</div> -.
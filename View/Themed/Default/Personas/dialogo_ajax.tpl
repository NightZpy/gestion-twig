<style>
    #product-table td{
        padding: 5px 0px 5px 5px;
    }
</style>
<div id="contenidoAjaxPersona">
    <?php
    $this->Paginator->options(array(
        'update' => '#contenidoAjaxPersona',
        'evalScripts' => true
    ));
    
    ?>

    <form id="FiltroFormulario">
        <?php echo $this->Form->input('concepto_id', array('name'=>'data[ConceptoProveedor][concepto_id]'), array('method'=>'post'))?>
        <?php echo $this->Form->input('razon_social', array('name'=>'data[Persona][razon_social]'), array('method'=>'post'))?>
        <button id="filtrarConcepto" class="btn">Buscar</button>
    </form>

    <div style="height:450px" id="">
        <div style="height:400px;">
            <table id="product-table" width="100%">
                <thead><tr><th class="table-header-check"></th><th class="table-header-repeat"><?php echo $this->Paginator->sort('razon_social', __('Razón Social')); ?></th></tr></thead>
                <tbody >
                    <?php foreach ($personas as $persona): ?>
                        <tr><td><a href="#" class="icon-14 selector" id="id<?php echo $persona['Persona']['id'] ?>"></a></td><td><?php echo $persona['Persona']['razon_social'] ?></td></tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paging">
            <?php
            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
        </div>
    </div>

    <?php
    echo $this->Js->writeBuffer();
    
    ?>
    <script>
        $(function(){
            $('#filtrarConcepto').bind('click', function(e){
                e.preventDefault();
                $.ajax({
                    url:'personas/ajaxGetDialogoPersona',
                    type:'post',
                    data:$('#FiltroFormulario').serialize()+'&data[Persona][clasificacion_id]=3'
                }).done(function(data){
                    $('#contenidoAjaxPersona').html(data);
                });
            })
            
            $('.selector').bind('click', function(e){
                e.preventDefault();
                var id=$(this).attr('id');
                if(typeof ajaxGetDialogoPersona == 'function'){
                    ajaxGetDialogoPersona(id.substring(2,id.length));
                }
                
            });
        })
        
    </script>

</div>


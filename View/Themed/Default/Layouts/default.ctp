<?php
$cakeDescription = __d('cake_dev', 'Sistema de Gesti&oacute;n');
?>
<!DOCTYPE HTML>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
    <base href="<?php echo FULL_BASE_URL;?>">
    <script>
        var GB_ROOT_DIR = "/js/greybox/";
    </script>
    <?= $this->fetch('meta'); ?>
    <link href="favicon.ico" type="image/x-icon" rel="icon" />
    <link href="favicon.ico" type="image/x-icon" rel="shortcut icon" />
    
    <link rel="stylesheet" type="text/css" href="/css/estilos.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="/css/fg.menu.css" />
    <link rel="stylesheet" type="text/css" href="/usermgmt/css/umstyle.css" />
    <link rel="stylesheet" type="text/css" href="/css/greybox/gb_styles.css" />
    <?= $this->fetch('css'); ?>
	<?php
        // Determinar donde se usan cada una de las librerías JS cargadas aquí
       // echo $this->Html->script(array('cargaClientes', 'cargaISLR', 'greybox/AJS', 'greybox/gb_scripts'));
    ?>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jqueryui.js"></script>
    <script type="text/javascript" src="/js/fg.menu.js"></script>
    <?= $this->fetch('script'); ?>
</head>
<body>
    <?php 
        // ENCABEZADO Y LOGO
        echo $this->element('header');
        // MENU
        echo $this->element('menu');
        // CONTENIDO
        echo $this->element('content');
        // PIE DE PAGINA
        echo $this->element('footer');
        // DEBUGGER
        //echo $this->element('sql_dump');
    ?>
</body>
</html>

        
        <?php 
        foreach ($instrumentos as $instrumento): ?>
            <tr>
                <td><?php echo $instrumento['TipoInstrumento']['nombre'] ?></td>
                <td class="td-number"><?php echo $instrumento['Instrumento']['nro_referencia'] ?></td>
                <td><?php echo $instrumento['Banco']['nombre'] ?></td>
                <td><?php echo $instrumento['Cuenta']['numero'] ?></td>
                <td class="td-number" style="padding-right: 5px;"><?php echo CakeNumber::currency($instrumento['Instrumento']['monto'], 'VEF') ?></td>
                <td><a class="icon-17 instelim" id="inst-<?php echo $instrumento['Instrumento']['id']?>" href="#"></a></td>
            </tr>
        <?php 
        
        endforeach; 
        $suma=$sumaInstrumentos;
        ?>
        

<div id="content-outer">
        <div id="content">
            
            <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
                <tr>
                    <td style="padding-left: 20px;" colspan="5" aling="right"><?php echo $this->Html->link('Volver', 'javascript: history.back();', array('class'=>'btn'));?></td>
                </tr>
                <tr>
                    <th rowspan="3" class="sized"><img src="img/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
                    <th class="topleft"></th>
                    <td id="tbl-border-top">&nbsp;</td>
                    <th class="topright"></th>
                    <th rowspan="3" class="sized"><img src="img/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
                </tr>
                <tr>
                    <td id="tbl-border-left"></td>
                    <td>
                        <div id="content-table-inner">
                            <div id="table-content">
                                <!--  MENSAJES DE SISTEMA  -->

                                <?php echo $this->Session->flash(); ?>

                                <!--  TABLA DE CONTENIDO  -->

                                <?php echo $this->fetch('content'); ?>
                            </div>
                        </div>
                    </td>
                    <td id="tbl-border-right"></td>
                </tr>
                <tr>
                    <th class="sized bottomleft"></th>
                    <td id="tbl-border-bottom">&nbsp;</td>
                    <th class="sized bottomright"></th>
                </tr>
            </table>
            <div class="clear">&nbsp;</div>
        </div>
        <div class="clear">&nbsp;</div>
    </div>
    
    <div class="clear">&nbsp;</div>
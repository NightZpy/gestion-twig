<div class="nav-outer-repeat"> 
    <div class="nav-outer"> 
        <div id="nav-right">

            <div class="account-content">
                <div class="account-drop-inner">
                    <a href="" id="acc-settings">&nbsp;</a>
                    <div class="clear">&nbsp;</div>
                    <div class="acc-line">&nbsp;</div>
                    <a href="" id="acc-details">&nbsp;</a>
                    <div class="clear">&nbsp;</div>
                    <div class="acc-line">&nbsp;</div>
                    <a href="" id="acc-project">&nbsp;</a>
                    <div class="clear">&nbsp;</div>
                    <div class="acc-line">&nbsp;</div>
                    <a href="" id="acc-inbox">&nbsp;</a>
                    <div class="clear">&nbsp;</div>
                    <div class="acc-line">&nbsp;</div>
                    <a href="" id="acc-stats">&nbsp;</a> 
                </div>
            </div>
        </div>
        <?php if ($this->UserAuth->isLogged()): ?>
            <div id="menu" class="hidden">
                <?php echo $menu; ?>
            </div>
			<?php
				$usuario = $this->UserAuth->getUser();
			?>
		
            <div style="font-size:1em;width: 96%;background: url('/img/asd2.png') repeat-x scroll 0% 50% transparent;height:36px;margin:-1px auto 0px; border: 1px solid #7e7e7e !important;" class="ui-widget ui-state-default ui-corner-all">
                <?php echo $this->Html->link('Escritorio', "/dashboard", array('class' => 'fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-left')) ?>
                <a id="prueba" class="fg-button fg-button-icon-right ui-widget ui-state-default">Procesos</a>
				<?php echo $this->Html->link('Desconectar', array('plugin' => 'usermgmt', 'controller' => 'usuarios', 'action' => 'logout'), array('class' => 'fg-button fg-button-icon-left ui-widget ui-state-default ui-corner-right', 'style' => 'float:right;')); ?>
				<div style="float:right;width: 200px; height:auto;text-align: right;padding-top: 3px;padding-right: 15px;">
					Usuario: <?= $usuario['Usuario']['first_name'] . " " . $usuario['Usuario']['last_name']?><br>
					Rol: <?= $usuario['UsuarioGrupo']['name']?>					
				</div>
                
				
            </div>

            <script>
                $(function(){
                    $('.fg-button').hover(
                    function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
                    function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
                );
                    $('#prueba').fgmenu({ content: $('#menu').html(), flyOut: true });
                })
            </script>

        <?php endif; ?>  

    </div>
    <div class="clear"></div>
</div>

<div class="clear"></div>
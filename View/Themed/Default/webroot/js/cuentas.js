function eliminarCuenta(id){   
    var numId = id.split('_')[1]; 
    $.ajax({
        type: "POST",
        url: "/Cuentas/eliminarAjax/"+numId,
        //data: id,
        dataType:"json",
        success: function(response) {
            if (response.success){
				 $('#'+id).parent().parent().remove();
            }else{
            	//console.log("Vacio");
            }
        }
    });    
}

$(function() {  
    $( "#cuentas-table" ).find('a')
        .live("click", function(e) {
        var id = $(this).attr('id');
        var tipo = id.split('_')[0];
        var numId = id.split('_')[1];
        if(tipo == 'eliminar'){
          if(numId>=0)
            eliminarCuenta(id);
          else
            $('#'+id).parent().parent().remove();
        } else if(tipo == 'editar'){
            cargarBancos();
            cargarTipoCuentas();    
            cargarDatosEditar(id);
            $( "#account-form" ).dialog( "open" );  
        }
        return false;
    });
});
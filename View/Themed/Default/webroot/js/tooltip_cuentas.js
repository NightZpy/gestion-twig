function cargarDatosEditar (id) {
    var numero = $('#'+id).parent().parent().find('#CuentaNumero');
    var banco = $('#'+id).parent().parent().find('#CuentaBancoId');
    var tipoCuenta = $('#'+id).parent().parent().find('#CuentaTipoCuentaId');

    $( "#account-form" ).find('#CuentaNumero').val(numero.val());

    $( "#account-form" ).find('#CuentaBancoId option').each(function () {
    	if($(this).text() == "-- seleccione --")
    		$(this).remove();
    	else
    		if($(this).val() == banco.val()){
    			$(this).attr({'selected': 'selected'});
    		}
    });

    $( "#account-form" ).find('#CuentaTipoCuentaId option').each(function () {
    	if($(this).text() == "-- seleccione --")
    		$(this).remove();
    	else
    		if($(this).val() == tipoCuenta.val()){
    			$(this).attr({'selected': 'selected'});
    		}
    });    
}

$(function() {
	/*$("#PersonaAgregarForm").validate({
        debug: true,
        rules: {
            banco_id: {
                required: true
            },
            tipo_cuenta_id: {
                required: true
            },            
            numero: {
                required: true,
                digits: true,
                min: 18,
                max: 99
            }
        }
	});*/


	$( "#account-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			"Guardar": function() {
				var nCuentas = ($('tr', $("#cuentas-table")).length) - 1;

				var link = '<a href="#" id="editar_{0}" title="Editar" class="icon-13 info-tooltip" title = "Editar"></a><a href="#" id="eliminar_{0}" onclick="return confirm({1});" title="Eliminar" class = "icon-17 info-tooltip" title = "Eliminar"></a>'
				var confirm = "'Esta seguro de eliminar este registro?'";
				link = String.format(link, (nCuentas*(-1)), confirm);
				
				var tr = '<tr class="registro"><td nowrap>{1}<input type="hidden" id="CuentaBancoId" name="data[Cuenta][{0}][banco_id]" value="{2}"></td><td>{3}<input type="hidden" id="CuentaTipoCuentaId" name="data[Cuenta][{0}][tipo_cuenta_id]" value="{4}"></td><td>{5}<input type="hidden" id="CuentaNumero" name="data[Cuenta][{0}][numero]" value="{5}"></td><td class="options-width">{6}</td></tr>';
				var bancoSelect = $("#account-form").find('#CuentaBancoId').find('option:selected');
				var bancoVal = $(bancoSelect).val();
				var bancoText = $(bancoSelect).text();

				var tipoSelect = $("#account-form").find('#CuentaTipoCuentaId').find('option:selected');
				var tipoVal = $(tipoSelect).val();
				var tipoText = $(tipoSelect).text();

				var numero = $("#account-form").find('#CuentaNumero').val();

				tr = String.format(tr, nCuentas, bancoText, bancoVal, tipoText, tipoVal, numero, link);

				$('#cuentas-table').append(tr);
				$( this ).dialog( "close" );

			  /*var bValid = true;
			  allFields.removeClass( "ui-state-error" );

			  bValid = bValid && checkLength( name, "username", 3, 16 );
			  bValid = bValid && checkLength( email, "email", 6, 80 );
			  bValid = bValid && checkLength( password, "password", 5, 16 );

			  bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
			  // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
			  bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
			  bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

				if ( bValid ) {
					$( "#users tbody" ).append( "<tr>" +
					  "<td>" + name.val() + "</td>" +
					  "<td>" + email.val() + "</td>" +
					  "<td>" + password.val() + "</td>" +
					"</tr>" );
					$( this ).dialog( "close" );
				}*/
			},
			Cancelar: function() {
			  	$( this ).dialog( "close" );
			}
		},
		close: function() {
			//allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});

	$( "#create-account" )
	  	.button()
	  	.click(function() {
		$( "#account-form" ).dialog( "open" );
	});

	$( "#edit-account-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			"Guardar": function() {

			},
			Cancelar: function() {
			  	$( this ).dialog( "close" );
			}
		},
		close: function() {
			//allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
});



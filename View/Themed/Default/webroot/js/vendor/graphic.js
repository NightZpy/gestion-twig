var Graphics = function (total, data, dataII, dataIII, dataIV) {
    // datos
    this.total          = total;
    this.data           = data;
    this.dataII         = dataII;
    this.dataIII		= dataIII;
    this.dataIV			= dataIV;
    this.setNames		= setNamesE;
    this.porcentajes    = porcentajesE;
    this.styles         = stylesE;
    this.setHtml        = setHtmlE;
    this.setLeyenda     = setLeyendaE;
     
}

function setNamesE(classData, classDataII, classDataIII, classDataIV) {
	this.nameData 		= classData;
	this.nameDataII	    = classDataII;
	this.nameDataIII	= classDataIII;
	this.nameDataIV	    = classDataIV;
}

function porcentajesE() {
    valor    	= this.data * 100 / this.total;
    valorII  	= this.dataII * 100 / this.total;
    valorIII 	= this.dataIII * 100 / this.total;
    valorIV	 	= this.dataIV * 100 / this.total;
}

function stylesE() {
    backgroundData 		= 'background:#02AF50;';
    heightData     		= 'height:'+ valor +'%;';

    backgroundDataII 	= 'background:#6AA5BA;';
    heightDataII     	= 'height:'+ valorII +'%;';
    
    backgroundDataIII 	= 'background:#FFB804;';
    heightDataIII     	= 'height:'+ valorIII +'%;';

    backgroundDataIV 	= 'background:#214733;';
    heightDataIV     	= 'height:'+ valorIV +'%;';

    stylesData 	  = backgroundData + heightData;
    stylesDataII  = backgroundDataII + heightDataII;
    stylesDataIII = backgroundDataIII + heightDataIII;
    stylesDataIV  = backgroundDataIV + heightDataIV;
}

function setHtmlE() {
	list 	= '<li class="list" style='+ stylesData +'><span>'+ this.data +'</span></li>';
	list	+= '<li class="listII" style='+ stylesDataII +'><span>'+ this.dataII +'</span></li>';
	list 	+= '<li class="listIII" style='+ stylesDataIII +'><span>'+ this.dataIII +'</span></li>';
	list	+= '<li class="listIV" style='+ stylesDataIV +'><span>'+ this.dataIV +'</span></li>';

    return $('#estadisticas').html(list);
}

function setLeyendaE() {
    legend  = '<li class="title">Leyenda</li>';
    legend  += '<li><span style='+ backgroundData +'></span>'+ this.nameData +'</li>';
    legend  += '<li><span style='+ backgroundDataII +'></span>'+ this.nameDataII +'</li>';
    legend  += '<li><span style='+ backgroundDataIII +'></span>'+ this.nameDataIII +'</li>';    
    legend  += '<li><span style='+ backgroundDataIV +'></span>'+ this.nameDataIV +'</li>';    

    return $('#leyenda').html(legend);
}

var g = new Graphics(800, 150, 350, 100, 300);
	g.setNames('Administradores', 'Programadores', 'Publicistas', 'Contadores');
    g.porcentajes();
    g.styles();
    g.setHtml();
    g.setLeyenda();
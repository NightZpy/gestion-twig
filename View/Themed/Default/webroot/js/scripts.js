$(document).ready(function() {

	// modal
  	var turnedOff 		= $('#turned-off'),
  		modal			= $('.modal a');

  	modal.click(function(event) {
  		event.preventDefault();
  		event.stopPropagation();

  		var height 			= $(document).height(),
  			modalContent	= $(this).parent('.modal').find('.content-modal');


  		if (turnedOff.hasClass('closed')) {
  			turnedOff.removeClass('closed');
  			turnedOff.css('height', height);
  			modalContent.removeClass('closed');
  		} else {
  			turnedOff.addClass('closed')
  		};
  	});

	$('.content-modal').click(function (event) {
    	event.stopPropagation();
  	});   

	$('.content-modal').find('.cancel').click(function(e) {
		e.preventDefault();

    	turnedOff.addClass('closed');
    	$('.content-modal').addClass('closed');		
	});

  	$(window).click(function (e) {
    	turnedOff.addClass('closed');
    	$('.content-modal').addClass('closed');
  	});		

  	// input file
	var wrapper 	= $('<div/>').css({height:0,width:0,'overflow':'hidden'}),
	 	fileInput 	= $(':file').wrap(wrapper);

	fileInput.change(function(){
	    $this = $(this);
	    $('.file').text($this.val());
	})

	$('.file').click(function(){
		link = $(this).parent('.controls').find(':file');
    	link.click();
	}).show();  	
	 
	// user-nav
	var wrap = $('.user-nav'),
	 	link = $('#user-nav'),
		menu = $('#user-options');

 	link.click(function(event) {
		event.preventDefault();

		menu.toggleClass('closed');
		wrap.toggleClass('activated');
	});

 	// menu: abrir y ocultar
 	var btnMenu 	= $('#zoom-menu'),
 		wrapMenu 	= $('.menu');
 		
 	btnMenu.click(function(event) { 
 		event.preventDefault();

 		wrapMenu.toggleClass('activated');
 		btnMenu.toggleClass('activated');
  	});

 	// abrir y cerrar menu con el teclado
 	$(document).keydown(function(tecla) {
 		if (tecla.keyCode == 39) {
			wrapMenu.addClass('activated');
			btnMenu.addClass('activated');
 		};

 		if (tecla.keyCode == 37) {
			wrapMenu.removeClass('activated');
			btnMenu.removeClass('activated'); 			
 		}
 	});

 	// menu: funcionalidades de lista
 	var btnElement 	= $('.element > .dropdown'),
 		actionsList = $('.list-actions li');

 	btnElement.click(function(event) {
 		event.preventDefault();
 		event.stopPropagation();

 		var parent = $(this).parent('li.element');
 		parent.toggleClass('closed');
 	});

	// calendar: datepicker of bootstrap
	if($('.date-picker').length > 0) {
	    $('.date-picker').datepicker({
	        format: 'mm-dd-yyyy'
	    });
	} 	

	// select2
	$('.select2').select2({
		allowClear: true
	});

	// call the function introjs
	$('#help').click(function(event) {
		event.preventDefault();
		introJs().start();
	});

	// checkbox
	var checkbox = $('.borrar-todo');

	checkbox.click(function(e) {
		$('.wrap-delete-all').addClass('activated');
		$('.actions input').prop('checked',true);
	});

	$('.option-cancel').click(function(e) {
		checkbox.prop('checked',false);
		$('.wrap-delete-all').removeClass('activated');
		$('.actions input').prop('checked',false);		
	});

	// change between windows at formulary book address
	var linkChange 	= $('.form-list li'),
		fieldset	= $('fieldset');

	linkChange.click(function(e) {
		e.preventDefault();

		linkChange.removeClass('activated');
		$(this).toggleClass('activated');
		fieldset.toggleClass('activated');
	});

	// collapse fieldset 
	$('.wrap-form legend > .btn-collapse').click(function(e) {
		content   = $(this).parents('.wrap-form');
		is_closed = content.hasClass('closed');

		if (is_closed) {
			content.removeClass('closed');
		} else {
			content.addClass('closed');
		};
	});
});
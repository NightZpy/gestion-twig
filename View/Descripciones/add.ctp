<div class="descripciones form">
<?php echo $this->Form->create('Descripcion'); ?>
	<fieldset>
		<legend><?php echo __('Add Descripcion'); ?></legend>
	<?php
		echo $this->Form->input('proveedor_id', array('style'=>'width:430px;'));
		echo $this->Form->input('concepto_id', array('style'=>'width:430px;'));
		echo $this->Form->input('descripcion', array('type'=>'textarea'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
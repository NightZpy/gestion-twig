<div class="descripciones form">
<?php echo $this->Form->create('Descripcion'); ?>
	<?php
		echo $this->Form->input('proveedor_id', array('style'=>'width:430px;'));
		echo $this->Form->input('concepto_id', array('style'=>'width:430px;'));
		echo $this->Form->input('descripcion', array('type'=>'textarea', 'style'=>'width:415px;height:90px;'));
	?>
    
<?php // echo $this->Form->end(__('Submit')); ?>
    <input type="submit" id="enviar">
</div>

<script>
  $('#enviar').bind('click', function(e){
      e.preventDefault();
      $.ajax({
          url:'/descripciones/ajaxGuardarDescripcion',
          type:'post',
          data:$('#DescripcionAddAjaxForm').serialize()
      }).done(function(data){
          if(typeof addAjax == 'function'){
              addAjax(data)
          }
      })
  })  
</script>
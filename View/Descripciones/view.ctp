<div class="descripciones view">
<h2><?php  echo __('Descripcion'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($descripcion['Descripcion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proveedor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($descripcion['Proveedor']['razon_social'], array('controller' => 'personas', 'action' => 'view', $descripcion['Proveedor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Concepto'); ?></dt>
		<dd>
			<?php echo $this->Html->link($descripcion['Concepto']['id'], array('controller' => 'conceptos', 'action' => 'view', $descripcion['Concepto']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripcion'); ?></dt>
		<dd>
			<?php echo h($descripcion['Descripcion']['descripcion']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Descripcion'), array('action' => 'edit', $descripcion['Descripcion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Descripcion'), array('action' => 'delete', $descripcion['Descripcion']['id']), null, __('Are you sure you want to delete # %s?', $descripcion['Descripcion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Descripciones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Descripcion'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Personas'), array('controller' => 'personas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proveedor'), array('controller' => 'personas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conceptos'), array('controller' => 'conceptos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Concepto'), array('controller' => 'conceptos', 'action' => 'add')); ?> </li>
	</ul>
</div>

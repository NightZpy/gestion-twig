
<div id="contenidoAjax">
    <?php
    $this->Paginator->options(array(
        'update' => '#contenidoAjax',
        'evalScripts' => true
    ));
    
    ?>
    

    <?php echo $this->Form->create('Descripcion')?>
    <?php echo $this->Form->input('proveedor_id', array('type'=>'hidden'))?>
    <?php echo $this->Form->input('concepto_id', array('type'=>'hidden'))?>
    <!--<button class="btn" id="filtrarDescripcion">Filtrar</button>-->
    <?php echo $this->Form->end()?>

  

<br>
<div style="overflow-y: scroll;height: 300px;width: 100%">
    <table id="product-table" width="95%" style="margin-left: 3px;">
        <thead>
            <tr><th class="table-header-check t"></th><th class="table-header-repeat"><a>Descripción</a></th></tr>
        </thead>
        <tbody>
            <?php foreach ($descripciones as $descripcion):?>
            <tr>
                <td>
                    <a id="id-<?php echo $descripcion['Descripcion']['id']?>" class="icon-14 selector"></a>
                </td>
                <td><pre id="txt-<?php echo $descripcion['Descripcion']['id']?>"><?php echo $descripcion['Descripcion']['descripcion']?></pre></td></tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<div class="paging">
    <?php
    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
</div>

<?php echo $this->Js->writeBuffer();?>

<script>
    $(function(){
        $('.btn').button();
    })
    
    $('.selector').bind('click', function(e){
        e.preventDefault();
        var id=$(this).attr('id');
        if (typeof ajaxGetDescripciones=='function'){
            ajaxGetDescripciones($('#txt-'+id.substring(3,5)).html());
        }
    })
    
    $('#btnEnviar').bind('click', function(){
        $.ajax({
            url:'descripciones/ajaxGuardarDescripcion',
            type:'post',
            data:$('#formularioDescripciones').serialize()
        }).done(function(data){
            
        })
    
        
    });
    
    $('#filtrarDescripcion').bind('click', function(e){
        e.preventDefault();
        $.ajax({
            url:'descripciones/ajaxGetDescripciones',
            type:'post',
            data:$('#DescripcionAjaxGetDescripcionesForm').serialize()+'&data[Persona][clasificacion_id]=3'
        }).done(function(data){
            $('#contenidoAjax').html(data);
        });
    });
    
</script>
</div>
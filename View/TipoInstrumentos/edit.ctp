<div class="tipoInstrumentos form">
<?php echo $this->Form->create('TipoInstrumento'); ?>
	<fieldset>
		<legend><?php echo __('Edit Tipo Instrumento'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nombre');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TipoInstrumento.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('TipoInstrumento.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tipo Instrumentos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Instrumentos'), array('controller' => 'instrumentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Instrumento'), array('controller' => 'instrumentos', 'action' => 'add')); ?> </li>
	</ul>
</div>

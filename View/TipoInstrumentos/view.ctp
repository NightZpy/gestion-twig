<div class="tipoInstrumentos view">
<h2><?php  echo __('Tipo Instrumento'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tipoInstrumento['TipoInstrumento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($tipoInstrumento['TipoInstrumento']['nombre']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipo Instrumento'), array('action' => 'edit', $tipoInstrumento['TipoInstrumento']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipo Instrumento'), array('action' => 'delete', $tipoInstrumento['TipoInstrumento']['id']), null, __('Are you sure you want to delete # %s?', $tipoInstrumento['TipoInstrumento']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipo Instrumentos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipo Instrumento'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Instrumentos'), array('controller' => 'instrumentos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Instrumento'), array('controller' => 'instrumentos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Instrumentos'); ?></h3>
	<?php if (!empty($tipoInstrumento['Instrumento'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Proveedor Id'); ?></th>
		<th><?php echo __('Tipo Instrumento Id'); ?></th>
		<th><?php echo __('Nro Referencia'); ?></th>
		<th><?php echo __('Banco Id'); ?></th>
		<th><?php echo __('Cuenta Id'); ?></th>
		<th><?php echo __('Monto'); ?></th>
		<th><?php echo __('Fecha Carga'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($tipoInstrumento['Instrumento'] as $instrumento): ?>
		<tr>
			<td><?php echo $instrumento['id']; ?></td>
			<td><?php echo $instrumento['cliente_id']; ?></td>
			<td><?php echo $instrumento['proveedor_id']; ?></td>
			<td><?php echo $instrumento['tipo_instrumento_id']; ?></td>
			<td><?php echo $instrumento['nro_referencia']; ?></td>
			<td><?php echo $instrumento['banco_id']; ?></td>
			<td><?php echo $instrumento['cuenta_id']; ?></td>
			<td><?php echo $instrumento['monto']; ?></td>
			<td><?php echo $instrumento['fecha_carga']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'instrumentos', 'action' => 'view', $instrumento['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'instrumentos', 'action' => 'edit', $instrumento['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'instrumentos', 'action' => 'delete', $instrumento['id']), null, __('Are you sure you want to delete # %s?', $instrumento['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Instrumento'), array('controller' => 'instrumentos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

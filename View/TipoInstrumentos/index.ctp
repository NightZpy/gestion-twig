<div class="tipoInstrumentos index">
	<h2><?php echo __('Tipo de Instrumentos de Pago'); ?></h2>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
	<tr>
			<th class="table-header-check"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="table-header-repeat line-left"><?php echo $this->Paginator->sort('nombre'); ?></th>
			<th class="table-header-options line-left"><a><?php echo __('Actions'); ?></a></th>
	</tr>
	<?php
	foreach ($tipoInstrumentos as $tipoInstrumento): ?>
	<tr>
		<td><?php echo h($tipoInstrumento['TipoInstrumento']['id']); ?>&nbsp;</td>
		<td><?php echo h($tipoInstrumento['TipoInstrumento']['nombre']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('', array('action' => 'edit', $tipoInstrumento['TipoInstrumento']['id']), array('class'=>'icon-13')); ?>
			<?php echo $this->Form->postLink('', array('action' => 'delete', $tipoInstrumento['TipoInstrumento']['id']), array('class'=>'icon-17'), __('Are you sure you want to delete # %s?', $tipoInstrumento['TipoInstrumento']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<fieldset>
    <legend>Buscar Personas</legend>
            <?php 
            echo $this->Form->create('Persona', array('url' => array_merge(array('action' => 'index'), $this->params['pass'])));
            echo $this->Form->input('codigo', array('class' => 'Search'));
            echo $this->Form->input('razon_social', array('class' => 'Search'));
            echo $this->Form->input('tipo_documento_id', array('empty' => '(Todos)'));
            echo $this->Form->input('documento', array('class' => 'Search', 'label'=>'Documento Fiscal'));
            echo $this->Form->input('clasificacion_id', array('empty' => '(Todos)'));
            echo $this->Form->submit('Filtrar', array('div' => false, 'class'=>'form-buscar'));
            echo $this->Form->end();
            ?>
</fieldset>
<br />
<?php
if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Personas', 'agregar'))) {
?>
    <p><a href="/personas/agregar/" class="btn">Agregar Persona</a></p>
<?php
}
?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-1"><?= $this->Paginator->sort('codigo'); ?></th>
        <th nowrap class="table-header-repeat line-left"><?= $this->Paginator->sort('razon_social'); ?></th>
        <th nowrap class="table-header-repeat line-left"><?= $this->Paginator->sort('tipo_persona_id'); ?></th>
        <th class="table-header-repeat line-left"><?= $this->Paginator->sort('clasificacion_id'); ?></th>
        <th class="table-header-repeat line-left"><?= $this->Paginator->sort('documento','Documento Fiscal'); ?></th>
        <th class="table-header-repeat line-left"><?= $this->Paginator->sort('contribuyente_especial'); ?></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php foreach ($personas as $persona): ?>
        <tr class="registros">
            <td></td>
            <td><?= $persona['Persona']['codigo']; ?></td>
            <td nowrap><?= $persona['Persona']['razon_social']; ?></td>
            <td><?= $persona['TipoPersona']['nombre']; ?></td>
            <td><?= $persona['Clasificacion']['nombre']; ?></td>
            <td><?= $persona['TipoDocumento']['id']. '-' . $persona['Persona']['documento']; ?></td>
            <td><?= $this->Html->mostrarBoolean($persona['Persona']['contribuyente_especial']);?></td>
            <td class="options-width">
                <?php
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Personas', 'editar'))) {
                    echo $this->Html->link('', array('action' => 'editar', $persona['Persona']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar'));
                }
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Personas', 'eliminar'))) {
                    echo $this->Form->postLink('', array('action' => 'eliminar', $persona['Persona']['id']), array('class' => 'icon-17 info-tooltip', 'title'=>'Eliminar'), __('Esta Seguro De Eliminar este registro?', $persona['Persona']['id']));
                }
                if($persona['Clasificacion']['relacion_cliente'] == 1) {
                    if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Personas', 'relacionCliente'))) {
                        echo $this->Html->link('', array('action' => 'relacionCliente', $persona['Persona']['id'], '1'), array('class' => 'icon-3 info-tooltip', 'title'=>'Relacionar Clientes'));
                    }
                }
                if($persona['Clasificacion']['relacion_concepto'] == 1) {
                    if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Personas', 'relacionConcepto'))) {
                        echo $this->Html->link('', array('action' => 'relacionConcepto', $persona['Persona']['id']), array('class' => 'icon-12 info-tooltip', 'title' => 'Relacionar Conceptos'));
                    }
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?= $this->element('paginate'); ?>

<script>
	$(function(){
		$('.btn').button();
	})
</script>
<?php
if ($tipo_relacion == 1) {
    echo "<h1>Relación Asociado - Clientes</h1>";
} else if ($tipo_relacion == 2) {
    echo "<h1>Relación Cliente - Proveedores Finales</h1>";
}
?>
<dl>
    <dt>Razon Social</dt>
    <dd><?= $persona['Persona']['razon_social']; ?>&nbsp;</dd>
    <dt>Clasificación</dt>
    <dd><?= $persona['Clasificacion']['nombre']; ?>&nbsp;</dd>
    <dt>Documento Fiscal</dt>
    <dd><?= $persona['TipoDocumento']['nombre_corto']. '-'. $persona['Persona']['documento']; ?></dd>
</dl>

 <fieldset>
    <?php
    if ($tipo_relacion == 1) {
        echo "<legend>Clientes Disponibles</legend>";
    } else if ($tipo_relacion == 2) {
        echo "<legend>Proveedores Finales Disponibles</legend>";
    }
    ?>
    <?= $this->Form->create('Persona', array(array('action' => 'relationcliente', $persona['Persona']['id']),'id' => 'filters')); ?>
        <?php 
        echo $this->Form->input('codigo', array('class' => 'Search'));
        echo $this->Form->input('razon_social', array('class' => 'Search'));
        echo $this->Form->input('documento', array('class' => 'Search'));
        //echo $this->Form->input('clasificacion_id', array('empty' => 'Todos'));
        ?>
    <?= $this->Form->end(array('label' => 'Filtrar', 'class' => 'form-buscar')); ?>
    <br />
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-check"></th>
            <th class="table-header-repeat line-left minwidth-1"><a href="">Código</a></th>
            <th nowrap class="table-header-repeat line-left"><a href="">Razón Social</a></th>
            <th class="table-header-repeat line-left"><a href="">Documento</a></th>
            <th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php if (count($personasDisponibles)>= 1): ?>
            <?php foreach ($personasDisponibles as $personaDisponible): ?>
        <tr class="registros">
            <td></td>
            <td><?= $personaDisponible['Persona']['codigo']; ?></td>
            <td nowrap><?= $personaDisponible['Persona']['razon_social']; ?></td>
            <td><?= $personaDisponible['0']['documento']; ?></td>
            <?php
            if ($tipo_relacion == 1) { ?>
                <td><?= $this->Form->create('PersonaPersona', array('action' => 'asociarcliente')); ?>
                    <?= $this->Form->input('persona_padre_id', array('type' => 'hidden', 'value' => $persona['Persona']['id']));?>
                    <?= $this->Form->input('persona_hijo_id', array('type' => 'hidden', 'value' => $personaDisponible['Persona']['id']));?>
                    <?= $this->Form->input('tipo_relacion', array('type' => 'hidden', 'value' => 'asociado_clientes'));?>
                    <?= $this->Form->button(' Relacionar ', array('type' => 'submit', 'title' => ' Relacionar ' )); ?>
                    <?=  $this->Form->end(); ?>
                </td>
            <?php
            } else if ($tipo_relacion == 2) { ?>
                <td><?= $this->Form->create('PersonaPersona', array('action' => 'asociarproveedor')); ?>
                    <?= $this->Form->input('persona_padre_id', array('type' => 'hidden', 'value' => $id));?>
                    <?= $this->Form->input('persona_hijo_id', array('type' => 'hidden', 'value' => $personaDisponible['Persona']['id']));?>
                    <?= $this->Form->input('tipo_relacion', array('type' => 'hidden', 'value' => 'cliente_proveedor'));?>
                    <?= $this->Form->button(' Relacionar ', array('type' => 'submit', 'title' => ' Relacionar ' )); ?>
                    <?=  $this->Form->end(); ?>
                </td>
            <?php
            }
            ?>
            
        </tr>
         <?php endforeach; ?>
        <?php else: ?>
        <tr class="registros">
            <?php
            if ($tipo_relacion == 1) {
                echo '<td colspan="5" align="center">No existen clientes disponibles</td>';
            } else if ($tipo_relacion == 2) {
                echo '<td colspan="5" align="center">No existen proveedores finales disponibles</td>';
            }
            ?>
        </tr>
        <?php endif; ?>
    </table>
</fieldset>
<br />
<br />
<fieldset>
    <?php
    if ($tipo_relacion == 1) {
        echo '<legend>Clientes Relacionados</legend>';
    } else if ($tipo_relacion == 2) {
        echo '<legend>Proveedores Finales Relacionados</legend>';
    }
    ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-1"><a href="">Código</a></th>
        <th nowrap class="table-header-repeat line-left"><a href="">Razón Social</a></th>
        <th class="table-header-repeat line-left"><a href="">Documento</a></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php if (count($personasRelacionadas)>= 1): ?>
        <?php foreach ($personasRelacionadas as $personaRelacionada): ?>
    <tr class="registros">
        <td></td>
        <td><?= $personaRelacionada['Persona']['codigo']; ?></td>
        <td nowrap><?= $personaRelacionada['Persona']['razon_social']; ?></td>
        <td><?= $personaRelacionada['Persona']['tipo_documento_id'] . $personaRelacionada['Persona']['documento']; ?></td>
        <?php
        if ($tipo_relacion == 1) { ?>
            <td><?= $this->Form->postLink('', array('controller' => 'PersonaPersonas', 'action' => 'removercliente', $personaRelacionada['PersonaPersona']['id'], $personaRelacionada['PersonaPersona']['persona_padre_id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Remover'), __('Esta seguro de remover este registro?', $persona['Persona']['id'])); ?></td>
        <?php
        } else if ($tipo_relacion == 2) { ?>
            <td><?= $this->Form->postLink('', array('controller' => 'PersonaPersonas', 'action' => 'removerproveedor', $id, $personaRelacionada['Persona']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Remover'), __('Esta seguro de remover este registro?', $persona['Persona']['id'])); ?></td>
        <?php
        }
        ?>
    </tr>
        <?php endforeach; ?>
    <?php else: ?>
    <tr class="registros">
        <td colspan="5" align="center">No existen clientes relacionados</td>
    </tr>
    <?php endif; ?>
</table>
</fieldset>
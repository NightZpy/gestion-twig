<?php 
$this->Html->script('cargaCiudades', array('inline' => false));
$this->Html->script('datosFiscales', array('inline' => false));
$this->Html->script('jquery.validate', array('inline' => false));
$this->Html->script('utilidades', array('inline' => false));
$this->Html->script('tooltip_cuentas', array('inline' => false));
$this->Html->script('cargarBancos', array('inline' => false));
$this->Html->script('cargarAdjuntos', array('inline' => false));
?>

<script>
    $(function() {
        $( "#tabs" ).tabs();

        $("#PersonaContribuyenteEspecial").click(function() {  
                    if($("#PersonaContribuyenteEspecial").is(':checked')) {  
                        $("#PersonaPorcentajeRetencionIva").val('75');
                    } else {  
                        $("#PersonaPorcentajeRetencionIva").val('0');
                    }  
                }); 

        $("#PersonaAgregarForm").submit(function(){
            return true;
        });
    }); 
</script>

<div id="tabs">
    <ul>
        <li class="tab"><a href="/personas/agregar/#info_basica">Información Básica</a></li>
        <li class="tab"><a href="/personas/agregar/#info_adicional">Información Adicional</a></li>
    </ul>
    <?php echo $this->Form->create('Persona', array('type' => 'file')); ?>
    <div id="info_basica">
        <fieldset>
            <legend>Información Básica</legend>            
            <?php
            echo $this->Form->input('id');
            echo $this->Form->input('codigo');
            echo $this->Form->input('tipo_persona_id', array('empty' => '-- seleccione --'));
            echo $this->Form->input('clasificacion_id', array('empty' => '-- seleccione --'));
            echo $this->Form->input('rubro_id', array('empty' => '-- seleccione --'));
            echo $this->Form->input('grupo_id', array('empty' => '-- seleccione --', 'label' => 'Agrupación'));

            echo $this->Form->input('documento', array(
                'label' => 'Documento Fiscal',
                'type' => 'text',
                'between' => $this->Form->input('tipo_documento_id', array(
                                'div'   => false,
                                'label' => false,
                                'empty' => '---',
                                'error' => false,
                            )),
                'class' => 'Search'
            ));
            echo $this->Form->input('razon_social');
            echo $this->Form->input('direccion');
            echo $this->Form->input('estado_id', array('empty' => '-- seleccione --'));
            echo $this->Form->input('ciudad_id', array('empty' => '-- seleccione --'));            
            echo $this->Form->input('telefono');            
            echo $this->Form->input('correo_electronico');
            echo $this->Form->input('pagina_web',  array('label' => 'Dirección Web'));            
            echo $this->Form->input('observaciones');
        
        ?>
        </fieldset>
    </div>

    <div id="info_adicional">
        <fieldset>
            <legend>Datos Fiscales</legend>
            <?php echo $this->Form->input('contribuyente_especial'); ?>
            <?php echo $this->Form->input('porcentaje_retencion_iva', array('label' => 'Porcentaje de Retención IVA',  'type' => 'text', 'class' => 'Montos')); ?>
        </fieldset>
        
        <fieldset>
            <legend>Porcentajes de Comisión</legend>
                <?php echo $this->Form->input('porcentaje_costo_gestion', array('type' => 'text', 'class' => 'Montos'));?>
                <?php echo $this->Form->input('porcentaje_costo_operativo', array('type' => 'text', 'class' => 'Montos'));?>
                <?php echo $this->Form->input('porcentaje_costo_venta', array('type' => 'text', 'class' => 'Montos'));?>
                <?php echo $this->Form->input('porcentaje_costo_venta_secundario', array('type' => 'text', 'class' => 'Montos'));?>
                <?php echo $this->Form->input('porcentaje_costo_administrativo', array('type' => 'text', 'class' => 'Montos'));?>
        </fieldset>  

       <div id="account-container" title="Cuentas Bancarias">
            <fieldset>
                <legend>Cuentas Bancarias</legend>
                <button id="create-account" type="button">Agregar</button>                
                <br>
                <div class="clear"></div>
                <table id="cuentas-table" border='1'>
                    <tr>
                        <th>Banco</th>
                        <th>Tipo</th>
                        <th>Número</th>
                        <th></th>
                    </tr>                    
                </table>
            </fieldset>
        </div>
        
        <div id="attach-container" title="Adjuntos">
            <fieldset>
                <legend>Adjuntos</legend>
                <button id="create-attach" type="button">Agregar</button>

                <div class="clear"></div>                
                <br>
                <?php echo $this->Form->input('tipo_adjunto_id', array('label' => 'Tipo de Adjunto', 'empty' => '-- seleccione --')); ?>
                <div class="clear"></div>
                <br>
                <table id="adjuntos-table">
                    <tr>
                        <th>Imagen</th>
                        <th>Tipo</th>
                    </tr>                    
                </table>
            </fieldset>
        </div>

    </div>
    <?php //echo $this->Form->end('Guardar', ['id' => 'enviar']); ?>            
    <input type="submit" id="enviar" value="Guardar">
    <div class="clear"></div>    
</div>

<ul>
</ul>

<div id="account-form" title="Administrar Cuentas Bancarias">
  <p class="validateTips">All form fields are required.</p>

  <fieldset>
    <?php echo $this->Form->input('banco_id', array('empty' => '-- seleccione --', 'class' => 'input select required')); ?>
    <?php echo $this->Form->input('tipo_cuenta_id', array('label' => 'Tipo de Cuenta', 'empty' => '-- seleccione --', 'class' => 'input select required')); ?>
    <?php echo $this->Form->input('numero', array('label' => 'Número de Cuenta', 'class' => 'input text required')); ?>
  </fieldset>
</div>

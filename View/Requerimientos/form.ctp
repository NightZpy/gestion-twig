<?php 
$this->Html->script('greybox/AJS.js', array('inline' => false));
$this->Html->script('greybox/gb_scripts.js', array('inline' => false));
$this->Html->css('greybox/gb_styles.css', null, array('inline' => false));
$this->Html->script('cargaClientes.js', array('inline' => false));
?>
<p><a href="/requerimientos/" class="btn"><< Regresar al listado de requerimientos</a></p>
<?php echo $this->Form->create('Requerimiento'); ?>
<!--	<fieldset>
            <legend>Requerimiento</legend> -->
        <?php
        if (isset($requerimiento_id) && count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Requerimientos', 'bitacora'))) {
            echo $this->Html->link('Bitacora', array('controller' => 'requerimientos', 'action' => 'bitacora', $requerimiento_id), 
                                                 array('title'=>'Bitacora del requerimiento', 'rel' => 'gb_page_fs[]', 'class'=>'btn','style'=>'float:right;'));
        }
        echo $this->Form->input('id', array('type' => 'hidden', 'value' => isset($requerimiento_id)?$requerimiento_id:null));
        echo $this->Form->input('asociado_id', array('empty' => 'Seleccione', 'selected' => isset($asociados_seleccionado[0])?$asociados_seleccionado[0]:null, 'div'=>array('class'=>'divhorizontal')));
        echo $this->Form->input('cliente_id', array('empty' => 'Seleccione', 'div'=>array('class'=>'divhorizontal')));
        echo $this->Form->input('prioridad_id', array('empty' => 'Seleccione', 'selected' => $prioridad_predeterminado[0], 'div'=>array('class'=>'divhorizontaln')));
        echo $this->Form->input('estatus_workflow', array('type' => 'text', 'label' => 'Estatus', 'readonly' => true, 'value' => $estatus_workflow_predeterminado_nombre, 'div'=>array('class'=>'divhorizontal'),'style'=>'width:90%;'));
        echo $this->Form->input('estatus_workflow_id', array('type' => 'hidden', 'value' => $estatus_workflow_predeterminado_id));
        ?>
<!--	</fieldset>
    <br />
    <fieldset>
        <legend>Configuracion de comisiones</legend> -->
        <?php
        echo $this->Form->input('porcentaje_costo_gestion', array('label' => '% Costo Gestion', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_operativo', array('label' => '% Costo Operativo', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_venta', array('label' => '% Costo Venta', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_venta_secundario', array('label' => '% Costo Venta Secundario', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_administrativo', array('label' => '% Costo Administrativo', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        ?>
    </fieldset>
    <br />
<?php
if (isset($requerimiento_id)) {
?>
<!--    <fieldset>
        <legend>Totales requerimiento</legend> -->
        <?php
        echo $this->Form->input('monto_base', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_retencion_islr', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_iva', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_retencion_iva', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_total', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        ?>
<!--    </fieldset>
    <br /> 
    <fieldset>
        <legend>Totales comisiones</legend> -->    
        <?php
        echo $this->Form->input('monto_costo_gestion', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_costo_operativo', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_costo_venta', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_costo_venta_secundario', array('label' => 'Monto Costo Venta II', 'class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_costo_administrativo', array('class' => 'Montos', 'type' => 'hidden', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        ?>
<!--    </fieldset> -->
<?php
}
?>
<?php 
    if (isset($requerimiento_id)) {
        $estatusWorkflowId = $this->request->data['Requerimiento']['estatus_workflow_id'];
        if ((array_key_exists($estatusWorkflowId, $estatus) && ($estatus[$estatusWorkflowId] == 1))) {
            echo $this->Form->end('Submit');
        } else {
            echo "</form>";
        }
    } else {
        echo $this->Form->end('Submit');
    }
?>
<br><br>
<?php if (isset($requerimiento_id)) {?>
    <fieldset>
        <legend><?php echo 'Acciones disponibles'; ?></legend>
<?php
	
		foreach ($acciones as $accion):
            if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Requerimientos', 'cambiarestatus'))) {    
                echo $this->Html->link($accion['Accion']['nombre'], array('controller' => 'requerimientos', 'action' => 'cambiarestatus', 
											$requerimiento_id, $accion['Accion']['id']), 
											array('title'=>'Aprobar requerimiento', 'rel' => 'gb_page_fs[]', 'class'=>'btn'));
            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        endforeach;
?>
    </fieldset>
<?php }?>
<br><br><br>
<?php
if (isset($requerimiento_id)) {
?>
<fieldset>
    <legend><?php echo 'Facturas'; ?></legend>
<?php 
    if ((array_key_exists($estatusWorkflowId, $estatus) && ($estatus[$estatusWorkflowId] == 1))) {
        if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Facturas', 'agregar'))) {    
            echo $this->Html->link('Nueva factura', array(
                        'controller' => 'facturas', 
                        'action' => 'agregar', 
                        $requerimiento_id), 
                        array('title' => 'Editar', 'class'=>'btn'));
        }
    }
?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-repeat line-left minwidth-1"><l><?php echo 'Proveedor'; ?></l></th>
 	<th class="table-header-repeat line-left minwidth-1 Center"><l><?php echo 'Fecha Requerida'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Center"><l><?php echo 'Fecha Facturacion'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Center"><l><?php echo 'Numero de Factura'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Monto Base'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Monto Iva'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Monto Total'; ?></l></th>
        <th class="table-header-repeat line-left"><l>Opciones</l></th>
    </tr>
    <?php
    foreach ($facturas as $factura): 
    ?>
    <tr class="registros">
        <td><?php echo h($factura['Persona']['razon_social']); ?></td>
        <td class="Center"><?php echo h(AppHelper::fechaNormal($factura['Factura']['fecha_requerida'])); ?></td>
        <td class="Center"><?php echo h(AppHelper::fechaNormal($factura['Factura']['fecha_facturacion'])); ?></td>
        <td class="Center"><?php echo h($factura['Factura']['numero_factura']); ?></td>        
        <td class="Right"><?php echo h(CakeNumber::currency($factura['Factura']['monto_base'], 'VEF')); ?></td>
        <td class="Right"><?php echo h(CakeNumber::currency($factura['Factura']['monto_iva'], 'VEF')); ?></td>
        <td class="Right"><?php echo h(CakeNumber::currency($factura['Factura']['monto_total'], 'VEF')); ?></td>
        <td class="options-width">
            <?php
            if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Facturas', 'editar'))) { 
                echo $this->Html->link('', array(
                                                   'controller' => 'facturas', 
                                                   'action' => 'editar', 
                                                   $requerimiento_id, 
                                                   $factura['Factura']['id']
                                                   ), 
                                              array(
                                                   'class' => 'icon-13 info-tooltip', 
                                                   'title'=>'Editar'
                                                   )
                                         ); 
            }
            ?>
            <?php 
                if ((array_key_exists($estatusWorkflowId, $estatus) && ($estatus[$estatusWorkflowId] == 1))) {  
                    if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Facturas', 'delete'))) {
                        echo $this->Form->postLink('', array(
                                                       'controller' => 'facturas', 
                                                       'action' => 'delete', 
                                                       $factura['Factura']['id'], 
                                                       $requerimiento_id
                                                       ), 
                                                  array(
                                                        'class' => 'icon-2 info-tooltip', 
                                                        'title'=>'Eliminar'
                                                       ), 
                                                  __('Esta seguro de eliminar la factura # %s ? \n\nNo podrá revertir este cambio.', 
                                                      $factura['Factura']['id']
                                                    )
                                             ); 
                    }
                }
            ?>
        </td>
    </tr>
    <?php 
    endforeach; 
    ?>
    <tr>
        <td colspan="4" style="text-align: right;"><b>TOTALES</b></td>
        <td class="td-number"><b><?=$this->data['Requerimiento']['monto_base']?></b></td>
        <td class="td-number"><b><?=$this->data['Requerimiento']['monto_iva']?></b></td>
        <td class="td-number"><b><?=$this->data['Requerimiento']['monto_total']?></b></td>
        <td></td>
        
        
    </tr>
</table>
</fieldset>
<?php
}
?>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
	width:280px;
}
</style>

<script>
	$(function(){
		$('.btn').button();
    })
</script>

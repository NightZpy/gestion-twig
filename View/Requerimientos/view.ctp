<h1><?php  echo __('Ver Requerimiento'); ?></h2>
<dl>
    <dt><?php echo __('Asociado'); ?></dt>
    <dd>
        <?php echo h($requerimiento['Asociado']['nombre']); ?>
        &nbsp;
    </dd>
    <dt><?php echo __('Cliente'); ?></dt>
    <dd>
        <?php echo h($requerimiento['Cliente']['nombre']); ?>
        &nbsp;
    </dd>
    <dt><?php echo __('Estatus'); ?></dt>
    <dd>
        <?php echo h($requerimiento['EstatusWorkflow']['nombre']); ?>
        &nbsp;
    </dd>
</dl>
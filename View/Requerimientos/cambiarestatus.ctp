<?php
if ($ocultar == true) {
?>
<script>
    parent.parent.location.href = "requerimientos/index"
    parent.parent.GB_CURRENT.hide();
</script>
<?php
}
?>
<?php echo $this->Form->create('Requerimiento'); ?>
<fieldset>
		<legend><?php echo 'Observaciones'; ?></legend>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden', 'value' => $id));
        echo $this->Form->input('accion_id', array('type' => 'hidden', 'value' => $accion_id));
        echo $this->Form->input('estatus_workflow_actual', array(
                                                            'value' => $transiciones[0]['EstatusWorkflowActual']['estatus_actual'], 
                                                            'label' => 'Estatus Actual',
                                                            'readonly' => true));
        echo $this->Form->input('estatus_workflow_proximo', array(
                                                            'value' => $transiciones[0]['EstatusWorkflowProximo']['estatus_proximo'], 
                                                            'label' => 'Estatus proximo',
                                                            'readonly' => true));
        echo $this->Form->input('observacion', array('type' => 'textarea'));
        echo $this->Form->input('estatus_workflow_id', array(
                                                            'type' => 'hidden',
                                                            'value' => $transiciones[0]['EstatusWorkflowProximo']['estatus_proximo_id']));
        
        echo $this->Form->input('monto_base', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_retencion_islr', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_iva', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_retencion_iva', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_total', array('class' => 'Montos', 'type' => 'hidden'));
        
        echo $this->Form->input('porcentaje_costo_gestion', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('porcentaje_costo_operativo', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_costo_gestion', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_costo_operativo', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_costo_venta', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_costo_venta_secundario', array('class' => 'Montos', 'type' => 'hidden'));
        echo $this->Form->input('monto_costo_administrativo', array('class' => 'Montos', 'type' => 'hidden'));        
        ?>
</fieldset>
<?php echo $this->Form->end('Submit');?>
<script>
$(function (){
    $("#RequerimientoCambiarestatusForm").submit(function(){
        if (jQuery("#RequerimientoAccionId").val() == 2) {
            if (jQuery("#RequerimientoObservacion").val() == "") {
                $( "#dialog-modal" ).dialog({
                    height: 140,
                    modal: true
                });
                return false;
            }else if (jQuery("#RequerimientoObservacion").val().length <= "10") {
                $( "#dialog-modal2" ).dialog({
                    height: 140,
                    modal: true
                });
                return false;
            }
        }else{
            return true;
        }
    });
});    
</script>
<div style="display:none;" id="dialog-modal" title="Mensaje de sistema">
    <p>Debe introducir una observacion.</p>
</div>
<div style="display:none;" id="dialog-modal2" title="Mensaje de sistema">
    <p>La observacion es muy corta.</p>
</div>
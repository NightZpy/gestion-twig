<?php 
$this->Html->script('greybox/AJS.js', array('inline' => false));
$this->Html->script('greybox/gb_scripts.js', array('inline' => false));
$this->Html->css('greybox/gb_styles.css', null, array('inline' => false));
$this->Html->script('cargaClientes.js', array('inline' => false));
?>
<?php echo $this->Form->create('Requerimiento'); ?>
	<fieldset>
		<legend><?php echo 'Requerimiento'; ?></legend>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden', 'value' => isset($requerimiento_id)?$requerimiento_id:null));
        echo $this->Form->input('asociado_id', array('empty' => 'Seleccione', 'selected' => isset($asociados_seleccionado[0])?$asociados_seleccionado[0]:null, 'div'=>array('class'=>'divhorizontal')));
        echo $this->Form->input('cliente_id', array('empty' => 'Seleccione', 'div'=>array('class'=>'divhorizontal')));
        ?>
	</fieldset>
    <br>
    <fieldset>
		<legend><?php echo 'Configuracion de comisiones'; ?></legend>
        <?php
        echo $this->Form->input('porcentaje_costo_gestion', array('label' => '% Costo Gestion', 'class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_operativo', array('label' => '% Costo Operativo', 'class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_venta', array('label' => '% Costo Venta', 'class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_venta_secundario', array('label' => '% Costo Venta Secundario', 'class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_costo_administrativo', array('label' => '% Costo Administrativo', 'class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        ?>
	</fieldset>
    <br>
    <?=$this->Form->end('Submit');?>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
	width:280px;
}
</style>

<script>
	$(function(){
		$('.btn').button();
    })
</script>

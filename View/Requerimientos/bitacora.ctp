<h1>Bitacora</h1>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-check"></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Estatus Anterior'; ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Estatus Actual'; ?></a></th>
	<th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Accion'; ?></a></th>        
	<th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Observacion'; ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Fecha'; ?></a></th>
        <th class="table-header-repeat line-left minwidth-1"><a href=""><?php echo 'Usuario'; ?></a></th>
    </tr>
    <?php
    foreach ($bitacoras as $bitacora):
	$accion = "Rechazar";
	if ($bitacora['Auditoria']['valor_anterior'] < $bitacora['Auditoria']['valor_nuevo']) {
		$accion = "Aprobar";
	}
    ?>
    <tr class="registros">
        <td></td>
        <td><?php echo h($bitacora['EstatusWorkflowAnterior']['nombre']); ?></td>
        <td><?php echo h($bitacora['EstatusWorkflowActual']['nombre']); ?></td>
	<td><?php echo h($accion); ?></td>        
	<td><?php echo h($bitacora['Auditoria']['observacion']); ?></td>
        <td><?php echo h($bitacora['Auditoria']['fecha']); ?></td>
        <td><?php echo h($bitacora['Usuario']['username']); ?></td>
    </tr>
    <?php 
    endforeach; 
    ?>
</table>

<?php
$this->Html->script('cargaClientes.js', array('inline' => false));
?>
<?php echo $this->Form->create('Requerimiento', array('action'=>'index','id'=>'filters')); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <fieldset>
            <legend>Buscar Requerimientos</legend>
                <?php 
                echo $this->Form->input('asociado_id', array('empty' => 'Todos', 'selected' => isset($asociados_seleccionado[0])?$asociados_seleccionado[0]:null));
                echo $this->Form->input('cliente_id', array('empty' => 'Todos'));
                echo $this->Form->input('estatus_id', array('empty' => 'Todos'));
                echo $this->Form->input('prioridad_id', array('empty' => 'Todos'));
                ?>
                <?php echo $this->Form->end(array('label' => 'Agregar', 'class' => 'form-buscar')); ?>
        </fieldset>
        <br />
        <?php
        if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Requerimientos', 'agregar'))) {    
            echo $this->Html->link('Agregar requerimiento', array('controller' => 'requerimientos', 'action' => 'agregar'), array('class'=>'btn'));
        }
        ?>
        <tr>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('asociado'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('cliente'); ?></th>
            <th class="table-header-repeat line-left minwidth-1 Center"><?php echo $this->Paginator->sort('estatus'); ?></th>
            <th class="table-header-repeat line-left minwidth-1 Center"><?php echo $this->Paginator->sort('prioridad'); ?></th>
            <th class="table-header-repeat line-left minwidth-1 Right"><?php echo $this->Paginator->sort('monto_base'); ?></th>
            <th class="table-header-repeat line-left minwidth-1 Right"><?php echo $this->Paginator->sort('monto_iva'); ?></th>
            <th class="table-header-repeat line-left minwidth-1 Right"><?php echo $this->Paginator->sort('monto_total'); ?></th>
            <th class="table-header-options line-left"><a href="">Acciones</a></th>
        </tr>
        <?php
        foreach ($requerimientos as $requerimiento): 
        ?>
        <tr class="registros">
            <td><?php echo h($requerimiento['Asociado']['razon_social']); ?></td>
            <td><?php echo h($requerimiento['Cliente']['razon_social']); ?></td>
            <td class="Center"><?php echo h($requerimiento['EstatusWorkflow']['nombre']); ?></td>
            <td class="Center"><?php echo h(ucFirst($requerimiento['Prioridad']['nombre'])); ?></td>
            <td class="Right"><?php echo h(number_format($requerimiento['Requerimiento']['monto_base'],2,",",".")); ?></td>
            <td class="Right"><?php echo h(number_format($requerimiento['Requerimiento']['monto_iva'],2,",",".")); ?></td>
            <td class="Right"><?php echo h(number_format($requerimiento['Requerimiento']['monto_total'],2,",",".")); ?></td>
            <td class="options-width" style="width:90px;">
                <?php
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Requerimientos', 'editar'))) {
                    echo $this->Html->link('', array('action' => 'editar', $requerimiento['Requerimiento']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Ver Requerimiento'));
                }
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Requerimientos', 'duplicar'))) {
                    echo $this->Html->link('', array('action' => 'duplicar', $requerimiento['Requerimiento']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Duplicar Requerimiento')); 
                }
                if ($requerimiento['Requerimiento']['monto_base'] > 0) {
                    if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'instruccionPagos', 'edit'))) {
                        echo $this->Html->link('', array('controller' => 'instruccionPagos', 'action' => 'edit', $requerimiento['Requerimiento']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Instrucciones de Pagos')); 
                    }
                }
                ?>
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
}
</style>

<script>
	$(function(){
		$('.btn').button();
	})
</script>
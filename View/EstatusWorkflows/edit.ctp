<div class="Concepto form">
    <?php echo $this->Form->create('EstatusWorkflow'); ?>
	<fieldset>
		<legend><?php echo __('Editar Estatus'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nombre');
        echo $this->Form->input('activo');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
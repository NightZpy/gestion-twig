<div class="cuentas form">
<?php echo $this->Form->create('Cuenta'); ?>
	<fieldset>
		<legend>Cuentas Bancarias</legend>
	<?php
        echo $this->Form->input('id');
        echo $this->Form->input('persona_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('banco_id', array('empty' => '-- seleccione --'));
        echo $this->Form->input('tipo_cuenta_id', array('empty' => '-- seleccione --'));
		echo $this->Form->input('numero');
	?>
	</fieldset>
<?php echo $this->Form->end('Guardar'); ?>
</div>


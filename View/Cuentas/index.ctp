<h1>Cuentas Bancarias</h1>
<p><a href="cuentas/agregar" class="btn">Agregar Registro</a></p>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('persona_id'); ?></th>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('banco_id'); ?></th>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('tipo_cuenta_id'); ?></th>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('numero'); ?></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php foreach ($cuentas as $cuenta): ?>
    <tr class="registros">
        <td nowrap><?= $cuenta['Persona']['razon_social']; ?></td>
        <td nowrap><?= $cuenta['Banco']['nombre']; ?></td>
        <td nowrap><?= $cuenta['TipoCuenta']['nombre']; ?></td>
        <td nowrap><?= $cuenta['Cuenta']['numero']; ?></td>
        <td class="options-width">
            <?php echo $this->Html->link('', array('action' => 'editar', $cuenta['Cuenta']['id']), array('class' => 'icon-13 info-tooltip', 'title' => 'Editar')); ?>
            <?php echo $this->Form->postLink('', array('action' => 'eliminar', $cuenta['Cuenta']['id']), array('class' => 'icon-17 info-tooltip', 'title' => 'Eliminar'), 'Esta seguro de eliminar este registro?', $cuenta['Cuenta']['id']); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?= $this->element('paginate'); ?>

<script>
	$('.btn').button();
</script>
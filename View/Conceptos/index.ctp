    
    <?php echo $this->Form->create('Concepto', array('action'=>'index','id'=>'filters')); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <fieldset>
            <legend>Buscar Conceptos</legend>
                <?php 
                echo $this->Form->input('nombre', array('class' => 'Search'));
                ?>
                <?php echo $this->Form->end(array('label'=>'Filtrar',  'class' => 'form-buscar')); ?>
        </fieldset>
    </table>
    <p><a href="/conceptos/agregar/" class="btn">Agregar Concepto</a></p>

    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre', 'Concepto'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('impuesto_id', 'Porcentaje (%)'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('activo'); ?></th>
            <th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php
        foreach ($conceptos as $concepto): 
        ?>
        <tr class="registros">
            <td><?php echo h($concepto['Concepto']['nombre']); ?></td>
            <td><?php echo h(CakeNumber::currency($concepto['Impuesto']['valor'],'VEF')); ?></td>
            <td><?php echo $this->Html->mostrarBoolean($concepto['Concepto']['activo']); ?></td>
            <td class="options-width">
                <?php echo $this->Html->link('', array('action' => 'editar', $concepto['Concepto']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
                <?php echo $this->Form->postLink('', array('action' => 'eliminar', $concepto['Concepto']['id']), array('class' => 'icon-17 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $concepto['Concepto']['id'])); ?>
                
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
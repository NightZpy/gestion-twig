<?php echo $this->Form->create('Impresion',array('novalidate' => true)); ?>
	<fieldset>
		<legend>Imprimir factura</legend>
		<?php
		//echo $this->Form->input('proveedor_id', array('onchange' => 'asignarCiudad()'));
		echo $this->Form->input('proveedor_id');
		echo $this->Form->input('numero_factura', array('label' => 'N&uacute;mero de factura', 'maxlength' => '20'));
		echo $this->Form->input('localidad', array('label' => 'Lugar de emisi&oacute;n', 'default' => 'Caracas', 'maxlength' => '80'));
		$opciones = array('0' => 'Contado', '1' => 'A cr&eacute;dito');
		$atributos = array('legend' => "Forma de Pago", 'default' => 0);
		echo $this->Form->radio('condicion', $opciones, $atributos);
		?>
	</fieldset>
<?php echo $this->Form->end('Ver Factura'); ?>
<?php  echo $this->Html->script('impresion'); ?>
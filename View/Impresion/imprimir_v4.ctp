<?php
require_once(APP. 'Vendor' .DS. 'JavaBridgeLibs\java\Java.inc'); //Incluimos la libreria JavaBridge
$dirJasperReportsLibs = APP . 'Vendor' .DS. 'JasperReportsLibs' .DS;
$dirReportes = APP . 'webroot' .DS. 'reportes' .DS;

$classpath="";
$handle = @opendir($dirJasperReportsLibs); //Creamos una variable que va a contener todas las librerías que están en la carpeta lib_reportes
while(($lib = readdir($handle)) !== false) {
    $classpath .= 'file:'.$dirJasperReportsLibs.'/'.$lib .';';
}

try {
    java_require($classpath); //Añadimos las librerías

    //Creamos la conexión JDBC
    $Conn = new Java("org.altic.jasperReports.JdbcConnection");
    //Seteamos el driver mysql
    $Conn->setDriver("com.mysql.jdbc.Driver");
    //Especificamos los datos de la conexión
    $Conn->setConnectString("jdbc:mysql://localhost/gestion");
    $Conn->setUser("root");
    $Conn->setPassword("root");
    $parametrosMap = new Java("java.util.HashMap");
    $parametrosMap->put("numero_factura", new Java('java.lang.Integer', $numero_factura ));
    $parametrosMap->put("lugar", new Java('java.lang.String', "Caracas" ));
    //Creamos el objeto JasperReport que permite obtener el reporte
    $sJfm = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
    
    //Guardamos el reporte en una variable $print para luego exportarla
    $jasperFile = "{$dirReportes}{$formato_factura}.jasper";
    $print = $sJfm->fillReportToFile($jasperFile , $parametrosMap, $Conn->getConnection());
    
    //Exportamos el informe y lo guardamos como pdf en el directorio donde están los reportes
    $view = new JavaClass("net.sf.jasperreports.view.JasperViewer"); //
    $view->viewReport($print, false);
} catch(JavaException $ex){
	echo "error ";
	$trace = new Java("java.io.ByteArrayOutputStream");
	$ex->printStackTrace(new Java("java.io.PrintStream", $trace));
	print "java stack trace: $trace\n";
}
<form action="impresion/mostrar">
	<fieldset>
		<legend>Impresi&oacute;n de facturas</legend>
		<select name="proveedor" id="proveedor">
			<option>Seleccione...</option>
			<?php for($i=0; $i<count($proveedores); $i++){
				echo '<option>'.$proveedores[$i]['impresion']['proveedor'].'</option>';
			} ?>
		</select>
	<fieldset>
		<legend>Fecha de facturas</legend>
		<table>
			<tr>
				<td>
					<?php echo $this->Form->input('fecha1', array('label' => 'Desde','type' => 'text')); ?>
				</td>
				<td>
					<?php echo $this->Form->input('fecha2', array('label' => 'Hasta','type' => 'text')); ?>
				</td>
			</tr>
		</table>
	</fieldset>
	<fieldset>
		<legend>N&uacute;mero de facturas</legend>
		<table>
			<tr>
				<td>
					<label for="numero1">Desde</label>
					<input id="numero1" name="numero1" />
				</td>
				<td>
					<label for="numero2">Hasta</label>
					<input id="numero2" name="numero2" />
				</td>
			</tr>
		</table>
	</fieldset>
	<input type="checkbox" id="reimprimir" name="reimprimir" />Reimprimir
	<hr />
	<button id="imprimir" name="imprimir" type="submit">Imprimir</button>
	</fieldset>
</form>
<script type="text/javascript">
  $(document).ready(function(){
	$( "#fecha1" ).hide().after('<input type="text" name="DueDate" id="DueDate" value="" style="width:200px" />');
    //enable datepicker
    $( "#DueDate" ).Zebra_DatePicker({
      format: 'd/m/Y'
    });
    //put values back into CakePHP input element
    $( "#TaskAddForm" ).submit(function() {
      var DueDate = $( "#DueDate" ).val();
      DueDate = DueDate.split('/');
      $( "#fecha1" ).val(DueDate[2] + '-' + DueDate[1] + '-' + DueDate[0] + ' ' + DueTime + ':00');
    });
	
	$( "#fecha2" ).hide().after('<input type="text" name="DueDate2" id="DueDate2" value="" style="width:200px" />');
    //enable datepicker
    $( "#DueDate2" ).Zebra_DatePicker({
      format: 'd/m/Y'
    });
    //put values back into CakePHP input element
    $( "#TaskAddForm" ).submit(function() {
      var DueDate2 = $( "#DueDate2" ).val();
      DueDate2 = DueDate2.split('/');
      $( "#fecha2" ).val(DueDate2[2] + '-' + DueDate2[1] + '-' + DueDate2[0] + ' ' + DueTime + ':00');
    });

  });
</script>
<script type="text/javascript">
	var numero1 = new LiveValidation('numero1', {onlyOnSubmit: true } ); numero1.add( Validate.Presence); numero1.add( Validate.Numericality );
	var numero2 = new LiveValidation('numero2', {onlyOnSubmit: true } ); numero2.add( Validate.Presence); numero2.add( Validate.Numericality );
</script>
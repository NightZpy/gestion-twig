<?php
require_once("JavaBridge/java/Java.inc"); //Incluimos la libreria JavaBridge
$dir ="C:\\xampp\\htdocs\\reportes\\"; //Especificamos la dirección donde guardamos el reporte
$informe = $reporte; //Especificamos el nombre del reporte
//$informe="paterbeni"; //Especificamos el nombre del reporte
$jrDirLib = "C:\\xampp\\htdocs\\lib_reportes\\"; //Describimos la dirección de las librerías
$handle = @opendir($jrDirLib); //Creamos una variable que va a contener todas las librerías que están en la carpeta lib_reportes
$classpath="";
while(($lib = readdir($handle)) !== false) {
	$classpath .= 'file:'.$jrDirLib.'/'.$lib .';';
}
try {
	java_require($classpath); //Añadimos las librerías
	//Creamos la conexión JDBC
	$Conn = new Java("org.altic.jasperReports.JdbcConnection");
	//Seteamos el driver mysql
	$Conn->setDriver("com.mysql.jdbc.Driver");
	//Especificamos los datos de la conexión
	$Conn->setConnectString("jdbc:mysql://localhost/gestion");
	$Conn->setUser("root");
	$Conn->setPassword("");
	/*Creamos una variable tipo arreglo que contendrá los parámetros En este caso el parametro se llama PARAMETRO_EJEMPLO, y el valor es un String */
	$parametrosMap = new Java("java.util.HashMap");
	$parametrosMap->put("numero_factura", new Java('java.lang.Integer',  $numero_factura ));
	$parametrosMap->put("lugar", new Java('java.lang.String', "bqto" ));
	//Creamos el objeto JasperReport que permite obtener el reporte
	$sJfm = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
	//Guardamos el reporte en una variable $print para luego exportarla
	$print = $sJfm->fillReportToFile( $dir .$informe.".jasper", $parametrosMap, $Conn->getConnection());
	//Exportamos el informe y lo guardamos como pdf en el directorio donde están los reportes
	$sJem = new JavaClass("net.sf.jasperreports.engine.JasperExportManager");
	$sJem->exportReportToPdfFile($print, $dir .$informe.".pdf");
	//Este código sirve para abrir el archivo generado desde el explorador
	if (file_exists($dir .$informe.".pdf")){
		header("Content-disposition: attachment; filename=".$informe.".pdf");
		header("Content-Type: application/pdf");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ". @filesize($dir .$informe.".pdf"));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Expires: 0"); set_time_limit(0);
		@readfile($dir .$informe.".pdf") or die("problem occurs.");
	}
} catch(JavaException $ex){
	echo "error ";
	$trace = new Java("java.io.ByteArrayOutputStream");
	$ex->printStackTrace(new Java("java.io.PrintStream", $trace));
	print "java stack trace: $trace\n";
}
?>
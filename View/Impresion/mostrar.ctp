<?php //echo print_r($personas); ?>
<form action="/gestion-facturas-c/impresion/imprimir" method="post">
	<fieldset>
		<legend>Impresi&oacute;n de facturas</legend>
		<label>Cliente</label>
		<select name="reporte" id="reporte">
			<option>Seleccione...</option>
			<?php for($i=0; $i<count($personas); $i++){
				echo '<option value="'.$personas[$i]['persona']['archivo_reporte'].'">'.$personas[$i]['persona']['razon_social'].'</option>';
			} ?>
		</select>
		<label>N&uacute;mero de factura</label>
		<input name="numero_factura" id="numero_factura" size="30" />
	<hr />
	<button id="imprimir" name="imprimir" type="submit">Imprimir</button>
	</fieldset>
</form>
<script type="text/javascript">
	var numero1 = new LiveValidation('numero_factura', {onlyOnSubmit: true } ); numero1.add( Validate.Presence); numero1.add( Validate.Numericality );
</script>
<?php
require_once(ROOT.DS.'app\Vendor'.DS.'JavaBridgeLibs\java\Java.inc');//Incluimos la libreria JavaBridge
$jrDirLib = APP.'Vendor'.DS.'JasperReportsLibs'.DS;
$dir = APP.'webroot'.DS.'Reportes'.DS;
$informe = $proveedor_id;
$handle = @opendir($jrDirLib);//Creamos una variable que va a contener todas las librerías que están en la carpeta lib_reportes
$classpath="";
while(($lib = readdir($handle)) !== false) {
$classpath .= 'file:'.$jrDirLib.'/'.$lib .';';
}
try {
	java_require($classpath); //Añadimos las librerías
	//Creamos la conexión JDBC
	$Conn = new Java("org.altic.jasperReports.JdbcConnection");
	//Seteamos el driver mysql
	$Conn->setDriver("com.mysql.jdbc.Driver");
	//Especificamos los datos de la conexión
	$Conn->setConnectString("jdbc:mysql://localhost/gestion");
	$Conn->setUser("root");
	$Conn->setPassword("");
	/*Creamos una variable tipo arreglo que contendrá los parámetros En este caso el parametro se llama PARAMETRO_EJEMPLO, y el valor es un String */
	$parametrosMap = new Java("java.util.HashMap");
	$parametrosMap->put("numero_factura", new Java('java.lang.Integer', $numero_factura ));
	$parametrosMap->put("lugar", new Java('java.lang.String', "bqto" ));
	//Creamos el objeto JasperReport que permite obtener el reporte
	$sJfm = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
	//Guardamos el reporte en una variable $print para luego exportarla
	$print = $sJfm->fillReportToFile( $dir.$informe.".jasper", $parametrosMap, $Conn->getConnection());
	//Exportamos el informe y lo guardamos como pdf en el directorio donde están los reportes
	$view = new JavaClass("net.sf.jasperreports.view.JasperViewer"); //
	$view->viewReport($print, false);
}catch(JavaException $ex){
	echo "error ";
	$trace = new Java("java.io.ByteArrayOutputStream");
	$ex->printStackTrace(new Java("java.io.PrintStream", $trace));
	print "java stack trace: $trace\n";
}

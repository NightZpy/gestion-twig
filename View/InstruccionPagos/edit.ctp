<?php 
$this->Html->script('greybox/AJS.js', array('inline' => false));
$this->Html->script('greybox/gb_scripts.js', array('inline' => false));
$this->Html->css('greybox/gb_styles.css', null, array('inline' => false));
$this->Html->script('cargaClientes.js', array('inline' => false));
?>
	<fieldset>
		<legend><?php echo 'Requerimiento'; ?></legend>
        <?php
        echo $this->Form->input('id', array('label' => 'Requerimiento Nro.', 'readonly' => 'readonly', 'type' => 'text', 'value' => $requerimiento['Requerimiento']['id'], 'class' => 'Montos'));
        echo $this->Form->input('asociado_id', array('type' => 'text', 'readonly' => 'readonly','value' => $requerimiento['Asociado']['razon_social']));
        echo $this->Form->input('cliente_id', array('type' => 'text', 'readonly' => 'readonly', 'value' => $requerimiento['Cliente']['razon_social']));
        ?>
	</fieldset>
    <br>
    <?php
    if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'InstruccionPagos', 'add'))) {    
    ?>
        <div id="agregarInstruccionPagos" class="btn">Agregar Instruccion de Pago</div>
    <?php
    }
    ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-check"></th>
            <th class="table-header-repeat line-left minwidth-1"><l><?php echo 'Proveedor Final'; ?></l></th>
            <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Monto'; ?></l></th>
            <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Estatus'; ?></l></th>
            <th class="table-header-repeat line-left"><l>Opciones</l></th>
        </tr>
        <?php
        foreach ($instrucciones as $instruccion): 
        ?>
        <tr class="registros">
            <td></td>
            <td><?php echo h($instruccion['Proveedor']['razon_social']); ?></td>
            <input type="hidden" id="proveedor_id<?= $instruccion['InstruccionPago']['id'];?>" value="<?= $instruccion['InstruccionPago']['proveedor_id'];?>">
            <td class="Right"><?php echo h(CakeNumber::currency($instruccion['InstruccionPago']['monto'], 'VEF')); ?></td>        
            <td class="Right"><?php echo h($instruccion['Estatus']['nombre']); ?></td>
            <td class="options-width">
                <?php
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'InstruccionPagos', 'agregarPago'))) {
                    echo $this->Html->link('', "javascript:agregarPagos(" . $instruccion['InstruccionPago']['id'] . ")",
                                          array(
                                               'class' => 'icon-13 info-tooltip', 
                                               'title'=>'Agregar Pago'
                                               )
                                     ); 
                }
                if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'InstruccionPagos', 'delete'))) {
                    echo $this->Form->postLink('', array(
                                                 'controller' => 'instruccionPagos',
                                                 'action' => 'delete', 
                                                 $instruccion['InstruccionPago']['id'],
                                                 $requerimiento['Requerimiento']['id']
                                              ), 
                                              array(
                                                 'class' => 'icon-2 info-tooltip', 
                                                 'title'=>'Eliminar'
                                              ), 
                                              'Esta seguro de eliminar este registro?'                                                  
                                         );
                }
                ?>
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
    </table>
        
<div style = "display:none;" id = "dialogForm2">
    <div clas = "seccion-izquierda">
        <?php
            echo $this->Form->create('InstruccionPago2', array('url' => array('controller' => 'InstruccionPagos', 'action' => 'add', $requerimiento['Requerimiento']['id'])));
            echo $this->Form->input('requerimiento_id', array('type' => 'hidden', 'value' => $requerimiento['Requerimiento']['id'])); 
            echo $this->Form->input('id', array('type' => 'hidden', 'value' => '')); 
            
            echo $this->Form->input('proveedor_id');
            echo $this->Form->input('monto', array('class' => 'Montos', 'readonly' => 'readonly','value' => $montoReintegro));
            echo $this->Form->input('estatus_instruccion_pago_id', array('value' => 1, 'type' => 'hidden'));
        ?>
        </form>
    </div>
</div>
        
<div style = "display:none;" id = "dialogForm">
    <div clas = "seccion-izquierda">
        <?php
            echo $this->Form->create('InstruccionPago', array('url' => array('controller' => 'InstruccionPagos', 'action' => 'agregarPago', $requerimiento['Requerimiento']['id'])));
            echo $this->Form->input('requerimiento_id', array('type' => 'hidden', 'value' => $requerimiento['Requerimiento']['id'])); 
            echo $this->Form->input('id', array('type' => 'hidden', 'value' => '')); 
            echo $this->Form->input('proveedor_id', array('type' => 'hidden'));
            
            echo $this->Form->input('tipo_instrumento_id');
            echo $this->Form->input('fecha_display', array('label'=>'Fecha', 'readonly' => 'readonly', 'type' =>'text', 'id' => 'fecha_display', 'class' => 'Montos'));
            echo $this->Form->input('fecha', array('type' =>'text', 'id' => 'fecha', 'type'=>'hidden'));
            echo $this->Form->input('banco_origen_id', array('empty' => 'Seleccione'));
            echo $this->Form->input('numero_referencia');
            echo $this->Form->input('banco_destino_id', array('empty' => 'Seleccione'));
            echo $this->Form->input('cuenta_destino_id', array('empty' => 'Seleccione'));
            echo $this->Form->input('estatus_instruccion_pago_id', array('value' => 2, 'type' => 'hidden'));
        ?>
        </form>
    </div>
</div>
        
<script>
function agregarPagos(id){
    $("#fecha_display" ).datepicker({dateFormat: 'dd-mm-yy', altFormat:'yy-mm-dd', altField:'#fecha'});
    $('#InstruccionPagoId').val(id)
    $('#InstruccionPagoProveedorId').val($('#proveedor_id'+id).val())
    $('#dialogForm').dialog({
        'title':'Editar Instruccion de Pago',
        'modal':true,
        'width':'700px',
        buttons:[{
                'text':'guardar', click:function(){
                    //$('#DetalleFacturaDescripcionId').val();
                    $('#InstruccionPagoEditForm').submit()
                }
            }]
    });
}

$('#agregarInstruccionPagos').bind('click',function(){
<?php
if (!isset($montoFacturaDisponibles)) {
?>
    alert("No existen facturas pagadas/abonadas para este requerimiento.");
    return false;
<?php
}
if (($montoReintegro == null) || ($montoReintegro == 0)) {
?>
    alert("El monto de reintegro no esta disponible para este requerimiento.");
    return false;
<?php
}
?>
$('#dialogForm2').dialog({
        'title':'Agregando Instruccion de Pago',
        'modal':true,
        'width':'700px',
        buttons:[{
                'text':'Guardar', click:function(){
                    $("#InstruccionPago2EditForm").submit();
                }
            }]
    });
    return false;
});

	$(function(){
		$('#InstruccionPagoBancoDestinoId').bind('change', function(){
            if ($('#InstruccionPagoBancoDestinoId').val() != ""){
                //alert($('#InstruccionPagoBancoDestinoId').val());
                $.ajax({
                    url: "/instruccionPagos/getCuentasBancarias",
                    data: $('#InstruccionPagoEditForm').serialize(),
                    type: 'post',
                    success: function(data){
                        $('#InstruccionPagoCuentaDestinoId').html(data);
                        if (data==""){
                            alert("El proveedor no tiene cuenta bancaria asociada en este banco")
                            $('#InstruccionPagoCuentaDestinoId').html("<option value=''>Seleccione</option>");
                        }
                    }
                });
            } else {
                $('#InstruccionPagoCuentaDestinoId').html("<option value=''>Seleccione</option>");
            }
		})
	})
</script>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
	width:280px;
}
</style>

<script>
	$(function(){
		$('.btn').button();
    })
</script>

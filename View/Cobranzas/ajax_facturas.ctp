
<?php foreach ($facturas as $factura): ?>
    <tr>
        <?php if ($factura[0]['abonos']>0){
            echo '<td><input id="id-'.$factura['f']['id'].'" type="checkbox" checked="checked" class="resetFactura"/></td>';
        }else{
            echo '<td><input id="id-'.$factura['f']['id'].'" type="checkbox"  class="abonaFactura"/></td>';
        }
        ?>
        <td style="text-align: center;"><?php echo $factura['f']['fecha_facturacion'] ?></td>
        <td style="text-align: center;"><?php echo $factura['f']['numero_factura'] ?></td>
        <td class="td-number"><?php echo CakeNumber::currency($factura['f']['monto_total'], 'VEF') ?></td>
        <td class="td-number"><?php echo CakeNumber::currency($factura['f']['monto_maximo'], 'VEF') ?></td>
        <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonos'], 'VEF') ?></td>
        <td>
            <?php 
            if (intval($factura[0]['abonos'])>0){
                echo $this->Form->input('CobranzaImpuestoIsrlIdModif'.$factura['f']['id'], array('name'=>'impuesto_islr_id_modif',  'label'=>false, 'options'=>$impuestos, 'class'=>'editable', 'disabled'=>'disabled', 'default'=>$factura['f']['impuesto_islr_id_modif']));
            }else{
                echo $this->Form->input('CobranzaImpuestoIsrlIdModif'.$factura['f']['id'], array('name'=>'impuesto_islr_id_modif',  'label'=>false, 'options'=>$impuestos, 'class'=>'editable', 'default'=>$factura['f']['impuesto_islr_id_modif']));
            }

            ?>
        </td>
        <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonosislr'], 'VEF') ?></td>
        <td>
            <?php 
            if (intval($factura[0]['abonos'])>0){
                echo $this->Form->input('CobranzaRetencionIvaIdModif'.$factura['f']['id'], array('name'=>'retencion_iva_id_modif', 'class'=>'editable', 'disabled'=>'disabled', 'options'=>$retenciones,'default'=>$factura['f']['retencion_iva_id_modif'], 'label'=>false));
            }else{
                echo $this->Form->input('CobranzaRetencionIvaIdModif'.$factura['f']['id'], array('name'=>'retencion_iva_id_modif', 'class'=>'editable', 'options'=>$retenciones,'default'=>$factura['f']['retencion_iva_id_modif'], 'label'=>false));
            }
            ?>
        </td>
        <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonosiva'], 'VEF') ?></td>
    </tr>
<?php endforeach; ?>


<script>
    $('.abonaFactura').bind('click', function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        id = id.substring(3,id.length);
        $.ajax({
            url:'/cobranzas/ajaxAbonaFactura/'+id,
            type:'post',
            data:'data[Cobranza][id]='+$('#CobranzaId').val()
        }).done(function(data){
            var obj = jQuery.parseJSON(data)
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
        });
    })
    
    $('.resetFactura').bind('click', function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        id = id.substring(3,id.length);
        $.ajax({
            url:'/cobranzas/ajaxResetFactura/'+id,
            type:'post',
            data:'data[Cobranza][id]='+$('#CobranzaId').val()
        }).done(function(data){
            var obj = jQuery.parseJSON(data)
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
            
        });
    })
    
    $(function(){
        $('.editable').bind('change', function(e){
            e.preventDefault();

            var impuestoIslrId=$(this).attr('id');
            
            impuestoIslrId=impuestoIslrId.substring(27, impuestoIslrId.length)
            
            var valor = $(this).val()
            var campo = $(this).attr('name')


            $.ajax({
                url:'/cobranzas/ajaxEditFactura',
                type:'post',
                data:'id='+impuestoIslrId+'&valor='+valor+'&campo='+campo
            }).done(function(){

            })
        })

    })
</script>
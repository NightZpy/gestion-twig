<div>
    <fieldset>
        <legend>Filtros</legend>
        <?php
        echo $this->Form->create();
        echo $this->Form->input('asociado_id', array('empty'=>'Seleccione...', 'style'=>'width:500px;'));
        echo $this->Form->input('proveedor_id', array('empty'=>'Seleccione...', 'style'=>'width:500px;'));
        echo $this->Form->input('cliente_id', array('empty'=>'Seleccione...', 'style'=>'width:500px;'));
        echo $this->Form->end('submit');
        ?>

    </fieldset>

    <table id="product-table" style="width:100%;">
        <thead>
            <tr>
                <th class="table-header-check"><a>Id</a></th>
                <th class="table-header-repeat"><a><?php echo $this->Paginator->sort('numero'); ?></a></th>
                <th class="table-header-repeat" style="width:300px;"><a><?php echo $this->Paginator->sort('Asociados.razon_social', 'Asociados'); ?></a></th>
                <th class="table-header-repeat" style="width:300px;"><a><?php echo $this->Paginator->sort('Proveedores.razon_social', 'Proveedores'); ?></a></th>
                <th class="table-header-repeat" style="width:300px;"><a><?php echo $this->Paginator->sort('Clientes.razon_social', 'Clientes'); ?></a></th>
                <th class="table-header-options" style="width:40px;"><a>Acciones</a></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cobranzas as $cobranza): ?>
                <tr>
                    <td><?php echo $cobranza['Cobranza']['id'] ?></td>
                    <td class="td-number"><?php echo $cobranza['Cobranza']['numero'] ?></td>
                    <td><?php echo $cobranza['Asociados']['razon_social'] ?></td>
                    <td><?php echo $cobranza['Proveedores']['razon_social'] ?></td>
                    <td><?php echo $cobranza['Clientes']['razon_social'] ?></td>
                    <td>
                        <?php echo $this->Html->link('', array('action' => 'edit', $cobranza['Cobranza']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
                    </td>
                </tr>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot><tr><td colspan="6"><?php echo $this->element('paginate'); ?></td></tr></tfoot>
    </table>
</div>
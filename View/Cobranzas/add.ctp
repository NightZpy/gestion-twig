<div id="addCobranza">
<?php $this->Html->script('cobranzasAdd.js', array('inline' => false)); ?>
    <fieldset>
        <?php
        echo $this->Form->create();
        echo $this->Form->input('id', array('type' => 'hidden', 'style' => 'width:500px;'));
        echo $this->Form->input('fecha', array('type' => 'hidden', 'style' => 'width:500px;'));
        echo $this->Form->input('asociado_id', array('empty' => '', 'style' => 'width:500px;'));
        echo $this->Form->input('cliente_id', array('empty' => '', 'style' => 'width:500px;'));
        echo $this->Form->input('proveedor_id', array('empty' => '', 'style' => 'width:500px;'));
        ?>
    </fieldset>
    <div style="margin-top: 20px;">
        <fieldset>
            <legend>Instrumentos de Pago</legend>
            <div id="divInstrumentos" style="width:100%;">
                <table id="product-table" style="width:100%;">
                    <thead>
                        <tr>
                            <th class="table-header-repeat"><a>Tipo</a></th>
                            <th class="table-header-repeat" style="width:120px;"><a>Número de <br>operación</a></th>
                            <th class="table-header-repeat"><a>Banco <br>Origen </a></th>
                            <th class="table-header-repeat"><a>Numero de <br>Cuenta</a></th>
                            <th class="table-header-repeat"><a>Monto</a></th>
                        </tr>
                    </thead>
                    <tbody>
                <?php echo $this->element('gridInstrumentos') ?>
                        <tr>
                            <td ><?php echo $this->Form->input('Instrumento.tipo_instrumento_id', array('empty' => 'Seleccione...', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.nro_referencia', array('div' => false, 'style' => 'width:110px;text-align:right;', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.banco_id', array('div' => false, 'label' => false, 'empty'=>'')); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.cuenta_id', array('div' => false, 'style' => 'width:310px;', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.monto', array('div' => false, 'style' => 'width:110px;text-align:right;', 'label' => false)); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
    
    <div style="margin-top: 20px;">
        <fieldset>
            <legend>Facturas</legend>
            <div id="divFacturas" style="width:100%;">
                <div style="margin-bottom: 10px;">
                    <fieldset>
                        <legend><h3 style="margin-bottom: 0;">Filtros</h3></legend>
                        <fieldset style="width:245px;">
                            <legend>Fecha de factura</legend>
                            <?php echo $this->Form->input('Factura.0.fecha_requerida', array('class'=>'fecha', 'div'=>array('style'=>'float:left;width:115px;clear:none;'), 'label'=>array('text'=>'desde', 'style'=>'display:block;float:none;')))?>
                            <?php echo $this->Form->input('Factura.1.fecha_requerida', array('class'=>'fecha', 'div'=>array('style'=>'float:left;width:115px;clear:none;'), 'label'=>array('text'=>'hasta', 'style'=>'display:block;float:none;')))?>
                            <?php echo $this->Form->input('Factura.0.fecha_requeridaH', array('type'=>'hidden'))?>
                            <?php echo $this->Form->input('Factura.1.fecha_requeridaH', array('type'=>'hidden'))?>
                        </fieldset>
                        
                        <button id="filtro">Filtrar</button>
                    </fieldset>
                </div>
                
                <table id="product-table" style="width:100%;margin-top: 0;">
                    <thead>
                        <tr>
                            <th class="table-header-check"></th>
                            <th class="table-header-repeat"><a>Fecha</a></th>
                            <th class="table-header-repeat"><a>Número</a></th>
                            <th class="table-header-repeat"><a>Total</a></th>
                            <th class="table-header-repeat"><a>Saldo</a></th>
                            <th class="table-header-repeat"><a>Abono</a></th>
                            <th class="table-header-repeat"><a>% ISLR</a></th>
                            <th class="table-header-repeat"><a>Abonado ISLR</a></th>
                            <th class="table-header-repeat"><a>% Retención IVA</a></th>
                            <th class="table-header-repeat"><a>Abonado Retencion IVA</a></th>
                        </tr>
                    </thead>
                    <tbody id="detalleFacturas">
                        
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
</div>

<div id="divFacturas">
    
</div>

<div id="script"></div>
<?php echo $this->element('gridInstrumentos');?>


<script>
    $('#InstrumentoTipoInstrumentoId').bind('change',function(e){
        $.ajax({
            url:'/cobranzas/getAjaxRequisitos',
            type:'post',
            data:$('#CobranzaEditForm').serialize()
        }).done(function(data){
            $('#script').html(data);
        });
    })
    
    
    $('.instelim').bind('click', function(e){
        e.preventDefault();
        var instIdStr = $(this).attr('id');
        var instId = instIdStr.substring(5,15);
        //alert(instId);
        
        $.ajax({
            url:'/cobranzas/ajaxEliminaInstrumento',
            type:'post',
            data:'instrumento_id='+instId
        }).done(function(data){
           var obj = jQuery.parseJSON(data)
           $('#tbodyEditCobranza').html(obj.body);
           $('#suma').html(obj.suma);
           $('#script').html(obj.script);
        }); 
    }) 
</script>
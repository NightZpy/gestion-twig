<div id="editCobranza">
<?php $this->Html->script('cobranzasEdit.js', array('inline' => false)); ?>
    <fieldset>
        <?php
        echo $this->Form->create();
        echo $this->Form->input('id', array('type' => 'hidden', 'style' => 'width:500px;'));
        echo $this->Form->input('fecha', array('type' => 'hidden', 'style' => 'width:500px;'));
        echo $this->Form->input('asociado_id', array('empty' => '', 'style' => 'width:500px;'));
        echo $this->Form->input('cliente_id', array('empty' => '', 'style' => 'width:500px;'));
        echo $this->Form->input('proveedor_id', array('empty' => '', 'style' => 'width:500px;'));
        ?>
    </fieldset>
    <div style="margin-top: 20px;">
        <fieldset>
            <legend>Instrumentos de Pago</legend>
            <div id="divInstrumentos" style="width:100%;">
                <table id="product-table" style="width:100%;margin-top: 0;">
                    <thead>
                        <tr>
                            <th class="table-header-repeat"><a>Tipo</a></th>
                            <th class="table-header-repeat" style="width:120px;"><a>Referencia</a></th>
                            <th class="table-header-repeat"><a>Banco Origen </a></th>
                            <th class="table-header-repeat"><a>Número de Cuenta</a></th>
                            <th class="table-header-repeat"><a>Monto</a></th>
                            <th class="table-header-options"><a>Acciones</a></th>
                        </tr>
                    </thead>
                    <tbody id="tbodyEditCobranza">
                <?php echo $this->element('gridInstrumentos') ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td ><?php echo $this->Form->input('Instrumento.tipo_instrumento_id', array('empty' => 'Seleccione...', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.nro_referencia', array('div' => false, 'style' => 'width:110px;text-align:right;', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.banco_id', array('div' => false, 'label' => false, 'empty'=>'')); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.cuenta_id', array('div' => false, 'style' => 'width:310px;', 'label' => false)); ?></td>
                            <td ><?php echo $this->Form->input('Instrumento.monto', array('div' => false, 'style' => 'width:110px;text-align:right;', 'label' => false)); ?></td>
                            <td style="border: none;"></td>
                        </tr>
                        <tr>
                            <td  style="border: none;" colspan="3"></td>
                            <td style="text-align: right;padding-right: 10px;border:none;">TOTAL RECIBIDO</td>
                            <td style="text-align: right;padding-right: 10px;" id="suma"><?php echo CakeNumber::currency($sumaInstrumentos, 'VEF')?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </fieldset>
    </div>
     <div style="margin-top: 20px;">
        <fieldset>
            <legend>Facturas</legend>
            <div id="divFacturas" style="width:100%;">
                <div style="margin-bottom: 10px;">
                    <fieldset>
                        <legend><h3 style="margin-bottom: 0;">Filtros</h3></legend>
                        <fieldset style="width:245px;">
                            <legend>Fecha de factura</legend>
                            <?php echo $this->Form->input('Factura.0.fecha_requerida', array('class'=>'fecha', 'div'=>array('style'=>'float:left;width:115px;clear:none;'), 'label'=>array('text'=>'desde', 'style'=>'display:block;float:none;')))?>
                            <?php echo $this->Form->input('Factura.1.fecha_requerida', array('class'=>'fecha', 'div'=>array('style'=>'float:left;width:115px;clear:none;'), 'label'=>array('text'=>'hasta', 'style'=>'display:block;float:none;')))?>
                            <?php echo $this->Form->input('Factura.0.fecha_requeridaH', array('type'=>'hidden'))?>
                            <?php echo $this->Form->input('Factura.1.fecha_requeridaH', array('type'=>'hidden'))?>
                        </fieldset>
                        
                        <button id="filtro">Filtrar</button>
                    </fieldset>
                </div>
                
                <table id="product-table" style="width:100%;margin-top: 0;">
                    <thead>
                        <tr>
                            <th class="table-header-check"></th>
                            <th class="table-header-repeat"><a>Fecha</a></th>
                            <th class="table-header-repeat"><a>Número</a></th>
                            <th class="table-header-repeat"><a>Total</a></th>
                            <th class="table-header-repeat"><a>Saldo</a></th>
                            <th class="table-header-repeat"><a>Abono</a></th>
                            <th class="table-header-repeat"><a>% ISLR</a></th>
                            <th class="table-header-repeat"><a>Abonado ISLR</a></th>
                            <th class="table-header-repeat"><a>% Retención IVA</a></th>
                            <th class="table-header-repeat"><a>Abonado Retencion IVA</a></th>
                        </tr>
                    </thead>
                    <tbody id="detalleFacturas">
                        <?php foreach ($facturas as $factura):?>
                        <tr>
                            <?php if (intval($factura[0]['abonos'])>0){
                                echo '<td><input id="id-'.$factura['f']['id'].'" type="checkbox" checked="checked" class="resetFactura"/></td>';
                            }else{
                                echo '<td><input id="id-'.$factura['f']['id'].'" type="checkbox" class="abonaFactura"/></td>';
                            }
                            ?>
                            <td style="text-align: center;"><?php echo $factura['f']['fecha_facturacion']?></td>
                            <td style="text-align: center;"><?php echo $factura['f']['numero_factura']?></td>
                            <td class="td-number"><?php echo CakeNumber::currency($factura['f']['monto_total'],'VEF')?></td>
                            <td class="td-number"><?php echo CakeNumber::currency($factura['f']['monto_maximo'],'VEF')?></td>
                            <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonos'],'VEF')?></td>
                            <td>
                                <?php 
                                if (intval($factura[0]['abonos'])>0){
                                    echo $this->Form->input('impuesto_islr_id_modif'.$factura['f']['id'], array('name'=>'impuesto_islr_id_modif',  'label'=>false, 'options'=>$impuestos, 'class'=>'editable', 'disabled'=>'disabled', 'default'=>$factura['f']['impuesto_islr_id_modif']));
                                }else{
                                    echo $this->Form->input('impuesto_islr_id_modif'.$factura['f']['id'], array('name'=>'impuesto_islr_id_modif',  'label'=>false, 'options'=>$impuestos, 'class'=>'editable', 'default'=>$factura['f']['impuesto_islr_id_modif']));
                                }
                                       
                                ?>
                            </td>
                            <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonosislr'],'VEF')?></td>
                            <td>
                                <?php 
                                if (intval($factura[0]['abonos'])>0){
                                    echo $this->Form->input('retencion_iva_id_modif'.$factura['f']['id'], array('name'=>'retencion_iva_id_modif', 'class'=>'editable', 'disabled'=>'disabled', 'options'=>$retenciones,'default'=>$factura['f']['retencion_iva_id_modif'], 'label'=>false));
                                }else{
                                    echo $this->Form->input('retencion_iva_id_modif'.$factura['f']['id'], array('name'=>'retencion_iva_id_modif', 'class'=>'editable', 'options'=>$retenciones,'default'=>$factura['f']['retencion_iva_id_modif'], 'label'=>false));
                                }
                                ?>
                            </td>
                                
                                
                                
                            
                            <td class="td-number"><?php echo CakeNumber::currency($factura[0]['abonosiva'],'VEF')?></td>   
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                    <tfoot>
                        <tr><td colspan="5" style="border:none;text-align: right;padding-right: 10px;">TOTAL ABONADO</td><td id="sumaAbonos" class="td-number"><?php echo CakeNumber::currency($sumaAbonos, 'VEF');?></td><td colspan="4" style="border:none;"></td></tr>
                        
                    </tfoot>
                </table>
            </div>
        </fieldset>
    </div>
</div>

<div id="script">
    
</div>

<script>
    $(function(){
        $('.editable').bind('change', function(e){
            e.preventDefault();
            
            var impuestoIslrId=$(this).attr('id');
            impuestoIslrId=impuestoIslrId.substring(27, impuestoIslrId.length)
            var valor = $(this).val()
            var campo = $(this).attr('name')
            
            
            $.ajax({
                url:'/cobranzas/ajaxEditFactura',
                type:'post',
                data:'id='+impuestoIslrId+'&valor='+valor+'&campo='+campo
            }).done(function(){
                
            })
        })
    })
</script>
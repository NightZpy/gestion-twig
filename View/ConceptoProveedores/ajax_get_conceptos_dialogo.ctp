<div id="divTodos">
    <a id="btnFrecuentes" class="classbutton">Mostrar conceptos usados</a>

    <table id="product-table" style="width:100%">
        <thead>
            <tr><th class="table-header-check"></th><th class="table-header-repeat">Concepto</th></tr>
        </thead>
        <tbody>

    <?php foreach ($conceptos as $concepto):?>
            <tr>
                <td><a href="#" class="icon-14 selector" id="id<?php echo $concepto['conceptos']['id'] ?>"></a></td>
                <td><?php echo $concepto['conceptos']['nombre']?><input type="hidden" id="descr<?php echo $concepto['conceptos']['id']?>" value="<?php echo $concepto['conceptos']['nombre']?>"></td></tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="divFrecuentes">
    <a id="btnTodos" class="classbutton">Mostrar todos los conceptos</a>
    
    <table id="product-table" style="width:100%">
        <thead>
            <tr><th class="table-header-check"></th><th class="table-header-repeat">Concepto</th></tr>
        </thead>
        <tbody>

    <?php foreach ($conceptos2 as $concepto):?>
            <tr>
                <td><a href="#" class="icon-14 selector" id="id<?php echo $concepto['conceptos']['id'] ?>"></a></td>
                <td><?php echo $concepto['conceptos']['nombre']?><input type="hidden" id="descr<?php echo $concepto['conceptos']['id']?>" value="<?php echo $concepto['conceptos']['nombre']?>"></td></tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    
</div>

<script>
    $(function(){
        $('#divTodos').hide();
        $('.classbutton').button();
        $('.selector').bind('click', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            id = id.substring(2, id.length)
            var descripcion = $('#descr'+id).val();
            var detalleFacturaId=<?php echo $detalleFacturaId?>;
            if(typeof <?php echo $callback?> == 'function'){
                <?php echo $callback?>(id, descripcion, detalleFacturaId);
            }
        });
        
        $('#btnTodos').bind('click', function(e){
            e.preventDefault();
            $('#divTodos').show();
            $('#divFrecuentes').hide();
        });
        
        $('#btnFrecuentes').bind('click', function(e){
            e.preventDefault();
            $('#divTodos').hide();
            $('#divFrecuentes').show();
        });
        
    })
</script>

<div class="conceptoProveedores form">
<?php echo $this->Form->create('ConceptoProveedor'); ?>
	<fieldset>
		<legend><?php echo __('Edit Concepto Proveedor'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('proveedor_id');
		echo $this->Form->input('concepto_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ConceptoProveedor.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ConceptoProveedor.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Concepto Proveedores'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Conceptos'), array('controller' => 'conceptos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Concepto'), array('controller' => 'conceptos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Personas'), array('controller' => 'personas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Persona'), array('controller' => 'personas', 'action' => 'add')); ?> </li>
	</ul>
</div>

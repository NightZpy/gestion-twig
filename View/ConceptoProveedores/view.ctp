<div class="conceptoProveedores view">
<h2><?php  echo __('Concepto Proveedor'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($conceptoProveedor['ConceptoProveedor']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proveedor Id'); ?></dt>
		<dd>
			<?php echo h($conceptoProveedor['ConceptoProveedor']['proveedor_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Concepto'); ?></dt>
		<dd>
			<?php echo $this->Html->link($conceptoProveedor['Concepto']['nombreconcepto'], array('controller' => 'conceptos', 'action' => 'view', $conceptoProveedor['Concepto']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Concepto Proveedor'), array('action' => 'edit', $conceptoProveedor['ConceptoProveedor']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Concepto Proveedor'), array('action' => 'delete', $conceptoProveedor['ConceptoProveedor']['id']), null, __('Are you sure you want to delete # %s?', $conceptoProveedor['ConceptoProveedor']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Concepto Proveedores'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Concepto Proveedor'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conceptos'), array('controller' => 'conceptos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Concepto'), array('controller' => 'conceptos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Personas'), array('controller' => 'personas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Persona'), array('controller' => 'personas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Personas'); ?></h3>
	<?php if (!empty($conceptoProveedor['Persona'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Codigo'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Apellido'); ?></th>
		<th><?php echo __('Razon Social'); ?></th>
		<th><?php echo __('Tipo Persona Id'); ?></th>
		<th><?php echo __('Clasificacion Id'); ?></th>
		<th><?php echo __('Telefono'); ?></th>
		<th><?php echo __('Direccion'); ?></th>
		<th><?php echo __('Tipo Documento Id'); ?></th>
		<th><?php echo __('Documento'); ?></th>
		<th><?php echo __('Contribuyente Especial'); ?></th>
		<th><?php echo __('Porcentaje Retencion Iva'); ?></th>
		<th><?php echo __('Porcentaje Costo Gestion'); ?></th>
		<th><?php echo __('Porcentaje Costo Operativo'); ?></th>
		<th><?php echo __('Correo Electronico'); ?></th>
		<th><?php echo __('Usuario'); ?></th>
		<th><?php echo __('Clave'); ?></th>
		<th><?php echo __('Es Usuario'); ?></th>
		<th><?php echo __('Estado Id'); ?></th>
		<th><?php echo __('Ciudad Id'); ?></th>
		<th><?php echo __('Pagina Web'); ?></th>
		<th><?php echo __('Monto Costo Venta'); ?></th>
		<th><?php echo __('Monto Costo Venta Secundario'); ?></th>
		<th><?php echo __('Monto Costo Administrativo'); ?></th>
		<th><?php echo __('Porcentaje Costo Venta'); ?></th>
		<th><?php echo __('Porcentaje Costo Venta Secundario'); ?></th>
		<th><?php echo __('Porcentaje Costo Administrativo'); ?></th>
		<th><?php echo __('Validado'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($conceptoProveedor['Persona'] as $persona): ?>
		<tr>
			<td><?php echo $persona['id']; ?></td>
			<td><?php echo $persona['codigo']; ?></td>
			<td><?php echo $persona['nombre']; ?></td>
			<td><?php echo $persona['apellido']; ?></td>
			<td><?php echo $persona['razon_social']; ?></td>
			<td><?php echo $persona['tipo_persona_id']; ?></td>
			<td><?php echo $persona['clasificacion_id']; ?></td>
			<td><?php echo $persona['telefono']; ?></td>
			<td><?php echo $persona['direccion']; ?></td>
			<td><?php echo $persona['tipo_documento_id']; ?></td>
			<td><?php echo $persona['documento']; ?></td>
			<td><?php echo $persona['contribuyente_especial']; ?></td>
			<td><?php echo $persona['porcentaje_retencion_iva']; ?></td>
			<td><?php echo $persona['porcentaje_costo_gestion']; ?></td>
			<td><?php echo $persona['porcentaje_costo_operativo']; ?></td>
			<td><?php echo $persona['correo_electronico']; ?></td>
			<td><?php echo $persona['usuario']; ?></td>
			<td><?php echo $persona['clave']; ?></td>
			<td><?php echo $persona['es_usuario']; ?></td>
			<td><?php echo $persona['estado_id']; ?></td>
			<td><?php echo $persona['ciudad_id']; ?></td>
			<td><?php echo $persona['pagina_web']; ?></td>
			<td><?php echo $persona['monto_costo_venta']; ?></td>
			<td><?php echo $persona['monto_costo_venta_secundario']; ?></td>
			<td><?php echo $persona['monto_costo_administrativo']; ?></td>
			<td><?php echo $persona['porcentaje_costo_venta']; ?></td>
			<td><?php echo $persona['porcentaje_costo_venta_secundario']; ?></td>
			<td><?php echo $persona['porcentaje_costo_administrativo']; ?></td>
			<td><?php echo $persona['validado']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'personas', 'action' => 'view', $persona['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'personas', 'action' => 'edit', $persona['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'personas', 'action' => 'delete', $persona['id']), null, __('Are you sure you want to delete # %s?', $persona['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Persona'), array('controller' => 'personas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

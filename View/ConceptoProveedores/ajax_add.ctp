<?php
    echo $this->Form->create();
    echo $this->Form->input('proveedor_id', array('type'=>'hidden', 'value'=>$proveedorId));
    echo $this->Form->input('impuesto_id', array('label' => 'I.S.L.R'));
    echo $this->Form->input('descripcion', array('type' => 'textarea', 'label' => 'Descripción del concepto'));
    echo $this->Html->link('Guardar', array(), array('id'=>'btnAgregarConcepto', 'title'=>'Agregar'));
?>

<script>
    $(function(){
        $('#btnAgregarConcepto').button();
        
        
            $('#btnAgregarConcepto').bind('click', function(e){
                e.preventDefault();
                if ($('#ConceptoProveedorDescripcion').val() != ''){
                    $.ajax({
                        url:'/ConceptoProveedores/ajaxAdd',
                        type:'post',
                        data:$('#ConceptoProveedorAjaxAddForm').serialize()
                    }).done(function(data){
                        if(typeof addAjax == 'function'){
                             addAjax(data);
                         }
                    })
                }else{
                    alert('Debe escribir una descripción para el concepto');
                }
                
            })
        
        
    })
    
    
    
</script>
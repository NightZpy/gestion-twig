	<h1><?php echo __('Tipo Documentos'); ?></h1>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <tr>
            <th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre_corto'); ?></th>
			<th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php
        foreach ($tipoDocumentos as $tipoDocumento): ?>
        <tr class="registros">
            <td></td>
            <td><?php echo h($tipoDocumento['TipoDocumento']['nombre']); ?>&nbsp;</td>
            <td><?php echo h($tipoDocumento['TipoDocumento']['nombre_corto']); ?>&nbsp;</td>
            <td class="options-width">
                <?php echo $this->Html->link('', array('action' => 'view', $tipoDocumento['TipoDocumento']['id']), array('class' => 'icon-5 info-tooltip', 'title'=>'Ver')); ?>
                <?php echo $this->Html->link('', array('action' => 'edit', $tipoDocumento['TipoDocumento']['id']), array('class' => 'icon-1 info-tooltip', 'title'=>'Editar')); ?>
                <?php echo $this->Form->postLink('', array('action' => 'delete', $tipoDocumento['TipoDocumento']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $tipoDocumento['TipoDocumento']['id'])); ?>
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>

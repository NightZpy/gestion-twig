<h1><?php  echo __('Tipo Documento'); ?></h1>
	<dl>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($tipoDocumento['TipoDocumento']['nombre']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Nombre Corto'); ?></dt>
		<dd>
			<?php echo h($tipoDocumento['TipoDocumento']['nombre_corto']); ?>
			&nbsp;
		</dd>
	</dl>
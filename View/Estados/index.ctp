<h1>Estados</h1>
<br />
<fieldset>
    <legend>Filtrar</legend>
    <?php
    echo $this->Form->create('Estado', array('url' => array_merge(array('action' => 'index'), $this->params['pass'])));
    echo $this->Form->input('nombre', array('div' => false));
    echo $this->Form->submit('Filtrar', array('div' => false, 'class'=>'form-buscar'));
    echo $this->Form->end();
    ?>
</fieldset>
<br />
<p><a href="estados/agregar" class="btn">Agregar Registro</a></p>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <tr>
        <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('nombre'); ?></th>
        <th class="table-header-options line-left"><a href="">Opciones</a></th>
    </tr>
    <?php foreach ($estados as $estado): ?>
    <tr class="registros">
        <td nowrap><?= $estado['Estado']['nombre']; ?></td>
        <td class="options-width">
            <?php echo $this->Html->link('', array('action' => 'editar', $estado['Estado']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
            <?php echo $this->Form->postLink('', array('action' => 'eliminar', $estado['Estado']['id']), array('class' => 'icon-17 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $estado['Estado']['id'])); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?= $this->element('paginate'); ?>

<script>
	$('.btn').button();
</script>
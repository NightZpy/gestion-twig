<?php
$this->Html->script('/js/cargaISLR.js', array('inline' => false));
$this->Html->script('cargaClientes.js', array('inline' => false));
$this->html->script('/js/agregarDetalleFacturas.js', array('inline' => false));
?>
<?php
if (isset($id)) {
    echo $this->Html->link('Ir al requerimiento', array('controller' => 'requerimientos', 'action' => 'editar', $requerimiento_id), array('class'=>'btn'));
}
?>
<br><br>
<fieldset>
    <legend>Datos del Cliente</legend>
    <div class="divhorizontalL" style="width:100%">
        <label style="width:120px;">Razón Social</label>
        <label class="info" id="rif"><?php echo $cliente['Persona']['razon_social']?></label>
    </div>
    <div class="divhorizontalL" style="width:100%">
        <label style="width:120px;">RIF</label>
        <label class="info" id="rif"><?php echo $cliente['Persona']['tipo_documento_id'].'-'.$cliente['Persona']['documento']?></label>
    </div>

    <div class="divhorizontalL" style="width:100%">
        <label style="width:120px;">Domicilio Fiscal</label>
        <label class="info" id="direccion"><?php echo $cliente['Persona']['direccion']?></label>
    </div>
</fieldset>
<br>
<?php echo $this->Form->create('Factura', array('name' => 'frmfactura')); ?>
	<fieldset>
		<legend><?php echo 'Datos Fiscales'; ?></legend>
        <input type="hidden" name="data[Requerimiento][cliente_id]" value="<?php echo $cliente['Persona']['id']?>">
        <?php
        echo $this->Form->input('id', array('value' => $id, 'type' => 'hidden'));
        echo $this->Form->input('requerimiento_id', array('value' => $requerimiento_id, 'type' => 'hidden'));        
        echo $this->Form->input('impuesto_islr_id', array('type' => 'hidden'));  
        echo $this->Form->input('fecha_requerida_display', array('label'=>array('text'=>'Fecha de Facturación Deseada', 'style'=>'display:inline-block;width:195px;'),  'type' =>'text', 'id' => 'fecha_requerida_display', 'class' => 'Montos', 'type'=>'text',  'div'=>array('class'=>'divhorizontaln', 'style'=>'width:450px;')));
		echo $this->Form->input('fecha_requerida', array('type' =>'text', 'id' => 'fecha_requerida', 'type'=>'hidden'));
/*        echo $this->Form->input('fecha_facturacion_display', array('label'=>'Fecha Facturacion', 'type' =>'text', 'disabled' => 'disabled', 'id' => 'fecha_facturacion_display', 'class' => 'Montos', 'div'=>array('class'=>'divhorizontal', 'style'=>'width:200px;')));
		echo $this->Form->input('fecha_facturacion', array('type' =>'hidden', 'disabled' => 'disabled', 'id' => 'fecha_facturacion', 'type'=>'hidden'));
        echo $this->Form->input('numero_factura', array('class' => 'Montos', 'type' => 'text', 'disabled' => 'disabled', 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('numero_control', array('class' => 'Montos', 'type' => 'text','disabled' => 'disabled', 'div'=>array('class'=>'divhorizontal15')));*/
        if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'Facturas', 'datosFiscales'))) {
            echo $this->Html->link('Datos fiscales', "javascript:editarDatosFiscales(" . $id . ")",
                      array('class'=>'btn','style'=>'float:left; '));
        }
        ?>
	</fieldset>
    <br>
    <?php 
    //if ($montoBase == 0) {
        /*echo $this->Form->input('proveedor_id', array('empty' => 'Seleccione', 'value' => isset($proveedorSeleccionado[0])?$proveedorSeleccionado[0]:null, 'label' => array('text'=> 'Proveedor', 'style'=>'width:120px;'), 'div'=>array('class'=>'divhorizontalL', 'style' => 'width:100%'),
                                'after' => $this->Html->link('Buscar', array('controller' => 'personas', 
                                'action' => 'find'), array('title'=>'Buscar Proveedoress', 'rel' => 'gb_page_fs[]', 'class' => 'btn', 'style'=>'margin-left:44px;'))));
         * 
         */
         echo $this->Form->input('proveedor_id', array(
             'empty' => 'Seleccione', 
             'style'=>'width:500px;',
             'value' => isset($proveedorSeleccionado[0])?$proveedorSeleccionado[0]:null, 
             'label' => array(
                 'text'=> 'Proveedor', 
                 'style'=>'width:120px;'
                 ), 
             'div'=>array(
                 'class'=>'divhorizontalL', 
                 'style' => 'width:100%'
                 ),                                
             'after' => $this->Html->link('Buscar', "", array(
                 'id'=>'buscarProveedor',
                 'title'=>'Buscar Proveedoress', 
                 'class' => 'btn', 
                 'style'=>'margin-left:44px;'))));
    //} else {
        //echo $this->Form->input('proveedor_id', array('empty' => 'Seleccione', 'value' => isset($proveedorSeleccionado[0])?$proveedorSeleccionado[0]:null, 'label' => array('text'=> 'Proveedor', 'style'=>'width:120px;'), 'div'=>array('class'=>'divhorizontalL', 'style'=>'width:100%')));
    //}
    
    //echo $this->Form->input('concepto_id', array('label'=>array('text'=> 'ISLR', 'style'=>'width:120px;'),  'style'=>'width:450px;', 'empty' => 'Seleccione', 'value' => isset($this->request->data['Factura']['concepto_id'])?$this->request->data['Factura']['concepto_id']:null, 'div'=>array('class'=>'divhorizontalL')));
    echo $this->Form->input('observaciones', array('label'=>array('text'=>'Observaciones', 'style'=>'width:120px;'), 'type'=>'textarea', 'rows'=>3, 'value' => isset($this->request->data['Factura']['observaciones'])?$this->request->data['Factura']['observaciones']:null));
    //echo $this->Form->input('porcentaje_iva', array('label'=>array('text'=> 'IVA', 'style'=>'width:120px;'), 'options' => $iva, 'default' => $iva_defecto,));
    ?>
    
    <!--
    <fieldset>
		<legend><?php echo 'Montos de la factura'; ?></legend>
        <?php
        echo $this->Form->input('monto_base', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_iva', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_total', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('porcentaje_retencion_iva', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_retencion_islr', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        echo $this->Form->input('monto_retencion_iva', array('class' => 'Montos', 'type' => 'text', 'readonly' => true, 'div'=>array('class'=>'divhorizontal15')));
        ?>
	</fieldset>
    -->
<?php 
    if (isset($id)) {
        $estatusWorkflowId = $estatusRequerimiento;
        if ((array_key_exists($estatusWorkflowId, $estatus) && ($estatus[$estatusWorkflowId] == 1))) {
            echo $this->Form->end('Submit');
        } else {
            echo "</form>";
        }
    } else {
        echo $this->Form->end('Submit');
    }
?>
    <div class="clear"></div>

<?php
if (isset($id)) {
?>
    <br>
<fieldset>
	<legend><?php echo 'Detalle de Factura'; ?></legend>
   
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" style="margin-top: 0px;">

    <tr>
        <th class="table-header-repeat line-left minwidth-10 Center" style="width:20px"><l><?php echo 'Cantidad'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1"  style="width:750px"><l><?php echo 'Concepto'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"  style="width:50px"><l><?php echo 'IVA'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Precio'; ?></l></th>
        <th class="table-header-repeat line-left minwidth-1 Right"><l><?php echo 'Sub Total'; ?></l></th>
        <th class="table-header-options" style="min-width:10px;width:45px;"></th>
    </tr>
    <?php
    //pr($detalleFacturas);
    foreach ($detalleFacturas as $detalleFactura): 
    ?>
    <tr class="registros" id="dt<?php echo $detalleFactura['DetalleFactura']['id']?>">
        <td class="Center editable" id="can<?php echo $detalleFactura['DetalleFactura']['id'] ?>">
            <?php echo h(number_format($detalleFactura['DetalleFactura']['cantidad'],2)); ?>
            <input type="hidden" id="cantidad<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['DetalleFactura']['cantidad'] ?>"> 
        </td>
        <td  style="padding:10px 0px 10px 10px !important;" >
            <pre class="editable" style="width: 580px;float:left;" id="des<?php echo $detalleFactura['DetalleFactura']['id'] ?>"><?php echo h($detalleFactura['Concepto']['nombre'].' '.$detalleFactura['DetalleFactura']['descripcion']); ?></pre>
            <input type="hidden" id="conceptoId<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['DetalleFactura']['concepto_id'] ?>"> 
            <input type="hidden" id="conceptodescripcion<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['Concepto']['nombre'] ?>"> 
            <input type="hidden" id="descripcion<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['DetalleFactura']['descripcion'] ?>"> 
            <div style="float:right;width:30px;">
                <?php echo $this->Html->link('', array(), array('id'=>'id'.$detalleFactura['DetalleFactura']['id'], 'class' => 'icon-9 info-tooltip txtDescripcion', 'title'=>'Agregar descripción'));  ?>    
            </div>
        </td>
        <td class="Right editable" id="iva<?php echo $detalleFactura['DetalleFactura']['id'] ?>">
            <?php //echo h(CakeNumber::currency($detalleFactura['ImpuestoIVA']['valor'],'VEF')); ?>
            <input type="hidden" id="porcentaje_iva<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['DetalleFactura']['impuesto_iva_id'] ?>"> 
            <?php echo $this->Form->input('impuesto_iva_id', array('id'=>'iva'.$detalleFactura['DetalleFactura']['id'],'class'=>'ivaedit',  'name'=>'valor','options'=>$impuesto_ivas, 'default'=>$detalleFactura['DetalleFactura']['impuesto_iva_id'],  'div'=>false, 'label'=>false))?>
        </td>
        <td class="Right editable" id="pre<?php echo $detalleFactura['DetalleFactura']['id'] ?>">
            <?php echo h(CakeNumber::currency($detalleFactura['DetalleFactura']['precio_unitario'],'VEF')); ?>
            <input type="hidden" id="precio_unitario<?php echo $detalleFactura['DetalleFactura']['id'] ?>" value="<?php echo $detalleFactura['DetalleFactura']['precio_unitario'] ?>"> 
        </td>
        
        <td class="Right">            
            <?php 
                $cantidad = isset($detalleFactura['DetalleFactura']['cantidad'])?$detalleFactura['DetalleFactura']['cantidad']:1;
                echo h(CakeNumber::currency($detalleFactura['DetalleFactura']['precio_unitario']*$cantidad,'VEF')); 
            ?>
        </td>
        <td>
            <?php 
            if(count($this->Html->Mostrar($this->UserAuth->getGroupId(), 'DetalleFacturas', 'delete'))) {
                        echo $this->Form->postLink('', array(
                                                     'controller' => 'detalleFacturas',
                                                     'action' => 'delete', 
                                                     $detalleFactura['DetalleFactura']['id'],
                                                     $requerimiento_id,
                                                     $id
                                                  ), 
                                                  array(
                                                     'class' => 'icon-2 info-tooltip', 
                                                     'title'=>'Eliminar'
                                                  ), 
                                                  'Esta seguro de eliminar este registro?'                                                  
                                             );
                    }
            ?>
        </td>
    </tr>
    
    
    <?php 
    endforeach; 
    
    echo $this->Form->create('DetalleFactura', array('url' => array('controller' => 'detalleFacturas'), 'action' => 'editar'));
    ?>
    
    <tr class="registros" id="nuevo-registro">
        <td class="Center" style="padding: 0px;">
            <?php echo $this->Form->input('cantidad', array('label'=>array('style'=>'display:none;'), 'class' => 'Porcentajes', 'value' => '', 'type' => 'text'));?>
        </td>
        <td  style="padding: 10px 0px 10px 10px;">
            
                <pre id="preconcepto" style="width:95%;float:left;">

                </pre>
            
            <?php echo $this->Form->input('concepto_id', array('style'=>'width:200px;', 'type'=>'hidden', 'empty' => 'Seleccione', 'value' => isset($this->request->data['DetalleFactura']['concepto_id'])?$this->request->data['Factura']['concepto_id']:'', 'div'=>array('class'=>'divhorizontalL')));?>
            
            <div style="float:right;width:30px;">
                <?php //echo $this->Html->link('', array(), array('id'=>'showConceptos', 'class' => 'icon-1 info-tooltip', 'title'=>'Mostrar conceptops guardados'));  ?>    
                <?php //echo $this->Html->link('', array(), array('id'=>'buscaConceptos', 'class' => 'icon-7 info-tooltip', 'title'=>'Buscar conceptos'));  ?>    
                <?php //echo $this->Html->link('', array(), array('id'=>'agregaConceptos', 'class' => 'icon-9 info-tooltip', 'title'=>'Nuevo concepto'));  ?>    
                <?php echo $this->Html->link('', array(), array('id'=>'buscaConceptosProveedor', 'class' => 'icon-7 info-tooltip', 'title'=>'Buscar conceptos'));  ?>    
                <?php echo $this->Html->link('', array(), array('id'=>'agregaConceptoProveedor', 'class' => 'icon-9 info-tooltip', 'title'=>'Nuevo concepto'));  ?>    
                <!--<a id="prueba33" href="#">prueba</a>-->
            </div>
            
        </td>
        <td class="Right">
            <?php echo $this->Form->input('impuesto_iva_id', array('label'=>false, 'options'=>$impuesto_ivas, 'default' => $iva_defecto));?>
        </td>
        <td class="Right">
            <?php echo $this->Form->input('precio_unitario', array('label'=>array('style'=>'display:none;'), 'class' => 'Montos', 'value' => '', 'type' => 'text'));?>
        </td>
        <td class="Right">
            <?php
                echo $this->Form->input('Factura.id', array('type' => 'hidden', 'value' => $id)); 
                echo $this->Form->input('Factura.monto_base', array('type' => 'hidden'));
                echo $this->Form->input('Factura.monto_iva', array('type' => 'hidden'));
                echo $this->Form->input('Factura.monto_total', array('type' => 'hidden'));
                echo $this->Form->input('Factura.porcentaje_retencion_iva', array('type' => 'hidden'));
                echo $this->Form->input('Factura.monto_retencion_islr', array('type' => 'hidden'));
                echo $this->Form->input('Factura.monto_retencion_iva', array('type' => 'hidden'));
                echo $this->Form->input('Factura.requerimiento_id', array('type' => 'hidden', 'value' => $requerimiento_id));
                echo $this->Form->input('factura_id', array('type' => 'hidden', 'value' => $id));                 
            ?>
        </td>
        <td></td>
    </tr>
    <tfoot>
        <tr>
            <td colspan="4" class="td-number">SUB-TOTAL</td>
            <td class="td-number"><?php echo $this->request->data['Factura']['monto_base'] ?></td>
        </tr>
        <tr>
            <td colspan="4" class="td-number">IVA</td>
            <td class="td-number" ><?php echo $this->request->data['Factura']['monto_iva'] ?></td>
        </tr>
        <tr>
            <td colspan="4" class="td-number">TOTAL</td>
            <td class="td-number"><?php echo $this->request->data['Factura']['monto_total'] ?></td>
        </tr>
    </tfoot>
</table>
    </fieldset>   
    
    
    <div style="margin-top: 15px;">
        <div id="retenciones" style="float:left;width:600px;">
            <fieldset>
                <legend>Retenciones</legend>
                <div style="">
                <div style="float:left;width:250px;clear:none;">
                    <label style="width:100px;te">Ret. ISLR: (Bs.) </label>
                    <label class="info" style="width:100px;"><?php echo $this->request->data['Factura']['monto_retencion_islr'] ?></label>
                </div>
                
                
                <div style="float:left;width:250px;clear:none;">
                    <label style="width:100px;">Ret. IVA: (Bs.) </label>
                    <label class="info" style="width:100px;" id="retIvaResumen"><?php echo $this->request->data['Factura']['monto_retencion_iva'] ?></label>
                </div>
                    </div>
            </fieldset>
            
        </div>

    </div>
    <table>
        
    </table>
<?php
}
?>
<div id="dialogDatosFiscales">
    
</div>
<div style = "display:none" id = "dialogForm">
    <div clas = "seccion-izquierda">
     
    </div>
</div>
    
<div id="dialogo" title="Conceptos" style="display:none">
    <form>
        <input type="hidden" name="proveedor_id" id="proveedorid" value="">
        <input type="hidden" name="concepto_id" id="conceptoid" value="">
        <textarea name="descripcion"></textarea>
    </form>
    <br>
    <div style="width:100%;height:300px;overflow-y: scroll">
        <table id="product-table" class="detalleConceptos" width="95%">
            <tbody>

            </tbody>
        </table>
    </div>
    
</div>
<div id="containerProveedor" title="Listado de Proveedores">

</div>
    
<div id="containerDescripcion" title="Agregar Nuevo Concepto">

</div>
    
<script>
    
    function ajaxGetDialogoPersona(id){
        $('#containerProveedor').dialog('close');
        $('#FacturaProveedorId').val(id);
        $('#FacturaProveedorId').change();        
    }       
    
    function ajaxGetConceptosDialogo(a, b){
        $('#preconcepto').html(b);
        $('#DetalleFacturaPrecioUnitario').focus();
        $('#DetalleFacturaConceptoId').val(a);
        $('#dialogo').dialog('close');
    }
    
    function addAjax(data){
        var obj = jQuery.parseJSON(data);
        $('#preconcepto').html(obj.concepto)
        $('#DetalleFacturaConceptoId').val(obj.id);
        $('#containerDescripcion').dialog('close');
    }
    
    function cb1(a,b,c){        
        $.ajax({
            url:'/DetalleFacturas/editarInGrid',
            type:'post',
            data:'id='+c+'&valor='+a+'&campo=des'
        }).done(function(){
            location.reload()
        })
    }
    
    $(function(){
    
    $('.txtDescripcion').bind('click', function(e){
        e.preventDefault();
        var detalleId=$(this).attr('id');
        detalleId=detalleId.substring(2, detalleId.length)
        var descripcion = $('#descripcion'+detalleId).val();
        $('#des'+detalleId).css('display','none');
        var html="<div><form method='post' action='/detalleFacturas/editarInGrid'><input type='hidden' value='"+detalleId+"' name='id'><input type='hidden' value='dsc' name='campo'><input value='"+descripcion+"' name='valor'/></form></div>";
        $('#des'+detalleId).after(html);
    })
        
        //$('#DetalleFacturaDescripcionesId').hide();
        
        $('#agregaConceptos').bind('click', function(e){
            e.preventDefault();
            
            $.ajax({
                url:'/descripciones/addAjax',
                type:'post',
                data:$('#FacturaEditarForm').serialize()
            }).done(function(data){
                $('#containerDescripcion').html(data)
                $('#containerDescripcion').dialog({width:'700px'});
            })
        })
        
        $('#agregaConceptoProveedor').bind('click', function(e){
            e.preventDefault();
            
            $.ajax({
               url:'/ConceptoProveedores/ajaxAdd',
               type:'post',
               data:$('#FacturaEditarForm').serialize()
            }).done(function(data){
                $('#containerDescripcion').html(data)
                $('#containerDescripcion').dialog({width:'700px'});
            })    
        })
        
        
        
        $('#buscarProveedor').bind('click',function(e){
            e.preventDefault();
            $.ajax({
                url:'/personas/ajaxGetDialogoPersona',
                type:'post',
                data:'data[Persona][clasificacion_id]=3'
            }).done(function (data){
                $('#containerProveedor').html(data)
                $('#containerProveedor').dialog({width:'700px'});
                
            });
        })
        
        $('#showConceptos').bind('click', function(e){
            e.preventDefault();
            $('#DetalleFacturaDescripcionesId').show();
        })
        
        
        $('#buscaConceptos').bind('click', function(e){
            e.preventDefault();
            $('#proveedorid').val($('#FacturaProveedorId').val());
            $('#conceptoid').val($('#FacturaConceptoId').val());
            
            $.ajax({
                'url':'descripciones/ajaxGetDescripciones',
                'type':'post',
                'data':'data[Descripcion][proveedor_id]='+$('#FacturaProveedorId').val()+'&data[Descripcion][concepto_id]='+$('#FacturaConceptoId').val()
            }).done(function(data){                
                $('#dialogo').html(data);
                $('#dialogo').dialog({width:'700px', modal:true});
            });              
        })
        
        $('#buscaConceptosProveedor').bind('click', function(e){
            e.preventDefault();
            $('#proveedorid').val($('#FacturaProveedorId').val());
            $('#conceptoid').val($('#FacturaConceptoId').val());
            
            $.ajax({
                'url':'/ConceptoProveedores/ajaxGetConceptosDialogo',
                'type':'post',
                'data':$('#FacturaEditarForm').serialize()+'&callback=ajaxGetConceptosDialogo'
            }).done(function(data){                
                $('#dialogo').html(data);
                $('#dialogo').dialog({width:'700px', modal:true});
            });              
        })
        
    })
    
    function selecciona(id){
        $('#DetalleFacturaDescripcionesId').val(id);
        $('#DetalleFacturaDescripcionesId').change();
    }
$('#agregarDetalleFactura').bind('click',function(){
$('#dialogForm').dialog({
        'title':'Agregando Detalle de Factura',
        'modal':true,
        'width':'700px',
        buttons:[{
                'text':'Guardar', click:function(){
                    $("#DetalleFacturaEditarForm").submit();
                }
            }]
    });
    return false;
});

$('#FacturaProveedorId').bind('change', function(){
    $.ajax({
        'url':'/ConceptoProveedores/ajaxGetConceptos',
        'type':'post',
        'data':'tipo=options&proveedor_id='+$(this).val()
    }).done(function(data){$('#FacturaConceptoId').html(data)});
})

$('#DetalleFacturaPrecioUnitario').keypress(function(e){
    if (e.which==13){
        
        
        $("#DetalleFacturaEditarForm").submit();
    }
})

$('#DetalleFacturaDescripcionesId').bind('change',function(){
    if ($('#DetalleFacturaDescripcionesId option:selected').val()!=""){
        $('#DetalleFacturaDescripcion').val($('#DetalleFacturaDescripcionesId option:selected').text());
    }else{
        $('#DetalleFacturaDescripcion').val('');
    }
})

    activarEdicion()

function activarEdicion(){
    
    $('.ivaedit').bind('change',function(){
        var td=$(this).attr('id');
        var detalleFacturaId = td.substring(3,10);
        var valor = $(this).val();
        $.ajax({
            url:'/detalleFacturas/editarInGrid',
            type:'post',
            data:'id='+detalleFacturaId+'&campo=iva&valor='+valor
        }).done(function(data){
            //var obj = jQuery.parseJSON(data);
            //$('#ivaResumen').html(obj.Factura.monto_iva);
            //$('#retIvaResumen').html(obj.Factura.monto_retencion_iva);
            location.reload();
        })
    })
    
    
    $('.editable').bind('click',function(e){
        var td=$(this).attr('id');
        var detalleFacturaId = td.substring(3,10);
        var campo = td.substring(0,3);
        var valor = $('#'+td+'>input').val();
        var anterior = $('#'+td).html();
        if (campo=="des"){

            $('#proveedorid').val($('#FacturaProveedorId').val());
            $('#conceptoid').val($('#FacturaConceptoId').val());
            
            $.ajax({
                'url':'ConceptoProveedores/ajaxGetConceptosDialogo',
                'type':'post',
                'data':$('#FacturaEditarForm').serialize()+'&callback=cb1&detalleFacturaId='+detalleFacturaId
            }).done(function(data){                
                $('#dialogo').html(data);
                $('#dialogo').dialog({
                    width:'700px', 
                    modal:true,
                    close: function(){
                        activarEdicion()
                    }
                });
                
            });      
            
        }else{
            if (campo == "iva"){
            }else{
                $('#'+td).html('<form accept-charset="utf-8" method="post" id="DetalleFacturaEditarForm2" action="/detalleFacturas/editarInGrid"><input type="hidden" name="id" value="'+detalleFacturaId+'"/><input type="hidden" name="campo" value="'+campo+'"/><input id="inputedit" type="text" name="valor" value="'+valor+'"/><input type="submit" style="display:none;"></form>');    
            
                $('#inputedit').keyup(function(e){
                    if (e.which==27){
                        $('#inputedit').hide();
                        $('#'+td).html(valor)
                        location.reload();
                    }
                })
            }
            
        }
        
        $('#inputedit').focus().select();
        $('.editable').unbind('click')
        $('#inputedit').blur(function(eo){
            $('#DetalleFacturaEditarForm2').remove();
            $('#'+td).html(anterior)
            activarEdicion()
        })

    })    
}

$('body').keyup(function(e){

})

</script>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
}
</style>

<div style="display:none;" id="dialog-modal" title="Mensaje de sistema">
    <p>Debe introducir la cantidad.</p>
</div>
<div style="display:none;" id="dialog-modal2" title="Mensaje de sistema">
    <p>Debe seleccionar un concepto.</p>
</div>
<div style="display:none;" id="dialog-modal3" title="Mensaje de sistema">
    <p>Debe introducir una descripción.</p>
</div>
<div style="display:none;" id="dialog-modal4" title="Mensaje de sistema">
    <p>Debe introducir el precio unitario.</p>
</div>

<script>
	$(function(){
		$('.btn').button();
	})
</script>

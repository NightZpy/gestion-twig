<?php
echo $this->Form->create('Factura', array('name' => 'frmfactura1'));
echo $this->Form->input('id', array('value' => $facturas[0]['Factura']['id'], 'readonly'=>'readonly', 'type' => 'hidden', 'label'=>'Factura', 'class'=>'Montos'));
echo $this->Form->input('requerimiento_id', array('value' => $facturas[0]['Factura']['requerimiento_id'], 'readonly'=>'readonly', 'type' => 'hidden', 'label'=>'Requerimiento', 'class'=>'Montos'));

echo $this->Form->input('fecha_requerida_display', array('label'=>'Fecha Requerida', 'value' => AppHelper::fechaNormal($facturas[0]['Factura']['fecha_requerida']), 'id' => 'fecha_requerida_display', 'class' => 'Montos', 'readonly'=>'readonly', 'div'=>array('class'=>'divhorizontalh')));
echo $this->Form->input('fecha_requerida', array('type' =>'hidden', 'value' => $facturas[0]['Factura']['fecha_requerida'], 'id' => 'fecha_requerida', 'type'=>'hidden'));
echo $this->Form->input('fecha_facturacion_display', array('label'=>'Fecha Facturacion', 'value' => AppHelper::fechaNormal($facturas[0]['Factura']['fecha_facturacion']), 'id' => 'fecha_facturacion_display_datos_fiscales', 'class' => 'Montos', 'div'=>array('class'=>'divhorizontalh')));
echo $this->Form->input('fecha_facturacion', array('type' =>'hidden', 'value' => $facturas[0]['Factura']['fecha_facturacion'], 'id' => 'fecha_facturacion_datos_fiscales', 'type'=>'hidden'));
echo $this->Form->input('numero_factura', array('class' => 'Montos', 'id' => 'FacturaNumeroFacturaDatosFiscales', 'value' => $facturas[0]['Factura']['numero_factura'], 'type' => 'text', 'div'=>array('class'=>'divhorizontalh')));
echo $this->Form->input('numero_control', array('class' => 'Montos', 'value' => $facturas[0]['Factura']['numero_control'], 'type' => 'text', 'div'=>array('class'=>'divhorizontalh')));
?>
<?php
    $feriados = null;
    foreach ($diasFeriados as $value) {
        $feriados .= '"' . $value[0]["feriado"] . '", ';
    }
    $feriados = "[" . $feriados . "]";
?>
<script>
    //mes/dia/año
    var feriados = <?php echo $feriados;?>

    //Esta funcion decide si un dia es feriado
    function esFeriado(date, feriados) {
        var parts, dateArray=[];
        //build Dates from the array
        for (var i=0; i<feriados.length; i++) {
            parts=feriados[i].split('/');
            if (parts.length==2) {
                dateArray.push(new Date(date.getFullYear(), parts[0]-1, parts[1]).getTime());
            } else if (parts.length==3) {
                dateArray.push(new Date(feriados[i]).getTime());
            } else {
                return false;
            }
        }
        return $.inArray(date.getTime(), dateArray)!=-1;
    }
    
    $(function() {
        $( "#fecha_facturacion_display_datos_fiscales" ).datepicker({
            dateFormat: 'dd-mm-yy', 
            altFormat:'yy-mm-dd', 
            altField:'#fecha_facturacion_datos_fiscales',
            beforeShowDay: function(date){
                //handling sundays
                if (date.getDay() == 0 || date.getDay() == 6) {
                    return [false];
                }
                //handling feriados
                if (esFeriado(date, feriados)) {
                    return [false];
                }
                return [true];
            } 
        });
    });
    
    $("#FacturaDatosfiscalesForm").submit(function(){
        if (jQuery("#fecha_facturacion_display_datos_fiscales").val() == ""){
            $( "#validacion_fecha_facturacion" ).dialog({
                height: 140,
                modal: true
            });
            return false;
        } else if (jQuery("#FacturaNumeroFacturaDatosFiscales").val() == "") {
            $( "#validacion_numero_factura" ).dialog({
                height: 140,
                modal: true
            });
            return false;
        } else {
            return true;
        }
    });
</script>
<div style="display:none;" id="validacion_fecha_facturacion" title="Mensaje de sistema">
    <p>Debe seleccionar la Fecha de Facturación.</p>
</div>
<div style="display:none;" id="validacion_numero_factura" title="Mensaje de sistema">
    <p>Debe introducir el Número de la Factura.</p>
</div>
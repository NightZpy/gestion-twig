    <?php echo $this->Form->create('Factura', array('action'=>'index','id'=>'filters')); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
        <fieldset>
            <legend>Buscar facturas</legend>
                <?php 
                echo $this->Form->input('proveedor_id', array('empty' => 'Todos'));
                ?>
                <?php echo $this->Form->end(__('Filtrar'), array('class' => 'Search')); ?>
        </fieldset>
        <tr>
            <th class="table-header-check"></th>
			<th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('id'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('proveedor'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('fecha_requerida'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('fecha_facturacion'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('monto_base'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('monto_iva'); ?></th>
            <th class="table-header-repeat line-left minwidth-1"><?php echo $this->Paginator->sort('monto_total'); ?></th>
            <th class="table-header-options line-left"><a href="">Opciones</a></th>
        </tr>
        <?php
        foreach ($facturas as $factura): 
        ?>
        <tr class="registros">
            <td></td>
            <td><?php echo h($factura['Factura']['id']); ?></td>
            <td><?php echo h($factura['Persona']['razon_social']); ?></td>
            <td><?php echo h($factura['Factura']['fecha_requerida']); ?></td>
            <td><?php echo h($factura['Factura']['fecha_facturacion']); ?></td>
            <td><?php echo h($factura['Factura']['monto_base']); ?></td>
            <td><?php echo h($factura['Factura']['monto_iva']); ?></td>
            <td><?php echo h($factura['Factura']['monto_total']); ?></td>
            <td class="options-width">
                <?php echo $this->Html->link('', array('action' => 'edit', $factura['Factura']['id']), array('class' => 'icon-13 info-tooltip', 'title'=>'Editar')); ?>
                <?php echo $this->Form->postLink('', array('action' => 'delete', $factura['Factura']['id']), array('class' => 'icon-2 info-tooltip', 'title'=>'Eliminar'), __('Esta seguro de eliminar este registro?', $factura['Factura']['id'])); ?>
                
            </td>
        </tr>
        <?php 
        endforeach; 
        ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} en total, a partir del registro {:start}, terminando en {:end}')
	));
	?>	
    </p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
<style>
.ui-combobox {
    position: relative;
    display: inline-block;
    min-width: 450px;
}
.ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}
.ui-combobox-input {
    margin: 0;
    padding: 0.3em;
}
</style>
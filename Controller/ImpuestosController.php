<?php
App::uses('AppController', 'Controller');

class ImpuestosController extends AppController 
{
    public $autoRender = false;
    public $uses = array('Concepto');
    public function islr( $concepto_id = null ) 
    {
        $islr = $this->Concepto->find('list', array('conditions'=>array('id' => $concepto_id), 
                                                    'fields' => 'porcentaje_islr'));
        if (count($islr) > 0) {
            $response = array('success' => true,'ISLR' => $islr);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }

}

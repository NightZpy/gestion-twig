<?php
App::uses('AppController', 'Controller');

class InstruccionPagosController extends AppController 
{
	
    public function edit($id = null) 
    {
        $requerimiento = $this->InstruccionPago->getRequerimientos($id);
        $instrucciones = $this->InstruccionPago->find('all', array('conditions'=> array('requerimiento_id' => $id)));
        $proveedor = $this->InstruccionPago->buscarProveedorFinal($requerimiento['Requerimiento']['cliente_id']);
        foreach ($proveedor as $value) {
            $proveedores[$value['Persona']['id']] = $value['Persona']['razon_social'];
        }
        $tipoInstrumentos = $this->InstruccionPago->TipoInstrumento->find('list');
        $bancoOrigenes = $this->InstruccionPago->BancoOrigen->find('list');
        $bancoDestinos = $this->InstruccionPago->BancoDestino->find('list');
        $cuentaDestinos = $this->InstruccionPago->CuentaDestino->find('list');
        $montoFacturaDisponibles = $this->InstruccionPago->getFacturasDisponibles($id);
        $montoReintegro = $this->InstruccionPago->getMontoReintegro($id);
        $this->set(compact('requerimiento', 'instrucciones', 'proveedores', 'tipoInstrumentos',
                           'bancoOrigenes', 'bancoDestinos', 'cuentaDestinos', 'montoFacturaDisponibles',
                           'montoReintegro'));
        
	}
    
    public function add($requerimiento_id = null) 
    {
        $this->request->data["InstruccionPago"] = $this->request->data["InstruccionPago2"];
        unset($this->request->data["InstruccionPago2"]);
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->InstruccionPago->create();
            if ($this->InstruccionPago->save($this->request->data)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'edit', $requerimiento_id));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
                $this->redirect(array('action' => 'edit', $requerimiento_id));
            }
        }
	}
    
    public function agregarPago($requerimiento_id = null) 
    {
        array_splice($this->request->data['InstruccionPago'], 4, 1); 
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->InstruccionPago->create();
            if ($this->InstruccionPago->save($this->request->data)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'edit', $requerimiento_id));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
                $this->redirect(array('action' => 'edit', $requerimiento_id));
            }
        }
    }
    
	public function delete($id = null, $requerimiento_id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->InstruccionPago->id = $id;
		if (!$this->InstruccionPago->exists()) {
			throw new NotFoundException(__('Invalid Instruccion de Pago'));
		}
		if ($this->InstruccionPago->delete()) {
			$this->Session->setFlash(__('Instruccion de Pago eliminado'), 'message_successful');
			$this->redirect(array('action' => 'edit', $requerimiento_id));
		}
		$this->Session->setFlash(__('Instruccion de Pago was not deleted'));
		$this->redirect(array('action' => 'edit', $requerimiento_id));
	}
    
    public function getCuentasBancarias(){
		$this->autoRender=FALSE;
		$this->layout = "ajax";
		$request = $this->request->data;
		$proveedorId = $request['InstruccionPago']['proveedor_id'];
        $BancoDestinoId = $request['InstruccionPago']['banco_destino_id'];
		$html="";
		if ($proveedorId>0){
			$options=array(
				'recursive'=>0,
				'fields'=>'Cuenta.id, Cuenta.numero, Banco.nombre',
				'conditions' => array(
					'persona_id' => $proveedorId,
                    'banco_id' => $BancoDestinoId,
				),
				'order'=>array('Banco.Nombre, Cuenta.numero')
			);
			
			$lista = $this->InstruccionPago->Cuenta->find('list',$options);
			foreach ($lista as $key => $value){
				foreach ($value as $cuenta => $row)
					$html .= '<option value="' . $cuenta . '">' . $key . " - " . $row . '</option>';
			}
		}
		
		return $html;
	}
}

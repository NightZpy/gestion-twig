<?php
App::uses('AppController', 'Controller');

class PrioridadesController extends AppController 
{
	public $components = array('Search.Prg');
	
    public function index() 
    {
        $this->Prioridad->recursive = 0;
		$this->Prg->commonProcess();
		$this->paginate = array('conditions' => $this->Prioridad->parseCriteria($this->passedArgs));
        $this->set('prioridades', $this->paginate());
    }

    public function view($id = null) 
    {
		$this->Prioridad->id = $id;
		if (!$this->Prioridad->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		$this->set('prioridad', $this->Prioridad->read(null, $id));
	}

	public function add() 
    {
		if ($this->request->is('post')) {
			$this->Prioridad->create();
			if ($this->Prioridad->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		}
	}

	public function edit($id = null) 
    {
        $this->Prioridad->id = $id;
		if (!$this->Prioridad->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Prioridad->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->Prioridad->read(null, $id);
		}
	}

	public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Prioridad->id = $id;
		if (!$this->Prioridad->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->Prioridad->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

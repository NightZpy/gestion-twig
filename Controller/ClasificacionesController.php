<?php
App::uses('AppController', 'Controller');

class ClasificacionesController extends AppController 
{
	public function index() 
    {
		$this->Clasificacion->recursive = 0;
		$this->set('clasificaciones', $this->paginate());
	}

	public function view($id = null) 
    {
		$this->Clasificacion->id = $id;
		if (!$this->Clasificacion->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		$this->set('clasificacion', $this->Clasificacion->read(null, $id));
	}

	public function add() 
    {
		if ($this->request->is('post')) {
			$this->Clasificacion->create();
			if ($this->Clasificacion->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		}
	}

	public function edit($id = null) 
    {
		$this->Clasificacion->id = $id;
		if (!$this->Clasificacion->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Clasificacion->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->Clasificacion->read(null, $id);
		}
	}

	public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Clasificacion->id = $id;
		if (!$this->Clasificacion->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->Clasificacion->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

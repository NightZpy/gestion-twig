<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');


class RequerimientosController extends AppController 
{
    public $components = array('Search.Prg');
    public $presetVars = true;
    public $uses = array('Requerimiento','Factura','DetalleFactura');

    public function index() 
    {
        $conditions = array();
        $groupId = $this->UserAuth->getGroupId();
        $this->Requerimiento->unbindModel(array(
                'hasMany' => array('Factura'), 
            ), false
        );
        
        $this->Prg->commonProcess();
        if(isset($this->passedArgs['estatus_id'])){
            $this->passedArgs['estatus_workflow_id'] = $this->passedArgs['estatus_id'];
            unset($this->passedArgs['estatus_id']);
        }
        
        $this->paginate = array('conditions' => $this->Requerimiento->parseCriteria($this->passedArgs));
        
        if ($this->UserAuth->isAsociado()) {
            $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id'))));
            $asociados_seleccionado = array_values($this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id')), 'fields' => 'Persona.id')));
            $options['joins'] = array(
                                    array('table' => 'persona_personas',
                                    'alias' => 'PersonaPersona',
                                    'type' => '',
                                    'conditions' => array(
                                                         'Persona.id = PersonaPersona.persona_hijo_id',
                                                         ),
                                          ),
                                    );
            $options['conditions'] = array(
                'PersonaPersona.persona_padre_id' => $asociados_seleccionado,
                'PersonaPersona.tipo_relacion' => 'asociado_clientes'
            );
            $options['fields'] = array('Persona.id', 'Persona.razon_social');
            $clientes = $this->Requerimiento->Persona->find('list', $options);
            $conditions[] = array('Requerimiento.asociado_id' => $this->Session->read('UserAuth.Usuario.persona_id'));
        } else {
            $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 2')));
            $clientes = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 1')));
        }
        $prioridades = $this->Requerimiento->Prioridad->find('list');
        $estatuses = $this->Requerimiento->EstatusWorkflow->find('list');
        $this->set(compact('asociados', 'clientes', 'asociados_seleccionado', 'prioridades', 'estatuses'));
        $this->set('requerimientos', $this->paginate($conditions));
    }
    
    public function bitacora($id = null)
    {
        $this->layout = 'greybox';
        $bitacoras = $this->Requerimiento->getBitacoras($id);
        $this->set('bitacoras', $bitacoras);
    }
    
    public function cambiarEstatus( $id = null, $accion_id = null, $ocultar = null ) 
    {
        $camposGuardar = array('estatus_workflow_id');
        $this->layout = 'greybox';
        $this->Requerimiento->id = $id;

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Requerimiento->create();
            $estatus_workflow_actual = $this->request->data['Requerimiento']['estatus_workflow_actual'];
            $estatus_workflow_proximo = $this->request->data['Requerimiento']['estatus_workflow_proximo'];
            $observacion = $this->request->data['Requerimiento']['observacion'];
            $accion = $this->Requerimiento->getAccion($accion_id);
            if ($accion == "Aprobar") {
                $accion = "Aprobado";
            } else {
                $accion = "Rechazado";
            }
            $usuario = $this->UserAuth->getUser();
            
            array_splice($this->request->data['Requerimiento'], 1, 3);

            $textoEmail = "El usuario " . $usuario['Usuario']['first_name'] . " " . $usuario['Usuario']['last_name'] . " 
                           ha " . $accion . " el requerimiento Nro. " . $id . "<br><br>
                           Fecha y hora del cambio de Estatus: " . date("d/m/Y h:i:s") ."<br>
                           Estatus Anterior: " . $estatus_workflow_actual . "<br>
                           Estatus Actual: " . $estatus_workflow_proximo . "<br>
                           Observación: " . $observacion;
            $asuntoEmail = "El requerimiento Nro: " . $id . " ha sido " . $accion;
            if ($this->Requerimiento->save($this->request->data, $validate = true, $fieldList = $camposGuardar)) {
                $usuariosEmail = $this->Requerimiento->getUsuariosEmail($this->request->data['Requerimiento']['estatus_workflow_id']);
                /*
                foreach ($usuariosEmail as $messageTo) {
                    $email = new CakeEmail('smtp');
                    $email->to($messageTo['Usuario']['email'])
                          ->emailFormat('html')
                          ->subject($asuntoEmail)
                          ->send($textoEmail);
                }
                 * 
                 */
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'cambiarestatus', $this->Requerimiento->id, $accion_id, true));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        }
        if (!$this->Requerimiento->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        
        $this->request->data = $this->Requerimiento->read(null, $id);
        
        
        $transiciones = $this->Requerimiento->getTransiciones(
                                                        $this->UserAuth->getGroupId(), 
                                                        $accion_id, 
                                                        $this->request->data['Requerimiento']['estatus_workflow_id']);
        $this->set('transiciones', $transiciones);

        $estatus_workflow_id = $this->request->data['Requerimiento']['estatus_workflow_id'];
        $estatus_workflow_nombre = $this->Requerimiento->EstatusWorkflow->find('list', 
                                                                                array('conditions' => array(
                                                                                                    'EstatusWorkflow.id' => $this->request->data['Requerimiento']['estatus_workflow_id']
                                                                                                     ), 
                                                                                      'fields' => 'EstatusWorkflow.nombre'
                                                                                     )
                                                                               );
        $this->set(compact('estatus_workflow_id', 'accion_id', 'id', 'estatus_workflow_nombre', 'ocultar'));
    }
    
    public function agregar() 
    {
        if ($this->UserAuth->isAsociado()) {
            $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id'))));
            $asociados_seleccionado = array_values($this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id')), 'fields' => 'Persona.id')));
            $options['joins'] = array(
                                    array('table' => 'asociado_clientes',
                                    'alias' => 'AsociadoCliente',
                                    'type' => '',
                                    'conditions' => array(
                                                         'Persona.id = AsociadoCliente.cliente_id',
                                                         ),
                                          ),
                                    );
            $options['conditions'] = array(
                'AsociadoCliente.asociado_id' => $asociados_seleccionado
            );
            $options['fields'] = array('Persona.id', 'Persona.razon_social');
            $clientes = $this->Requerimiento->Persona->find('list', $options);
        } else {
            $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 2')));
            $clientes = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 1')));
        }
        $prioridades = $this->Requerimiento->Prioridad->find('list');
        $prioridad_predeterminado = array_values($this->Requerimiento->Prioridad->find('list', array('conditions' => array('Prioridad.predeterminado = 1'), 'fields' => 'Prioridad.id')));
        $estatus_workflow_predeterminado_id = $this->Requerimiento->EstatusWorkflow->find('list', array('conditions' => array('EstatusWorkflow.predeterminado = 1'), 'fields' => 'EstatusWorkflow.id'));
        $estatus_workflow_predeterminado_nombre = $this->Requerimiento->EstatusWorkflow->find('list', array('conditions' => array('EstatusWorkflow.predeterminado = 1'), 'fields' => 'EstatusWorkflow.nombre'));
        $this->set(compact(
                          'asociados',
                          'asociados_seleccionado',
                          'clientes', 
                          'prioridades', 
                          'prioridad_predeterminado', 
                          'estatus_workflow_predeterminado_id',
                          'estatus_workflow_predeterminado_nombre'
                          )
                  );
        $this->_guardar();
        $this->render('form');
    }
    
    public function editar($id = null) 
    {
        $this->Requerimiento->id = $id;
        if (!$this->Requerimiento->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        $this->_guardar($id);
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Requerimiento->actualizaDatosRequerimiento($this->data);
        }
        $this->render('form');
	}
    
    public function duplicar($id = null) 
    {
        $this->Requerimiento->unbindModel(array('belongsTo'=>array('EstatusWorkflow','Prioridad', 'Persona', 'Cliente', 'Asociado')));
        $this->Requerimiento->Factura->unbindModel(array('belongsTo'=>array('Impuesto','Persona','Requerimiento')));
        $this->Requerimiento->recursive = 2;
        $this->Requerimiento->id = $id;
        $this->Requerimiento->read();
        
        $datos['Requerimiento'] = $this->Requerimiento->data['Requerimiento'];
        $datos['Requerimiento']['Factura'] = $this->Requerimiento->data['Factura'];
        
        array_splice($datos['Requerimiento'], 0, 1); 
        
        for ($i=0;$i<count($datos['Requerimiento']['Factura']);$i++){
            $datos['Requerimiento']['Factura'][$i]['copia_factura_id']=$datos['Requerimiento']['Factura'][$i]['id'];
            array_splice($datos['Requerimiento']['Factura'][$i], 0, 1); 
            array_splice($datos['Requerimiento']['Factura'][$i], 2, 5); 
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {
            array_splice($this->request->data['Requerimiento'], 0, 1); 
            $datos['Requerimiento']['cliente_id']=$this->request->data['Requerimiento']['cliente_id'];
            $this->Requerimiento->create();
            $requerimiento = $this->Requerimiento->save($datos['Requerimiento']);
            
            $this->Requerimiento->Factura->duplicaFacturas($id, $requerimiento['Requerimiento']['id']);
            $this->Requerimiento->Factura->DetalleFactura->duplicaDetalleFacturas2($requerimiento['Requerimiento']['id']);
            $this->redirect(array('action' => 'editar', $requerimiento['Requerimiento']['id']));

        } else {
            $this->_guardar($id);
        }
        
        
        $this->render('duplicar');
	}
    
    public function instruccionPago($id = null) 
    {
        $this->Requerimiento->id = $id;
        if ($this->request->is('post') || $this->request->is('put')) {
            //array_splice($this->request->data['Requerimiento'], 0, 1); 
            $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
            $this->redirect(array('action' => 'editar', $this->request->data['Requerimiento']['id']));
        } else {
            $this->_guardar($id);
            $instrucciones = $this->Requerimiento->getInstruccionesPagos($id);
        }
        $tipoInstrumento = $this->Requerimiento->getTipoInstrumentos();
        foreach ($tipoInstrumento as $value) {
            $tipoInstrumentos[$value['TipoInstrumento']['id']] = $value['TipoInstrumento']['nombre'];
        }
        $banco = $this->Requerimiento->getBancos();
        foreach ($banco as $value) {
            $bancoOrigenes[$value['Banco']['id']] = $value['Banco']['nombre'];
        }
        
        $this->set(compact('instrucciones', 'tipoInstrumentos', 'bancoOrigenes'));
        $this->render('instruccionpago');
	}
    
    private function _guardar($id = null) 
    {
        $camposGuardar = array('asociado_id', 'cliente_id', 'prioridad_id', 'estatus_workflow_id', 
                               'porcentaje_costo_gestion', 'porcentaje_costo_operativo',
                               'porcentaje_costo_venta', 'porcentaje_costo_venta_secundario',
                               'porcentaje_costo_administrativo');
        $groupId = $this->UserAuth->getGroupId();
        $estatus = array();
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Requerimiento->create();
            array_splice($this->request->data['Requerimiento'], 4, 1); 
            if ($this->Requerimiento->save($this->request->data, $validate = true, $fieldList = $camposGuardar)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'editar', $this->Requerimiento->id));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        } else {
            if (isset($id)) {
                $this->Requerimiento->unbindModel(
                    array('hasMany' => array('Factura'), 'belongsTo' => array('EstatusWorkflow', 'Persona'))
                );
                
                $this->request->data = $this->Requerimiento->read(null, $id);
                
                if ($this->UserAuth->isAsociado()) {
                    $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id'))));
                    $asociados_seleccionado = array_values($this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.id = ' . $this->Session->read('UserAuth.Usuario.persona_id')), 'fields' => 'Persona.id')));
                    $options['joins'] = array(
                                            array('table' => 'persona_personas',
                                            'alias' => 'PersonaPersona',
                                            'type' => '',
                                            'conditions' => array(
                                                                 'Persona.id = PersonaPersona.persona_hijo_id',
                                                                 ),
                                                  ),
                                            );
                    $options['conditions'] = array(
                        'PersonaPersona.persona_padre_id' => $asociados_seleccionado,
                        'PersonaPersona.tipo_relacion' => 'asociado_clientes'
                    );
                    $options['fields'] = array('Persona.id', 'Persona.razon_social');
                    $clientes = $this->Requerimiento->Persona->find('list', $options);
                } else {
                    $asociados = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 2')));
                    $clientes = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 1')));
                }
                $acciones = array();
                if ($this->request->data['Requerimiento']['monto_base'] > 0){
                    $acciones = $this->Requerimiento->getAcciones($groupId, $this->request->data['Requerimiento']['estatus_workflow_id']);
					$this->request->data['Requerimiento']['monto_base']= CakeNumber::currency($this->request->data['Requerimiento']['monto_base'],'VEF');
					$this->request->data['Requerimiento']['monto_retencion_islr']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_retencion_islr'],'VEF');
					$this->request->data['Requerimiento']['monto_iva']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_iva'],'VEF');
					$this->request->data['Requerimiento']['monto_retencion_iva']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_retencion_iva'],'VEF');
					$this->request->data['Requerimiento']['monto_total']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_total'],'VEF');
					$this->request->data['Requerimiento']['porcentaje_costo_gestion']=  CakeNumber::currency($this->request->data['Requerimiento']['porcentaje_costo_gestion'],'VEF');
					$this->request->data['Requerimiento']['porcentaje_costo_operativo']=  CakeNumber::currency($this->request->data['Requerimiento']['porcentaje_costo_operativo'],'VEF');
					$this->request->data['Requerimiento']['monto_costo_gestion']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_costo_gestion'],'VEF');
					$this->request->data['Requerimiento']['monto_costo_operativo']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_costo_operativo'],'VEF');
					$this->request->data['Requerimiento']['monto_costo_venta']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_costo_venta'],'VEF');
					$this->request->data['Requerimiento']['monto_costo_venta_secundario']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_costo_venta_secundario'],'VEF');
					$this->request->data['Requerimiento']['monto_costo_administrativo']=  CakeNumber::currency($this->request->data['Requerimiento']['monto_costo_administrativo'],'VEF');
                }
                $prioridades = $this->Requerimiento->Prioridad->find('list');
                $prioridad_predeterminado = $this->request->data['Requerimiento']['prioridad_id'];
                $estatus_workflow_predeterminado_id = $this->request->data['Requerimiento']['estatus_workflow_id'];
                $estatus_workflow_predeterminado_nombre = $this->Requerimiento->EstatusWorkflow->find('list', 
                                                                        array(
                                                                         'conditions' => 
                                                                            array(
                                                                              'EstatusWorkflow.id' => $this->request->data['Requerimiento']['estatus_workflow_id']
                                                                                 ), 
                                                                          'fields' => 'EstatusWorkflow.nombre'
                                                                              )
                                                                                                    );
                $estatusDisponibles = $this->Requerimiento->getEstatus($groupId);
                foreach ($estatusDisponibles as $estatusDisponible):
                    $estatus[$estatusDisponible['Transicion']['estatus_inicial_id']] = $estatusDisponible['Transicion']['es_modificable'];
                endforeach;
                $proveedores = $this->Requerimiento->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 3')));
                $this->set(compact(
                              'acciones',
                              'asociados', 
                              'clientes', 
                              'proveedores', 
                              'prioridades', 
                              'estatus',
                              'prioridad_predeterminado', 
                              'estatus_workflow_predeterminado_id',
                              'estatus_workflow_predeterminado_nombre'
                              )
                      );
                $this->Requerimiento->unbindModel(
                    array('belongsTo' => array('Prioridad', 'Asociado', 'Cliente', 'EstatusWorkflow'))
                );
                $facturas = $this->Requerimiento->getFacturas($id);
                $this->set('facturas', $facturas);
                $this->set(compact('proveedores'));
                $this->set('requerimiento_id', $id);
            }
        }
    }
   
	public function delete( $id = null ) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
        
		$this->Requerimiento->id = $id;
        
		if (!$this->Requerimiento -> exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
        
		if ($this->Requerimiento->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
        
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

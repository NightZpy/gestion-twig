<?php
App::uses('AppController', 'Controller');
/**
 * Estados Controller
 *
 * @property Estado $Estado
 */
class CuentasController extends AppController {

    /**
     * index method
     *
     * @return void
     */
	public function index() {
		$this->Cuenta->recursive = 0;
		$this->set('cuentas', $this->paginate());
	}

    /**
     * agregar method
     *
     * @return void
     */
	public function agregar() {
        $this->_guardar();
        $this->render('form');
	}

    /**
     * editar method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function editar($id = null) {
        
		$this->Cuenta->id = $id;
		if (!$this->Cuenta->exists()) {
			throw new NotFoundException('Registro Inválido.');
		}
        $this->_guardar($id);
        $this->render('form');
		
	}
    
    private function _guardar($id=null) {
        
        if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cuenta->save($this->request->data)) {
				$this->Session->setFlash('Registro guardado con éxito.', 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		} else {
			$this->request->data = $this->Cuenta->read(null, $id);
		}
        
        $personas = $this->Cuenta->Persona->find('list');
        $bancos = $this->Cuenta->Banco->find('list', array('conditions' => array('activo' => 1)));
        $tipoCuentas = $this->Cuenta->TipoCuenta->find('list', array('conditions' => array('activo' => 1)));
        
        $this->set(compact('personas', 'bancos', 'tipoCuentas'));
    }

    /**
     * eliminar method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function eliminar($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Cuenta->id = $id;
		if (!$this->Cuenta->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
		if ($this->Cuenta->delete()) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('action' => 'index'));
	}
   
}
<?php
App::uses('AppController', 'Controller');

class EstatusWorkflowsController extends AppController 
{
	public $components = array('Search.Prg');
	
    public function index() 
    {
        $conditions = array();
        $this->EstatusWorkflow->recursive = 0;
		$this->Prg->commonProcess();
		$this->paginate = array('conditions' => $this->EstatusWorkflow->parseCriteria($this->passedArgs));       
        $this->set('EstatusWorkflows', $this->paginate($conditions));
    }

    public function view($id = null) 
    {
		$this->EstatusWorkflow->id = $id;
		if (!$this->EstatusWorkflow->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		$this->set('EstatusWorkflows', $this->EstatusWorkflow->read(null, $id));
	}

	public function add() 
    {
		if ($this->request->is('post')) {
			$this->EstatusWorkflow->create();
			if ($this->EstatusWorkflow->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		}
	}

	public function edit($id = null) 
    {
        $this->EstatusWorkflow->id = $id;
		if (!$this->EstatusWorkflow->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->EstatusWorkflow->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->EstatusWorkflow->read(null, $id);
		}
	}

	public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->EstatusWorkflow->id = $id;
		if (!$this->EstatusWorkflow->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->EstatusWorkflow->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Ciudades Controller
 *
 * @property Ciudad $Ciudad
 */
class TipoAdjuntosController extends AppController {

	public $autoRender = false;
    public function listar() 
    {
        $tipos = $this->TipoAdjunto->find('all');
        if (count($tipos) > 0) {
            $response = array('success' => true, 'tipos' => $tipos);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }
}
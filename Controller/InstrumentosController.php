<?php
App::uses('AppController', 'Controller');
/**
 * Instrumentos Controller
 *
 * @property Instrumento $Instrumento
 */
class InstrumentosController extends AppController {
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->paginate = array('limit'=>10);
		$this->Instrumento->recursive = 0;
		
		$conditions=array();
		if ($this->UserAuth->isAsociado()){
			$conditions[] = array('Instrumento.asociado_id'=>$this->UserAuth->getUserId());
		}
		//$this->paginate=array('limit'=>10);
		$this->set('instrumentos', $this->paginate($conditions));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Instrumento->id = $id;
		if (!$this->Instrumento->exists()) {
			throw new NotFoundException(__('Invalid instrumento'));
		}
		
		$this->set('instrumento', $this->Instrumento->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Instrumento->set($this->request->data);
			
			if (!$this->Instrumento->validates()){
				$this->Session->setFlash(__('El instrumento de pago no pudo ser guardado debido a errores'),'message_error');
			}else{
				//$this->Instrumento->create();
				$this->Instrumento->save();
				$this->Session->setFlash(__('El instrumento de pago ha sido guardado con éxito'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			}
		}
		
		$asociados=array();
		$esAsociado = false;
		if ($this->UserAuth->isAsociado()){
			$clientes = $this->Instrumento->getPersonasConRequerimiento($this->UserAuth->getUserId());
			$esAsociado = true;
		}else{
			$clientes = array();
			$params['conditions'] = array('clasificacion_id'=>2);
			$asociados = $this->Instrumento->Asociado->find('list',$params);
		}
		
		$proveedores = array();
		$cuentas = array();
		if (isset($this->request->data['Instrumento'])){
			if ($this->request->data['Instrumento']['cliente_id']){
				$proveedores = $this->Instrumento->getProveedoresEnFacturas($this->request->data['Instrumento']['cliente_id']);
			}		
			
			if (isset($this->request->data['Instrumento']['proveedor_id'])?true:false){
				$cuentas = $this->Instrumento->getCuentasProveedor($this->request->data['Instrumento']['proveedor_id']);
			}
			
			if (isset($this->request->data['Instrumento']['asociado_id'])){
				$clientes = $this->Instrumento->getPersonasConRequerimiento($this->request->data['Instrumento']['asociado_id']);
			}
		}
		
		$tipoInstrumentos = $this->Instrumento->TipoInstrumento->find('list');
		$bancos = $this->Instrumento->Banco->find('list');
		
		$asociadoId = $this->UserAuth->getUserId();
		$this->set(compact('clientes', 'proveedores', 'tipoInstrumentos', 'bancos', 'cuentas','asociadoId','esAsociado','asociados'));
	}
	
	public function getClientes(){
		$this->autoRender=FALSE;
		$this->layout = "ajax";
		$asociado = $this->request->data;
		$asociadoId = $asociado['Instrumento']['asociado_id'];
		$html="";
		if ($asociadoId>0){
			$clientes = $this->Instrumento->getPersonasConRequerimiento($asociadoId);
			foreach ($clientes as $key=>$value){
				$html .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}	
		
		return $html;
	}
	
	public function getProveedores(){
		$this->autoRender=FALSE;
		$this->layout = "ajax";
		$cliente = $this->request->data;
		$clienteId = $cliente['Instrumento']['cliente_id'];
		$html="";
		if ($clienteId>0){
			$proveedores = $this->Instrumento->getProveedoresEnFacturas($clienteId);
			foreach ($proveedores as $key=>$value){
				$html .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}	
		
		return $html;
	}
	
	public function getCuentasBancarias(){
		$this->autoRender=FALSE;
		$this->layout = "ajax";
		$request = $this->request->data;
		$proveedorId = $request['Instrumento']['proveedor_id'];
		$html="";
		if ($proveedorId>0){
			$options=array(
				'recursive'=>0,
				'fields'=>'Cuenta.id, Cuenta.numero, Banco.nombre',
				'conditions' => array(
					'persona_id'=>$proveedorId,
				),
				'order'=>array('Banco.Nombre, Cuenta.numero')
			);
			
			$lista = $this->Instrumento->Cuenta->find('list',$options);
			foreach ($lista as $key => $value){
				foreach ($value as $cuenta => $row)
					$html .= '<option value="' . $cuenta . '">' . $key . " - " . $row . '</option>';
			}
		}
		
		return $html;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Instrumento->id = $id;
		
		if (!$this->Instrumento->exists()) {
			throw new NotFoundException(__('Invalid instrumento'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Instrumento->save($this->request->data)) {
				$this->Session->setFlash(__('El instrumento de pago ha sido guardado con éxito'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El instrumento de pago no pudo ser guardado debido a errores'),'message_error');
			}
		} else {
			$this->request->data = $this->Instrumento->read(null, $id);
		}
		
		$asociados = $this->Instrumento->Asociado->find('list');
		$clientes = $this->Instrumento->getPersonasConRequerimiento($this->request->data['Instrumento']['asociado_id']);
		$proveedores = $this->Instrumento->getProveedoresEnFacturas($this->request->data['Instrumento']['cliente_id']);
		$tipoInstrumentos = $this->Instrumento->TipoInstrumento->find('list');
		$bancos = $this->Instrumento->Banco->find('list');
		if (isset($this->request->data['Instrumento']['proveedor_id'])){
			$cuentas = $this->Instrumento->getCuentasProveedor($this->request->data['Instrumento']['proveedor_id']);
		}
		
		$tipos = $this->Instrumento->TipoInstrumento->find('list');
		$transiciones = $this->Instrumento->getTransiciones($this->UserAuth->getGroupId(), $this->Instrumento->data['Instrumento']['estatus_instrumento_id']);
		$this->set(compact('clientes', 'proveedores', 'tipoInstrumentos', 'bancos', 'cuentas', 'tipos','asociados', 'transiciones'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Instrumento->id = $id;
		if (!$this->Instrumento->exists()) {
			throw new NotFoundException(__('Invalid instrumento'));
		}
		if ($this->Instrumento->delete()) {
			$this->Session->setFlash(__('Instrumento eliminado'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Instrumento was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function cambiarEstatus($id){
		$this->autoRender=false;
		$camposGuardar = array('estatus_instrumento_id');
		$this->Instrumento->id = $id;
		
		if ($this->request->is('post')){			
			if ($this->Instrumento->save($this->request->data, $validate = true, $camposGuardar)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
			$this->redirect(array('action' => 'index'));
		}
	}
	
	public function recibo(){
		
		if ($this->request->is('post')){
			$this->Instrumento->Recibo->save($this->request->data);
			$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
			$this->redirect(array('action' => 'recibo'));
		}
		
		$params['conditions'] = array('clasificacion_id'=>2);
		$asociados = $this->Instrumento->Asociado->find('list',$params);
		$clientes = array();
		$proveedores = array();
		$this->set(compact('asociados'));
	}
}

<?php
App::uses('AppController', 'Controller');

class TipoDocumentosController extends AppController 
{

    public function index() 
    {
        $this->TipoDocumento->recursive = 0;
        $this->set('tipoDocumentos', $this->paginate());
    }

    public function view($id = null) 
    {
        $this->TipoDocumento->id = $id;
        if (!$this->TipoDocumento->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        $this->set('tipoDocumento', $this->TipoDocumento->read(null, $id));
    }

    public function add() 
    {
        if ($this->request->is('post')) {
            $this->TipoDocumento->create();
            if ($this->TipoDocumento->save($this->request->data)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        }
    }

    public function edit($id = null) 
    {
        $this->TipoDocumento->id = $id;
        if (!$this->TipoDocumento->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->TipoDocumento->save($this->request->data)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        } else {
            $this->request->data = $this->TipoDocumento->read(null, $id);
        }
    }

    public function delete($id = null) 
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->TipoDocumento->id = $id;
        if (!$this->TipoDocumento->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        if ($this->TipoDocumento->delete()) {
            $this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
	$this->redirect(array('action' => 'index'));
    }
}

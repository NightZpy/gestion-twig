<?php
App::uses('AppController', 'Controller');
/**
 * Transiciones Controller
 *
 * @property Transiciones $Transiciones
 */
class TransicionesController extends AppController 
{
    public function index() 
    {
        $conditions = array();
        $this->Transicion->recursive = 0;
        $this->set('transiciones', $this->paginate($conditions));
    }
    
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Transicion->create();
			if ($this->Transicion->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
        }
        
        $estatusIniciales = $this->Transicion->EstatusInicial->find('list',  array('conditions' => array('EstatusInicial.activo' => '1')));
		$estatusFinales = $this->Transicion->EstatusFinal->find('list',  array('conditions' => array('EstatusFinal.activo' => '1')));
		$acciones = $this->Transicion->Accion->find('list',  array('conditions' => array('Accion.activo' => '1')));
        $roles = $this->Transicion->Rol->find('list',  array('conditions' => array('Rol.activo' => '1')));
		
        $this->set(compact('estatusIniciales', 'estatusFinales', 'acciones', 'roles'));
    }
    
    public function edit($id = null)
    {
        $this->Transicion->id = $id;
		if (!$this->Transicion->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Transicion->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->Transicion->read(null, $id);
		}   
        
        $estatusIniciales = $this->Transicion->EstatusInicial->find('list',  array('conditions' => array('EstatusInicial.activo' => '1')));
		$estatusFinales = $this->Transicion->EstatusFinal->find('list',  array('conditions' => array('EstatusFinal.activo' => '1')));
		$acciones = $this->Transicion->Accion->find('list',  array('conditions' => array('Accion.activo' => '1')));
        $roles = $this->Transicion->Rol->find('list',  array('conditions' => array('Rol.activo' => '1')));
		
        $this->set(compact('estatusIniciales', 'estatusFinales', 'acciones', 'roles'));
    }
    
    public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Transicion->id = $id;
		if (!$this->Transicion->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->Transicion->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}
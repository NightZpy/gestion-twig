<?php
App::uses('AppController', 'Controller');
/**
 * TipoInstrumentos Controller
 *
 * @property TipoInstrumento $TipoInstrumento
 */
class TipoInstrumentosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipoInstrumento->recursive = 0;
		$this->set('tipoInstrumentos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->TipoInstrumento->id = $id;
		if (!$this->TipoInstrumento->exists()) {
			throw new NotFoundException(__('Invalid tipo instrumento'));
		}
		$this->set('tipoInstrumento', $this->TipoInstrumento->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TipoInstrumento->create();
			if ($this->TipoInstrumento->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de instrumento ha sido guardado con éxito'),'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de Instrumento de pago no se pudo guardar debido a errores'),'message_error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->TipoInstrumento->id = $id;
		if (!$this->TipoInstrumento->exists()) {
			throw new NotFoundException(__('Invalid tipo instrumento'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TipoInstrumento->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de instrumento de pago fue modificado con éxito'),'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de instrumento de pago no pudo ser guardado debido a errores'),'message_error');
			}
		} else {
			$this->request->data = $this->TipoInstrumento->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TipoInstrumento->id = $id;
		if (!$this->TipoInstrumento->exists()) {
			throw new NotFoundException(__('Invalid tipo instrumento'));
		}
		if ($this->TipoInstrumento->delete()) {
			$this->Session->setFlash(__('Tipo instrumento deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tipo instrumento was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Rubros Controller
 *
 * @property Rubro $Rubro
 */
class RubrosController extends AppController 
{

    public function index() 
    {
		$this->Rubro->recursive = 0;
		$this->set('rubros', $this->paginate());
	}

	public function view($id = null) 
    {
		$this->Rubro->id = $id;
		if (!$this->Rubro->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		$this->set('rubro', $this->Rubro->read(null, $id));
	}

	public function add() 
    {
		if ($this->request->is('post')) {
			$this->Rubro->create();
			if ($this->Rubro->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		}
	}

	public function edit($id = null) 
    {
		$this->Rubro->id = $id;
		if (!$this->Rubro->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Rubro->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->Rubro->read(null, $id);
		}
	}

	public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Rubro->id = $id;
		if (!$this->Rubro->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->Rubro->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

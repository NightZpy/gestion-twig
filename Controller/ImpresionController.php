<?php
App::uses('AppController', 'Controller');

class ImpresionController extends AppController {
    public $uses = array('Persona', 'Impresion', 'Factura', 'Ciudad');
        	
    public function index() {
        $proveedores = $this->Persona->find('list', array(  'conditions' => array('clasificacion_id=3'), 
                                                            'fields'     => array('id', 'razon_social'),
                                                            'order'      => array('Persona.razon_social'), ));
		$this->set(compact('proveedores'));
		if(!empty($this->data)){
			if(isset($this->request->data['Impresion'])){
				$this->Impresion->set($this->request->data['Impresion']);
			}
			if($this->Impresion->validates()){
				$numero_factura = $this->request->data['Impresion']['numero_factura'];
				$proveedor_id = $this->request->data['Impresion']['proveedor_id'];
				$localidad = $this->request->data['Impresion']['localidad'];
				$condicion = "Contado";
				if($this->request->data['Impresion']['condicion'] == 1){$condicion = "A cr�dito";}
				$this->redirect('/Impresion/imprimir/'.$numero_factura.'/'.$proveedor_id.'/'.$localidad.'/'.$condicion);
			}
			else {}
		}
    }
	
    public function getCiudad($proveedor_id) {
		$this->autoRender = FALSE;
		$ciudad = $this->Persona->field('ciudad_id', 'id='.$proveedor_id);
		return $this->Ciudad->field('nombre', 'id='.$ciudad);
	}	
	
    public function imprimir($numero_factura, $proveedor_id, $localidad = "Caracas", $condicion = "Contado") {
		if($this->Factura->find('count',array('conditions' => array('Factura.numero_factura' => $numero_factura, 'Factura.proveedor_id' => $proveedor_id))) > 0){
			require_once(APP. 'Vendor'.DS.'JavaBridgeLibs'.DS.'java'.DS.'Java.inc'); //Incluimos la libreria JavaBridge

			$dirJasperReportsLibs = APP . 'Vendor' .DS. 'JasperReportsLibs' .DS;
			$dirReportes = APP . 'webroot' .DS. 'formatos_facturas' .DS;
			$dirUploads = APP . 'webroot' .DS. 'uploads' .DS. 'facturas' .DS;

			$handle = @opendir($dirJasperReportsLibs);//Creamos una variable que va a contener todas las librer?as que est?n en la carpeta lib_reportes
			$nombre_informe = $this->Persona->field('archivo_reporte', 'id='.$proveedor_id);
			$classpath="";

			while(($lib = readdir($handle)) !== false) {
				$classpath .= 'file:'.$dirJasperReportsLibs.'/'.$lib .';';
			}
			try {
				java_require($classpath); //A?adimos las librer?as
				//Creamos la conexi?n JDBC
				$Conn = new Java("org.altic.jasperReports.JdbcConnection");
				//Seteamos el driver mysql
				$Conn->setDriver("com.mysql.jdbc.Driver");
				//Especificamos los datos de la conexi?n
				$Conn->setConnectString("jdbc:mysql://localhost/gestion");
				$Conn->setUser('root');
				$Conn->setPassword('root');
				/*Creamos una variable tipo arreglo que contendr? los par?metros En este caso el parametro se llama PARAMETRO_EJEMPLO, y el valor es un String */
				$parametrosMap = new Java("java.util.HashMap");
				$parametrosMap->put("numero_factura", new Java('java.lang.String', $numero_factura ));
				$parametrosMap->put("proveedor", new Java('java.lang.Integer', $proveedor_id ));
				$parametrosMap->put("lugar", new Java('java.lang.String', $localidad ));
				$parametrosMap->put("condicion", new Java('java.lang.String', $condicion ));

				//Creamos el objeto JasperReport que permite obtener el reporte
				$sJfm = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
				//Guardamos el reporte en una variable $print para luego exportarla
				$jasperFile = $dirReportes .$nombre_informe.".jasper";
				$print = $sJfm->fillReportToFile( $jasperFile, $parametrosMap, $Conn->getConnection());
				//Exportamos el informe y lo guardamos como pdf en el directorio donde est?n los reportes
				$factura = "{$nombre_informe}-{$numero_factura}.pdf";
				$pdfFile = "{$dirUploads}{$factura}";
				$sJem = new JavaClass("net.sf.jasperreports.engine.JasperExportManager");
				$sJem->exportReportToPdfFile($print, $pdfFile);
				// Marca la factura como que ya ha sido impresa
				$this->Factura->id = $this->Factura->field('id', array('numero_factura' => $numero_factura, 'proveedor_id' => $proveedor_id));
				$this->Factura->saveField('impresa', 1, false);
				//Este c?digo sirve para abrir el archivo generado desde el explorador
				$this->redirect('/uploads/facturas/'.$factura);
			} catch(JavaException $ex) {
				$trace = new Java("java.io.ByteArrayOutputStream");
				$ex->printStackTrace(new Java("java.io.PrintStream", $trace));
				print "java stack trace: $trace\n";
			}
		} else {
			$this->set('mensaje', '<h2>Error:</h2><p>El proveedor <strong>'.$this->Persona->field('razon_social', 'id='.$proveedor_id).'</strong> no tiene asociado el n&uacute;mero de factura <strong>'.$numero_factura.'</strong></p>');
		}
	}
}
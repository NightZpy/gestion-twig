<?php
App::uses('AppController', 'Controller');

class PersonaPersonasController extends AppController 
{
    public $autoRender = false;
    
    public function asociarCliente()
    {
        pr($this->request->data);
        if ($this->request->is('post')) {
			$this->PersonaPersona->create();
			if ($this->PersonaPersona->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $this->request->data['PersonaPersona']['persona_padre_id'], '1'));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		}
    }
    
    public function asociarproveedor()
    {
        pr($this->request->data);
        if ($this->request->is('post')) {
			$this->PersonaPersona->create();
			if ($this->PersonaPersona->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $this->request->data['PersonaPersona']['persona_padre_id'], '2'));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		}
    }
    
    
    public function removerCliente( $id = null, $asociado_id = null ) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PersonaPersona->id = $id;
		if ($this->PersonaPersona->deleteall(array('PersonaPersona.id' => $this->PersonaPersona->id),false)) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $asociado_id, '1'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $asociado_id, '1'));
	}
    
    public function removerproveedor( $id = null, $cliente_id = null )
    {
        if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PersonaPersona->id = $id;
		if ($this->PersonaPersona->deleteall(array('PersonaPersona.id' => $this->PersonaPersona->id),false)) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $cliente_id, '2'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $cliente_id, '2'));
    }
    
    public function listar( $asociado_id = null) 
    {
        $options['joins'] = array(
                                array('table' => 'personas',
                                'alias' => 'Persona',
                                'type' => '',
                                'conditions' => array(
                                                     'Persona.id = PersonaPersona.cliente_id',
                                                     ),
                                      ),
                                );
        $options['conditions'] = array(
            'PersonaPersona.asociado_id' => $asociado_id
        );
        $options['fields'] = array('Persona.id', 'Persona.razon_social');
        $clientes = $this->PersonaPersona->find('list', $options);
        if (count($clientes) > 0) {
            $response = array('success' => true,'clientes' => $clientes);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }
    
    public function comision( $cliente_id = null, $asociado_id = null )
    {
        $ComisionCliente = $this->PersonaPersona->getComisionCliente($cliente_id);
        if (count($ComisionCliente) > 0) {
            $response = array('success' => 'personas','comisiones' => $ComisionCliente);
        } else {
            $ComisionAsociado = $this->PersonaPersona->getComisionAsociado($asociado_id);
            if (count($ComisionAsociado) > 0) {
                $response = array('success' => 'personas','comisiones' => $ComisionAsociado);
            } else {
                $ComisionConfiguracion = $this->PersonaPersona->getComisionConfiguracion();
                if (count($ComisionConfiguracion) > 0) {
                    $response = array('success' => 'comisiones','comisiones' => $ComisionConfiguracion);
                } else {
                    $response = array('success' => false);
                }
            }
        }
        return json_encode($response);
    }
    
    public function asociarConcepto()
    {
        if ($this->request->is('post')) {
			$this->PersonaPersona->create();
			if ($this->PersonaPersona->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('controller' => 'personas', 'action' => 'relacionConcepto', $this->request->data['PersonaPersona']['persona_padre_id']));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		}
    }
    
    public function removerConcepto($id = null, $proveedor_id = null) 
    {
        //echo $id;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		if ($this->PersonaPersona->deleteall(array('PersonaPersona.id' => $id), false)) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
            $this->redirect(array('controller' => 'personas', 'action' => 'relacionConcepto', $proveedor_id));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
	}
}

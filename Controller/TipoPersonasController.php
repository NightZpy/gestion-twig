<?php
App::uses('AppController', 'Controller');
/**
 * TipoPersonas Controller
 *
 * @property TipoPersona $TipoPersona
 */
class TipoPersonasController extends AppController 
{

    public function index() 
    {
		$this->TipoPersona->recursive = 0;
		$this->set('tipoPersonas', $this->paginate());
	}

	public function view($id = null) 
    {
		$this->TipoPersona->id = $id;
		if (!$this->TipoPersona->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		$this->set('tipoPersona', $this->TipoPersona->read(null, $id));
	}

	public function add() 
    {
		if ($this->request->is('post')) {
			$this->TipoPersona->create();
			if ($this->TipoPersona->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		}
	}

	public function edit($id = null) 
    {
		$this->TipoPersona->id = $id;
		if (!$this->TipoPersona->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TipoPersona->save($this->request->data)) {
				$this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->TipoPersona->read(null, $id);
		}
	}

	public function delete($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TipoPersona->id = $id;
		if (!$this->TipoPersona->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->TipoPersona->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
}

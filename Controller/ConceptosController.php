<?php
App::uses('AppController', 'Controller');

class ConceptosController extends AppController 
{
    public $components = array('Search.Prg');
	
    public function index() 
    {
        $conditions = array();
        $this->Concepto->recursive = 0;
        $this->Prg->commonProcess();
        $this->paginate = array('conditions' => $this->Concepto->parseCriteria($this->passedArgs));
        $this->set('conceptos', $this->paginate($conditions));
    }

    public function agregar() 
    {
        $this->_guardar();
        $this->render('form');
    }

    public function editar($id = null) 
    {
        $this->Concepto->id = $id;
        if (!$this->Concepto->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        
        $this->_guardar();
        $this->render('form');
    }
    
    private function _guardar($id=null) 
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Concepto->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
            }
        } else {
            $this->request->data = $this->Concepto->read(null, $id);
        }
        
        $impuestos = $this->Concepto->Impuesto->find('list', array(
                                                            'conditions' => array(
                                                                'activo' => 1, 
                                                                'es_retencion_islr' => 1
                                                            ), 
                                                            'order' => array('Impuesto.valor'),
        ));
        
        $this->set(compact('impuestos'));
    }

    public function eliminar($id = null) 
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        
        $this->Concepto->id = $id;
        if (!$this->Concepto->exists()) {
            throw new NotFoundException('Registro Invalido.');
        }
        
        if ($this->Concepto->delete()) {
            $this->Session->setFlash('Registro eliminado.', 'message_successful');
            $this->redirect(array('action' => 'index'));
        }
        
        $this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
        $this->redirect(array('action' => 'index'));
    }
}
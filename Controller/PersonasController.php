<?php
App::uses('AppController', 'Controller');

class PersonasController extends AppController 
{
    public $autoRender = true;
    public $components = array('Search.Prg', 'Rif', 'RequestHandler');
    public $presetVars = true;
    public $helpers = array('Js');
    
    public function index() 
    {
        $conditions = array();
        $this->_eliminarValidacionEnBusqueda();

        $this->Persona->recursive = 0;
        $this->Prg->commonProcess();
        $this->paginate = array('conditions' => $this->Persona->parseCriteria($this->passedArgs));
        
        $filtro="";
        
        if ($this->UserAuth->isAsociado()) {
                $asociadoId = $this->Session->read('UserAuth.Usuario.persona_id');
                $conditions[] = array('Persona.id' => $this->Persona->getAsociadoClientes($asociadoId));
                $filtro=array('conditions' => array('id !='=>2));
        }

        $this->set('clasificaciones', $this->Persona->Clasificacion->find('list',$filtro));
        $this->set('tipoDocumentos', $this->Persona->TipoDocumento->find('list'));
        $this->set('personas', $this->paginate($conditions));
    }
    
    private function _eliminarValidacionEnBusqueda() {
        $this->Persona->validator()->remove('codigo');
        $this->Persona->validator()->remove('razon_social');
        $this->Persona->validator()->remove('tipo_documento_id');
        $this->Persona->validator()->remove('documento');
        $this->Persona->validator()->remove('clasificacion_id');
    }
    
    public function find() 
    {
        $conditions = array();
        $this->Persona->recursive = 0;
        $this->Prg->commonProcess();
            if ($this->UserAuth->isAsociado()) {
                $this->passedArgs['clasificacion_id'] = 3;
            }
		
        $this->paginate = array('conditions' => $this->Persona->parseCriteria($this->passedArgs));

        $estados = $this->Persona->Estado->find('list', array('order' => 'Estado.nombre ASC'));
        $ciudades = $this->Persona->Ciudad->find('list', array('order' => 'Ciudad.nombre ASC'));
        $conceptos = $this->Persona->Concepto->find('list', array('order' => 'Concepto.nombre ASC', 'fields' => 'id, nombre'));
        $this->set('personas', $this->paginate());
        $this->set(compact('estados', 'ciudades', 'conceptos'));
        $this->layout = 'greybox';
    }
    
    public function relacionConcepto($id = null) 
    {
        $this->Persona->id = $id;
        $condicion = '';
        if ($this->request->is('post')) {
            if($this->request->data['Concepto']['nombre']) {
                $condicion = " AND Concepto.nombre LIKE '%".$this->request->data['Concepto']['nombre']."%'";
            }
        }
		        
        if (!$this->Persona->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
		$this->set('persona', $this->Persona->read(null, $id));
		$this->set('conceptos_disponibles', $this->Persona->getConceptosDisponibles($this->Persona->id, $condicion));
        $this->set('conceptos_asociados', $this->Persona->getConceptosAsociados($this->Persona->id));
	}
    
    public function relacionCliente($id = null, $tipo_relacion = null) 
    {
        $this->Persona->id = $id;
        $conditions = array();
        $conditions['Persona'] = '';
        $this->Persona->recursive = 0;
        if ($this->request->is('post')) {
            if($this->request->data['Persona']['codigo']){
                $conditions['Persona'] = " AND Persona.codigo LIKE '%". $this->request->data['Persona']['codigo']."%'";
            }
            if($this->request->data['Persona']['razon_social']){
                $conditions['Persona'] = " AND Persona.razon_social LIKE '%".$this->request->data['Persona']['razon_social']."%'";
            }
            if($this->request->data['Persona']['documento']){
                $conditions['Persona'] = "AND Persona.documento LIKE '%".$this->request->data['Persona']['documento']."%'";
            }
            if($this->request->data['Persona']['clasificacion_id']){
                $conditions['Persona'] = " AND Persona.clasificacion_id = '". $this->request->data['Persona']['clasificacion_id']."'";
            }
        }
        
        if (!$this->Persona->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
		$this->set('persona', $this->Persona->read(null, $id));
        
        $clasificaciones = $this->Persona->Clasificacion->find('list');
        $this->set(compact('clasificaciones'));
        if ($tipo_relacion == 1){
            $this->set('personasDisponibles', $this->Persona->getClientesDisponibles());
            $this->set('personasRelacionadas', $this->Persona->getClientesRelacionadas($this->Persona->id));
        } else if ($tipo_relacion == 2){
            $this->set('personasDisponibles', $this->Persona->getProveedoresFinalesDisponibles($this->Persona->id));
            $this->set('personasRelacionadas', $this->Persona->getProveedoresFinalesRelacionadas($this->Persona->id));
        }
        $this->set(compact('tipo_relacion', 'id'));
	}
        
	public function agregar() 
    {
        $this->_guardar();
        $this->render('form');
	}

	public function editar($id = null) 
    {
        //$this->Persona->recursive=-1;
        $this->Persona->id = $id;
		if (!$this->Persona->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
        $this->_guardar($id);
        $this->render('form');
		
	}
    
    private function _guardar($id = null) {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Persona->create();
           
           //$this->_obtenerDatosFiscales();        

            echo "<br><b>Data: "; 
            echo "<pre>";
            print_r($this->request->data);
            echo "</pre>";
			if ($this->Persona->save($this->request->data)) {
                $lastID = $this->Persona->getInsertID();
                $adjuntos = $this->request->data['Adjunto'];
                for ($i=0; $i < count($adjuntos) ; $i++) { 
                    $adjuntos[$i]['persona_id'] = $lastID;
                    $archivo = $adjuntos[$i]['ruta'][0];                      
                    $prefijo = substr(md5(uniqid(rand())),0,6);
                    $adjuntos[$i]['nombre'] = $prefijo.$archivo['name'];
                    $destino =  "uploads/media/image/adjuntos/".$adjuntos[$i]['nombre'];
                    $adjuntos[$i]['ruta'] = $destino;
                    copy($archivo['tmp_name'],$destino);                 
                }                
                echo "<br><b>Adjuntos: "; 
                echo "<pre>";
                print_r($adjuntos);
                echo "</pre>";
                $this->loadModel('Adjunto');               
                if($this->Adjunto->saveAll($adjuntos)){
                    $cuentas = $this->request->data['Cuenta'];
                    for ($i=0; $i < count($cuentas) ; $i++) { 
                        $cuentas[$i]['persona_id'] = $lastID;
                    } 
                    $this->loadModel('Cuenta');    
                    if($this->Cuenta->saveAll($cuentas)){
                        $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                        $this->redirect(array('action' => 'index'));                        
                    } else {
                        //debug($this->validationErrors); die();
                       $this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
                    }
                } else {
                    $this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
                }              
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		} else {
			$this->request->data = $this->Persona->read(null, $id);
		}
        

        $estado = isset($this->request->data['Persona']['estado_id']) ? $this->request->data['Persona']['estado_id']:0;
		$tipoPersonas = $this->Persona->TipoPersona->find('list');
		if ($this->UserAuth->isAsociado()) {
			$clasificaciones = $this->Persona->Clasificacion->find('list',array('conditions'=>array('id'=>'1')));
			if (count($clasificaciones) == 1) {
				$this->set('seleccionado', array_keys($clasificaciones));
			}
		}else{
			$clasificaciones = $this->Persona->Clasificacion->find('list');
		}
		
        $estados = $this->Persona->Estado->find('list', array('order' => 'Estado.nombre ASC'));
        $ciudades = $this->Persona->Ciudad->find('list', array('conditions' => array('Ciudad.estado_id' => $estado), 'order' => 'Ciudad.nombre ASC'));
		
        $tipoDocumentos = $this->Persona->TipoDocumento->find('list');
		if ($this->request->data){
			$adjuntos = $this->request->data('Adjunto');
		}else{
			$adjuntos =array();
		}

        $rubros = $this->Persona->Rubro->find('list');
        $grupos = $this->Persona->Grupo->find('list');
		
		$this->set(compact('tipoPersonas', 'grupos', 'rubros', 'clasificaciones', 'tipoDocumentos', 'estados', 'ciudades', 'adjuntos'));
    }
    
    public function obtenerDatosFiscales($rif) 
    {
        $this->autoRender = false;
        return $this->Rif->getInfo($rif);
    }
    
    private function _obtenerDatosFiscales()  
    {
        $rif = trim($this->data['Persona']['tipo_documento_id']) . trim($this->data['Persona']['documento']);
        if (!empty($rif)) {
            $informacionFiscal = json_decode($this->Rif->getInfo($rif));
            if ($informacionFiscal->code_result == 1) {
                if (empty($this->request->data['Persona']['razon_social'])) {
                    $this->request->data['Persona']['razon_social'] = $informacionFiscal->seniat->nombre;
                }

                $this->request->data['Persona']['contribuyente_especial'] = $informacionFiscal->seniat->agenteretencioniva;

                if (empty($this->request->data['Persona']['porcentaje_retencion_iva'])) {
                    $this->request->data['Persona']['porcentaje_retencion_iva'] = $informacionFiscal->seniat->tasa;
                }
            }
        }
    }

	public function eliminar($id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Persona->id = $id;
		if (!$this->Persona->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
		if ($this->Persona->delete()) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('action' => 'index'));
	}
    
    /*public function restauraDatos(){
        $this->autoRender = false;
        //echo header("Refresh: 1; url=http://gestion.dev/personas/restauraDatos");
        $personas = $this->Persona->obtenerRif();
        if(count($personas))
        foreach ($personas as $persona){
            $obj = new stdClass();
            $obj = json_decode($this->obtenerDatosFiscales($persona[0]['rif']));
            pr($obj);
            if(!count($obj->seniat)){
                $SQL = "UPDATE personas SET validado = 2
                                   WHERE 
                                        documento = '" . $persona['personas']['documento'] ."'";
                $this->Persona->query($SQL);
            }else{
                $SQL = "UPDATE personas SET validado = 1, 
                                        documento = '" . $persona[0]['rif2'] . "',
                                        razon_social = '" . $obj->seniat->nombre . "',
                                        contribuyente_especial = '" . $obj->seniat->agenteretencioniva . "',
                                        porcentaje_retencion_iva = '" . $obj->seniat->tasa . "'
                                   WHERE 
                                        documento = '" . $persona['personas']['documento'] ."'";
                $this->Persona->query($SQL);
            }
            echo $SQL;
            pr($obj->seniat);
            unset($obj);
        }
    }*/
    
    public function ajaxGetDialogoPersona(){           
        $this->Persona->unbindModel(array(
            'belongsTo'=>array('TipoPersona', 'Estado', 'Ciudad', 'TipoDocumento'),
            'hasMany'=>array('Cuenta','Factura')));
        $this->Persona->recursive = 1;
        $this->layout="ajax";
        if ($this->request->is('post')){
            //$personas = $this->Persona->find('all', array('conditions'=>array('Persona.clasificacion_id'=>$tipoPersonaId)));
            
        }
        
        $this->Prg->commonProcess();
        
        $this->paginate = array('limit'=>10,'order'=>array('Persona.razon_social'=>'asc'),'conditions' => $this->Persona->parseCriteria($this->passedArgs));
        app::import('model', 'Concepto');
        $c = new Concepto();
        $conceptos=$c->find('list');
        
        $this->set('personas', $this->paginate());
        $this->set('conceptos', $conceptos);
        
        $this->render('dialogoAjax');
    }
    
}

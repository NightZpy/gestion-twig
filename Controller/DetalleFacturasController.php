<?php
App::uses('AppController', 'Controller');

class DetalleFacturasController extends AppController 
{
   
    public function index() 
    {
        $this->DetalleFactura->unbindModel(array('belongsTo' => array('Factura')));
        $this->set('detallefacturas', $this->paginate());
    }

    public function editar($factura_id = null, $id = null ) 
    {
        //QUITAR DEL ARREGLO EL INDICE descripcion_id
        /*if(count($this->request->data['DetalleFactura']) == 9){
            array_splice($this->request->data['DetalleFactura'], 4, 1);
        }*/
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->DetalleFactura->create(); 
            if ($this->DetalleFactura->save($this->request->data)) {
                $this->Session->setFlash(__('Registro guardado con éxito.'), 'message_successful');
                $this->redirect(array(
                                    'controller' => 'facturas', 
                                    'action' => 'editar', 
                                    $this->request->data['Factura']['requerimiento_id'], 
                                    $this->request->data['Factura']['id'],
                               ));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        }
	}
    
    public function editarInGrid() 
    {
        $id=$this->request->data['id'];
        $valor=$this->request->data['valor'];
        $campo=$this->request->data['campo'];
        
        $this->DetalleFactura->read(null, $id);
        
        $reqId = $this->DetalleFactura->data['Factura']['requerimiento_id'];
        $facturaId = $this->DetalleFactura->data['DetalleFactura']['factura_id'];
        
        $this->DetalleFactura->data['DetalleFactura']['factura_id']=$this->DetalleFactura->data['DetalleFactura']['factura_id'];
        switch ($campo){
            case 'can':
                $this->DetalleFactura->data['DetalleFactura']['cantidad'] = $valor;
                break;
            case 'dsc':
                $this->DetalleFactura->data['DetalleFactura']['descripcion'] = $valor;
                break;
            case 'des':
                $this->DetalleFactura->data['DetalleFactura']['concepto_id'] = $valor;
                //$this->DetalleFactura->saveField('descripcion',$valor);
                break;
            case 'iva':
                $this->autoRender=false;
                $this->DetalleFactura->data['DetalleFactura']['impuesto_iva_id'] = $valor;
                $this->DetalleFactura->save();
                $this->DetalleFactura->read(null, $id);
                $this->DetalleFactura->data['Factura']['monto_iva'] = CakeNumber::currency($this->DetalleFactura->data['Factura']['monto_iva'], 'VEF');
                $this->DetalleFactura->data['Factura']['monto_retencion_iva'] = CakeNumber::currency($this->DetalleFactura->data['Factura']['monto_retencion_iva'], 'VEF');
                return json_encode($this->DetalleFactura->data);
                //$this->DetalleFactura->saveField('descripcion',$valor);
                break;
            case 'pre':
                $this->DetalleFactura->data['DetalleFactura']['precio_unitario'] = $valor;
                //$this->DetalleFactura->saveField('descripcion',$valor);
                break;
        }
        
        $this->DetalleFactura->save();
        
        $this->redirect(array(
                            'controller' => 'facturas', 
                            'action' => 'editar', 
                            $reqId, 
                            $facturaId,
                       ));
	}
    
    
    public function delete($id = null, $requerimiento_id = null, $factura_id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->DetalleFactura->id = $id;
        $this->DetalleFactura->read();
		if (!$this->DetalleFactura->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}

        if ($this->DetalleFactura->delete()) {
			/*
             * Agrupamos la tabla detalle_facturas
            */
            $montosDetalleFactura = ($this->DetalleFactura->montosDetalleFacturas($factura_id));
            /*
             * Buscamos el porcentaje_retencion_iva de la tabla facturas.
            */
            $porcentajeRetencionIva = $this->DetalleFactura->Factura->find('first',
                                            array('conditions' => 
                                              array('Factura.id' => $factura_id),
                                                    'fields' => 'porcentaje_retencion_iva', 
                                                 ));
            /*
             * Verificamos si el array() $montosDetalleFactura esta vacio
             * En caso de estar vacio significa que no hay registros en la tabla detalle_facturas para la factura $factura_id
            */
            if (count($montosDetalleFactura) == 0) {
                /*
                 * Preparamos el modelo Factura para guardar los datos ya actualizados
                 * Si el array() $montosDetalleFactura esta vacio el modelo Factura sera seteado con '0'
                */
                $facturas = array('Factura' => array(
                                                'id' => $factura_id,
                                                'monto_base' => 0,
                                                'monto_iva' => 0,
                                                'monto_total' => 0,
                                                /*'saldo_deudor_monto_base' => 0,
                                                'saldo_deudor_monto_iva' => 0,
                                                'saldo_deudor_monto_total' => 0,*/
                                                'monto_retencion_islr' => 0,
                                                'monto_retencion_iva' => 0,
                                                'monto_maximo' => 0,
                                                'porcentaje_retencion_iva' => 0,
                                                'monto_costo_gestion' => 0,
                                                'monto_costo_operativo' => 0,
                                                'monto_costo_venta' => 0,
                                                'monto_costo_venta_secundario' => 0,
                                                'monto_costo_administrativo' => 0,
                                                    )
                                 );  
            } else {
                /*
                 * Preparamos el modelo Factura para guardar los datos ya actualizados
                 * Si el array() $montosDetalleFactura tiene datos el modelo Factura sera actualizado con los datos agrupados
                 * en el array() $montosDetalleFactura
                */
                $facturas = array('Factura' => array(
                                                'id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id'],
                                                'monto_base' => $montosDetalleFactura[0][0]['monto_base'],
                                                'monto_iva' => $montosDetalleFactura[0][0]['monto_iva'],
                                                'monto_total' => $montosDetalleFactura[0][0]['monto_total'],
                                                /*'saldo_deudor_monto_base' => $montosDetalleFactura[0][0]['monto_base'],
                                                'saldo_deudor_monto_iva' => $montosDetalleFactura[0][0]['monto_iva'],
                                                'saldo_deudor_monto_total' => $montosDetalleFactura[0][0]['monto_total'],*/
                                                'monto_retencion_islr' => $montosDetalleFactura[0][0]['monto_islr'],
                                                'monto_retencion_iva' => $montosDetalleFactura[0][0]['monto_iva'] * 
                                                                         $porcentajeRetencionIva['Factura']['porcentaje_retencion_iva'] / 
                                                                         100,
                                                     )
                                );
                $fact = ClassRegistry::init('Factura');
                $fact->save($facturas);
                
                
                $montosFacturasCostos = ($this->DetalleFactura->montosFacturasCostos($factura_id));
                $facturas = array('Factura' => array(
                                                    'id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id'],
                                                    'monto_costo_gestion' => $montosFacturasCostos[0][0]['monto_costo_gestion'],
                                                    'monto_costo_operativo' => $montosFacturasCostos[0][0]['monto_costo_operativo'],
                                                    'monto_costo_venta' => $montosFacturasCostos[0][0]['monto_costo_venta'],
                                                    'monto_costo_venta_secundario' => $montosFacturasCostos[0][0]['monto_costo_venta_secundario'],
                                                    'monto_costo_administrativo' => $montosFacturasCostos[0][0]['monto_costo_administrativo'],
                                                    )
                                 );
                $fact = ClassRegistry::init('Factura');
                $fact->save($facturas);
        
            }
            /*
             * Guardamos el Modelo Factura 
            */
            $fact = ClassRegistry::init('Factura');
            $fact->save($facturas);
            
            
            /*
             * Agrupamos la tabla facturas
            */
            $montosFactura = ($this->DetalleFactura->montosFacturas($requerimiento_id));
            /*
             * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
             * Si el array() $montosFactura tiene datos el modelo Factura sera actualizado con los datos agrupados
             * en el array() $montosFactura
            */
            if (count($montosFactura) == 0) {
                /*
                 * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
                 * Si el array() $montosFactura esta vacio el modelo Requerimiento sera seteado con '0'
                */
                $requerimientos = array('Requerimiento' => array(
                                                    'id' => $requerimiento_id,
                                                    'monto_costo_gestion' => 0,
                                                    'monto_costo_operativo' => 0,
                                                    'monto_costo_venta' => 0,
                                                    'monto_costo_venta_secundario' => 0,
                                                    'monto_costo_administrativo' => 0,
                                                    'monto_base' => 0,
                                                    'monto_iva' => 0,
                                                    'monto_total' => 0,
                                                    'monto_retencion_islr' => 0,
                                                    'monto_retencion_iva' => 0,
                                                    )
                                 );
            } else {
                /*
                 * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
                 * Si el array() $montosFactura tiene datos el modelo Requerimiento sera actualizado con los datos agrupados
                 * en el array() $montosFactura
                */
                $requerimientos = array('Requerimiento' => array(
                                                    'id' => $montosFactura[0]['Factura']['requerimiento_id'],
                                                    'monto_costo_gestion' => $montosFactura[0][0]['monto_costo_gestion'],
                                                    'monto_costo_operativo' => $montosFactura[0][0]['monto_costo_operativo'],
                                                    'monto_costo_venta' => $montosFactura[0][0]['monto_costo_venta'],
                                                    'monto_costo_venta_secundario' => $montosFactura[0][0]['monto_costo_venta_secundario'],
                                                    'monto_costo_administrativo' => $montosFactura[0][0]['monto_costo_administrativo'],
                                                    'monto_base' => $montosFactura[0][0]['monto_base'],
                                                    'monto_iva' => $montosFactura[0][0]['monto_iva'],
                                                    'monto_total' => $montosFactura[0][0]['monto_total'],
                                                    'monto_retencion_islr' => $montosFactura[0][0]['monto_retencion_islr'],
                                                    'monto_retencion_iva' => $montosFactura[0][0]['monto_retencion_iva'],
                                                    )
                                 );
            }
            /*
             * Guardamos el Modelo Requerimiento 
            */
            $reque = ClassRegistry::init('Requerimiento');
            $reque->save($requerimientos);
        
            $this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
            $this->redirect(array('controller' => 'facturas', 'action' => 'editar', $requerimiento_id, $factura_id));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
        $this->redirect(array('controller' => 'facturas', 'action' => 'editar', $requerimiento_id, $factura_id));
	}
    
    public function buscarDescripcion( $concepto_id = null ){
        $this->autoRender=FALSE;
        
        $descripcion = $this->DetalleFactura->find('list', array('conditions'=>array('concepto_id' => $concepto_id), 
                                                    'fields' => 'descripcion',
                                                    'group' => 'descripcion',
                                                  ));
        if (count($descripcion) > 0) {
            $response = array('success' => true,'Descripcion' => $descripcion);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }
}

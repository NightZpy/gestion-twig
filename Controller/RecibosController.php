<?php

class RecibosController extends AppController {

	public $components = array('Search.Prg');
    public $presetVars = true;
	
	public function index() {
		$this->Prg->commonProcess();
		
		$datos = $this->request->data;
		
		if (!$this->UserAuth->isAsociado()){
			$params['conditions'] = array('clasificacion_id' => 2);
			$asociados = $this->Recibo->Asociado->find('list', $params);
			if (isset($datos['Recibo']['asociado_id'])){
				$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento($datos['Recibo']['asociado_id']);
			}else{
				$params['conditions'] = array('clasificacion_id' => 1);
				$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento();
			}
		}else{
			$asociados = $this->UserAuth->getUserId();
			$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento($asociados);
		}		
		
		$this->paginate = array('limit'=>10, 'conditions' => $this->Recibo->parseCriteria($this->passedArgs));
		
		$this->set('recibos', $this->paginate());
		$this->set(compact('asociados', 'clientes'));
	}

	public function add() {
		if ($this->request->is('post')) {
			if (!$this->Recibo->save($this->request->data)) {
				$this->Session->setFlash(__('El instrumento de pago no pudo ser guardado debido a errores'), 'message_error');
			} else {
				$this->Session->setFlash(__('El instrumento de pago ha sido guardado con éxito'), 'message_successful');
				$this->redirect(array('action' => 'edit', $this->Recibo->id));
			}
		}

		$clientes = array();
		$proveedores = array();
		if ($this->UserAuth->isAsociado()){
			//$this->request->data['Recibo']['asociado_id'] = $this->UserAuth->getUserId();
			$this->request->data['Recibo']['asociado_id'] = $this->Session->read('UserAuth.Persona.id');
		}else{
			$params['conditions'] = array('clasificacion_id' => 2);
			$asociados = $this->Recibo->Asociado->find('list', $params);
		}
		
		if (isset($this->request->data['Recibo'])){
			if (isset($this->request->data['Recibo']['asociado_id'])){
				$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento($this->request->data['Recibo']['asociado_id']);
			}
			if (isset($this->request->data['Recibo']['cliente_id'])){
				$proveedores = $this->Recibo->Instrumento->getProveedoresEnFacturas($this->request->data['Recibo']['cliente_id']);
			}
			
		}
		
		$this->set(compact('asociados', 'clientes', 'proveedores'));
	}

	public function edit($id = NULL) {
		if (!isset($id)) {
			$this->redirect(array('action' => 'add'));
		}

		$this->Recibo->id = $id;

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->request->data['accion'] == 'detalle' || $this->request->data['accion'] == 'detalle-editar') {
				
				if ($this->request->data['accion'] == 'detalle-editar') {
						$this->Recibo->Instrumento->id = $this->request->data['Instrumento']['id'];
				}
				if ($this->Recibo->Instrumento->save($this->request->data)) {
					$this->Session->setFlash(__('El instrumento de pago ha sido guardado con éxito'), 'message_successful');
				} else {
					$this->Session->setFlash(__('Hubo un error guardando el instrumento de pago'), 'message_error');
				}
			} else {
				if ($this->Recibo->save($this->request->data)) {
					$this->Session->setFlash(__('El Recibo ha sido guardado con éxito'), 'message_successful');
				} else {
					$this->Session->setFlash(__('Hubo un error guardando el Recibo'), 'message_error');
				}
			}

		}

		if (!$this->Recibo->exists()) {
			throw new NotFoundException(__('El Recibo no existe'));
		}
		$this->Recibo->recursive = 1;
		$instrumentos = $this->Recibo->getInstrumentos();
		$facturaRecibos = $this->Recibo->getFacturas();
		$totalesRecibo = $this->Recibo->getTotalesRecibo();
		$this->request->data = $this->Recibo->read(null, $id);
		$this->request->data['Recibo']['fechafld'] = $this->request->data['Recibo']['fecha'];
		$params['conditions'] = array('clasificacion_id' => 2);
		$asociados = $this->Recibo->Instrumento->Asociado->find('list', $params);
		$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento($this->request->data['Recibo']['asociado_id']);
		$proveedores = $this->Recibo->Instrumento->getProveedoresEnFacturas($this->request->data['Recibo']['cliente_id']);
		$tipoInstrumentos = $this->Recibo->Instrumento->TipoInstrumento->find('list');
		$bancos = $this->Recibo->Instrumento->Banco->find('list');
		if (isset($this->request->data['Recibo']['proveedor_id'])) {
			$cuentas = $this->Recibo->Instrumento->getCuentasProveedor($this->request->data['Recibo']['proveedor_id']);
			$instrumentosConciliados = $this->Recibo->Instrumento->getInstrumentosConciliados($this->request->data['Recibo']['proveedor_id']);
		}

		$this->set(compact('asociados', 'clientes', 'proveedores', 'tipoInstrumentos', 'bancos', 'cuentas', 'totalesRecibo', 'instrumentos', 'facturaRecibos', 'instrumentosConciliados'));
	}
	
	public function delete($id = NULL){
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
        
		$this->Recibo->id = $id;
		
		if (!$this->Recibo -> exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
        
		if ($this->Recibo->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
        
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
	
	public function deleteInstrumento($id, $retorno){
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$this->Recibo->Instrumento->id = $id;
		$this->Recibo->Instrumento->read();
		
		$this->Recibo->read(null, $this->Recibo->Instrumento->data['Instrumento']['recibo_id']);
		$totalesRecibo = $this->Recibo->getTotalesRecibo();
		
		$disponible = $recibido = 0;
		foreach ($totalesRecibo as $total){
			$disponible += $total['disponible'];
			$recibido += $total['recibido'];
			
		}
		
		if ($recibido-$disponible == 0){
			if (!$this->Recibo->Instrumento->exists()) {
				throw new NotFoundException(__('No existe el Instrumento'), 'message_error');
			}

			if ($this->Recibo->Instrumento->data['Instrumento']['estatus_instrumento_id'] == 1){
				//if ($this->Recibo->Instrumento->delete()) {
                if ($this->Recibo->Instrumento->saveField('recibo_id', null)) {
					$this->Session->setFlash(__('Instrumento eliminado'), 'message_successful');
					$this->redirect(array('action' => 'edit', $retorno));
				}
			}else{
				if ($this->Recibo->Instrumento->saveField('recibo_id', null)) {
					$this->Session->setFlash(__('Instrumento eliminado'), 'message_successful');
					$this->redirect(array('action' => 'edit', $retorno));
				}
			}
		}else{
			$this->Session->setFlash(__('No es posible eliminar ningún instrumento hasta que no se eliminen las facturas, o todos sus abonos esten en cero'), 'message_error');
			$this->redirect(array('action' => 'edit', $retorno));
		}
		
		$this->Session->setFlash(__('Instrumento was not deleted'), 'message_error');
		$this->redirect(array('action' => 'edit', $retorno));
	}

	public function getClientes() {
		$this->autoRender = FALSE;
		$this->layout = "ajax";
		$asociado = $this->request->data;
		$asociadoId = $asociado['Recibo']['asociado_id'];
		$html = "";
		if ($asociadoId > 0) {
			$clientes = $this->Recibo->Instrumento->getPersonasConRequerimiento($asociadoId);
			foreach ($clientes as $key => $value) {
				$html .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}

		return $html;
	}

	public function getProveedores() {
		$this->autoRender = FALSE;
		$this->layout = "ajax";
		$cliente = $this->request->data;
		$clienteId = $cliente['Recibo']['cliente_id'];
		$html = "";
		if ($clienteId > 0) {
			$proveedores = $this->Recibo->Instrumento->getProveedoresEnFacturas($clienteId);
			foreach ($proveedores as $key => $value) {
				$html .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}

		return $html;
	}

	public function getCuentasBancarias() {
		$this->autoRender = FALSE;
		$this->layout = "ajax";
		$request = $this->request->data;
		$proveedorId = $request['Recibo']['proveedor_id'];
		$html = "";
		if ($proveedorId > 0) {
			$options = array(
				'recursive' => 0,
				'fields' => 'Cuenta.id, Cuenta.numero, Banco.nombre',
				'conditions' => array(
					'persona_id' => $proveedorId,
				),
				'order' => array('Banco.Nombre, Cuenta.numero')
			);

			$lista = $this->Recibo->Instrumento->Cuenta->find('list', $options);
			foreach ($lista as $key => $value) {
				foreach ($value as $cuenta => $row)
					$html .= '<option value="' . $cuenta . '">' . $key . " - " . $row . '</option>';
			}
		}

		return $html;
	}
	
	public function getCamposRequeridos($tipoInstrumentoId){
		$this->autoRender = false;
		$campos = $this->Recibo->Instrumento->getCamposRequeridos($tipoInstrumentoId);
		return json_encode($campos);
	}
	
	public function getFacturasPendientes(){
		$this->layout = 'ajax';
		$this->Recibo->id = $this->request->data['recibo_id'];
		$page = isset($this->request->named['page'])?$this->request->named['page']:0;
		$facturas = $this->Recibo->getFacturasPendientes($this->request->data['Factura'], $page);
		$totales = $this->Recibo->getTotalesRecibo();
		$this->set(compact('facturas','page','totales'));
	}
	
	public function agregaFactura(){
		$this->autoRender = false;
		
		if ($this->request->is('post')){
			$datos = $this->request->data;
		}
		
		$dataSource = $this->Recibo->getDataSource();
		$dataSource->begin();
		
		App::import('model', 'Factura');
		$factura = new Factura();
		$factura->read(null, $datos['FacturaRecibo']['factura_id']);
		
		App::import('model', 'FacturaRecibo');
		$facturaRecibo = new FacturaRecibo();
        $reciboId = $datos['FacturaRecibo']['recibo_id'];;
		$this->Recibo->id = $reciboId;
		$totales = $this->Recibo->getTotalesRecibo();
        $Pendientes= $this->Recibo->getMontosPendientes($factura->data['Factura']['id']);
        $montosPendientes=$Pendientes[0];
        
		$disponible=0;
        $datos['FacturaRecibo']['abono_monto_base']=0;
        $datos['FacturaRecibo']['abono_monto_islr']=0;
        $datos['FacturaRecibo']['abono_monto_iva']=0;
        foreach ($totales as $key => $total){
            $disponible += $total['disponible'];
            switch ($key){
                case 1:
                    if ($total['disponible'] > 0 and $this->request->data['automatico']=="true"){
                        //if ($total['disponible'] >= $factura->data['Factura']['saldo_deudor_monto_base']){
                        if ($total['disponible'] >= $montosPendientes[0]['saldo_deudor_monto_base']){
                            //$datos['FacturaRecibo']['abono_monto_base'] = $factura->data['Factura']['saldo_deudor_monto_base'];
                            $datos['FacturaRecibo']['abono_monto_base'] = $montosPendientes[0]['saldo_deudor_monto_base'];
                            $factura->set('saldo_deudor_monto_base', 0);
                        }else{
                            $datos['FacturaRecibo']['abono_monto_base'] = $total['disponible'];
                            //$factura->set('saldo_deudor_monto_base', $factura->data['Factura']['saldo_deudor_monto_base'] - $total['disponible']);
                        }
                        $factura->set('monto_maximo', $factura->data['Factura']['monto_maximo']-$datos['FacturaRecibo']['abono_monto_base']);
                    }

                    break;
                case 2:
                    if ($total['disponible'] > 0 and $this->request->data['automatico']=="true"){
                        //if ($total['disponible'] >= $factura->data['Factura']['saldo_deudor_monto_islr']){
                        if ($total['disponible'] >= $montosPendientes[0]['saldo_deudor_monto_islr']){
                            //$datos['FacturaRecibo']['abono_monto_islr'] = $factura->data['Factura']['saldo_deudor_monto_islr'];
                            $datos['FacturaRecibo']['abono_monto_islr'] = $montosPendientes[0]['saldo_deudor_monto_islr'];
                            $factura->set('saldo_deudor_monto_islr', 0);
                        }else{
                            $datos['FacturaRecibo']['abono_monto_islr'] = $total['disponible'];
                            //$factura->set('saldo_deudor_monto_islr', $factura->data['Factura']['saldo_deudor_monto_islr'] - $total['disponible']);
                        }
                        $factura->set('monto_maximo', $factura->data['Factura']['monto_maximo']-$datos['FacturaRecibo']['abono_monto_islr']);
                    }						
                    break;
                case 3:
                    if ($total['disponible'] > 0 and $this->request->data['automatico']=="true"){
                        //if ($total['disponible'] >= $factura->data['Factura']['saldo_deudor_monto_iva']){
                        if ($total['disponible'] >= $montosPendientes[0]['saldo_deudor_monto_iva']){
                            //$datos['FacturaRecibo']['abono_monto_iva'] = $factura->data['Factura']['saldo_deudor_monto_iva'];
                            $datos['FacturaRecibo']['abono_monto_iva'] = $montosPendientes[0]['saldo_deudor_monto_iva'];
                            $factura->set('saldo_deudor_monto_iva', 0);
                        }else{
                            $datos['FacturaRecibo']['abono_monto_iva'] = $total['disponible'];
                            //$factura->set('saldo_deudor_monto_iva', $factura->data['Factura']['saldo_deudor_monto_iva'] - $total['disponible']);
                        }
                        $factura->set('monto_maximo', $factura->data['Factura']['monto_maximo']-$datos['FacturaRecibo']['abono_monto_iva']);
                    }
                    break;
            }
        }


		$error = FALSE;
		
		if ($disponible == 0){
			return "NO";
		}
		
		if (!$facturaRecibo->save($datos) || !$factura->save()){
			$error = TRUE;
		}
        
        $this->_actualizaEstatusRecibo($reciboId);

		if ($error == TRUE){
			$dataSource->rollback();
		}else{
			$dataSource->commit();
		}
		
		return "OK";
		
	}
	
	public function deleteFactura($facturaReciboId){
		App::import('model', 'FacturaRecibo');
		App::import('model', 'Factura');
		$facturaRecibo = new FacturaRecibo();
		$facturaRecibo->read(null, $facturaReciboId);
		
		$facturaId = $facturaRecibo->data['FacturaRecibo']['factura_id'];
		$reciboId = $facturaRecibo->data['FacturaRecibo']['recibo_id'];		
		
		$factura = new Factura();
		$factura->read(null, $facturaId);
		
		$error = FALSE;
		$dataSource = $this->Recibo->getDataSource();
		$dataSource->begin();

        $montoBase=$facturaRecibo->data['FacturaRecibo']['abono_monto_base'];
        $montoISLR=$facturaRecibo->data['FacturaRecibo']['abono_monto_islr'];
        $montoIVA=$facturaRecibo->data['FacturaRecibo']['abono_monto_iva'];
		
		if ($facturaRecibo->delete()){
			$this->Session->setFlash(__('Factura eliminada del recibo'), 'message_successful');	
			
            $factura->data['Factura']['monto_maximo'] = $factura->data['Factura']['monto_maximo']+$montoBase+$montoISLR+$montoIVA;
			$factura->data['Factura']['pagado'] = 0;
			if (!$factura->save()){
				$error = TRUE;
			}
            $this->_actualizaEstatusRecibo($reciboId);
		}else{
			$error = TRUE;
		}
		
		if ($error == TRUE){
			$dataSource->rollback();
			$this->Session->setFlash(__('No se pudo eliminar la factura del recibo'), 'message_error');	
		}else{
			$dataSource->commit();
		}
		
		if (isset($this->request->data['via'])){
			if ($this->request->data['via']=='ajax'){
				$this->autoRender = false;
				return "OK";
			}
		}else{
			$this->redirect('/recibos/edit/'.$reciboId);
		}
		
		
	}
	
	public function actualizaMonto(){
		
        App::import('model', 'FacturaRecibo');
		App::import('model', 'Factura');
		$this->autoRender=false;
				
		$campo = $this->request->data['campo'];
		
		$datos['id'] =  $this->request->data['factura_recibo_id'];
		
		$reciboId = $this->request->data['recibo_id'];
		$facturaId = $this->request->data['factura_id'];
		
		$dataSource = $this->Recibo->getDataSource();
		$dataSource->begin();
		$error = 0;
		
		$factura = new Factura();
		$factura->read(null, $facturaId);
             
        if ($this->request->data['monto'] > 0){
            if ($factura->data['Factura']['monto_maximo'] >= $this->request->data['monto']){
                $monto=$factura->data['Factura']['monto_maximo'] - $this->request->data['monto'];
                $factura->saveField('monto_maximo', $monto);
                if ($monto==0){
                    $factura->saveField('pagado', 1);
                }
            }else{
                $this->Session->setFlash(__('El monto ingresado es mayor al adeudado'), 'message_error');
                $error++;
            }
        }else{
            if ($this->request->data['monto'] == 0){
                $facturaReciboTmp = new FacturaRecibo();
                $facturaReciboTmp->read(null, $this->request->data['factura_recibo_id']);
                $abonoBase=$facturaReciboTmp->data['FacturaRecibo']['abono_monto_base'];
                $abonoISLR=$facturaReciboTmp->data['FacturaRecibo']['abono_monto_islr'];
                $abonoIVA=$facturaReciboTmp->data['FacturaRecibo']['abono_monto_iva'];
                $montoBase=$factura->data['Factura']['monto_base'];
                $montoMaximo=$factura->data['Factura']['monto_maximo'];
                $montoIVA=$factura->data['Factura']['monto_iva'];
                $montoRetencionIVA=$factura->data['Factura']['monto_retencion_iva'];  
                $factura->saveField('pagado', 0);
                switch ($campo){
                    case 'mba':
                        if($abonoBase==$montoBase+$montoIVA-$montoRetencionIVA){
                            $factura->saveField('monto_maximo', $montoBase+$montoIVA-$montoRetencionIVA);
                        }else{
                            $monto=$abonoBase+$montoMaximo;
                            $factura->saveField('monto_maximo', $monto);
                        }
                        break;
                    case 'mis':
                        if($abonoISLR==$montoBase+$montoIVA-$montoRetencionIVA){
                            $factura->saveField('monto_maximo', $montoBase+$montoIVA-$montoRetencionIVA);
                        }else{
                            $monto=$abonoISLR+$montoMaximo;
                            $factura->saveField('monto_maximo', $monto);
                        }

                        break;
                    case 'miv':
                        if($abonoIVA==$montoBase+$montoIVA-$montoRetencionIVA){
                            $factura->saveField('monto_maximo', $montoBase+$montoIVA-$montoRetencionIVA);
                        }else{
                            $monto=$abonoIVA+$montoMaximo;
                            $factura->saveField('monto_maximo', $monto);
                        }
                        break;
                }
            }
        }
                
		switch ($campo){
			case 'mba':
				$datos['abono_monto_base'] = $this->request->data['monto'];
				break;
			case 'mis':
				$datos['abono_monto_islr'] = $this->request->data['monto'];
				break;
			case 'miv':
				$datos['abono_monto_iva'] = $this->request->data['monto'];
				break;
		}
		
		$facturaRecibo = new FacturaRecibo();
		$facturaRecibo->save($datos);
        
        $this->_actualizaEstatusRecibo($reciboId);        
		
		if ($error>0){
			$dataSource->rollback();
		}else{
			$dataSource->commit();
		}
		
		$this->redirect('/recibos/edit/'.$reciboId);
		
	}
	
	public function agregarInstrumentoAprobado(){
		$this->autoRender = false;
		if ($this->request->is('post')){
			if ($this->Recibo->Instrumento->setInstrumentoRecibo($this->request->data['instrumento_id'], $this->request->data['recibo_id'])){
				echo "OK";
			}else{
				echo "NO";
			}
				
		}
		
	}
	
	public function getInstrumentosConciliados(){
		$this->layout = 'ajax';
		$instrumentosConciliados = $this->Recibo->Instrumento->getInstrumentosConciliados($this->request->data['Recibo']['proveedor_id']);
		$this->set(compact('instrumentosConciliados'));
	}
	
	public function aprobarInstrumento($instrumentoId, $reciboId){
		App::import('model', 'Instrumento');
		$this->autoRender=false;
		$camposGuardar = array('estatus_instrumento_id');
		$instrumento = new Instrumento();
		$instrumento->read(null, $instrumentoId);
		
		if ($this->request->is('post')){			
			if ($instrumento->saveField('estatus_instrumento_id',2)) {
                $this->Session->setFlash(__('El instrumento ha sido aprobado, ya puede aplicar el instrumento a alguna factura'), 'message_successful');
            } else {
                $this->Session->setFlash(__('No se pudo procesar debido a errores.'), 'message_error');
            }
			$this->redirect(array('action' => 'edit', $reciboId));
		}

	}
	
	public function setFacturaPagada(){
		$this->autoRender = FALSE;
		$this->Recibo->setFacturaPagada();
	}
    
    public function _actualizaEstatusRecibo($reciboId){
        $this->Recibo->read(null, $reciboId);
        
        $valor=$this->Recibo->getEstatusRecibo($reciboId);
        
        if ($valor['monto_maximo'] > 0){
            if ($valor['monto_maximo'] < $valor['monto_total']){
                //monto pagado parcialmente
                $this->Recibo->saveField('estatus_monto',2);
            }else{
                //sin pago registrado
                $this->Recibo->saveField('estatus_monto',1);
            }
        }else{
            //estatus pagado completamente
            $this->Recibo->saveField('estatus_monto',3);
        }
        
        $this->Recibo->saveField('estatus_instrumento',1);
        
        
        
    }

}

?>

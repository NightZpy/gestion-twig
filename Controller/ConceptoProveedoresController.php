<?php
App::uses('AppController', 'Controller');

class ConceptoProveedoresController extends AppController 
{
    public function asociarConcepto()
    {
        if ($this->request->is('post')) {
			$this->ConceptoProveedor->create();
			if ($this->ConceptoProveedor->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('controller' => 'personas', 'action' => 'relacionConcepto', $this->request->data['ConceptoProveedor']['proveedor_id']));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		}
    }
    
    public function removerConcepto($id = null, $proveedor_id = null) 
    {
        //echo $id;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		if ($this->ConceptoProveedor->deleteall(array('ConceptoProveedor.id' => $id), false)) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
            $this->redirect(array('controller' => 'personas', 'action' => 'relacionConcepto', $proveedor_id));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
	}
    
    public function ajaxGetConceptos(){
        $this->autoRender=false;
        if ($this->request->is('post')){
            $proveedorId = $this->request->data['proveedor_id'];
            $tipo = isset($this->request->data['tipo'])?$this->request->data['tipo']:"";
            $res=$this->ConceptoProveedor->getConceptoProveedor($proveedorId);
            
            $html="<option value=''>Seleccione...</option>";
            if ($tipo=='options'){
                foreach ($res as $row){
                    $nombre=$row['conceptos']['nombre']." (".$row['conceptos']['porcentaje_islr']."%)";
                    $html.="<option value='".$row['conceptos']['id']."'>$nombre</option>\n";
                }
            }

            return $html;
            }
            return "";

    }
    
    public function ajaxGetConceptosDialogo(){
        $proveedorId=$this->request->data['Factura']['proveedor_id'];
        $impuestoIslrId=  $this->request->data['Factura']['impuesto_islr_id'];
        $clienteId= $this->request->data['Requerimiento']['cliente_id'];
        $conceptos=$this->ConceptoProveedor->buscarConceptos($proveedorId, $impuestoIslrId);
        $conceptos2=$this->ConceptoProveedor->buscarConceptosUsados($proveedorId, $clienteId, $impuestoIslrId);
        $callback=$this->request->data['callback'];
        $detalleFacturaId=  isset($this->request->data['detalleFacturaId'])?$this->request->data['detalleFacturaId']:"0";
        $this->set(compact('conceptos','conceptos2','callback','detalleFacturaId'));
    }
    
    public function ajaxAdd(){
        if ($this->request->is('post')){
            $this->autoRender=false;
            //pr($this->request->data);
            
            app::import('model','Concepto');
            $c=new Concepto();
            $data['nombre']=$this->request->data['ConceptoProveedor']['descripcion'];
            $data['activo']=1;
            $data['impuesto_id']=$this->request->data['ConceptoProveedor']['impuesto_id'];
            if ($c->save($data)){
                $this->request->data['ConceptoProveedor']['concepto_id']=$c->id;

                $this->ConceptoProveedor->save($this->request->data);  
                $resp['id']=$c->id;
                $resp['concepto']=$this->request->data['ConceptoProveedor']['descripcion'];
                return json_encode($resp); 
            }else{
                return 'false';
            }

        }
        
        app::import('model','impuesto');
        $i=new Impuesto();
        $proveedorId=$this->request->data['Factura']['proveedor_id'];
        $impuestos = $i->find('list',array('conditions'=>array('es_retencion_islr'=>1)));
        $this->set(compact('impuestos', 'proveedorId'));
    }
}

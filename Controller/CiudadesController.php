<?php
App::uses('AppController', 'Controller');
/**
 * Ciudades Controller
 *
 * @property Ciudad $Ciudad
 */
class CiudadesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Ciudad->recursive = 0;
		$this->set('ciudades', $this->paginate());
	}



    /**
     * add method
     *
     * @return void
     */
	public function agregar() {
		$this->_guardar();
        $this->render('form');
	}

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function editar($id = null) {
		$this->Ciudad->id = $id;
		if (!$this->Ciudad->exists()) {
			throw new NotFoundException('Registro Inválido.');
		}
        
        $this->_guardar($id);
        $this->render('form');
	}
    
    public function _guardar($id=null) {
        if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Ciudad->save($this->request->data)) {
				$this->Session->setFlash('Registro guardado con éxito.', 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		} else {
			$this->request->data = $this->Ciudad->read(null, $id);
		}
        
        $estados = $this->Ciudad->Estado->find('list', array('conditions' => array('activo' => 1)));
        $this->set(compact('estados'));
    }

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Ciudad->id = $id;
		if (!$this->Ciudad->exists()) {
			throw new NotFoundException('Registro Invalido.');
		}
		if ($this->Ciudad->delete()) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('action' => 'index'));
	}
    
     
    public function listar( $estado_id = null ) 
    {
        $this->autoRender = false;
        $ciudades = $this->Ciudad->find('list', array('conditions' => array('estado_id' => $estado_id, 'activo' => '1')));
        if (count($ciudades) > 0) {
            $response = array('success' => true,'ciudades' => $ciudades);
        } else {
            $response = array('success' => false);
        }
       
        return json_encode($response);
    }
}

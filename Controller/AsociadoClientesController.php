<?php
App::uses('AppController', 'Controller');

class AsociadoClientesController extends AppController 
{
    public $autoRender = false;
    
    public function asociarCliente()
    {
        if ($this->request->is('post')) {
			$this->AsociadoCliente->create();
			if ($this->AsociadoCliente->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado con exito.', 'message_successful');
                $this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $this->request->data['AsociadoCliente']['asociado_id'], '1'));
			} else {
				$this->Session->setFlash('Registro no guardado. Por favor, intente nuevamente.', 'message_error');
			}
		}
    }
    
    public function removerCliente( $id = null, $asociado_id = null ) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AsociadoCliente->id = $id;
		if ($this->AsociadoCliente->deleteall(array('AsociadoCliente.id' => $this->AsociadoCliente->id),false)) {
			$this->Session->setFlash('Registro eliminado.', 'message_successful');
			$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $asociado_id, '1'));
		}
		$this->Session->setFlash('Registro no eliminado. Por favor, intente nuevamente.', 'message_error');
		$this->redirect(array('controller' => 'personas', 'action' => 'relacionCliente', $asociado_id, '1'));
	}
    
    public function listar( $asociado_id = null) 
    {
        $options['joins'] = array(
                                array('table' => 'personas',
                                'alias' => 'Persona',
                                'type' => '',
                                'conditions' => array(
                                                     'Persona.id = AsociadoCliente.cliente_id',
                                                     ),
                                      ),
                                );
        $options['conditions'] = array(
            'AsociadoCliente.asociado_id' => $asociado_id
        );
        $options['fields'] = array('Persona.id', 'Persona.razon_social');
        $clientes = $this->AsociadoCliente->find('list', $options);
        if (count($clientes) > 0) {
            $response = array('success' => true,'clientes' => $clientes);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }
    
    public function comision( $cliente_id = null, $asociado_id = null )
    {
        $ComisionCliente = $this->AsociadoCliente->getComisionCliente($cliente_id);
        if (count($ComisionCliente) > 0) {
            $response = array('success' => 'personas','comisiones' => $ComisionCliente);
        } else {
            $ComisionAsociado = $this->AsociadoCliente->getComisionAsociado($asociado_id);
            if (count($ComisionAsociado) > 0) {
                $response = array('success' => 'personas','comisiones' => $ComisionAsociado);
            } else {
                $ComisionConfiguracion = $this->AsociadoCliente->getComisionConfiguracion();
                if (count($ComisionConfiguracion) > 0) {
                    $response = array('success' => 'comisiones','comisiones' => $ComisionConfiguracion);
                } else {
                    $response = array('success' => false);
                }
            }
        }
        return json_encode($response);
    }
}

<?php
App::uses('AppController', 'Controller');
/**
 * Estados Controller
 *
 * @property Estado $Estado
 */
class EstadosController extends AppController 
{
    public $components = array('Search.Prg');
    public $presetVars = true;
    /**
     * index method
     *
     * @return void
     */
	public function index() {
		$this->Estado->recursive = 0;
        $this->Prg->commonProcess();
        $this->paginate = array('conditions' => $this->Estado->parseCriteria($this->passedArgs));
        $this->set('estados', $this->paginate());
	}
    /**
     * agregar method
     *
     * @return void
     */
	public function agregar() {
        $this->_guardar();
        $this->render('form');
	}

    /**
     * editar method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function editar($id = null) {
        
		$this->Estado->id = $id;
		if (!$this->Estado->exists()) {
			throw new NotFoundException(__('Registro Inválido.'));
		}
        $this->_guardar($id);
        $this->render('form');
		
	}
    
    private function _guardar($id=null) {
        $this->Estado->unbindModel(array(
                'hasMany' => array('Persona', 'Ciudad')
            )
        );
        if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Estado->save($this->request->data)) {
				$this->Session->setFlash('Registro guardado con éxito.', 'message_successful');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
			}
		} else {
			$this->request->data = $this->Estado->read(null, $id);
		}
    }

    /**
     * eliminar method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function eliminar($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Estado->id = $id;
		if (!$this->Estado->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
		if ($this->Estado->delete()) {
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
		$this->redirect(array('action' => 'index'));
	}
   
}

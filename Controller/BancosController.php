<?php
App::uses('AppController', 'Controller');
/**
 * Ciudades Controller
 *
 * @property Ciudad $Ciudad
 */
class BancosController extends AppController {

	public $autoRender = false;
    public function listar() 
    {
        $bancos = $this->Banco->find('all');
        if (count($bancos) > 0) {
            $response = array('success' => true, 'bancos' => $bancos);
        } else {
            $response = array('success' => false);
        }
        return json_encode($response);
    }
}
<?php
App::uses('AppController', 'Controller');
App::uses('CakeNumber', 'Utility');

class FacturasController extends AppController 
{
    
    public $uses = array('Factura', 'DetalleFactura');
    public $helpers = array('Number');
    
    public function index() 
    {
        $this->Factura->unbindModel(
            array('belongsTo' => array('Requerimiento'))
        );
        
        $conditions = array();
        if ($this->request->is('post')) {
            if ($this->request->data['Factura']['proveedor_id']) {
                $conditions[] = array('Factura.proveedor_id' => $this->request->data['Factura']['proveedor_id']);
            }
        }
        $proveedores = $this->Factura->Persona->find('list', array('conditions' => array('Persona.clasificacion_id = 3')));
        $this->set(compact('proveedores'));
        $this->set('facturas', $this->paginate($conditions));
    }
    
    public function ajaxGetPersona()
    {
        $this->autoRender=false;
        if ($this->request->is('post')) {
            $this->Factura->Persona->read(null, $this->request->data['id']) ;
            $data['rif']= $this->Factura->Persona->data['Persona']['tipo_documento_id'].'-'.$this->Factura->Persona->data['Persona']['documento'];
            $data['direccion']=$this->Factura->Persona->data['Persona']['direccion'];
            return json_encode($data);
        }
    }
    
    public function datosFiscales($factura_id = null) 
    {
        $this->layout = 'ajax';
		$this->Factura->id = $factura_id;
        
        if (!$this->Factura->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $camposGuardar = array('fecha_facturacion', 'numero_factura', 'numero_control');
            $this->Factura->create();
            if ($this->Factura->save($this->request->data, $validate = true, $fieldList = $camposGuardar)) {
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('controller' => 'facturas', 'action' => 'editar', $this->request->data['Factura']['requerimiento_id'], $this->Factura->id));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        } else {
            $this->Factura->unbindModel(
                array('belongsTo' => array(
                                        'Requerimiento',
                                        'Persona',
                                        'Impuesto',
                                        'DetalleFactura',))
            );
            
            $diasFeriados = $this->Factura->getDiasFeriados();
            $facturas = $this->Factura->find('all', array('conditions' => array('Factura.id = ' . $factura_id)));
            $this->set(compact('facturas', 'actualizado', 'diasFeriados'));
            $this->render('datosfiscales');
        }
    }
    
    public function agregar($requerimiento_id = null) 
    {
        $modificarDatosFiscales = array();
        
        $proveedores = $this->Factura->Persona->find('list', array(
            'fields' => array('razon_social'), 
            'conditions' => array('Persona.clasificacion_id = 3'),
            'order' => array('razon_social'),
        ));
        
        $this->set(compact('proveedores', 'modificarDatosFiscales'));
        
        App::import('model', 'Requerimiento');
        $r = new Requerimiento($requerimiento_id);
        $r->read();
        
        App::import('model', 'Persona');
        $c=new Persona();
        $c->read(null,$r->data['Cliente']['id']);
        $cliente=$c->data;
        
        $this->_guardar($requerimiento_id);
        $this->set(compact('razonsocial', 'modificarDatosFiscales','cliente'));
        $this->render('form');
    }
    
    public function editar($requerimiento_id = null, $id = null) 
    {
        $this->Factura->id = $id;
        if (!$this->Factura->exists()) {
            throw new NotFoundException(__('Registro Invalido.'));
        }
        $this->Factura->read();
        $groupId = $this->UserAuth->getGroupId();
        $modificarDatosFiscales = $this->Factura->getAccesoControlador($groupId);
        
        $proveedorSeleccionado = array_values($this->Factura->find('list', array('fields' => 'proveedor_id', 'conditions' => array('Factura.id = ' . $id))));
        
        App::import('model', 'Persona');
        $c=new Persona();
        $c->read(null,$this->Factura->data['Requerimiento']['cliente_id']);
        $cliente=$c->data;
        
        //$retenciones = $this->Factura->getRetenciones();
        
        $proveedores = $this->Factura->Persona->find('list', array('fields' => array('razon_social'), 'conditions' => array('Persona.clasificacion_id = 3')));
        $this->set(compact('proveedores', 'proveedorSeleccionado', 'modificarDatosFiscales','cliente'));
        
        $this->_guardar($requerimiento_id, $id);
        $this->render('form');
    }
    
    private function _guardar($requerimiento_id = null, $id = null) {
        $estatus = array();
        $camposGuardar = array('requerimiento_id', 'fecha_requerida', 
                               'fecha_facturacion', 'proveedor_id', 
                               'numero_factura', 'numero_control', 'concepto_id', 'observaciones','retencion_iva_id','retencion_iva_id_modif');
        $groupId = $this->UserAuth->getGroupId();
        $montoBase = "";
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Factura->create();
            $retencionId = $this->Factura->getRetencionIVAProveedor($this->request->data['Factura']['proveedor_id']);
            $this->request->data['Factura']['retencion_iva_id'] = $retencionId;
            $this->request->data['Factura']['retencion_iva_id_modif'] = $retencionId;
            if ($this->Factura->save($this->request->data, $validate = true, $fieldList = $camposGuardar)) {
                
                $this->Session->setFlash(__('Registro guardado con exito.'), 'message_successful');
                $this->redirect(array('controller' => 'facturas', 'action' => 'editar', $requerimiento_id, $this->Factura->id));
            } else {
                $this->Session->setFlash(__('Registro no guardado. Por favor, intente nuevamente.'), 'message_error');
            }
        } else {
            if (isset($id)) {
                $this->Factura->unbindModel(
                    array('belongsTo' => array('Requerimiento'))
                );
                
                $detalleFacturas = $this->Factura->getDetalleFactura($this->Factura->id);
                $this->request->data = $this->Factura->read(null, $id);
                $estatusRequerimiento = $this->Factura->getEstatusRequerimiento($requerimiento_id);
                $estatusRequerimiento = $estatusRequerimiento[0]['Requerimiento']['estatus_workflow_id'];
                
                $estatusDisponibles = $this->Factura->Requerimiento->getEstatus($groupId);
                foreach ($estatusDisponibles as $estatusDisponible):
                    $estatus[$estatusDisponible['Transicion']['estatus_inicial_id']] = $estatusDisponible['Transicion']['es_modificable'];
                endforeach;
                $montoBase = $this->request->data['Factura']['monto_base'];
				$this->request->data['Factura']['monto_base']=  CakeNumber::currency($this->request->data['Factura']['monto_base'],'VEF');
				$this->request->data['Factura']['monto_iva']=  CakeNumber::currency($this->request->data['Factura']['monto_iva'],'VEF');
				$this->request->data['Factura']['monto_total']=  CakeNumber::currency($this->request->data['Factura']['monto_total'],'VEF');
				$this->request->data['Factura']['porcentaje_retencion_iva']=  CakeNumber::currency($this->request->data['Factura']['porcentaje_retencion_iva'],'VEF');
				$this->request->data['Factura']['monto_retencion_iva']=  CakeNumber::currency($this->request->data['Factura']['monto_retencion_iva'],'VEF');
				$this->request->data['Factura']['monto_retencion_islr']=  CakeNumber::currency($this->request->data['Factura']['monto_retencion_islr'],'VEF');
				$this->request->data['Factura']['fecha_requerida_display'] = AppHelper::fechaNormal($this->request->data['Factura']['fecha_requerida']);
				$this->request->data['Factura']['fecha_facturacion_display'] = AppHelper::fechaNormal($this->request->data['Factura']['fecha_facturacion']);
                /*$iva = $this->Factura->Impuesto->find('list', array('conditions' => array('nombre' => 'IVA'), 'fields' => 'valor, valor'));
                $iva_defecto = $this->Factura->Impuesto->find('list', array('conditions' => array('nombre' => 'IVA', 'predeterminado' => '1'), 'fields' => 'valor'));*/
                
                $conceptos = $this->Factura->getConceptoProveedor($this->request->data['Factura']['proveedor_id']);
                $this->set(compact('conceptos', 'iva', 'iva_defecto', 'detalleFacturas', 'estatusRequerimiento', 
                                   'estatus', 'descripcioneses'));
            }
        }
        
        $impuesto_ivas = $this->Factura->Impuesto->find('list', array('conditions' => array('es_iva' => 1), 'fields' => 'id, valor'));
        $iva_defecto = $this->Factura->Impuesto->find('list', array('conditions' => array('es_iva' => 1, 'predeterminado' => '1'), 'fields' => 'id'));
        
        $this->set(compact('impuesto_ivas'));
        $this->set('iva_defecto', $iva_defecto);
        $this->set('requerimiento_id', $requerimiento_id);
        $this->set('id', $id);
        $this->set('montoBase', $montoBase);
    }
    
    public function delete($id = null, $requerimiento_id = null) 
    {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Factura->id = $id;
		if (!$this->Factura->exists()) {
			throw new NotFoundException(__('Registro Invalido.'));
		}
        /*
         * RECORDAR QUE EXISTE UN FOREIGN KEY EN LA TABLA 'detalle_facturas' QUE SE ENCARGA DE ELIMINAR EN 
         * CASCADA LOS REGISTROS DE DICHA TABLA Y NOS ASEGURA NO DEJAR REGISTROS HUERFANOS.
         */
		if ($this->Factura->delete()) {
            $montosFactura = ($this->Factura->DetalleFactura->montosFacturas($requerimiento_id));
            /*
             * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
             * Si el array() $montosFactura tiene datos el modelo Factura sera actualizado con los datos agrupados
             * en el array() $montosFactura
            */
            if (count($montosFactura) == 0) {
                /*
                 * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
                 * Si el array() $montosFactura esta vacio el modelo Requerimiento sera seteado con '0'
                */
                $requerimientos = array('Requerimiento' => array(
                                                    'id' => $requerimiento_id,
                                                    'monto_costo_gestion' => 0,
                                                    'monto_costo_operativo' => 0,
                                                    'monto_costo_venta' => 0,
                                                    'monto_costo_venta_secundario' => 0,
                                                    'monto_costo_administrativo' => 0,
                                                    'monto_base' => 0,
                                                    'monto_iva' => 0,
                                                    'monto_total' => 0,
                                                    'monto_retencion_islr' => 0,
                                                    'monto_retencion_iva' => 0,
                                                    )
                                 );
            } else {
                /*
                 * Preparamos el modelo Requerimiento para guardar los datos ya actualizados
                 * Si el array() $montosFactura tiene datos el modelo Requerimiento sera actualizado con los datos agrupados
                 * en el array() $montosFactura
                */
                $requerimientos = array('Requerimiento' => array(
                                                    'id' => $montosFactura[0]['Factura']['requerimiento_id'],
                                                    'monto_costo_gestion' => $montosFactura[0][0]['monto_costo_gestion'],
                                                    'monto_costo_operativo' => $montosFactura[0][0]['monto_costo_operativo'],
                                                    'monto_costo_venta' => $montosFactura[0][0]['monto_costo_venta'],
                                                    'monto_costo_venta_secundario' => $montosFactura[0][0]['monto_costo_venta_secundario'],
                                                    'monto_costo_administrativo' => $montosFactura[0][0]['monto_costo_administrativo'],
                                                    'monto_base' => $montosFactura[0][0]['monto_base'],
                                                    'monto_iva' => $montosFactura[0][0]['monto_iva'],
                                                    'monto_total' => $montosFactura[0][0]['monto_total'],
                                                    'monto_retencion_islr' => $montosFactura[0][0]['monto_retencion_islr'],
                                                    'monto_retencion_iva' => $montosFactura[0][0]['monto_retencion_iva'],
                                                    )
                                 );
            }
            /*
             * Guardamos el Modelo Requerimiento 
            */
            $reque = ClassRegistry::init('Requerimiento');
            $reque->save($requerimientos);
            
            
			$this->Session->setFlash(__('Registro eliminado.'), 'message_successful');
			$this->redirect(array('controller' => 'requerimientos', 'action' => 'editar', $requerimiento_id));
		}
		$this->Session->setFlash(__('Registro no eliminado. Por favor, intente nuevamente.'), 'message_error');
        $this->redirect(array('controller' => 'requerimientos', 'action' => 'editar', $requerimiento_id));
	}
    
}

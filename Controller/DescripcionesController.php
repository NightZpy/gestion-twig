<?php

App::uses('AppController', 'Controller');

/**
 * Descripciones Controller
 *
 * @property Descripcion $Descripcion
 */
class DescripcionesController extends AppController {

    public $components=array('Search.Prg', 'RequestHandler');
    public $helpers = array('Js');
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Descripcion->recursive = 0;
        $this->set('descripciones', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Descripcion->id = $id;
        if (!$this->Descripcion->exists()) {
            throw new NotFoundException(__('Invalid descripcion'));
        }
        $this->set('descripcion', $this->Descripcion->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Descripcion->create();
            if ($this->Descripcion->save($this->request->data)) {
                $this->Session->setFlash(__('The descripcion has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The descripcion could not be saved. Please, try again.'));
            }
        }
        $proveedores = $this->Descripcion->Proveedor->find('list');
        $conceptos = $this->Descripcion->Concepto->find('list');
        $this->set(compact('proveedores', 'conceptos'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->Descripcion->id = $id;
        if (!$this->Descripcion->exists()) {
            throw new NotFoundException(__('Invalid descripcion'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Descripcion->save($this->request->data)) {
                $this->Session->setFlash(__('The descripcion has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The descripcion could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Descripcion->read(null, $id);
        }
        $proveedores = $this->Descripcion->Proveedor->find('list');
        $conceptos = $this->Descripcion->Concepto->find('list');
        $this->set(compact('proveedores', 'conceptos'));
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Descripcion->id = $id;
        if (!$this->Descripcion->exists()) {
            throw new NotFoundException(__('Invalid descripcion'));
        }
        if ($this->Descripcion->delete()) {
            $this->Session->setFlash(__('Descripcion deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Descripcion was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function ajaxGetDescripcionesJSON() {
        $this->autoRender = false;
        $json = $this->Descripcion->find('all');
        echo json_encode($json);
    }

    public function ajaxGetDescripciones() {
        $this->layout="ajax";
        
        //$this->Prg->commonProcess();
        //pr($this->passedArgs);
        
        //$this->paginate=array('limit'=>2);
        
        $this->Prg->commonProcess();
        
        $this->paginate = array('limit'=>3,'conditions' => $this->Descripcion->parseCriteria($this->passedArgs));
        //$this->set('parametros', $this->request->data);
        $this->request->data['Descripcion']=$this->passedArgs;
        //pr($this->request->data);
        app::import('model','Persona');
        $p=new Persona();
        $proveedores = $p->find('list', array('fields' => array('razon_social'), 'conditions' => array('Persona.clasificacion_id = 3')));
        
        app::import('model','Concepto');
        $c=new Concepto();
        
        $conceptos=$c->find('list');
        $this->set('descripciones', $this->paginate());
        $this->set(compact('proveedores','conceptos'));
        $this->render('descripcionesFrm');
        
    }
    
    public function addAjax() {
        
        if ($this->request->is('post')) {
            
            
            $this->Descripcion->create();
            if ($this->Descripcion->save($this->request->data)) {
                $this->Session->setFlash(__('The descripcion has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The descripcion could not be saved. Please, try again.'));
            }
        }
        
        $this->request->data['Descripcion']['proveedor_id']= $this->request->data['Factura']['proveedor_id'];
        $this->request->data['Descripcion']['concepto_id']= $this->request->data['Factura']['concepto_id'];
        $proveedores = $this->Descripcion->Proveedor->find('list');
        $conceptos = $this->Descripcion->Concepto->find('list');
        $this->set(compact('proveedores', 'conceptos'));
    }
    
    public function ajaxGuardarDescripcion(){
        $this->autoRender=false;
        if ($this->request->is('post')){            
            //pr($this->request->data);
            if ($this->Descripcion->save($this->request->data)) {
                echo $this->request->data['Descripcion']['descripcion'];
            }
        }
    }

}

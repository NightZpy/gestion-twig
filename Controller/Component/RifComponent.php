<?php
App::uses('Component', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class RifComponent extends Component {
    /**
     *[code_result] =  -2: Formato de rif inválido
     *                 -1: No hay soporte a curl
     *                  0: No hay conexion a internet
     *                  1: Consulta satisfactoria
     *      Otherwise:
     *                450: Formato de rif invalido
     *                452: Rif no existe
     *
     * [seniat]      =  nombre: [CADENA CON EL NOMBRE]
     *                  agenteretencioniva: [SI|NO]
     *                  contribuyenteiva: [SI|NO]
     *                  tasa: [VACIO|ENTERO MONTO TASA]
     * @var Array
     */
    private $_responseJson = array(
        'code_result' => '', 
		'seniat' => array()
    );  
    /**
     * 
     * @var String
     */
    private $_url = 'http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=';
    /**
     *
     * @var String
     */
    private $_rif;
    /**
     * Obtener la información en formato Json
     * 
     * @param String $rif
     * @return Json
     * @throws Exception
     */
    public function getInfo($rif) {
        $this->_rif = $this->_format($rif);
        
        if ($this->_validar()) {
                $this->_url .= $this->_rif;
                $request = new HttpSocket();
                $result = $request->get($this->_url);
                
            if ($result) {
                try {
                    if(substr($result,0,1)!= '<' ) {
                        throw new Exception($result);
                    }

                    $xml = simplexml_load_string($result);

                    if(!is_bool($xml)) {
                        $elements = $xml->children('rif');
                        $seniat = array();
                        $this->_responseJson['code_result'] = 1;

                        foreach($elements as $key => $node) {
                            $index = strtolower($node->getName());
                            $seniat[$index] = (string)$node;
                        }
                        $this->_responseJson['seniat'] = $seniat;
                    }
                } catch(Exception $e) {
                    $exception = explode(' ', $result, 2);
                    $this->_responseJson['code_result'] =(int) $exception[0];
                }
            } else {
                // No hay conexión a internet
                $this->_responseJson['code_result'] = 0;
            }
        } else {
            // Formato de RIF inválido
            $this->_responseJson['code_result'] = -2;
        }
    
        $this->_cleanFields();
        unset($request);
        return json_encode($this->_responseJson);
    }
    /**
     * Validar formato del RIF
     * 
     * Basado en el método módulo 11 para el cálculo del dígito verificador 
     * y aplicando las modificaciones propias ejecutadas por el seniat
     * @link http://es.wikipedia.org/wiki/C%C3%B3digo_de_control#C.C3.A1lculo_del_d.C3.ADgito_verificador
     * 
     * @return boolean 
     */
    private function _validar() {
        $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $this->_rif);
        
        if ($retorno) {
            $digitos = str_split($this->_rif);
           
            $digitos[8] *= 2; 
            $digitos[7] *= 3; 
            $digitos[6] *= 4; 
            $digitos[5] *= 5; 
            $digitos[4] *= 6; 
            $digitos[3] *= 7; 
            $digitos[2] *= 2; 
            $digitos[1] *= 3; 
            
            // Determinar dígito especial según la inicial del RIF
            // Regla introducida por el SENIAT
            switch ($digitos[0]) {
                case 'V':
                    $digitoEspecial = 1;
                    break;
                case 'E':
                    $digitoEspecial = 2;
                    break;
                case 'J':
                    $digitoEspecial = 3;
                    break;
                case 'P':
                    $digitoEspecial = 4;
                    break;
                case 'G':
                    $digitoEspecial = 5;
                    break;
            }
            
            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);
            $residuo = $suma % 11;
            $resta = 11 - $residuo;
            
            $digitoVerificador = ($resta >= 10) ? 0 : $resta;
            
            if ($digitoVerificador != $digitos[9]) {
                $retorno = false;
            }
        }        
        
        return $retorno;
    }
    
    /**
     * Format
     * Transforma a mayúsculas y elimina los guiones en caso de existir
     * 
     * @param String $rif
     * @return String
     */
    private function _format($rif) {
        return str_replace('-', '', strtoupper($rif));;
    }
    /**
     * Clean Fields
     * 
     * Limpia los campos provenientes de la página del seniat
     * Quita los valores repetidos dentro de los paréntesis.
     * Sustituye los valores SI y NO por true y false respectivamente.
     * Si es agente de retención y la tasa es cero le asigna de forma automática 75.
     */
    private function _cleanFields() {
        if ($this->_responseJson['code_result'] == 1) {
            $primerParentesis = strpos($this->_responseJson['seniat']['nombre'], '(');

            if ($primerParentesis > 0) {
                $this->_responseJson['seniat']['nombre'] = substr($this->_responseJson['seniat']['nombre'], 0, $primerParentesis);
            }
            
            $this->_responseJson['seniat']['contribuyenteiva'] = ($this->_responseJson['seniat']['contribuyenteiva'] == 'SI')       ? true : false;
            $this->_responseJson['seniat']['agenteretencioniva'] = ($this->_responseJson['seniat']['agenteretencioniva'] == 'SI')   ? true : false;
            
            if ( ($this->_responseJson['seniat']['agenteretencioniva']) && 
                  empty($this->_responseJson['seniat']['tasa']) ) {
                $this->_responseJson['seniat']['tasa'] = 75;
            }
        }
    }
}
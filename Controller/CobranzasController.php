<?php

class CobranzasController extends AppController {

    public $components = array('Search.Prg');
    public $presetVars = true;
    public $uses = array('Cobranza', 'Impuesto', 'Persona');

    public function index() {
        $this->Prg->commonProcess();
        
        $proveedores = $this->Cobranza->Proveedores->find('list', array('order' => array('Proveedores.razon_social' => 'Asc'), 'conditions' => array('clasificacion_id' => 3)));
        if ($this->UserAuth->isAsociado()) {
            $asociadoId = $this->Session->read('UserAuth.Usuario.persona_id');
            $this->passedArgs['asociado_id']=$asociadoId;
            $this->paginate = array('conditions' => $this->Cobranza->parseCriteria($this->passedArgs));
            $this->set('cobranzas', $this->paginate());
            
            $asociados = $this->Cobranza->Asociados->find('list', array('order' => array('Asociados.razon_social' => 'Asc'), 'conditions' => array('clasificacion_id' => 2, 'Asociados.id'=>$asociadoId)));
            $clientes = $this->Persona->getAsociadoClientes2('list', $asociadoId);
        }else{
            $this->paginate = array('conditions' => $this->Cobranza->parseCriteria($this->passedArgs));
            $this->set('cobranzas', $this->paginate());
            $asociados = $this->Cobranza->Asociados->find('list', array('order' => array('Asociados.razon_social' => 'Asc'), 'conditions' => array('clasificacion_id' => 2)));
            $clientes = $this->Cobranza->Clientes->find('list', array('order' => array('Clientes.razon_social' => 'Asc'), 'conditions' => array('clasificacion_id' => 1)));
        }
        
        
        $this->set(compact('proveedores', 'asociados', 'clientes'));
    }

    public function add() {
        $id = 0;
        if ($this->request->is('post')) {
            if ($this->Cobranza->save($this->request->data)) {
                if (intval($this->request->data['Cobranza']['id']) == 0) {
                    $this->request->data['Cobranza']['fecha'] = date('Y-m-d');
                }
                $id = $this->Cobranza->id;
                $this->Cobranza->read();

                if ($id > 0) {
                    app::import('model', 'Instrumento');
                    $inst = new Instrumento();
                    $this->request->data['Instrumento']['asociado_id'] = $this->request->data['Cobranza']['asociado_id'];
                    $this->request->data['Instrumento']['estatus_instrumento_id'] = 1;
                    $this->request->data['Instrumento']['fecha_carga'] = date('Y-m-d');
                    $this->request->data['Instrumento']['cobranza_id'] = $id;
                    $this->request->data['Instrumento']['proveedor_id'] = $this->request->data['Cobranza']['proveedor_id'];
                    $this->request->data['Instrumento']['cliente_id'] = $this->request->data['Cobranza']['cliente_id'];
                    $inst->save($this->request->data);
                    $this->redirect(array('action' => 'edit', $id));
                }

                $this->request->data = $this->Cobranza->data;
                $asociadoId = $this->Cobranza->data['Cobranza']['asociado_id'];
                $clientes = $this->Cobranza->getClientes($asociadoId);
                $clienteId = $this->Cobranza->data['Cobranza']['cliente_id'];
                $proveedores = $this->Cobranza->getProveedores($clienteId);
            }
        } else {
            $proveedores = array();
            $clientes = array();
        }

        $asociados = $this->Cobranza->getAsociados();

        app::import('model', 'TipoInstrumento');
        app::import('model', 'Banco');
        $i = new TipoInstrumento();
        $tipoInstrumentos = $i->find('list', array('conditions' => array('TipoInstrumento.grupo_tipo_instrumento_id' => 1)));
        $b = new Banco();
        $bancos = $b->find('list', array('order' => array('Banco.nombre' => 'asc')));
        $bancoDestinos = $b->find('list', array('order' => array('Banco.nombre' => 'asc')));
        $instrumentos = $this->Cobranza->Instrumento->find('all', array('conditions' => array('Instrumento.cobranza_id' => $id, 'estatus_instrumento_id'=>array(1,2))));
        $this->request->data['Cobranza']['fecha'] = date('Y-m-d');
        $sumaInstrumentos = 0;
        $this->set(compact('proveedores', 'asociados', 'clientes', 'tipoInstrumentos', 'bancos', 'bancoDestinos', 'instrumentos', 'sumaInstrumentos'));
    }

    public function edit($id) {
        //echo $disponible = $this->Cobranza->getDisponibleInstrumentos($id);
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['Instrumento'])) {
                if ($this->request->data['agregaInstrumento'] == 1) {
                    app::import('model', 'Instrumento');
                    $inst = new Instrumento();
                    $this->request->data['Instrumento']['asociado_id'] = $this->request->data['Cobranza']['asociado_id'];
                    $this->request->data['Instrumento']['estatus_instrumento_id'] = 1;
                    $this->request->data['Instrumento']['fecha_carga'] = date('Y-m-d');
                    $this->request->data['Instrumento']['cobranza_id'] = $id;
                    $this->request->data['Instrumento']['proveedor_id'] = $this->request->data['Cobranza']['proveedor_id'];
                    $this->request->data['Instrumento']['cliente_id'] = $this->request->data['Cobranza']['cliente_id'];
                    $inst->save($this->request->data);
                    $instrumentos = $this->Cobranza->Instrumento->find('all', array('conditions' => array('Instrumento.cobranza_id' => $id)));
                    $sumaInstrumentos = $this->Cobranza->getSumaInstrumentos($id);
                    $this->set(compact('instrumentos', 'sumaInstrumentos'));
                    //$response = $this->render('instrumentosvw');
                    $this->autoRender = false;
                    $view = new View($this, false);
                    $view->set(compact('instrumentos', 'sumaInstrumentos'));
                    $data['body'] = $view->render('instrumentosvw');
                    $data['suma'] = CakeNumber::currency($sumaInstrumentos, 'VEF');
                    return json_encode($data);

                    return;
                }
            }
        } else {
            
        }

        $this->Cobranza->id = $id;

        if (!$this->Cobranza->exists()) {
            throw new NotFoundException(__('Registro Inválido.'));
        }
        $this->request->data = $this->Cobranza->read(null, $id);
        $sumaInstrumentos = $this->Cobranza->getSumaInstrumentos($id);
        $sumaAbonos = $this->Cobranza->getSumaAbonos();
        $asociados = $this->Cobranza->getAsociados();
        $clientes = $this->Cobranza->getClientes($this->request->data['Cobranza']['asociado_id']);
        $proveedores = $this->Cobranza->getProveedores($this->request->data['Cobranza']['cliente_id']);
        $cuentas = $this->Cobranza->getCuentas($this->request->data['Cobranza']['proveedor_id']);
        $instrumentos = $this->Cobranza->Instrumento->find('all', array('conditions' => array('Instrumento.cobranza_id' => $id, 'estatus_instrumento_id'=>array(1,2))));

        $facturas = $this->Cobranza->getFacturas();
        app::import('model', 'TipoInstrumento');
        app::import('model', 'Banco');
        app::import('model', 'Impuesto');
        $i = new TipoInstrumento();
        $b = new Banco();
        $im = new Impuesto();
        $impuestos = $im->find('list', array('conditions' => array('es_retencion_islr' => 1)));
        //$retenciones = array(0=>0, 75=>75);
        $retenciones=$this->Impuesto->find('list', array('conditions'=>array('es_retencion_iva'=>1)));
        $bancos = $b->find('list', array('order' => array('Banco.nombre' => 'asc')));
        $tipoInstrumentos = $i->find('list', array('conditions' => array('TipoInstrumento.grupo_tipo_instrumento_id' => 1)));
        $this->set(compact('instrumentos', 'asociados', 'bancos', 'tipoInstrumentos', 'clientes', 'proveedores', 'cuentas', 'facturas', 'sumaInstrumentos', 'impuestos', 'retenciones', 'sumaAbonos'));
    }

    public function getAjaxClientes() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $asociadoId = $this->request->data['Cobranza']['asociado_id'];
            $clientes = $this->Cobranza->getClientes($asociadoId);
            if ($clientes) {
                echo "\n" . '<option value="0" selected="selected"></option>';
                foreach ($clientes as $key => $cliente) {
                    echo "\n" . '<option value="' . $key . '">' . $cliente . '</option>';
                }
            }
        }
    }

    public function getAjaxProveedores() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $clienteId = $this->request->data['Cobranza']['cliente_id'];
            $proveedores = $this->Cobranza->getProveedores($clienteId);
            if ($proveedores) {
                echo "\n" . '<option value="0" selected="selected"></option>';
                foreach ($proveedores as $key => $proveedor) {
                    echo "\n" . '<option value="' . $key . '">' . $proveedor . '</option>';
                }
            }
        }
    }

    public function getAjaxCuentas() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $proveedorId = $this->data['Cobranza']['proveedor_id'];
            if ($proveedorId > 0) {
                $cuentas = $this->Cobranza->getCuentas($proveedorId);
                if ($cuentas) {
                    foreach ($cuentas as $key => $cuenta) {
                        echo '<option value="' . $key . '">' . $cuenta . '</option>';
                    }
                }
            }
        }
    }

    public function setAjaxInstrumento() {
        //$this->autoRender=false;
        $id = 0;
        if ($this->request->is('post')) {
            if ($this->Cobranza->save($this->request->data)) {
                if (intval($this->request->data['Cobranza']['id']) == 0) {
                    $this->request->data['Cobranza']['fecha'] = date('Y-m-d');
                }
                $id = $this->Cobranza->id;

                if ($id > 0) {
                    app::import('model', 'Instrumento');
                    $inst = new Instrumento();
                    $this->request->data['Instrumento']['proveedor_id'] = $this->request->data['Cobranza']['proveedor_id'];
                    $this->request->data['Instrumento']['cliente_id'] = $this->request->data['Cobranza']['cliente_id'];
                    $inst->save($this->request->data);
                }

                $this->request->data['Cobranza']['id'] = $id;
            }
        }

        app::import('model', 'TipoInstrumento');
        app::import('model', 'Banco');
        $b = new Banco();
        $i = new TipoInstrumento();
        $tipoInstrumentos = $i->find('list', array('conditions' => array('TipoInstrumento.grupo_tipo_instrumento_id' => 1)));
        $bancos = $b->find('list', array('order' => array('Banco.nombre' => 'asc')));
        $bancoDestinos = $b->find('list', array('order' => array('Banco.nombre' => 'asc')));
        $instrumentos = $this->Cobranza->Instrumento->find('all', array('conditions' => array('Instrumento.cobranza_id' => $id)));
        $proveedores = array();
        $clientes = array();
        $this->set(compact('proveedores', 'asociados', 'clientes', 'tipoInstrumentos', 'bancos', 'bancoDestinos', 'instrumentos'));

        $this->render('add');
    }

    public function ajaxAbonaFactura($facturaId) {
        $this->autoRender = false;
        app::import('model', 'Factura');
        app::import('model', 'Impuesto');
        $f = new Factura($facturaId);
        $f->unbindModel(array(
            'belongsTo' => array('Persona', 'Requerimiento', 'DetalleFactura')
        ));
        $f->bindModel(array(
            'belongsTo' => array('Concepto')
        ));
        $f->read();
        //pr($f->data);
        
        
        $imp = $this->Cobranza->getIslr($f->data['Factura']['id']);
        $retIva = $this->Cobranza->getRetencionIva($f->data['Factura']['id']);
        
        //$imp = new Impuesto($f->data['Concepto']['impuesto_id']);
        //$imp->read();

        $cobranzaId = $this->request->data['Cobranza']['id'];

        $sumaInstrumentos = $this->Cobranza->getSumaInstrumentos($cobranzaId);
        $disponible = $this->Cobranza->getDisponibleInstrumentos($cobranzaId);
        $montoMaximo = $f->data['Factura']['monto_maximo'];

        //$retencionIva = $f->data['Factura']['monto_iva'] * $f->data['Factura']['porcentaje_retencion_iva'] / 100;
        $retencionIva = $f->data['Factura']['monto_iva'] * $retIva[0]['Impuesto']['valor'] / 100;
        $islr = $f->data['Factura']['monto_base'] * $imp[0]['Impuesto']['valor'] / 100;
        $abono = $montoMaximo - $islr - $retencionIva;

        app::import('model', 'CobranzaFactura');
        $cf = new CobranzaFactura();
        $cobranzaFactura = $cf->find('all', array('conditions' => array('factura_id' => $facturaId, 'cobranza_id' => $cobranzaId)));

        $error = FALSE;

        $datasource = $this->Cobranza->getDataSource();
        $datasource->begin();
        if (!empty($cobranzaFactura)) {
            $cf->read(null, $cobranzaFactura[0]['CobranzaFactura']['id']);
        }

        if ($disponible > 0) {
            if ($abono <= $disponible) {
                $error = $f->saveField('monto_maximo', 0) ? FALSE : TRUE;
                $data['abono_monto_base'] = $abono;
                $data['abono_monto_iva'] = $retencionIva;
                $data['abono_monto_islr'] = $islr;
                $data['cobranza_id'] = $cobranzaId;
                $data['factura_id'] = $facturaId;
            } else {
                $error = $f->saveField('monto_maximo', $abono - $disponible) ? FALSE : TRUE;
                $data['abono_monto_base'] = $disponible;
                $data['abono_monto_iva'] = $retencionIva;
                $data['abono_monto_islr'] = $islr;
                $data['cobranza_id'] = $cobranzaId;
                $data['factura_id'] = $facturaId;
            }

            if (!$cf->save($data)) {
                $error = true;
            }
        }

        if ($error == true) {
            $datasource->rollback();
        } else {
            $datasource->commit();
        }

        $this->Cobranza->id = $cobranzaId;
        $sumaAbonos=$this->Cobranza->getSumaAbonos();
        $this->request->data = $this->Cobranza->read();
        $facturas = $this->Cobranza->getFacturas();
        //$this->set(compact('facturas'));
        //$this->render('ajaxFacturas');
        
        $view = new View($this, false);
        $retenciones=$this->Impuesto->find('list', array('conditions'=>array('es_retencion_iva'=>1)));
        $impuestos = $this->Impuesto->find('list', array('conditions' => array('es_retencion_islr' => 1)));
        $view->set(compact('facturas','retenciones', 'impuestos'));
        $data['body'] = $view->render('ajaxFacturas');
        $data['suma'] = CakeNumber::currency($sumaAbonos,'VEF');
        
        return json_encode($data);
        
    }

    public function ajaxResetFactura($facturaId) {
        $this->autoRender = false;
        app::import('model', 'Factura');
        $f = new Factura($facturaId);
        $f->unbindModel(array(
            'belongsTo' => array('Persona', 'Requerimiento', 'DetalleFactura')
        ));
        $f->bindModel(array(
            'belongsTo' => array('Concepto')
        ));
        $f->read();
        $cobranzaId = $this->request->data['Cobranza']['id'];
        app::import('model', 'CobranzaFactura');
        $cf = new CobranzaFactura();

        $cobranzaFactura = $cf->find('all', array('conditions' => array('factura_id' => $facturaId, 'cobranza_id' => $cobranzaId)));
        if (isset($cobranzaFactura[0])) {
            $cfid = $cobranzaFactura[0]['CobranzaFactura']['id'];

            $abonoMontoBase = $cobranzaFactura[0]['CobranzaFactura']['abono_monto_base'] + $cobranzaFactura[0]['CobranzaFactura']['abono_monto_islr'] + $cobranzaFactura[0]['CobranzaFactura']['abono_monto_iva'];

            $cf->read(null, $cfid);
            $cf->saveField('abono_monto_base', 0);
            $cf->saveField('abono_monto_iva', 0);
            $cf->saveField('abono_monto_islr', 0);

            $f->saveField('monto_maximo', $f->data['Factura']['monto_maximo'] + $abonoMontoBase);
        }

        $this->Cobranza->id = $cobranzaId;
        $sumaAbonos=$this->Cobranza->getSumaAbonos();
        $this->request->data = $this->Cobranza->read();
        $facturas = $this->Cobranza->getFacturas();
        
        $this->set(compact('facturas'));
        //$this->render('ajaxFacturas');
        
        $view = new View($this, false);
        $retenciones=$this->Impuesto->find('list', array('conditions'=>array('es_retencion_iva'=>1)));
        $impuestos = $this->Impuesto->find('list', array('conditions' => array('es_retencion_islr' => 1)));
        $view->set(compact('facturas','retenciones','impuestos'));
        $data['body'] = $view->render('ajaxFacturas');
        $data['suma'] = CakeNumber::currency($sumaAbonos,'VEF');
        
        return json_encode($data);
        
    }

    function getAjaxFacturas() {
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            $facturas = $this->Cobranza->getFacturasACP($this->request->data);
            $this->Cobranza->id = 0;
            if (!empty($this->request->data['Cobranza'])) {
                $this->Cobranza->id = $this->request->data['Cobranza']['id'];
            }

            $sumaAbonos = $this->Cobranza->getSumaAbonos();
            $view = new View($this, false);
            //$retenciones = array(0=>0, 75=>75);
            
            $retenciones=$this->Impuesto->find('list', array('conditions'=>array('es_retencion_iva'=>1)));
            $impuestos = $this->Impuesto->find('list', array('conditions' => array('es_retencion_islr' => 1)));
            $view->set(compact('facturas', 'retenciones', 'impuestos'));
            $data['body'] = $view->render('getAjaxFacturas');
            $data['sumaAbonos'] = CakeNumber::currency($sumaAbonos,'VEF');

            return json_encode($data);
        }
    }

    function getAjaxRequisitos() {
        $this->autoRender = false;
        app::import('model', 'TipoInstrumento');
        $ti = new TipoInstrumento();
        $ti->unbindModel(array('hasMany' => array('Instrumento')));
        $tipoInstrumento = $ti->findById($this->request->data['Instrumento']['tipo_instrumento_id']);

        //pr($tipoInstrumento);
        $this->set(compact('tipoInstrumento'));
        $this->render('jsRequisitos');
    }

    function ajaxEliminaInstrumento() {
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            $id = $this->request->data['instrumento_id'];
            app::import('model', 'Instrumento');
            $i = new Instrumento();

            $i->unbindModel(array(
                'belongsTo' => array('Asociado', 'Cliente', 'Proveedor', 'TipoInstrumento', 'Banco', 'Cuenta')
            ));

            $instrumento = $i->read(null, $id);

            if ($i->data['Instrumento']['estatus_instrumento_id']==1){
                $i->delete();
            }else{
                $i->saveField('estatus_instrumento_id', 3);
            }

            $instrumentos = $this->Cobranza->Instrumento->find('all', array('conditions' => array('Instrumento.cobranza_id' => $instrumento['Instrumento']['cobranza_id'])));
            $sumaInstrumentos = $this->Cobranza->getSumaInstrumentos($instrumento['Instrumento']['cobranza_id']);
            $response = $this->set(compact('instrumentos', 'sumaInstrumentos'));

            $view = new View($this, false);
            $view->set(compact('instrumentos', 'sumaInstrumentos'));
            $data['body'] = $view->render('instrumentosvw');
            $data['suma'] = CakeNumber::currency($sumaInstrumentos, 'VEF');
            return json_encode($data);
//            $prueba = $this->render('instrumentosvw', 'ajax');
//            $this->output="";
            //pr($instrumento);
        }
    }
    
    
    public function ajaxEditFactura(){
        $this->uses = array('Factura');
        $this->autoRender=false;
        $this->Factura->id=$this->request->data['id'];
        $this->Factura->saveField($this->request->data['campo'], $this->request->data['valor']);
        
    }

}
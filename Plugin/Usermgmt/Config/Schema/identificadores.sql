--
-- Estructura de tabla para la tabla `usuarios`
--
alter table usuarios add (is_certified tinyint(1) NOT NULL DEFAULT '0');

--
-- Estructura de tabla para la tabla `identificadores`
--

CREATE TABLE IF NOT EXISTS `identificadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(100) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_user_id` (`unique_id`,`usuario_id`),
  KEY `user_id` (`usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Filtros para la tabla `identificadores`
--
ALTER TABLE `identificadores`
  ADD CONSTRAINT `usuarios_identificadores_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
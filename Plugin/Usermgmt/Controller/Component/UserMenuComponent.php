<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class UserMenuComponent extends Component {
	public $helpers=array('Inflector');
	public $components = array('Usermgmt.ControllerList', 'Usermgmt.UserAuth');

	public function getMenu() {
		$menu = '';
		if ($this->UserAuth->isAdmin()) {
			$allControllers = $this->ControllerList->get();
		} else {
			$allControllers = $this->UserAuth->getControllersAllowed();
		}		
		
		$menu = $this->_getControllersMenu($allControllers);
		return $menu;
	}

	private function _getControllersMenu($allControllers) {
		$menu = "<ul>\n";
		foreach ($allControllers as $controller => $value) {
			$menu .= "\t\t\t<li><a href='" . Inflector::underscore($controller) . "/index' onclick='return false;'>$controller <span class='ui-icon ui-icon-triangle-1-e'></span></a>\n\t\t\t\t<ul>\n";
			foreach ($value as $key=>$accion) {
				if ($key==$accion){
					$menu .= "\t\t\t\t\t\t<li><a href='" . Inflector::underscore($controller) . "/$accion'>$accion</a></li>\n";
				}else{
					if (is_int($key)){
						$menu .= "\t\t\t\t\t\t<li><a href='" . Inflector::underscore($controller) . "/$accion'>$accion</a></li>\n";
					}else{
						$menu .= "\t\t\t\t\t\t<li><a href='" . Inflector::underscore($controller) . "/$key'>$accion</a></li>\n";
					}
					
				}
				
			}
			$menu .= "\t\t\t\t</ul>\n\t\t\t</li>\n";
		}
		$menu.="</ul>";
		
		return $menu;
	}

}

?>
<?php

/*
  This file is part of UserMgmt.

  Author: Chetan Varshney (http://ektasoftwares.com)

  UserMgmt is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  UserMgmt is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

App::uses('UserMgmtAppController', 'Usermgmt.Controller');

class UsuariosController extends UserMgmtAppController {

	/**
	 * This controller uses following models
	 *
	 * @var array
	 */
	public $uses = array('Usermgmt.Usuario', 'Usermgmt.UsuarioGrupo', 'Usermgmt.LoginToken','Persona');

	/**
	 * Called before the controller action.  You can use this method to configure and customize components
	 * or perform logic that needs to happen before each controller action.
	 *
	 * @return void
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Usuario->userAuth = $this->UserAuth;
	}

	/**
	 * Used to display all users by Admin
	 *
	 * @access public
	 * @return array
	 */
	public function index() {
		$this->Usuario->unbindModel(array('hasMany' => array('LoginToken')));
		$users = $this->Usuario->find('all', array('order' => 'Usuario.id desc'));
		$this->set('users', $users);
	}

	/**
	 * Used to display detail of user by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @return array
	 */
	public function viewUser($userId = null) {
		if (!empty($userId)) {
			$user = $this->Usuario->read(null, $userId);
			$this->set('user', $user);
		} else {
			$this->redirect('/allUsers');
		}
	}

	/**
	 * Used to display detail of user by user
	 *
	 * @access public
	 * @return array
	 */
	public function myprofile() {
		$userId = $this->UserAuth->getUserId();
		$user = $this->Usuario->read(null, $userId);

		$fields = array(
			'conditions' => array(								
				'Machines.unique_id' => $macAddress, 
				'Machines.is_active' => '1',
				'Machines.usuario_id' => $user['User']['id']
				),
			'fields' => array('Machines.usuario_id')
			);						
		$userMac = $this->Usuario->Machines->find('first', $fields);		
		$this->set('user', $user);
		$this->set('userMachine', $userMac);
	}

	/**
	 * Used to logged in the site
	 *
	 * @access public
	 * @return void
	 */
	public function login() {
		if ($this->request->isPost()) {
			$this->Usuario->set($this->data);
			if ($this->Usuario->LoginValidate()) {
				$email = $this->data['Usuario']['email'];
				$password = $this->data['Usuario']['password'];
				$macAddress = $this->data['Usuario']['mac_address'];

				$user = $this->Usuario->findByUsername($email);
				if (empty($user)) {
					$user = $this->Usuario->findByEmail($email);
					if (empty($user)) {
						$this->Session->setFlash(__('Incorrect Email/Username or Password'), 'message_error');
						return;
					}
				}
				// check for inactive account
				if ($user['Usuario']['id'] != 1 and $user['Usuario']['active'] == 0) {
					$this->Session->setFlash(__('Sorry your account is not active, please contact to Administrator'), 'message_error');
					return;
				}
				// check for verified account
				if ($user['Usuario']['id'] != 1 and $user['Usuario']['email_verified'] == 0) {
					$this->Session->setFlash(__('Your registration has not been confirmed please verify your email or contact to Administrator'), 'message_error');
					return;
				}

				// check for especial user requirements unique mac address for identifications
				
				if($user['Usuario']['is_certified'] == 1){
					if(empty($macAddress)){
						$this->Session->setFlash('¡Por favor, debe permitir el acceso al Applet para poder registrar su identificador único!');
						return;
					} else {
						$fields = array(
							'conditions' => array(								
								'Machines.unique_id' => $macAddress, 
								'Machines.is_active' => '1',
								'Machines.usuario_id' => $user['Usuario']['id']
								),
							'fields' => array('Machines.usuario_id')
							);						
						$userMac = $this->Usuario->Machines->find('first', $fields);
						// check if this registered at least for once unique_id and active!
						if(empty($userMac)) {
							unset($fields['conditions']['Machines.is_active']);
							unset($fields['conditions']['Machines.unique_id']);
							$userMac = $this->Usuario->Machines->find('first', $fields);
							// check if this registered at least for once unique_id and don't matter is_active or not!
							if(empty($userMac)){								
								$this->Usuario->Machines->set('unique_id', $macAddress);
								$this->Usuario->Machines->set('is_active', '1');
								$this->Usuario->Machines->set('usuario_id', $user['Usuario']['id']);
								$this->Usuario->Machines->save();
							} else {
								$fields['conditions']['Machines.unique_id'] = $macAddress;
								$userMac = $this->Usuario->Machines->find('first', $fields);
								if(empty($userMac)){
									$this->Usuario->Machines->set('unique_id', $macAddress);
									//$this->Usuario->Machines->set('is_active', '0');
									$this->Usuario->Machines->set('usuario_id', $user['User']['id']);
									$this->Usuario->Machines->save();									
									$this->Session->setFlash(__('¡La maquina ha sido registrada, pero esta pendiente de aprobación!'));
									return;
								} else {
									$this->Session->setFlash(__('¡Maquina pendiente de aprobación!'));
									return;								
								}
							}
						}
					}									
				}	

				if (empty($user['Usuario']['salt'])) {
					$hashed = md5($password);
				} else {
					$hashed = $this->UserAuth->makePassword($password, $user['Usuario']['salt']);
				}
				if ($user['Usuario']['password'] === $hashed) {
					if (empty($user['Usuario']['salt'])) {
						$salt = $this->UserAuth->makeSalt();
						$user['Usuario']['salt'] = $salt;
						$user['Usuario']['password'] = $this->UserAuth->makePassword($password, $salt);
						$this->Usuario->save($user, false);
					}
					$this->UserAuth->login($user);
					$remember = (!empty($this->data['Usuario']['remember']));
					if ($remember) {
						$this->UserAuth->persist('2 weeks');
					}
					$OriginAfterLogin = $this->Session->read('Usermgmt.OriginAfterLogin');
					$this->Session->delete('Usermgmt.OriginAfterLogin');
					$redirect = (!empty($OriginAfterLogin)) ? $OriginAfterLogin : LOGIN_REDIRECT_URL;
					$this->redirect($redirect);
				} else {
					$this->Session->setFlash(__('Incorrect Email/Username or Password'), 'message_error');
					return;
				}
			}
		}
	}

	/**
	 * Used to logged out from the site
	 *
	 * @access public
	 * @return void
	 */
	public function logout() {
		$this->UserAuth->logout();
		$this->Session->setFlash(__('You are successfully signed out'), 'message_info');
		$this->redirect(LOGOUT_REDIRECT_URL);
	}

	/**
	 * Used to register on the site
	 *
	 * @access public
	 * @return void
	 */
	public function register() {
		$userId = $this->UserAuth->getUserId();
		if ($userId) {
			$this->redirect("/dashboard");
		}
		if (SITE_REGISTRATION) {
			$userGroups = $this->UsuarioGrupo->getGroupsForRegistration();
			$this->set('userGroups', $userGroups);
			if ($this->request->isPost()) {
				if (USE_RECAPTCHA && !$this->RequestHandler->isAjax()) {
					$this->request->data['Usuario']['captcha'] = (isset($this->request->data['recaptcha_response_field'])) ? $this->request->data['recaptcha_response_field'] : "";
				}
				$this->Usuario->set($this->data);
				if ($this->Usuario->RegisterValidate()) {
					if (!isset($this->data['Usuario']['usuario_grupo_id'])) {
						$this->request->data['Usuario']['usuario_grupo_id'] = DEFAULT_GROUP_ID;
					} elseif (!$this->UsuarioGrupo->isAllowedForRegistration($this->data['Usuario']['usuario_grupo_id'])) {
						$this->Session->setFlash(__('Please select correct register as'), 'message_info');
						return;
					}

					// check for especial register of unique mac identifier
					$macAddress = $this->request->data['User']['mac_address'];					
					if(empty($macAddress)){
						$this->Session->setFlash('¡Por favor, debe permitir el acceso al Applet para poder registrar su identificador único!');
						return;
					}

					$this->request->data['Usuario']['active'] = 1;
					if (!EMAIL_VERIFICATION) {
						$this->request->data['Usuario']['email_verified'] = 1;
					}
					$ip = '';
					if (isset($_SERVER['REMOTE_ADDR'])) {
						$ip = $_SERVER['REMOTE_ADDR'];
					}
					$this->request->data['Usuario']['ip_address'] = $ip;
					$salt = $this->UserAuth->makeSalt();
					$this->request->data['Usuario']['salt'] = $salt;
					$this->request->data['Usuario']['password'] = $this->UserAuth->makePassword($this->request->data['Usuario']['password'], $salt);
					$this->Usuario->save($this->request->data, false);
					$machine = array('unique_id' => $macAddress, 'is_active' => 1, 'usuario_id' => $this->User->id);
					$this->User->Machines->save($machine);					
					$userId = $this->Usuario->getLastInsertID();
					$user = $this->Usuario->findById($userId);
					if (SEND_REGISTRATION_MAIL && !EMAIL_VERIFICATION) {
						$this->Usuario->sendRegistrationMail($user);
					}
					if (EMAIL_VERIFICATION) {
						$this->Usuario->sendVerificationMail($user);
					}
					if (isset($this->request->data['Usuario']['email_verified']) && $this->request->data['Usuario']['email_verified']) {
						$this->UserAuth->login($user);
						$this->redirect('/');
					} else {
						$this->Session->setFlash(__('Please check your mail and confirm your registration'), 'message_info');
						$this->redirect('/register');
					}
				}
			}
		} else {
			$this->Session->setFlash(__('Sorry new registration is currently disabled, please try again later'), 'message_error');
			$this->redirect('/login');
		}
	}

	/**
	 * Used to change the password by user
	 *
	 * @access public
	 * @return void
	 */
	public function changePassword() {
		$userId = $this->UserAuth->getUserId();
		if ($this->request->isPost()) {
			$this->Usuario->set($this->data);
			if ($this->Usuario->RegisterValidate()) {
				$user = array();
				$user['Usuario']['id'] = $userId;
				$salt = $this->UserAuth->makeSalt();
				$user['Usuario']['salt'] = $salt;
				$user['Usuario']['password'] = $this->UserAuth->makePassword($this->request->data['Usuario']['password'], $salt);
				$this->Usuario->save($user, false);
				$this->LoginToken->deleteAll(array('LoginToken.usuario_id' => $userId), false);
				$this->Session->setFlash(__('Password changed successfully'), 'message_successful');
				$this->redirect('/dashboard');
			}
		}
	}

	/**
	 * Used to change the user password by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @return void
	 */
	public function changeUserPassword($userId = null) {
		if (!empty($userId)) {
			$name = $this->Usuario->getNameById($userId);
			$this->set('name', $name);
			if ($this->request->isPost()) {
				$this->Usuario->set($this->data);
				if ($this->Usuario->RegisterValidate()) {
					$user = array();
					$user['Usuario']['id'] = $userId;
					$salt = $this->UserAuth->makeSalt();
					$user['Usuario']['salt'] = $salt;
					$user['Usuario']['password'] = $this->UserAuth->makePassword($this->request->data['Usuario']['password'], $salt);
					$this->Usuario->save($user, false);
					$this->LoginToken->deleteAll(array('LoginToken.usuario_id' => $userId), false);
					$this->Session->setFlash(__('Password for %s changed successfully', $name), 'message_successful');
					$this->redirect('/allUsers');
				}
			}
		} else {
			$this->redirect('/allUsers');
		}
	}

	/**
	 * Used to add user on the site by Admin
	 *
	 * @access public
	 * @return void
	 */
	public function addUser() {
		$userGroups = $this->UsuarioGrupo->getGroups();
		$this->set('usuarioGrupos', $userGroups);
		$usuarios = $this->Persona->getPersonas();
		$this->set('personas', $usuarios);
		
		if ($this->request->isPost()) {
			$this->Usuario->set($this->data);
			if ($this->Usuario->RegisterValidate()) {
				$this->request->data['Usuario']['email_verified'] = 1;
				$this->request->data['Usuario']['active'] = 1;
				$salt = $this->UserAuth->makeSalt();
				$this->request->data['Usuario']['salt'] = $salt;
				$this->request->data['Usuario']['password'] = $this->UserAuth->makePassword($this->request->data['Usuario']['password'], $salt);
				$this->Usuario->save($this->request->data, false);
				$this->Session->setFlash(__('The user is successfully added'), 'message_successful');
				$this->redirect('/addUser');
			}
		}
	}

	/**
	 * Used to edit user on the site by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @return void
	 */
	public function editUser($userId = null) {
		if (!empty($userId)) {
			$userGroups = $this->UsuarioGrupo->getGroups();
			$this->set('usuarioGrupos', $userGroups);
			$usuarios = $this->Persona->getPersonas();
			$this->set('personas', $usuarios);
			if ($this->request->isPut()) {
				$this->Usuario->set($this->data);
				if ($this->Usuario->RegisterValidate()) {
					$this->Usuario->save($this->request->data, false);
					if(!empty($this->data['Machines'])){
						foreach ($this->data['Machines'] as $machine) {
							$this->User->Machines->updateAll($machine, array('usuario_id' => $this->User->id));
						}
					}					
					$this->Session->setFlash(__('The user is successfully updated'), 'message_successful');
					$this->redirect('/allUsers');
				}
			} else {
				$user = $this->Usuario->read(null, $userId);
				$this->request->data = null;
				if (!empty($user)) {
					$user['Usuario']['password'] = '';
					$this->request->data = $user;
					$this->set('user', $user);
				}
			}
		} else {
			$this->redirect('/allUsers');
		}
	}

	/**
	 * Used to delete the user by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @return void
	 */
	public function deleteUser($userId = null) {
		if (!empty($userId)) {
			if ($this->request->isPost()) {
				if ($this->Usuario->delete($userId, false)) {
					$this->LoginToken->deleteAll(array('LoginToken.usuario_id' => $userId), false);
					$this->Session->setFlash(__('User is successfully deleted'), 'message_successful');
				}
			}
			$this->redirect('/allUsers');
		} else {
			$this->redirect('/allUsers');
		}
	}

	/**
	 * Used to show dashboard of the user
	 *
	 * @access public
	 * @return array
	 */
	public function dashboard() {
		$userId = $this->UserAuth->getUserId();
		$user = $this->Usuario->findById($userId);
		$this->set('user', $user);
	}

	/**
	 * Used to activate or deactivate user by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @param integer $active active or inactive
	 * @return void
	 */
	public function makeActiveInactive($userId = null, $active = 0) {
		if (!empty($userId)) {
			$user = array();
			$user['Usuario']['id'] = $userId;
			$user['Usuario']['active'] = ($active) ? 1 : 0;
			$this->Usuario->save($user, false);
			if ($active) {
				$this->Session->setFlash(__('User is successfully activated'), 'message_successful');
			} else {
				$this->Session->setFlash(__('User is successfully deactivated'), 'message_successful');
			}
		}
		$this->redirect('/allUsers');
	}

	/**
	 * Used to verify email of user by Admin
	 *
	 * @access public
	 * @param integer $userId user id of user
	 * @return void
	 */
	public function verifyEmail($userId = null) {
		if (!empty($userId)) {
			$user = array();
			$user['Usuario']['id'] = $userId;
			$user['Usuario']['email_verified'] = 1;
			$this->Usuario->save($user, false);
			$this->Session->setFlash(__('User email is successfully verified') . 'message_successful');
		}
		$this->redirect('/allUsers');
	}

	/**
	 * Used to show access denied page if user want to view the page without permission
	 *
	 * @access public
	 * @return void
	 */
	public function accessDenied() {
		
	}

	/**
	 * Used to verify user's email address
	 *
	 * @access public
	 * @return void
	 */
	public function userVerification() {
		if (isset($_GET['ident']) && isset($_GET['activate'])) {
			$userId = $_GET['ident'];
			$activateKey = $_GET['activate'];
			$user = $this->Usuario->read(null, $userId);
			if (!empty($user)) {
				if (!$user['Usuario']['email_verified']) {
					$password = $user['Usuario']['password'];
					$theKey = $this->Usuario->getActivationKey($password);
					if ($activateKey == $theKey) {
						$user['Usuario']['email_verified'] = 1;
						$this->Usuario->save($user, false);
						if (SEND_REGISTRATION_MAIL && EMAIL_VERIFICATION) {
							$this->Usuario->sendRegistrationMail($user);
						}
						$this->Session->setFlash(__('Thank you, your account is activated now'), 'message_successful');
					}
				} else {
					$this->Session->setFlash(__('Thank you, your account is already activated'), 'message_info');
				}
			} else {
				$this->Session->setFlash(__('Sorry something went wrong, please click on the link again'), 'message_error');
			}
		} else {
			$this->Session->setFlash(__('Sorry something went wrong, please click on the link again'), 'message_error');
		}
		$this->redirect('/login');
	}

	/**
	 * Used to send forgot password email to user
	 *
	 * @access public
	 * @return void
	 */
	public function forgotPassword() {
		if ($this->request->isPost()) {
			$this->Usuario->set($this->data);
			if ($this->Usuario->LoginValidate()) {
				$email = $this->data['Usuario']['email'];
				$user = $this->Usuario->findByUsername($email);
				if (empty($user)) {
					$user = $this->Usuario->findByEmail($email);
					if (empty($user)) {
						$this->Session->setFlash(__('Incorrect Email/Username'), 'message_error');
						return;
					}
				}
				// check for inactive account
				if ($user['Usuario']['id'] != 1 and $user['Usuario']['email_verified'] == 0) {
					$this->Session->setFlash(__('Your registration has not been confirmed yet please verify your email before reset password'), 'message_info');
					return;
				}
				$this->Usuario->forgotPassword($user);
				$this->Session->setFlash(__('Please check your mail for reset your password'), 'message_info');
				$this->redirect('/login');
			}
		}
	}

	/**
	 *  Used to reset password when user comes on the by clicking the password reset link from their email.
	 *
	 * @access public
	 * @return void
	 */
	public function activatePassword() {
		if ($this->request->isPost()) {
			if (!empty($this->data['Usuario']['ident']) && !empty($this->data['Usuario']['activate'])) {
				$this->set('ident', $this->data['Usuario']['ident']);
				$this->set('activate', $this->data['Usuario']['activate']);
				$this->Usuario->set($this->data);
				if ($this->Usuario->RegisterValidate()) {
					$userId = $this->data['Usuario']['ident'];
					$activateKey = $this->data['Usuario']['activate'];
					$user = $this->Usuario->read(null, $userId);
					if (!empty($user)) {
						$password = $user['Usuario']['password'];
						$thekey = $this->Usuario->getActivationKey($password);
						if ($thekey == $activateKey) {
							$user['Usuario']['password'] = $this->data['Usuario']['password'];
							$salt = $this->UserAuth->makeSalt();
							$user['Usuario']['salt'] = $salt;
							$user['Usuario']['password'] = $this->UserAuth->makePassword($user['Usuario']['password'], $salt);
							$this->Usuario->save($user, false);
							$this->Session->setFlash(__('Your password has been reset successfully'),'message_success');
							$this->redirect('/login');
						} else {
							$this->Session->setFlash(__('Something went wrong, please send password reset link again'), 'message_error');
						}
					} else {
						$this->Session->setFlash(__('Something went wrong, please click again on the link in email'), 'message_error');
					}
				}
			} else {
				$this->Session->setFlash(__('Something went wrong, please click again on the link in email'), 'message_error');
			}
		} else {
			if (isset($_GET['ident']) && isset($_GET['activate'])) {
				$this->set('ident', $_GET['ident']);
				$this->set('activate', $_GET['activate']);
			}
		}
	}

	/**
	 * Used to send email verification mail to user
	 *
	 * @access public
	 * @return void
	 */
	public function emailVerification() {
		if ($this->request->isPost()) {
			$this->Usuario->set($this->data);
			if ($this->Usuario->LoginValidate()) {
				$email = $this->data['Usuario']['email'];
				$user = $this->Usuario->findByUsername($email);
				if (empty($user)) {
					$user = $this->Usuario->findByEmail($email);
					if (empty($user)) {
						$this->Session->setFlash(__('Incorrect Email/Username'), 'message_error');
						return;
					}
				}
				if ($user['Usuario']['email_verified'] == 0) {
					$this->Usuario->sendVerificationMail($user);
					$this->Session->setFlash(__('Please check your mail to verify your email'), 'message_info');
				} else {
					$this->Session->setFlash(__('Your email is already verified'), 'message_info');
				}
				$this->redirect('/login');
			}
		}
	}

}
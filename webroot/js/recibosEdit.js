$(function(){
	if (cuenta > 0){
		$('#dialogForm').dialog({
			'title':'Agregando Instrumento de Pago', 
			'modal':true, 
			'width':'700px', 
			buttons:[{
					'text':'guardar', click:function(){
						$('#InstrumentoFechaCarga').val($('#ReciboFecha').val());
						$('#InstrumentoAsociadoId').val($('#ReciboAsociadoId').val());
						$('#InstrumentoClienteId').val($('#ReciboClienteId').val());
						$('#InstrumentoProveedorId').val($('#ReciboProveedorId').val());
						$('#InstrumentoEditForm').submit()
					}
				}]
		});
	}

	$('#ReciboFechafld').datepicker({'altFormat':'yy-mm-dd' , 'altField':'#ReciboFecha'});

	$('.fechas').datepicker({'dateFormat':'yy-mm-dd'});

	$('#agregarFactura').bind('click',function(){
		if (disponible > 0){
			$('#facturas').dialog({
				'title':'Buscar Facturas Pendientes', 
				'modal':true, 
				'width':'900px',
				buttons:{"Aceptar":function(){
					$.ajax({
						url:'/recibos/setFacturaPagada'
					}).done(function(data){
						window.location.href = '/recibos/edit/'+reciboId
					});
					
				}}
			})

			$.ajax({
				url:'/recibos/getFacturasPendientes',
				type:'post',
				data:'recibo_id='+reciboId+'&data[Factura][recibo_id]='+reciboId+'&data[Factura][cliente_id]='+$('#ReciboClienteId').val()
			}).done(function(data){
				$('#detalleFacturas').html(data);
			});
		}else{
			alert('Debe tener disponibilidad de instrumentos, para agregar facturas al recibo');
		}
		
		return false;
	})

	$('.btn1').button();

	$('#agregarInstrumento').bind('click',function(){
		$('#InstrumentoId').val('');
		$('#dialogForm').dialog({
			'title':'Agregando Instrumento de Pago', 
			'modal':true, 
			'width':'700px', 
			buttons:[{
					'text':'Guardar', click:function(){
						$('#InstrumentoFechaCarga').val($('#ReciboFecha').val());
						$('#InstrumentoAsociadoId').val($('#ReciboAsociadoId').val());
						$('#InstrumentoClienteId').val($('#ReciboClienteId').val());
						$('#InstrumentoProveedorId').val($('#ReciboProveedorId').val());
						$('#InstrumentoEditForm').submit()
					}
				}]
		});
		return false;

	})
	
	$('#dialogForm').bind('dialogclose',function(){
		if (instAgregado > 0){
			instAgregado = 0;
			window.location.href = '/recibos/edit/'+reciboId
		}
		
	})

	$('#ReciboAsociadoId').bind('change', function(){
		$.ajax({
			url: "/recibos/getClientes",
			data: $('#ReciboEditForm').serialize(),
			type: 'post',
			success: function(data){
				$('#ReciboClienteId').html(data);
				$('#ReciboProveedorId').html('');
				$('#ReciboCuentaId').html('');
			}
		});
	})

	$('#ReciboClienteId').bind('change', function(){
		$.ajax({
			url: "/recibos/getProveedores",
			data: $('#ReciboEditForm').serialize(),
			type: 'post',
			success: function(data){
				$('#ReciboProveedorId').html(data);
				$('#ReciboCuentaId').html('');
			}
		});
	})

	$('#ReciboProveedorId').bind('change', function(){
		$.ajax({
			url: "/recibos/getCuentasBancarias",
			data: $('#ReciboEditForm').serialize(),
			type: 'post',
			success: function(data){
				$('#InstrumentoCuentaId').html(data);
				if (data==""){
					alert("El proveedor no tiene cuenta bancaria asociada")
				}
			}
		});
		
		$.ajax({
			url: "/recibos/getInstrumentosConciliados",
			data: $('#ReciboEditForm').serialize(),
			type:'post'
		}).done(function(data){$('#dataInstrumentosConciliados').html(data)});
	})

	$('#InstrumentoTipoInstrumentoId').bind('change',function(){
		$.ajax({
			'url':'/recibos/getCamposRequeridos/'+$(this).val(),
			'type':'post'
		}).done(function(data){
			var campos = jQuery.parseJSON(data);

			habilitaCampos(campos);


		})
	})

})

function habilitaCampos(campos){
	if (!campos.requiere_banco){
		$('#InstrumentoBancoId').val('');
		$('#fldBanco').hide();
	}else{
		$('#fldBanco').show();
	}

	if (!campos.requiere_referencia){
		$('#InstrumentoNroReferencia').val('');
		$('#fldReferencia').hide();
	}else{
		$('#fldReferencia').show();
	}

	if (!campos.requiere_cuenta){
		$('#InstrumentoCuentaId').val('');
		$('#fldCuenta').hide();
	}else{
		$('#fldCuenta').show();
	}
}

function editarInstrumento(id){
	$('#InstrumentoCuentaId').hide();
	
	$.ajax({
		url: "/recibos/getCuentasBancarias",
		data: 'data[Recibo][proveedor_id]='+$('#proveedorId'+id).val(),
		type: 'post'
	}).done(function(data){
		$('#InstrumentoCuentaId').html(data).val($('#cuentaId'+id).val()).show();

		if (data==""){
			alert("El proveedor no tiene cuenta bancaria asociada")
		}

	});

	$('#InstrumentoTipoInstrumentoId').val($('#tipoInstrumentoId'+id).val())
	$('#InstrumentoTipoInstrumentoId').change();
	$('#InstrumentoMonto').val($('#monto'+id).val())
	$('#InstrumentoNroReferencia').val($('#nroReferencia'+id).val())
	$('#InstrumentoBancoId').val($('#bancoId'+id).val())
	$('#InstrumentoId').val(id);
	$('#accion').val('detalle-editar');

	$('#dialogForm').dialog({
		'title':'Editando Instrumento de Pago', 
		'modal':true, 
		'width':'700px', 
		buttons:[{
				'text':'guardar', click:function(){
					$('#InstrumentoFechaCarga').val($('#ReciboFecha').val());
					$('#InstrumentoAsociadoId').val($('#ReciboAsociadoId').val());
					$('#InstrumentoClienteId').val($('#ReciboClienteId').val());
					$('#InstrumentoProveedorId').val($('#ReciboProveedorId').val());
					$('#InstrumentoEditForm').submit()
				}
			}]
	});
}
$(function() {
    $( "#fecha_requerida_display" ).datepicker({dateFormat: 'dd-mm-yy', altFormat:'yy-mm-dd', altField:'#fecha_requerida'});
    $( "#fecha_facturacion_display" ).datepicker({dateFormat: 'dd-mm-yy', altFormat:'yy-mm-dd', altField:'#fecha_facturacion'});

    $("#DetalleFacturaEditarForm").submit(function(){
        if ((jQuery("#DetalleFacturaCantidad").val() == "") || (jQuery("#DetalleFacturaCantidad").val() == "0")){
            //jQuery("#DetalleFacturaCantidad").val("1");
        } else if (jQuery("#DetalleFacturaDescripcion").val() == "") {
            $( "#dialog-modal3" ).dialog({
                height: 140,
                modal: true
            });
            return false;
        } else if ((jQuery("#DetalleFacturaPrecioUnitario").val() == "") || (jQuery("#DetalleFacturaPrecioUnitario").val() == "0")){
            $( "#dialog-modal4" ).dialog({
                height: 140,
                modal: true
            });
            return false;
        } else {
            return true;
        }
    });
});


function editarDetalleFactura(id){
    $('#DetalleFacturaId').val()
    $('#DetalleFacturaId').val(id)
    $('#DetalleFacturaCantidad').val($('#cantidad'+id).val())
    $('#DetalleFacturaPrecioUnitario').val($('#precio_unitario'+id).val())
    $('#DetalleFacturaConceptoId').val($('#concepto'+id).val())
    $('#DetalleFacturaConceptoId').change();
    $('#DetalleFacturaDescripcion').val($('#descripcion'+id).val())
    $('#DetalleFacturaPorcentajeIva').val($('#porcentaje_iva'+id).val())
    $('#DetalleFacturaPorcentajeIslr').val($('#porcentaje_islr'+id).val())
    $('#dialogForm').dialog({
        'title':'Editando Detalle de Factura',
        'modal':true,
        'width':'700px',
        buttons:[{
                'text':'guardar', click:function(){
                    $('#DetalleFacturaDescripcionId').val();
                    $('#DetalleFacturaEditarForm').submit()
                }
            }]
    });
}

function editarDatosFiscales(id){
    $('#dialogDatosFiscales').load('facturas/datosfiscales/' + id).dialog({
        'title':'Editando Datos Fiscales',
        'modal':true,
        'width':'700px',
        buttons:[{
                'text':'guardar', click:function(){
                    $('#FacturaDatosfiscalesForm').submit()
                }
            }]
    });
}
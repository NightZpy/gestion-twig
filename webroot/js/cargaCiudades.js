//AGREGAR PERSONAS
$(function (){
    $("#PersonaEstadoId").change(function(){
        $.ajax({
            type: "POST",
            url: "/ciudades/listar/"+jQuery("#PersonaEstadoId").val(),
            dataType:"json",
            success: function(response) {
                if (response.success == true) {
                    $("#PersonaCiudadId").html("");
                    $("#PersonaCiudadId").append('<option value="">-- seleccione --</option>');
                    $.each(response.ciudades,function (k,v){
                        $("#PersonaCiudadId").append('<option value='+k+'>'+v+'</option>');
                    });
                }else{
                    $("#PersonaCiudadId").html("");
                    $("#PersonaCiudadId").append('<option value="">-- seleccione --</option>');
                }
            }
        });
    });
});
//OBTENER DATOS FISCALES
$(document).ready(function() {
    $('#PersonaDocumento').focusout(function() {
        rif = $('#PersonaTipoDocumentoId').val()+$('#PersonaDocumento').val();
        url = 'personas/obtenerDatosFiscales/'+rif;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function(response) {
                if (response.code_result == 1) {
                    $('#PersonaRazonSocial').val(response.seniat.nombre);
                    $('#PersonaContribuyenteEspecial').attr('checked', response.seniat.agenteretencioniva);
                    $('#PersonaPorcentajeRetencionIva').val(response.seniat.tasa);
                }
            }
        });
    });
});
function cargarTipoAdjuntos(){	
	if (($('option', $("#PersonaTipoAdjuntoId")).length == 1) ){		
	    $.ajax({
	        type: "POST",
	        url: "/TipoAdjuntos/listar/",
	        dataType:"json",
	        success: function(response) {
	            if (response.success){
					$.each(response.tipos, function (i, tipo) { 
					  tipo = tipo.TipoAdjunto;
					  //$("#tipo_cuenta_id option[value='"+tipo.id+"']").remove();
					  //$('#tipo_adjunto_id').append("<option value='"+tipo.id+"'>"+tipo.nombre+"</option>");
					  $('#PersonaTipoAdjuntoId').append("<option value='"+tipo.id+"'>"+tipo.nombre+"</option>");
					}); 
	            }else{
	            	//console.log("Vacio");
	            }
	        }
	    });    
	}
}

function agregarNuevoCampoArchivo(){
	var tr = '<tr><td><input type="file" name="data[Adjunto][{0}][ruta][]" id="file_{0}"></td><td>{1}<input type="hidden" name="data[Adjunto][{0}][tipo_adjunto_id]" value="{2}"></td></tr>';
	var tipoAdjunto = $('#PersonaTipoAdjuntoId').find('option:selected');
	var tipoText = $(tipoAdjunto).text();
	var tipoId = $(tipoAdjunto).val();
	var nAdjuntos = ($('tr', $("#adjuntos-table")).length) - 1;
	tr = String.format(tr, nAdjuntos, tipoText, tipoId);
	$('#adjuntos-table').append(tr);	
}



$(function() {
	$('#PersonaTipoAdjuntoId').click(function() {
		cargarTipoAdjuntos();
	});

	$( "#create-attach" )
	  	.button()
	  	.click(function() {
		agregarNuevoCampoArchivo();
	});
});
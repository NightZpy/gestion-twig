$(function(){
    $('#CobranzaAsociadoId').bind('change', function(e){
        $('#CobranzaClienteId').html('');
        $('#CobranzaProveedorId').html('');
        $.ajax({
            url:'/cobranzas/getAjaxClientes/',
            type:'post',
            data:$('#CobranzaEditForm').serialize()
        }).done(function(data){
            $('#CobranzaClienteId').html(data);
            
            $('#CobranzaClienteId').bind('change', function(e){
                $('#CobranzaProveedorId').html('');
                $.ajax({
                    url:'/cobranzas/getAjaxProveedores/',
                    type:'post',
                    data:$('#CobranzaEditForm').serialize()
                }).done(function(data){
                    $('#CobranzaProveedorId').html(data);
                })
            })
        })
    })
    
    $('#CobranzaProveedorId').bind('change', function(e){
        $.ajax({
            url:'/cobranzas/getAjaxCuentas',
            type:'post',
            data:$('#CobranzaEditForm').serialize()
        }).done(function(data){
            $('#InstrumentoCuentaId').html(data)
        })
    }) 
    

    $('#InstrumentoMonto').keypress(function(e){
        if (e.which==13){
            e.preventDefault();
            
            error=0;
            mensaje="";
            if (requiereReferencia==1 && $('#InstrumentoNroReferencia').val()==''){
                mensaje="El instrumento requiere un número de referencia\n";
                error=1;
            }

            if (requiereBanco==1 && $('#InstrumentoBancoId').val()==''){
                mensaje+='El instrumento requiere de un banco origen';
                error=1;
            }
            
            if (!error){
                $.ajax({
                    url:'/cobranzas/edit/'+$('#CobranzaId').val(),
                    type:'post',
                    data:$('#CobranzaEditForm').serialize()+"&agregaInstrumento=1"
                }).done(function (data){
                    var obj = jQuery.parseJSON(data)
                    $('#tbodyEditCobranza').html(obj.body)
                    $('#suma').html(obj.suma)
                    $('#InstrumentoMonto').val('');
                    $('#InstrumentoNroReferencia').val('');
                    $('#InstrumentoTipoInstrumentoId').val('');
                    $('#InstrumentoBancoId').val('');
                    $('#InstrumentoTipoInstrumentoId').focus();
                    
                    
                })
            }else{
                alert(mensaje)
            }
            
        }

    });    
    
    $('.abonaFactura').bind('click', function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        id = id.substring(3,id.length);
        $.ajax({
            url:'/cobranzas/ajaxAbonaFactura/'+id,
            type:'post',
            data:'data[Cobranza][id]='+$('#CobranzaId').val()
        }).done(function(data){
            var obj = jQuery.parseJSON(data)
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
        });
    })
    
    $('.resetFactura').bind('click', function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        id = id.substring(3,id.length);
        $.ajax({
            url:'/cobranzas/ajaxResetFactura/'+id,
            type:'post',
            data:'data[Cobranza][id]='+$('#CobranzaId').val()
        }).done(function(data){
            var obj = jQuery.parseJSON(data)
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
            
        });
    })

    $('#InstrumentoTipoInstrumentoId').bind('change',function(e){
        $.ajax({
            url:'/cobranzas/getAjaxRequisitos',
            type:'post',
            data:$('#CobranzaEditForm').serialize()
        }).done(function(data){
            $('#script').html(data);
        });
    })
    
    
    $('.instelim').bind('click', function(e){
        e.preventDefault();
        var instIdStr = $(this).attr('id');
        var instId = instIdStr.substring(5,15);
        //alert(instId);
        
        $.ajax({
            url:'/cobranzas/ajaxEliminaInstrumento',
            type:'post',
            data:'instrumento_id='+instId
        }).done(function(data){
           var obj = jQuery.parseJSON(data)
           $('#tbodyEditCobranza').html(obj.body);
           $('#suma').html(obj.suma);
           $('#script').html(obj.script);
        }); 
    })    
    
    $('.fecha').datepicker({
        altFormat:'yy-mm-dd',
        onClose:function(){
            $(this).datepicker('option','altField', '#'+$(this).attr('id')+'H')
        }
    });
    
    $('#filtro').bind('click', function(e){
        e.preventDefault();
        //alert('niño pobre');
        $.ajax({
            url:'/cobranzas/getAjaxFacturas',
            type:'post',
            data:$('#CobranzaEditForm').serialize()
        }).done(function(data){
            var obj = jQuery.parseJSON(data);
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
        });
    })
});
function agregarOpciones (opciones, tipo, elemento) {
	$.each(opciones, function (i, opcion) {
		var dato; 
		if(tipo=="bancos")
	  		dato = opcion.Banco;
	  	else if(tipo=="tipo_cuentas")
	  		dato = opcion.TipoCuenta;
	  	//$("#PersonaBancoId option[value='"+banco.id+"']").remove();
	  	elemento.append("<option value='"+dato.id+"'>"+dato.nombre+"</option>");
	}); 
}

function cargarBancos(){
	//$("#account-form").find('#CuentaBancoId option').empty();
	//$("#account-form").find('#CuentaBancoId option').each(function(){ $(this).remove();});
	if (($('option', $("#account-form").find('#CuentaBancoId')).length) <= 1){
	    $.ajax({
	        type: "POST",
	        url: "/Bancos/listar/",
	        dataType:"json",
	        success: function(response) {
	            if (response.success){            	
					agregarOpciones(response.bancos, "bancos", $("#account-form").find('#CuentaBancoId'));
	            }else{
	            	//console.log("Vacio");
	            }
	        }
	    });
    }    
}

function cargarTipoCuentas(){
	//$("#account-form").find('#CuentaTipoCuentaId option').each(function(){ $(this).remove();});
	if (($('option', $("#account-form").find('#CuentaTipoCuentaId')).length) <= 1){
	    $.ajax({
	        type: "POST",
	        url: "/TipoCuentas/listar/",
	        dataType:"json",
	        success: function(response) {
	            if (response.success){
					agregarOpciones(response.tipos, "tipo_cuentas", $("#account-form").find('#CuentaTipoCuentaId'))
	            }else{
	            	//console.log("Vacio");
	            }
	        }
	    });    
	}
}



$(function() {
	$( "#create-account" )
	  	.button()
	  	.click(function() {
		cargarBancos();
		cargarTipoCuentas();
	});
});
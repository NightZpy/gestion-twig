$(function(){
    $('#CobranzaAsociadoId').bind('change', function(e){
        $('#CobranzaClienteId').html('');
        $('#CobranzaProveedorId').html('');
        $.ajax({
            url:'/cobranzas/getAjaxClientes/',
            type:'post',
            data:$('#CobranzaAddForm').serialize()
        }).done(function(data){
            $('#CobranzaClienteId').html(data);
            
            $('#CobranzaClienteId').bind('change', function(e){
                $('#CobranzaProveedorId').html('');
                $.ajax({
                    url:'/cobranzas/getAjaxProveedores/',
                    type:'post',
                    data:$('#CobranzaAddForm').serialize()
                }).done(function(data){
                    $('#CobranzaProveedorId').html(data);
                })
                
            })
        })
    })
    
    $('#CobranzaProveedorId').bind('change', function(e){
        $.ajax({
            url:'/cobranzas/getAjaxCuentas',
            type:'post',
            data:$('#CobranzaAddForm').serialize()
        }).done(function(data){
            $('#InstrumentoCuentaId').html(data)
        })
        
        $.ajax({
            url:'cobranzas/getAjaxFacturas/',
            type:'post',
            data:$('#CobranzaAddForm').serialize()
        }).done(function(data){
            var obj = jQuery.parseJSON(data)
            $('#detalleFacturas').html(obj.body);
            $('#sumaAbonos').html(obj.suma);
        })
        
        
    })    
    
    $(function(){
        $('#InstrumentoMonto').keypress(function(e){
            if (e.which==13){
                e.preventDefault();
                error=0;
                mensaje="";
                if (requiereReferencia==1 && $('#InstrumentoNroReferencia').val()==''){
                    mensaje="El instrumento requiere un numero de referencia\n";
                    error=1;
                }
                
                if (requiereBanco==1 && $('#InstrumentoBancoId').val()==''){
                    mensaje+='El instrumento requiere de un banco origen';
                    error=1;
                }
                
                if (!error){
                    $('#CobranzaAddForm').submit()
                }else{
                    alert(mensaje)
                }
                
                /*$.ajax({
                    url:'/cobranzas/setAjaxInstrumento',
                    type:'post',
                    data:$('#CobranzaAddForm').serialize()
                }).done(function (data){
                    $('#addCobranza').html(data)
                })*/
            }
            
        });    
    })
    
    
    $('#InstrumentoTipoInstrumentoId').bind('change',function(e){
        $.ajax({
            url:'/cobranzas/getAjaxRequisitos',
            type:'post',
            data:$('#CobranzaAddForm').serialize()
        }).done(function(data){
            $('#script').html(data);
        });
    })
    
    
    
})


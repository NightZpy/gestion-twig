$(function(){
	$('#ReciboFechafld').datepicker({'altFormat':'yy-mm-dd' , 'altField':'#ReciboFecha'});

	$('#ReciboAsociadoId').bind('change', function(){
		$.ajax({
			url: "/recibos/getClientes",
			data: $('#ReciboAddForm').serialize(),
			type: 'post',
			success: function(data){
				$('#ReciboClienteId').html(data);
				$('#ReciboProveedorId').html('');
				$('#ReciboCuentaId').html('');
			}
		});
	})

	$('#ReciboClienteId').bind('change', function(){
		$.ajax({
			url: "/recibos/getProveedores",
			data: $('#ReciboAddForm').serialize(),
			type: 'post',
			success: function(data){
				$('#ReciboProveedorId').html(data);
				$('#ReciboCuentaId').html('');
			}
		});
	})
})

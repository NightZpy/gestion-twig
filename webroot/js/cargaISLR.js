$(document).ready(function (){
    $("#DetalleFacturaConceptoId").change(function(){
        $.ajax({
            type: "POST",
            url: "/impuestos/islr/"+jQuery("#DetalleFacturaConceptoId").val(),
            dataType:"json",
            success: function(response) {
            if (response.success == true){
                        $("#DetalleFacturaPorcentajeIslr").html("");
                        $.each(response.ISLR,function (k,v){
                            $("#DetalleFacturaPorcentajeIslr").append('<option value='+v+'>'+v+'</option>');
                        });
                    }else{
                        $("#DetalleFacturaPorcentajeIslr").html("");
                        $("#DetalleFacturaPorcentajeIslr").append('<option value="">Seleccione</option>');
                    }
            }
        });
        $.ajax({
            type: "POST",
            url: "/detalle_facturas/buscarDescripcion/"+jQuery("#DetalleFacturaConceptoId").val(),
            dataType:"json",
            success: function(response) {
            if (response.success == true){
                        $("#DetalleFacturaDescripcionId").html("");
                        $.each(response.Descripcion,function (k,v){
                            $("#DetalleFacturaDescripcionId").append('<option value="'+v+'">'+v+'</option>');
                        });
                    }else{
                        $("#DetalleFacturaDescripcionId").html("");
                    }
            }
        });
    });
});

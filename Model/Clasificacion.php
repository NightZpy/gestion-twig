<?php
App::uses('AppModel', 'Model');

class Clasificacion extends AppModel 
{
   
    public $displayField = 'nombre';
   
	public $hasMany = array(
		'Persona' => array(
			'className' => 'Persona',
			'foreignKey' => 'clasificacion_id',
		)
	);
    public $validate = array(
        'nombre' => array(
            'rule' => 'notempty',
            'message' => 'Este campo no puede estar vacio.',
            'required' => true,
        ),
    );
}
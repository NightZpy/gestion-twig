<?php
App::uses('AppModel', 'Model');
/**
 * Descripcion Model
 *
 * @property Proveedor $Proveedor
 * @property Concepto $Concepto
 */
class Descripcion extends AppModel {

    
    public $displayField='descripcion';
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'proveedor_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'concepto_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descripcion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Proveedor' => array(
			'className' => 'Persona',
			'foreignKey' => 'proveedor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Concepto' => array(
			'className' => 'Concepto',
			'foreignKey' => 'concepto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public $actsAs = array(
        'Search.Searchable',
    );
    
    public $filterArgs=array(
        'concepto_id'=>array('type'=>'value'),
        'proveedor_id'=>array('type'=>'value'),
        'descripcion'=>array('type'=>'like'),
      
    );
    
    public function getConceptosGrupo(Array $options){
        $listaCompleta=$this->find('list', $options);
         $arr['Desc. ISLR']=$listaCompleta;
         
         $lista = $this->find('list');
         $arr['Desc. PROVEEDOR'] = $lista;
         return $arr;
    }
}

<?php
App::uses('AppModel', 'Model');
class Impresion extends AppModel 
{
    var $useTable = false;
	var $_schema = array(  
    'numero_factura'   =>array('type'=>'string', 'lenght'=>100),   
	'proveedor_id'   =>array('type'=>'integer'), 
	'condicion'   =>array('type'=>'string', 'lenght'=>100),
	'locallidad'   =>array('type'=>'string', 'lenght'=>100),	
);  
	public $validate = array(
		'numero_factura' => array(
            'rule' => '/^[a-zA-Z0-9-]{1,}$/i',
            'message' => 'Debe indicar un numero de factura. Debe ser solo numeros, letras y guiones',
		),
		'proveedor_id' => array(
            'rule' => 'numeric',
            'message' => 'Debe seleccionar un proveedor.',
		),
		'condicion' => array(
            'rule' => 'numeric',
            'message' => 'Marque la condicion de pago.',
		),
        'localidad' => array(
            'rule' => 'notEmpty',
            'message' => 'Debe indicar una localidad',
		)
    );
}
<?php
App::uses('AppModel', 'Model');

class DetalleFactura extends AppModel 
{

    public $belongsTo = array(
		'Concepto' => array(
			'className' => 'Concepto',
			'foreignKey' => 'concepto_id',
		),
        'Factura' => array(
			'className' => 'Factura',
			'foreignKey' => 'factura_id',
		),
    );
    
    function montosDetalleFacturas($id = null)
    {
        $Query = $this->Factura->DetalleFactura->query('
                                SELECT 
                                    DetalleFactura.factura_id,
                                    SUM(DetalleFactura.monto_base) AS monto_base,
                                    SUM(DetalleFactura.monto_iva) AS monto_iva,
                                    SUM(DetalleFactura.monto_islr) AS monto_islr,
                                    SUM(DetalleFactura.monto_total) AS monto_total
                                FROM
                                    detalle_facturas AS DetalleFactura
                                WHERE 
                                    DetalleFactura.factura_id = ' . $id . '
                                GROUP BY DetalleFactura.factura_id');
        return $Query;
    }
    
    function montosFacturas($requerimiento_id = null)
    {
        $Query = $this->Factura->query('
                                SELECT 
                                    Factura.requerimiento_id,
                                    SUM(Factura.monto_base) AS monto_base,
                                    SUM(Factura.monto_retencion_islr) AS monto_retencion_islr,
                                    SUM(Factura.monto_iva) AS monto_iva,
                                    SUM(Factura.monto_retencion_iva) AS monto_retencion_iva,
                                    SUM(Factura.monto_total) AS monto_total,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 AS monto_costo_gestion,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100 AS monto_costo_operativo,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END AS monto_costo_venta,
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END AS monto_costo_venta_secundario,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END -
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END
                                    AS monto_costo_administrativo
                                FROM
                                    facturas AS Factura,
                                    requerimientos AS Requerimiento
                                WHERE 
                                    Requerimiento.id = Factura.requerimiento_id
                                AND
                                    Factura.requerimiento_id = ' . $requerimiento_id . '
                                GROUP BY Factura.requerimiento_id');
        return $Query;
    }
    
    function montosFacturasCostos($factura_id = null)
    {
        $Query = $this->Factura->query('
                                SELECT 
                                    Factura.requerimiento_id,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 AS monto_costo_gestion,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100 AS monto_costo_operativo,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END AS monto_costo_venta,
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END AS monto_costo_venta_secundario,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END -
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END
                                    AS monto_costo_administrativo
                                FROM
                                    facturas AS Factura,
                                    requerimientos AS Requerimiento
                                WHERE
                                    Requerimiento.id = Factura.requerimiento_id
                                AND
                                    Factura.id = ' . $factura_id . '
                                GROUP BY Factura.requerimiento_id');
        return $Query;
    }
    
    function afterSave($options = Array())
    {
        $montosDetalleFactura = ($this->montosDetalleFacturas($this->data['DetalleFactura']['factura_id']));
        $porcentajeRetencionIva = $this->Factura->find('first',
                                         array('conditions' => 
                                           array('Factura.id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id']),
                                                 'fields' => 'porcentaje_retencion_iva', 
                                                                ));
        $facturas = array('Factura' => array(
                                            'id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id'],
                                            'monto_base' => $montosDetalleFactura[0][0]['monto_base'],
                                            'monto_iva' => $montosDetalleFactura[0][0]['monto_iva'],
                                            'monto_total' => $montosDetalleFactura[0][0]['monto_total'],
                                            'monto_maximo' => $montosDetalleFactura[0][0]['monto_total'],
                                            'monto_retencion_islr' => $montosDetalleFactura[0][0]['monto_islr'],
                                            'monto_retencion_iva' => $montosDetalleFactura[0][0]['monto_iva'] * 
                                                                     $porcentajeRetencionIva['Factura']['porcentaje_retencion_iva'] / 
                                                                     100,
                                            )
                         );
        $fact = ClassRegistry::init('Factura');
        $fact->save($facturas);
        
        $montosFacturasCostos = $this->montosFacturasCostos($this->data['DetalleFactura']['factura_id']);

        $facturas = array('Factura' => array(
                                            'id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id'],
                                            'monto_costo_gestion' => $montosFacturasCostos[0][0]['monto_costo_gestion'],
                                            'monto_costo_operativo' => $montosFacturasCostos[0][0]['monto_costo_operativo'],
                                            'monto_costo_venta' => $montosFacturasCostos[0][0]['monto_costo_venta'],
                                            'monto_costo_venta_secundario' => $montosFacturasCostos[0][0]['monto_costo_venta_secundario'],
                                            'monto_costo_administrativo' => $montosFacturasCostos[0][0]['monto_costo_administrativo'],
                                            )
                         );
        $fact = ClassRegistry::init('Factura');
        $fact->save($facturas);
        
        $islrId = $this->getISLRDetalle($montosDetalleFactura[0]['DetalleFactura']['factura_id']);        
        
        $facturas = array(
            'Factura' => array(
                'id' => $montosDetalleFactura[0]['DetalleFactura']['factura_id'],
                'impuesto_islr_id' => $islrId,
                'impuesto_islr_id_modif' => $islrId,
                )
            );
        $fact->save($facturas);
        
        $montosFactura = ($this->montosFacturas($this->data['Factura']['requerimiento_id']));
        $requerimientos = array('Requerimiento' => array(
                                            'id' => $montosFactura[0]['Factura']['requerimiento_id'],
                                            'monto_costo_gestion' => $montosFactura[0][0]['monto_costo_gestion'],
                                            'monto_costo_operativo' => $montosFactura[0][0]['monto_costo_operativo'],
                                            'monto_costo_venta' => $montosFactura[0][0]['monto_costo_venta'],
                                            'monto_costo_venta_secundario' => $montosFactura[0][0]['monto_costo_venta_secundario'],
                                            'monto_costo_administrativo' => $montosFactura[0][0]['monto_costo_administrativo'],
                                            'monto_base' => $montosFactura[0][0]['monto_base'],
                                            'monto_iva' => $montosFactura[0][0]['monto_iva'],
                                            'monto_total' => $montosFactura[0][0]['monto_total'],
                                            'monto_retencion_islr' => $montosFactura[0][0]['monto_retencion_islr'],
                                            'monto_retencion_iva' => $montosFactura[0][0]['monto_retencion_iva'],
                                            )
                         );
        $reque = ClassRegistry::init('Requerimiento');
        $reque->save($requerimientos);
    }
    
    function beforeSave($options = Array()) 
    {
        //pr ($this->data);
        App::import('model', 'Factura');
        $f=new Factura();
        $f->read(null, $this->data['DetalleFactura']['factura_id']);
        
        $sql = "select valor from impuestos where id=" . $this->data['DetalleFactura']['impuesto_iva_id'];
        $ivaRes = $this->query($sql);
        $iva = $ivaRes[0]['impuestos']['valor'];
        
        
        $sql="select valor from conceptos c
                inner join impuestos i on  i.id=c.impuesto_id
                where c.id=" . $this->data['DetalleFactura']['concepto_id'];
        $r=$this->query($sql);
        //pr($r);
        $islr=$r[0]['i']['valor'];
        $cantidad= (isset($this->data['DetalleFactura']['cantidad']) && $this->data['DetalleFactura']['cantidad']!="")?$this->data['DetalleFactura']['cantidad']:1;
        
        $this->data['DetalleFactura']['monto_base'] = ($this->data['DetalleFactura']['precio_unitario'] * 
                                                          $cantidad);
        $this->data['DetalleFactura']['monto_iva']  = (($this->data['DetalleFactura']['monto_base'] * 
                                                          $iva) / 100);
        $this->data['DetalleFactura']['monto_islr']  = (($this->data['DetalleFactura']['monto_base'] * 
                                                          $islr) / 100);
        $this->data['DetalleFactura']['monto_total']  = $this->data['DetalleFactura']['monto_base'] +
                                                       $this->data['DetalleFactura']['monto_iva'];
        $clienteId = $this->Factura->Requerimiento->find('first', 
                                                          array('conditions' => 
                                                            array('Requerimiento.id' => 
                                                                  $this->data['Factura']['requerimiento_id']),
                                                                  'fields' => 'cliente_id',
                                                                 )
                                                        );
        $porcentajeRetencionIva = $this->Factura->Persona->find('first',
                                         array('conditions' => 
                                           array('Persona.id' => $clienteId['Requerimiento']['cliente_id']),
                                                 'fields' => 'porcentaje_retencion_iva', 
                                                                ));
        $porcentaje_retencion_iva = array('Factura' => array(
                                            'id' => $this->data['DetalleFactura']['factura_id'],
                                            'porcentaje_retencion_iva' => $porcentajeRetencionIva['Persona']['porcentaje_retencion_iva'],
                                            'monto_maximo'=>$f->data['Factura']['monto_total'],
                                            )
                         );
        $fact = ClassRegistry::init('Factura');
        $fact->save($porcentaje_retencion_iva);

        return true;
    }
        
    function afterDelete() {
        parent::afterDelete();        
        $fact = ClassRegistry::init('Factura');        
        $islrId = $this->getISLRDetalle($this->data['Factura']['id']);        
        
        $facturas = array(
            'Factura' => array(
                'id' => $this->data['Factura']['id'],
                'impuesto_islr_id' => $islrId,
                'impuesto_islr_id_modif' => $islrId,
                )
            );
        $fact->save($facturas);
        
        
        
    }
       
    function getDescripciones()
    {
        $query = $this->query("SELECT descripcion
                               FROM detalle_facturas DetalleFactura
                               GROUP BY descripcion
                              ");
        return $query;
    }
    
    function duplicaDetalleFacturas($requerimiento_id, $factura_id)
    {
        $query = $this->query("INSERT INTO detalle_facturas (factura_id, 
                               concepto_id, descripcion, cantidad, precio_unitario, 
                               monto_base, monto_iva, monto_islr, monto_total, 
                               porcentaje_iva, porcentaje_islr, impuesto_iva_id) 
                               SELECT " . $factura_id . ", detalle_facturas.concepto_id, 
                               detalle_facturas.descripcion, detalle_facturas.cantidad, 
                               detalle_facturas.precio_unitario, detalle_facturas.monto_base, 
                               detalle_facturas.monto_iva, detalle_facturas.monto_islr, 
                               detalle_facturas.monto_total, detalle_facturas.porcentaje_iva, 
                               detalle_facturas.porcentaje_islr, detalle_facturas.impuesto_iva_id FROM detalle_facturas, facturas 
                               WHERE facturas.id = detalle_facturas.factura_id 
                               AND requerimiento_id = " . $requerimiento_id);
        return $query;
    }
    
    function duplicaDetalleFacturas2($requerimiento_id)
    {
        $query = $this->query("insert into detalle_facturas(factura_id, concepto_id, descripcion, cantidad, precio_unitario, monto_base, monto_iva, monto_islr, monto_total, impuesto_iva_id)
                                select destino.id, DetalleFactura.concepto_id, DetalleFactura.descripcion, DetalleFactura.cantidad, DetalleFactura.precio_unitario,
                                       DetalleFactura.monto_base, DetalleFactura.monto_iva, DetalleFactura.monto_islr, DetalleFactura.monto_total, DetalleFactura.impuesto_iva_id from facturas destino 
                                inner join facturas origen on origen.id=destino.copia_factura_id
                                inner join detalle_facturas DetalleFactura on DetalleFactura.factura_id=origen.id
                                where destino.requerimiento_id=$requerimiento_id");
        return $query;
    }
    
    public function getISLRDetalle($facturaId){
        $return = NULL;
        $sql="select impuesto_id from detalle_facturas DetalleFactura
                inner join conceptos Concepto on Concepto.id=DetalleFactura.concepto_id
                where factura_id=$facturaId";
        $res=$this->query($sql);
        
        if($res){
            $return = $res[0]['Concepto']['impuesto_id'];
        }
        
        return $return;
    }
}
<?php
App::uses('AppModel', 'Model');

class Transicion extends AppModel 
{
    public $belongsTo = array(
        'EstatusInicial' => array(
            'className' => 'EstatusWorkflow',
            'foreignKey' => 'estatus_inicial_id'
        ),
        'EstatusFinal' => array(
            'className' => 'EstatusWorkflow',
            'foreignKey' => 'estatus_final_id'
        ),
        'Accion'
    );
    
    public $validate = array(
        'estatus_inicial_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un valor.',
			),
		),
        'estatus_final_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un valor.',
			),
		),
        'accion_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un valor.',
			),
		),
        'rol_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un valor.',
			),
		),
    );
}
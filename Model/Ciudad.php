<?php
App::uses('AppModel', 'Model');

class Ciudad extends AppModel 
{
    public $displayField = 'nombre';
    
    public $validate = array(
        'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacío.',
			),
		),
        'estado_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
				'message' => 'Debe seleccionar una opción.',
            ),
        ),
    );
    
    public $hasMany = array('Persona');
    
    public $belongsTo = array('Estado');
}
<?php
App::uses('AppModel', 'Model');

class Rubro extends AppModel 
{
	public $displayField = 'nombre';

	public $hasMany = array(
		'Persona' => array(
			'className' => 'Persona',
			'foreignKey' => 'rubro_id',
		)
	);

	public $validate = array(
        'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacio.',
			),
		),
    );
}
<?php
App::uses('AppModel', 'Model');

class TipoAdjunto extends AppModel 
{
	public $displayField = 'nombre';
    
    public $hasMany = array('Adjunto');
}
<?php
App::uses('AppModel', 'Model');

class ConceptoProveedor extends AppModel 
{

	//public $displayField = 'nombre_corto';

	public $hasMany = array(
		'Proveedor' => array(
			'className' => 'Persona',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
		),
	);
    
    public $belongsTo=array(
        'Concepto'
    );
    
    public function getConceptoProveedor($proveedorId){
        $sql="select conceptos.id, conceptos.nombre, porcentaje_islr from concepto_proveedores
                inner join conceptos on conceptos.id = concepto_proveedores.concepto_id
                where proveedor_id=$proveedorId;";
        $result=$this->query($sql);
        return $result;
    }
    
    public function buscarConceptos($proveedorId, $impuestoIslrId=NULL){
        $valor=NULL;
        $filtro="";
        if ($impuestoIslrId){
            $sql="select valor from impuestos Impuesto where es_retencion_islr=1 and id=$impuestoIslrId";
            $res=$this->query($sql);
            $valor=$res[0]['Impuesto']['valor'];
        }
        
        if ($valor){
            $filtro=" and valor=$valor";
        }
        
        $sql="select conceptos.id, conceptos.nombre, impuestos.valor from concepto_proveedores cp
                inner join personas prov on prov.id=cp.proveedor_id
                inner join conceptos on conceptos.id=cp.concepto_id 
                inner join impuestos on impuestos.id=conceptos.impuesto_id
                where prov.id=$proveedorId $filtro
                order by 1";
        return $this->query($sql);
    }
    
    public function buscarConceptosUsados($proveedorId, $clienteId, $impuestoIslrId=NULL){
        $valor=NULL;
        $filtro="";
        if ($impuestoIslrId){
            $sql="select valor from impuestos Impuesto where es_retencion_islr=1 and id=$impuestoIslrId";
            $res=$this->query($sql);
            $valor=$res[0]['Impuesto']['valor'];
        }
        
        if ($valor){
            $filtro=" and valor=$valor";
        }
        
        $sql="select 
                conceptos.id, conceptos.nombre, count(*) 
            from 
                detalle_facturas DetalleFactura
            inner join conceptos  on conceptos.id=DetalleFactura.concepto_id
            inner join facturas Factura on Factura.id=DetalleFactura.factura_id
            inner join requerimientos Requerimiento on Requerimiento.id=Factura.requerimiento_id
            inner join impuestos Impuesto on Impuesto.id=conceptos.impuesto_id
            where
                cliente_id=$clienteId $filtro
            group by 
                DetalleFactura.concepto_id , conceptos.nombre
            order by 3 desc";
        return $this->query($sql);
    }

}

<?php
App::uses('AppModel', 'Model');

class Accion extends AppModel 
{
    public $displayField = 'nombre';
    public $hasMany = array('Transicion');
}
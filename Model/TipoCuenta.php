<?php
App::uses('AppModel', 'Model');

class TipoCuenta extends AppModel 
{
	public $displayField = 'nombre';
    
    public $hasMany = array('Cuenta');
}
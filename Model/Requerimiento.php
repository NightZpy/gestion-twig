<?php
App::uses('AppModel', 'Model');
App::uses('CakeNumber', 'Utility');

class Requerimiento extends AppModel 
{
    public $validate = array(
        'asociado_id' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Este campo no puede estar vacio.',
            )
        ),
        'cliente_id' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Este campo no puede estar vacio.',
           )
        )
    );
    
    public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'asociado_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Cliente' => array(
            'className' => 'Persona',
            'foreignKey' => 'cliente_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Asociado' => array(
            'className' => 'Persona',
            'foreignKey' => 'asociado_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'EstatusWorkflow' => array(
            'className' => 'EstatusWorkflow',
            'foreignKey' => 'estatus_workflow_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Prioridad' => array(
            'className' => 'Prioridad',
            'foreignKey' => 'prioridad_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    public $hasMany = array(
        'Factura',
    );
    
    public $actsAs = array('Search.Searchable');
    
    public $filterArgs = array(
        'asociado_id' => array('type' => 'value'),
        'cliente_id' => array('type' => 'value'),
        'prioridad_id' => array('type' => 'value'),
        'estatus_workflow_id' => array('type' => 'value'),
    );

    public function getRequerimientoExpress($requerimiento_id = null){
        if (!isset($requerimiento_id)){
            if (!isset($this->id)){
                $requerimiento_id=$this->id;
            }
        }
        $sql="select Factura.id, DetalleFactura.id, Proveedor.razon_social, Factura.numero_factura, Concepto.nombre, DetalleFactura.cantidad, DetalleFactura.precio_unitario  from detalle_facturas DetalleFactura
                inner join facturas Factura on Factura.id=DetalleFactura.factura_id
                inner join conceptos Concepto on  Concepto.id=DetalleFactura.concepto_id
                inner join personas Proveedor on Proveedor.id=Factura.proveedor_id
                where requerimiento_id=$requerimiento_id order by DetalleFactura.id;";
        $query = $this->query($sql);
        return $query;
    }

    function getAcciones($usuarioGrupoId = null, $estatusInicialId = null){
        $query = $this->query('SELECT 
                                    Accion.nombre, 
                                    Accion.id 
                                FROM
                                    transiciones Transicion, acciones Accion
                                WHERE
                                    Transicion.usuario_grupo_id = ' . $usuarioGrupoId . '
                                AND
                                    Transicion.estatus_inicial_id = ' . $estatusInicialId . '
                                AND
                                    Transicion.accion_id = Accion.id
                                ');
        return $query;
    }
    
    function getEstatus($groupId = null){
        $query = $this->query("SELECT 
                                    DISTINCT (Transicion.estatus_inicial_id) AS estatus_inicial_id,
                                    Transicion.es_modificable AS es_modificable
                                FROM 
                                    transiciones AS Transicion
                                where 
                                    Transicion.usuario_grupo_id = " . $groupId);
        return $query;
    }
    
    function getFacturas($requerimientoId = null){
        $query = $this->query('
                                SELECT 
                                    Factura.id,
                                    Persona.razon_social,
                                    Factura.fecha_requerida,
                                    Factura.fecha_facturacion,
                                    Factura.numero_factura,
                                    Factura.numero_control,
                                    Factura.monto_base,
                                    Factura.monto_iva,
                                    Factura.monto_total
                                FROM
                                    facturas AS Factura LEFT JOIN
                                    personas AS Persona ON (Factura.proveedor_id = Persona.id)
                                WHERE
                                    Factura.requerimiento_id = ' . $requerimientoId);
        return $query;
    }
    
    function getBitacoras($registroId = null){
        $query = $this->query("SELECT 
                                    Auditoria.registro_id,
                                    EstatusWorkflowAnterior.nombre,
                                    EstatusWorkflowActual.nombre,
                                    Auditoria.observacion,
                                    Auditoria.valor_anterior,
                                    Auditoria.valor_nuevo,
                                    Auditoria.fecha,
                                    Usuario.username
                                FROM
                                    auditorias AS Auditoria,
                                    estatus_workflows AS EstatusWorkflowAnterior,
                                    estatus_workflows AS EstatusWorkflowActual,
                                    usuarios AS Usuario
                                WHERE
                                    Auditoria.valor_anterior = EstatusWorkflowAnterior.id
                                AND
                                    Auditoria.valor_nuevo = EstatusWorkflowActual.id
                                AND
                                    Auditoria.usuario_id = Usuario.id
                                AND 
                                    Auditoria.modelo = 'Requerimiento'
                                AND 
                                    Auditoria.registro_id = " . $registroId ."
                                ORDER BY
                                    Auditoria.fecha DESC");
        return $query;
    }
    
    function getTransiciones($groupId = null, $accionId = null, $estatusId = null){
        $query = $this->query('
                                        SELECT 
                                            Transicion.*,
                                            EstatusWorkflowActual.nombre AS estatus_actual,
                                            EstatusWorkflowProximo.nombre AS estatus_proximo,
                                            EstatusWorkflowProximo.id AS estatus_proximo_id
                                        FROM
                                            transiciones AS Transicion,
                                            estatus_workflows AS EstatusWorkflowActual,
                                            estatus_workflows AS EstatusWorkflowProximo
                                        WHERE
                                            EstatusWorkflowActual.id = Transicion.estatus_inicial_id AND
                                            EstatusWorkflowProximo.id = Transicion.estatus_final_id AND
                                            Transicion.usuario_grupo_id = ' . $groupId . ' AND 
                                            Transicion.accion_id = ' . $accionId . ' AND 
                                            Transicion.activo = 1 AND
                                            Transicion.estatus_inicial_id = ' . $estatusId);
        return $query;
    }
    
    function getUsuariosEmail($estatus_id = null)
    {
        $strQuery = $this->query("SELECT Usuario.email
                                  FROM usuarios Usuario
                                  WHERE Usuario.usuario_grupo_id
                                  IN (
                                    SELECT t.`usuario_grupo_id`
                                    FROM `transiciones` t
                                    WHERE `estatus_inicial_id` =2
                                    GROUP BY t.usuario_grupo_id
                                  )");
        return $strQuery;
    }
    
    function getAccion($accion_id = null)
    {
        $strQuery = $this->query("SELECT nombre
                                  FROM acciones Accion
                                  WHERE Accion.id = " . $accion_id);
        return $strQuery[0]['Accion']['nombre'];
    }
    
    function actualizaDatosRequerimiento($data = null) 
    {
        $porcentajeRetencionIva = $this->Persona->find('first',
                                         array('conditions' => 
                                           array('Persona.id' => $data['Requerimiento']['cliente_id']),
                                                 'fields' => 'porcentaje_retencion_iva', 
                                                                ));
        $MontoIva = $this->Factura->find('all', 
                                    array('conditions' => 
                                      array('requerimiento_id' => 
                                            $data['Requerimiento']['id']),
                                            'fields' => array('id', 
                                                              'monto_iva',
                                                              'monto_base')
                                           )
                                  );
        foreach ($MontoIva as $MontoIva) {
            $monto_costo_gestion = $MontoIva['Factura']['monto_base'] *
                                   $data['Requerimiento']['porcentaje_costo_gestion'] /
                                   100;
            $monto_costo_operativo = $MontoIva['Factura']['monto_base'] *
                                     $data['Requerimiento']['porcentaje_costo_operativo'] /
                                     100;
            $monto_costo_venta = (!is_null($data['Requerimiento']['porcentaje_costo_venta']) && $data['Requerimiento']['porcentaje_costo_venta'] != 0)?
                                    ($monto_costo_gestion - $monto_costo_operativo) / $data['Requerimiento']['porcentaje_costo_venta']:0;
            $monto_costo_venta_secundario = (!is_null($data['Requerimiento']['porcentaje_costo_venta_secundario']) && $data['Requerimiento']['porcentaje_costo_venta_secundario'] != 0)?
                                                ($monto_costo_gestion - $monto_costo_operativo) * $data['Requerimiento']['porcentaje_costo_venta_secundario']:0;
            $monto_costo_administrativo = $monto_costo_venta - $monto_costo_venta_secundario;
            $facturas = array('id' => $MontoIva['Factura']['id'],
                              'porcentaje_retencion_iva' => $porcentajeRetencionIva['Persona']['porcentaje_retencion_iva'],
                              'monto_retencion_iva' => $MontoIva['Factura']['monto_iva'] *
                                                       $porcentajeRetencionIva['Persona']['porcentaje_retencion_iva'] /
                                                       100,
                              'monto_costo_gestion' => $monto_costo_gestion,
                              'monto_costo_operativo' => $monto_costo_operativo,
                              'monto_costo_venta' => $monto_costo_venta,
                              'monto_costo_venta_secundario' => $monto_costo_venta_secundario,
                              'monto_costo_administrativo' => $monto_costo_administrativo,
                            );
            $fact = ClassRegistry::init('Factura');
            $fact->save($facturas);
        }
        
        $montosFactura = $this->montosFacturas($data['Requerimiento']['id']);
        if (count($montosFactura) > 0) {
            $requerimientos = array('Requerimiento' => array(
                                                'id' => $montosFactura[0]['Factura']['requerimiento_id'],
                                                'monto_costo_gestion' => $montosFactura[0][0]['monto_costo_gestion'],
                                                'monto_costo_operativo' => $montosFactura[0][0]['monto_costo_operativo'],
                                                'monto_costo_venta' => $montosFactura[0][0]['monto_costo_venta'],
                                                'monto_costo_venta_secundario' => $montosFactura[0][0]['monto_costo_venta_secundario'],
                                                'monto_costo_administrativo' => $montosFactura[0][0]['monto_costo_administrativo'],
                                                'monto_base' => $montosFactura[0][0]['monto_base'],
                                                'monto_iva' => $montosFactura[0][0]['monto_iva'],
                                                'monto_total' => $montosFactura[0][0]['monto_total'],
                                                'monto_retencion_islr' => $montosFactura[0][0]['monto_retencion_islr'],
                                                'monto_retencion_iva' => $montosFactura[0][0]['monto_retencion_iva'],
                                                )
                             );
            $reque = ClassRegistry::init('Requerimiento');
            $reque->save($requerimientos);
        }
    }
    
    function montosFacturas($requerimiento_id = null)
    {
        $Query = $this->Factura->query('
                                SELECT 
                                    Factura.requerimiento_id,
                                    SUM(Factura.monto_base) AS monto_base,
                                    SUM(Factura.monto_retencion_islr) AS monto_retencion_islr,
                                    SUM(Factura.monto_iva) AS monto_iva,
                                    SUM(Factura.monto_retencion_iva) AS monto_retencion_iva,
                                    SUM(Factura.monto_total) AS monto_total,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 AS monto_costo_gestion,
                                    SUM(Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100 AS monto_costo_operativo,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END AS monto_costo_venta,
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END AS monto_costo_venta_secundario,
                                    CASE WHEN porcentaje_costo_venta <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) / porcentaje_costo_venta ELSE 0 END -
                                    CASE WHEN porcentaje_costo_venta_secundario <> 0 THEN SUM((Factura.monto_base * Requerimiento.porcentaje_costo_gestion) / 100 - (Factura.monto_base * Requerimiento.porcentaje_costo_operativo) / 100) * porcentaje_costo_venta_secundario ELSE 0 END
                                    AS monto_costo_administrativo
                                FROM
                                    facturas AS Factura,
                                    requerimientos AS Requerimiento
                                WHERE 
                                    Requerimiento.id = Factura.requerimiento_id
                                AND
                                    Factura.requerimiento_id = ' . $requerimiento_id . '
                                GROUP BY Factura.requerimiento_id');
        return $Query;
    }
    
    function duplicaRequerimientos($data, $id)
    {
        $asociado_id = $data['Requerimiento']['asociado_id'];
        $cliente_id = $data['Requerimiento']['cliente_id'];
        $porcentaje_costo_gestion = $data['Requerimiento']['porcentaje_costo_gestion'] == 0 ? 0 : number_format($data['Requerimiento']['porcentaje_costo_gestion']);
        $porcentaje_costo_operativo = $data['Requerimiento']['porcentaje_costo_operativo'] == 0 ? 0 : number_format($data['Requerimiento']['porcentaje_costo_operativo']);
        $porcentaje_costo_venta = empty($data['Requerimiento']['porcentaje_costo_venta']) ? 0 : number_format($data['Requerimiento']['porcentaje_costo_venta']);
        $porcentaje_costo_venta_secundario = empty($data['Requerimiento']['porcentaje_costo_venta_secundario']) ? 0 : number_format($data['Requerimiento']['porcentaje_costo_venta_secundario']);
        $porcentaje_costo_administrativo = empty($data['Requerimiento']['porcentaje_costo_administrativo']) ? 0 : number_format($data['Requerimiento']['porcentaje_costo_administrativo']);
        
        $strSQL = "INSERT INTO requerimientos"
                . "(asociado_id, cliente_id, estatus_workflow_id, prioridad_id, "
                . "porcentaje_costo_gestion, porcentaje_costo_operativo, porcentaje_costo_venta, "
                . "porcentaje_costo_venta_secundario, porcentaje_costo_administrativo, relacion) "
                . "VALUES "
                . "({$asociado_id}, {$cliente_id}, 1, 1, "
                . "{$porcentaje_costo_gestion}, {$porcentaje_costo_operativo}, {$porcentaje_costo_venta}, "
                . "{$porcentaje_costo_venta_secundario}, {$porcentaje_costo_administrativo}, {$id});";
        
                
        return $this->query($strSQL);
    }
    
    function getInstruccionesPagos($id = null){
        $strQuery = $this->query("SELECT 
                                    InstruccionPago.*, Persona.razon_social, EstatusInstruccionPago.nombre
                                  FROM 
                                    instruccion_pagos InstruccionPago,
                                    estatus_intruccion_pagos EstatusInstruccionPago,
                                    personas Persona
                                  WHERE 
                                    InstruccionPago.estatus_instruccion_pago_id = EstatusInstruccionPago.id
                                    AND InstruccionPago.proveedor_id = Persona.id
                                    AND requerimiento_id = " . $id                              
                                 );
        return $strQuery;
    }
    function getTipoInstrumentos(){
        $strQuery = $this->query("SELECT 
                                    TipoInstrumento.nombre, TipoInstrumento.id
                                  FROM 
                                    tipo_instrumentos TipoInstrumento
                                 ");
        return $strQuery;
    }
    function getBancos(){
        $strQuery = $this->query("SELECT 
                                    Banco.nombre, Banco.id
                                  FROM 
                                    bancos Banco
                                 ");
        return $strQuery;
    }
    
}

<?php
App::uses('AppModel', 'Model');

class Adjunto extends AppModel 
{
	public $displayField = 'ruta';
	public $belongsTo = array('Persona', 'TipoAdjunto');

	public $validate = array(
            'Image' => array(
            'extension' => array(
                'rule' => array(
                    'extension', array(
                        'jpg',
                        'jpeg',
                        'bmp',
                        'gif',
                        'png',
                        'jpg'
                    )
                ),
                'message' => 'File extension is not supported',
                'on' => 'create'
            ),
            'mime' => array(
                'rule' => array('mime', array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/bmp',
                    'image/x-ms-bmp',
                    'image/gif',
                    'image/png'
                )),
                'on' => 'create'
            ),
            'size' => array(
                'rule' => array('size', 2097152),
                'on' => 'create'
            )
        ));
}
<?php
App::uses('AppModel', 'Model');

class EstatusWorkflow extends AppModel 
{
	public $actsAs = array('Search.Searchable');
	public $filterArgs = array(
		'nombre'=>array('type'=>'like')
		);
	
    public $displayField = 'nombre';
    
    public $hasMany = array('Transicion');
    
	public $validate = array(
        'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacio.',
			),
		),
    );
}
<?php
App::uses('AppModel', 'Model');

class Banco extends AppModel 
{
    public $displayField = 'nombre';
    
     public $hasMany = array('Cuenta');
}
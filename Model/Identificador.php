<?php
App::uses('UserMgmtAppModel', 'Usermgmt.Model');

class Identificador extends UserMgmtAppModel {
	
	public $belongsTo = array('Usuario' => array(
        'className' => 'Usermgmt.User',
    ));
}
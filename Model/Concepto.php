<?php
App::uses('AppModel', 'Model');

class Concepto extends AppModel 
{

    public $validate = array(
        'nombre' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Este campo no puede estar vacio.',
            ),
        ),
    );
    
    public $belongsTo = array('Impuesto');
    
    public $actsAs = array('Search.Searchable');
    public $filterArgs = array(
        'nombre' => array('type' => 'like')
    );

    
    
    
}
<?php
App::uses('AppModel', 'Model');

class PersonaPersonas extends AppModel 
{
    public $useTable = 'persona_personas'; 
    
    public function getComisionCliente($cliente_id = null)
    {
        $strSql = $this->query('
                            SELECT 
                                porcentaje_costo_gestion,
                                porcentaje_costo_operativo,
                                porcentaje_costo_venta,
                                porcentaje_costo_venta_secundario,
                                porcentaje_costo_administrativo
                            FROM
                                personas 
                            WHERE
                                id = ' . $cliente_id . '
                            AND (porcentaje_costo_gestion IS NOT NULL
                            OR porcentaje_costo_operativo IS NOT NULL)');
        return $strSql;
    }
    
    public function getComisionAsociado($asociado_id = null)
    {
        $strSql = $this->query('
                            SELECT 
                                porcentaje_costo_gestion,
                                porcentaje_costo_operativo,
                                porcentaje_costo_venta,
                                porcentaje_costo_venta_secundario,
                                porcentaje_costo_administrativo
                            FROM
                                personas
                            WHERE
                                id = ' . $asociado_id . '
                            AND (porcentaje_costo_gestion IS NOT NULL
                            OR porcentaje_costo_operativo IS NOT NULL)');
        return $strSql;
    }
    
    public function getComisionConfiguracion()
    {
        $strSql = $this->query('
                            SELECT 
                                porcentaje_costo_gestion,
                                porcentaje_costo_operativo,
                                porcentaje_costo_venta,
                                porcentaje_costo_venta_secundario,
                                porcentaje_costo_administrativo
                            FROM
                                configuraciones');
        return $strSql;
    }

}

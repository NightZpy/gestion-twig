<?php
App::uses('AppModel', 'Model');

class Cuenta extends AppModel 
{
    public $displayField = 'numero';
    
    public $belongsTo = array('Banco', 'TipoCuenta', 'Persona');
    
    public $validate = array(
        'persona_id' => array(
            'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar una persona.',
			),
        ),
        'banco_id' => array(
            'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar un banco.',
			),
        ),
        'tipo_cuenta_id' => array(
            'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe seleccionar el tipo de cuenta.',
			),
        ),
        'numero' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'El campo debe ser numérico.',
            ),
            'minLength' => array(
                'rule' => array('minLength', 10),
                'message' => 'El número de cuenta debe estar compuesto por 20 caracteres.',
            ),
            'maxLength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'El número de cuenta debe estar compuesto por 20 caracteres.',
            ),
        ),
    );
}
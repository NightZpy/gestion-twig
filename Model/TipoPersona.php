<?php
App::uses('AppModel', 'Model');

class TipoPersona extends AppModel 
{
    /**
     *
     * @var String
     */
    public $displayField = 'nombre';

    /**
     *
     * @var String
     */
	public $hasMany = array(
		'Persona' => array(
			'className' => 'Persona',
			'foreignKey' => 'tipo_persona_id',
		)
	);
    
    /**
     *
     * @var Array 
     */
    public $validate = array(
        'nombre' => array(
            'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacío.',
			),
        ),
        'nombre_corto' => array(
            'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacío.',
			),
        ),
    );
}
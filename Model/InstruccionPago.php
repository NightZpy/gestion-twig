<?php
App::uses('AppModel', 'Model');
/**
 * Instrumento Model
 *
 * @property Asociado $Asociado
 * @property Cliente $Cliente
 * @property Proveedor $Proveedor
 * @property TipoInstrumento $TipoInstrumento
 * @property Banco $Banco
 * @property Cuenta $Cuenta
 * @property Monto $Monto
 */
class InstruccionPago extends AppModel {

    public $belongsTo = array(
		'Proveedor' => array(
			'className' => 'Persona',
			'foreignKey' => 'proveedor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Estatus' => array(
			'className' => 'EstatusIntruccionPago',
			'foreignKey' => 'estatus_instruccion_pago_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'TipoInstrumento' => array(
            'className' => 'TipoInstrumento',
			'foreignKey' => 'tipo_instrumento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
        'BancoOrigen' => array(
            'className' => 'Banco',
			'foreignKey' => 'banco_origen_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
        'BancoDestino' => array(
            'className' => 'Banco',
			'foreignKey' => 'banco_destino_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
        'CuentaDestino' => array(
            'className' => 'Cuenta',
			'foreignKey' => 'cuenta_destino_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
        'Cuenta' => array(
            'className' => 'Cuenta',
			'foreignKey' => 'cuenta_destino_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
    );
    
    function buscarProveedorFinal($cliente_id = null) {
        $strQuery = $this->query("SELECT 
                                    Persona.id, Persona.razon_social
                                  FROM
                                    personas Persona,
                                    persona_personas PersonaPersona
                                  WHERE
                                    Persona.id = PersonaPersona.persona_hijo_id
                                    AND PersonaPersona.tipo_relacion = 'cliente_proveedor'
                                    AND PersonaPersona.persona_padre_id = " . $cliente_id);
        return $strQuery;
    }
    
    function getFacturasDisponibles($id = null) {
        $strQuery = $this->query("SELECT 
                                    SUM(Factura.monto_total) monto_total
                                  FROM
                                    facturas Factura
                                  WHERE
                                    monto_total > monto_maximo 
                                    AND requerimiento_id = " . $id);
        return $strQuery[0][0]['monto_total'];
    }
    
	function getRequerimientos($id = null) {
        $strQuery = $this->query("SELECT Requerimiento.id, Asociado.razon_social, Requerimiento.cliente_id, Cliente.razon_social
                                  FROM 
                                    requerimientos Requerimiento,
                                    personas Asociado,
                                    personas Cliente
                                  WHERE
                                    Cliente.id = Requerimiento.cliente_id
                                    AND Asociado.id =Requerimiento.asociado_id
                                    AND Requerimiento.id = " . $id);
        return $strQuery[0];
    }
    
    function getMontoReintegro($id = null) {
        $strQuery = $this->query("SELECT 
                                    SUM(abono_monto_base) - SUM(monto_iva - monto_retencion_iva) -
                                    SUM(monto_costo_gestion) MontoReintegro
                                  FROM
                                    facturas Factura,
                                    factura_recibos FacturaRecibos
                                  WHERE
                                    Factura.id = FacturaRecibos.factura_id AND
                                    Factura.requerimiento_id = " . $id
                                 );
        return $strQuery[0][0]['MontoReintegro'];
    }
}

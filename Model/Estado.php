<?php
App::uses('AppModel', 'Model');

class Estado extends AppModel 
{
    public $displayField = 'nombre';
    
    public $validate = array(
        'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo no puede estar vacio.',
			),
		),
    );
    
    public $hasMany = array('Persona', 'Ciudad');

    public $actsAs = array('Search.Searchable');
    public $filterArgs = array(
        'nombre' => array('type' => 'like'),
    );
}
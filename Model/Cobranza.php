<?php

class Cobranza extends AppModel {

    public $actsAs = array('Search.Searchable');
    public $hasMany = array(
        'Instrumento'
    );
    public $belongsTo = array(
        'Clientes' => array(
            'className' => 'Persona',
            //'conditions'=>array('Cliente.clasificacion_id'=>1),
            'foreignKey' => 'cliente_id',
        ),
        'Asociados' => array(
            'className' => 'Persona',
            //'conditions'=>array('Asociado.clasificacion_id'=>2),
            'foreignKey' => 'asociado_id',
        ),
        'Proveedores' => array(
            'className' => 'Persona',
            //'conditions'=>array('Proveedor.clasificacion_id'=>3),
            'foreignKey' => 'proveedor_id',
        ),
    );
    public $filterArgs = array(
        'asociado_id' => array('type' => 'value'),
        'proveedor_id' => array('type' => 'value'),
        'cliente_id' => array('type' => 'value'),
    );

    public function getAsociados() {
        $sql = "select distinct a.id, a.razon_social from requerimientos r
                inner join personas a on a.id=r.asociado_id
                order by 2;";
        return $this->_lista($sql, 'a');
    }

    public function getClientes($asociadoId) {
        $sql = "select c.id, c.razon_social from requerimientos r
                inner join personas c on c.id=r.cliente_id
                where asociado_id=$asociadoId
                order by 2";
        return $this->_lista($sql, 'c');
    }

    public function getProveedores($clienteId) {
        $sql = "select distinct p.id, p.razon_social from facturas f
                inner join requerimientos r on r.id=f.requerimiento_id
                inner join personas p on p.id=f.proveedor_id
                where r.cliente_id=$clienteId
                order by 2;";
        return $this->_lista($sql, 'p');
    }

    public function getCuentas($proveedorId) {
        $sql = "select c.id, concat(c.numero, ' - ', b.nombre) as nombre from cuentas c
                inner join bancos b on b.id=c.banco_id
                where persona_id=$proveedorId order by 2";
        return $this->_lista($sql, 'c');
    }

    private function _lista($sql, $tabla) {
        $q = $this->query($sql);
        $datos = array();
        if (!empty($q)) {
            foreach ($q as $row) {
                if (!empty($row[$tabla]['razon_social'])) {
                    $datos[$row[$tabla]['id']] = $row[$tabla]['razon_social'];
                } else {
                    $datos[$row[$tabla]['id']] = $row[0]['nombre'];
                }
            }
        }

        return $datos;
    }

    public function getFacturas() {
        $proveedorId = $this->data['Cobranza']['proveedor_id'];
        $clienteId = $this->data['Cobranza']['cliente_id'];
        $asociadoId = $this->data['Cobranza']['asociado_id'];
        $cobranzaId = $this->id;

        $sql = "select f.id, f.fecha_facturacion, f.numero_factura, f.monto_total, f.monto_maximo,  f.porcentaje_retencion_iva,
                    coalesce(abonos,0) as abonos, 
                    coalesce(abonosislr,0) as abonosislr,
                    coalesce(abonosiva,0) as abonosiva,
                    f.retencion_iva_id_modif,
                    f.impuesto_islr_id_modif
                from facturas f
                inner join requerimientos r on r.id=f.requerimiento_id
                left outer join (
                    select cobranza_id, factura_id, 
                    coalesce(sum(abono_monto_base),0) as abonos, 
                    coalesce(sum(abono_monto_islr),0) as abonosislr,
                    coalesce(sum(abono_monto_iva),0) as abonosiva  from cobranza_facturas group by cobranza_id, factura_id
                ) as cobros on cobros.cobranza_id=$cobranzaId and f.id=cobros.factura_id
                where proveedor_id=$proveedorId and asociado_id=$asociadoId and cliente_id=$clienteId";
        //echo $sql;
        return $this->query($sql);
    }
    
    function getSumaAbonos(){
        if (!empty($this->id)){
            $sql="select coalesce(sum(abono_monto_base),0) as suma from cobranza_facturas where cobranza_id=$this->id";
            $res=$this->query($sql);
            return $res[0][0]['suma'];
        }
        
        return 0;
    }

    public function getFacturasACP($filtros) {
        $asociadoId = $filtros['Cobranza']['asociado_id'];
        $proveedorId = $filtros['Cobranza']['proveedor_id'];
        $clienteId = $filtros['Cobranza']['cliente_id'];
        $fechaDesde="";
        $fechaHasta="";
        if (!empty($filtros['Factura'])){
            $fechaDesde = $filtros['Factura'][0]['fecha_requeridaH'];
            $fechaHasta = $filtros['Factura'][1]['fecha_requeridaH'];
        }

        $cobranzaId = 0;
        if (!empty($filtros['Cobranza']['id']) or $filtros['Cobranza']['id']!="") {
            $cobranzaId = $filtros['Cobranza']['id'];
        }

        $filtro = "";
        if ($fechaDesde != "" and $fechaHasta == "") {
            $filtro = " and fecha_requerida>='$fechaDesde'";
        }

        if ($fechaDesde == "" and $fechaHasta != "") {
            $filtro = " and fecha_requerida<='$fechaHasta'";
        }

        if ($fechaDesde != "" and $fechaHasta != "") {
            $filtro = " and fecha_requerida between '$fechaDesde' and '$fechaHasta'";
        }

        $sql = "select * from facturas
                inner join personas as proveedores on facturas.proveedor_id=proveedores.id
                inner join requerimientos on facturas.requerimiento_id=requerimientos.id
                inner join personas as clientes on requerimientos.cliente_id=clientes.id
                inner join personas as asociados on requerimientos.asociado_id=asociados.id
                where clientes.id=$clienteId and proveedores.id=$proveedorId and asociados.id=$asociadoId $filtro";

        $sql = "select f.id, f.fecha_facturacion, f.numero_factura, f.monto_total, f.monto_maximo, f.porcentaje_retencion_iva,
                    coalesce(abonos,0) as abonos, 
                    coalesce(abonosislr,0) as abonosislr,
                    coalesce(abonosiva,0) as abonosiva,
                    f.retencion_iva_id_modif,
                    f.impuesto_islr_id_modif
                from facturas f
                inner join personas as proveedores on f.proveedor_id=proveedores.id
                inner join requerimientos on f.requerimiento_id=requerimientos.id
                inner join personas as clientes on requerimientos.cliente_id=clientes.id
                inner join personas as asociados on requerimientos.asociado_id=asociados.id
                left outer join (
                    select cobranza_id, factura_id, 
                    coalesce(sum(abono_monto_base),0) as abonos, 
                    coalesce(sum(abono_monto_islr),0) as abonosislr,
                    coalesce(sum(abono_monto_iva),0) as abonosiva  from cobranza_facturas group by cobranza_id, factura_id
                ) as cobros on cobros.cobranza_id=$cobranzaId and f.id=cobros.factura_id
                where clientes.id=$clienteId and proveedores.id=$proveedorId and asociados.id=$asociadoId  $filtro";
        //echo $sql;
        return $this->query($sql);
    }

    public function getSumaInstrumentos($cobranzaId) {
        $sql = "select sum(monto) as suma from instrumentos where cobranza_id=$cobranzaId and estatus_instrumento_id in (1,2);";

        $q = $this->query($sql);

        if (!empty($q[0])) {
            return $q[0][0]['suma'];
        } else {
            return 0;
        }
    }

    public function getDisponibleInstrumentos($cobranzaId) {
        $sql = "select sum(monto)-coalesce(abono_monto_base, 0) as disponible from instrumentos
                left outer join (
                select cobranza_id, sum(abono_monto_base) as abono_monto_base from cobranza_facturas where cobranza_id=$cobranzaId
                ) as cosa on cosa.cobranza_id=instrumentos.cobranza_id
                where instrumentos.cobranza_id=$cobranzaId";
        $q = $this->query($sql);

        if (!empty($q[0])) {
            return $q[0][0]['disponible'];
        } else {
            return 0;
        }
    }
    
    public function getIslr($facturaId){
        $sql="select valor from detalle_facturas DetalleFactura
            inner join conceptos Concepto on Concepto.id=DetalleFactura.concepto_id
            inner join impuestos Impuesto on Impuesto.id=Concepto.Impuesto_id
            where factura_id=$facturaId limit 1";
        
        $sql="select valor from impuestos Impuesto 
            inner join facturas Factura on Factura.impuesto_islr_id_modif=Impuesto.id
            where Factura.id=$facturaId";
        return $this->query($sql);
    }
    
    public function getRetencionIva($facturaId){
        $sql="select valor from impuestos Impuesto 
            inner join facturas Factura on Factura.retencion_iva_id_modif=Impuesto.id
            where Factura.id=$facturaId";
        return $this->query($sql);
    }

}
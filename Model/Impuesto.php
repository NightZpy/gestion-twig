<?php
App::uses('AppModel', 'Model');

class Impuesto extends AppModel 
{
    public $displayField = 'valor';
    public $validate = array(
        'nombre' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Este campo no puede estar vacio.',
            ),
        ),
    );
    
    public $hasMany = array('Concepto');
}
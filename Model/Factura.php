<?php
App::uses('AppModel', 'Model');

class Factura extends AppModel 
{
    public $validate = array(
        'proveedor_id' => array(
            'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo no puede estar vacio.',
            ),
        ),
        'concepto_id' => array(
            'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Debe especificar un concepto para la factura.',
            ),
        ),
    );
    
    public $belongsTo = array(
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'proveedor_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Requerimiento' => array(
            'className' => 'Requerimiento',
            'foreignKey' => 'requerimiento_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Impuesto' => array(
            'className' => 'Impuesto',
            'foreignKey' => 'id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    
    public $hasMany = array(
        'DetalleFactura',
    );
    
    function getConceptoProveedor($proveedor_id = null)
    {
        $strSQL = "SELECT  Concepto.id"
                .       ", CONCAT(Concepto.nombre,' (',Impuesto.valor, '%)') AS nombre "
                . "FROM   conceptos AS Concepto "
                . "INNER JOIN impuestos AS Impuesto "
                . "ON Impuesto.id = Concepto.impuesto_id "
                . "INNER JOIN concepto_proveedores AS ConceptoProveedor "
                . "ON 	Concepto.id = ConceptoProveedor.concepto_id "
                . "WHERE ConceptoProveedor.proveedor_id = {$proveedor_id};";
        $query = $this->query($strSQL);

        $conceptos = array();
        foreach ($query as $retorno):
            $conceptos[$retorno['Concepto']['id']] =  $retorno[0]['nombre'];
        endforeach;
        
        return $conceptos;
    }
    
    function getDetalleFactura($factura_id = null)
    {
       $strSQL = "SELECT 
                    DetalleFactura.id, DetalleFactura.factura_id, DetalleFactura.concepto_id, Concepto.nombre, 
                    DetalleFactura.cantidad, DetalleFactura.descripcion, DetalleFactura.impuesto_iva_id, 
                    DetalleFactura.precio_unitario, DetalleFactura.monto_base, 
                    DetalleFactura.monto_iva, ImpuestoIVA.valor, ImpuestoISLR.valor
                FROM 
                    detalle_facturas AS DetalleFactura 
                LEFT JOIN 
                    conceptos AS Concepto ON DetalleFactura.concepto_id = Concepto.id 
                LEFT JOIN impuestos as ImpuestoIVA ON ImpuestoIVA.id=DetalleFactura.impuesto_iva_id
                LEFT JOIN impuestos as ImpuestoISLR ON ImpuestoISLR.id=Concepto.impuesto_id
                WHERE DetalleFactura.factura_id ={$factura_id}";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    function getEstatusRequerimiento($requerimientoId = null)
    {
        $strSQL = "SELECT Requerimiento.estatus_workflow_id AS estatus_workflow_id "
                . "FROM   requerimientos AS Requerimiento "
                . "WHERE  Requerimiento.id = {$requerimientoId};";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    function getAccesoControlador($groupId = null) 
    {
        $strSQL = "SELECT id "
                . "FROM  usuario_grupo_permisos UsuarioGrupoPermiso "
                . "WHERE usuario_grupo_id = {$groupId} "
                . "AND controller = 'Facturas' "
                . "AND action = 'datosFiscales';";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    function getPermisos($groupId = null, $controller = null, $action = null) 
    {
        $strSQL = "SELECT id, action "
                . "FROM usuario_grupo_permisos UsuarioGrupoPermiso "
                . "WHERE usuario_grupo_id = {$groupId} "
                . "AND controller = '{$controller}' "
                . "AND action = '{$action}' "
                . "AND allowed = 1;";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    /**
     * @param $cliente_id integer
     * @todo: Eliminar el hardcode de clasificacion
    */
    function getProveedores($cliente_id = null)
    {
        $strSQL = "SELECT razon_social id, razon_social "
                . "FROM personas Persona "
                . "WHERE clasificacion_id = 3;";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    function getDiasFeriados()
    {
        $strSQL = "SELECT CONCAT_WS('/', mes,dia,ano) as feriado "
                . "FROM dias_feriados DiasFeriados;";
        $query = $this->query($strSQL);
        
        return $query;
    }
    
    function duplicaFacturas($anterior_id, $nuevo_id)
    {
        $strSQL = "INSERT INTO facturas "
                . "(requerimiento_id, proveedor_id, razon_social, monto_base, "
                . "monto_iva, monto_total, porcentaje_retencion_iva, monto_retencion_iva, "
                . "monto_retencion_islr, monto_costo_gestion, monto_costo_operativo, "
                . "monto_costo_venta, monto_costo_venta_secundario, monto_pago_proveedor, monto_costo_administrativo, pagado, monto_maximo, observaciones, "
                . "retencion_iva_id, impuesto_islr_id, retencion_iva_id_modif, impuesto_islr_id_modif, copia_factura_id) "
                . "SELECT {$nuevo_id}, proveedor_id, razon_social, monto_base, "
                . "monto_iva, monto_total, porcentaje_retencion_iva, monto_retencion_iva, "
                . "monto_retencion_islr, monto_costo_gestion, monto_costo_operativo, "
                . "monto_costo_venta, monto_costo_venta_secundario, monto_pago_proveedor, monto_costo_administrativo, pagado, monto_maximo, observaciones, "
                . "retencion_iva_id, impuesto_islr_id, retencion_iva_id_modif, impuesto_islr_id_modif, id "
                . "FROM facturas "
                . "WHERE requerimiento_id = {$anterior_id};";
        $query = $this->query($strSQL);
        
        return $query;
    }
        
    public function getRetenciones()
    {
        $strSQL = "SELECT SUM(df.monto_iva*porcentaje_retencion_iva)/100 AS retencion_iva, SUM(monto_islr) AS retencion_islr "
                . "FROM detalle_facturas df "
                . "INNER JOIN facturas AS f "
                . "ON f.id=df.factura_id "
                . "WHERE f.id={$this->id};";

        $result = $this->query($strSQL);
        
        return $result[0][0];
    }
    
    public function getRetencionIVAProveedor($proveedorId){
        $sql = "select porcentaje_retencion_iva from personas Persona where id=$proveedorId";
        $res = $this->query($sql);
        $valor = $res[0]['Persona']['porcentaje_retencion_iva'];
        
        $sql = "select id from impuestos Impuesto where valor=$valor and es_retencion_iva=1";
        $res = $this->query($sql);
        
        return $res[0]['Impuesto']['id'];
    }
}
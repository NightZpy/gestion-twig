<?php
App::import('Model', 'Auditoria');

class AuditoriaBehavior extends ModelBehavior {
    /** 
     * Behavior settings 
     *  
     * @access public 
     * @var array 
     */ 
    public $settings = array(); 

    private $ignoreModels = array('Auditoria');
    private $ignoreFields = array('creado', 'creado_por', 'modificado', 'modificado_por', 'observacion', 
                                  'fecha_requerida_display', 'fecha_facturacion_display', 'Image', 'oldpassword','cpassword','fechafld', 
                                  'saldo_deudor_monto_base','saldo_deudor_monto_islr','saldo_deudor_monto_iva');
   
    public function beforeSave(Model $Model)
    {
        if (!in_array($Model->alias, $this->ignoreModels)) {
            $this->recursive = -1;
            $this->old = $Model->find('first', 
                                    array('conditions' => array("{$Model->alias}.{$Model->primaryKey}" => $Model->id)));
            if ($this->old) {
                $changed_fields = array();
                foreach ($Model->data[$Model->alias] as $campo => $valorNuevo) {
                    if (!in_array($campo, $this->ignoreFields)) {
                        $valorAnterior = "{$this->old[$Model->alias][$campo]}";
                        if ($valorAnterior != $valorNuevo) {
                            $changed_fields = array('Auditoria' => array(
                                                            'id' => null,
                                                            'modelo' => $Model->alias,
                                                            'registro_id' => $Model->id,    
                                                            'campo' => $campo,
                                                            'valor_anterior' => $valorAnterior,
                                                            'valor_nuevo' => $valorNuevo,
                                                            'fecha' => date('Y-m-d H:i:s', time()),
                                                            'observacion' => isset($Model->data[$Model->alias]['observacion'])?$Model->data[$Model->alias]['observacion']:null,
                                                            'usuario_id' => isset($_SESSION['UserAuth']['Usuario']['id'])?$_SESSION['UserAuth']['Usuario']['id']:null,
                                                        )
                                                );
                            $audit = ClassRegistry::init('Auditoria');
                            $audit->save($changed_fields);
                        }
                    }
                }
            }
        }
        
        return true;
    }

}

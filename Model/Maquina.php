<?php

App::uses('UserMgmtAppModel', 'Usermgmt.Model');

class Maquina extends UserMgmtAppModel {
	
	public $belongsTo = array('Usuario' => array(
        'className' => 'Usermgmt.User',
    ));
}
<?php

App::uses('AppModel', 'Model');


class Persona extends AppModel 
{
    /**
     *
     * @var String
     */
    public $displayField = 'razon_social';
    /**
     *
     * @var Array
     */
    public $validate = array(
        'codigo' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Este campo no puede estar vacio.',
            ),
            'Unique' => array(
                'rule' => 'isUnique', 
                'message' => 'Este código ya ha sido registrado.',
            ),
        ),
        
        'tipo_persona_id' => array(
            'rule' => 'numeric',
            'message' => 'Seleccione el tipo de persona',
        ),
        
        'clasificacion_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione la clasificacion.',
        ),
        
        'tipo_documento_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione el tipo de documento',
        ),
        'rubro_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione el rubro.',
        ),
        
        'grupo_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione el grupo',
        ),
        'documento' => array(
            'digitoVerificador' => array(
                'rule' => 'digitoVerificador',
                'message' => 'Documento fiscal inválido.',
            ),
            'Unique' => array(
                'rule' => 'isUnique', 
                'message' => 'El documento fiscal ya ha sido registrado.',
            ),
        ),
        
        'razon_social' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Este campo no puede estar vacio.',
            ),
            'Unique' => array(
                'rule' => 'isUnique', 
                'message' => 'Esta razón social ya ha sido registrada.',
            ),
        ),
        
        'estado_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione un estado.',
	),
        
        'ciudad_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Seleccione un ciudad.',
	),
	
        'direccion' => array(
            'rule' => 'notEmpty',
            'message' => 'Este campo no puede estar vacio.',
	),
        
        'correo_electronico' => array(
            'rule' => 'email',
            'allowEmpty' => true,
            'message' => 'Ingrese un email valido.',
	),
        
        'pagina_web' => array(
            'rule' => 'url',
            'allowEmpty' => true,
            'message' => 'Página web inválida.'
        ),
        
        'contribuyente_especial' => array(
            'rule'    => 'es_contribuyente_especial',
            'message' => 'Si la persona es contribuyente especial, llene el campo Porcentaje Retencion Iva.'
        ),
        
        'porcentaje_costo_gestion' => array(
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Este campo debe ser numerico.'
        ),
        
        'porcentaje_costo_operativo' => array(
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Este campo debe ser numerico.'
        ),
        /*
        'Image' => array(
            'extension' => array(
                'rule' => array(
                    'extension', array(
                        'jpg',
                        'jpeg',
                        'bmp',
                        'gif',
                        'png',
                        'jpg'
                    )
                ),
                'message' => 'File extension is not supported',
                'on' => 'create'
                ),
            'mime' => array(
                'rule' => array('mime', array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/bmp',
                    'image/x-ms-bmp',
                    'image/gif',
                    'image/png'
                )),
                'on' => 'create'
            ),
            'size' => array(
                'rule' => array('size', 2097152),
                'on' => 'create'
            )
        )*/
    );
    
	    
    public $hasMany = array(
        'Factura' => array(
            'className' => 'Factura',
            'foreignKey' => 'proveedor_id',
        ),
        'Cuenta',
        'Adjunto'
    );
    
    public $belongsTo = array(
        'TipoPersona',
        'Clasificacion',
        'TipoDocumento',
        'Estado',
        'Ciudad',
        'Rubro',
        'Grupo'
    );
    
    /*public $actsAs = array(
        'Search.Searchable',
        'Attach.Upload' => array(
            'Image' => array(
                'dir' => 'webroot{DS}uploads{DS}media{DS}image',
                'multiple' => TRUE,
            ),
        ),
    );*/

    public $actsAs = array('Search.Searchable');
    
    public $filterArgs = array(
        'codigo' => array('type' => 'like'),
        'razon_social' => array('type' => 'like'),
        'tipo_documento_id' => array('type' => 'value'),
        'documento' => array('type' => 'like'),
        'clasificacion_id' => array('type' => 'value'),
        'estado_id' => array('type' => 'value'),
        'ciudad_id' => array('type' => 'value'),
        'rubro_id' => array('type' => 'value'),
        'grupo_id' => array('type' => 'value'),        
        'nombre' => array('type' => 'like'),
    );
    
    public function es_contribuyente_especial($check) 
    {
        $value = array_values($check);
        if ($value[0]) {
             if (!empty($this->data['Persona']['porcentaje_retencion_iva'])) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->data['Persona']['porcentaje_retencion_iva'] = 0;
            return true;
        }
    }
    
    public function digitoVerificador($check) {
        $value = array_values($check);
        $rif = $this->data['Persona']['tipo_documento_id'] . $value[0];
        
        $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif);
        
        if ($retorno) {
            $digitos = str_split($rif);
           
            $digitos[8] *= 2; 
            $digitos[7] *= 3; 
            $digitos[6] *= 4; 
            $digitos[5] *= 5; 
            $digitos[4] *= 6; 
            $digitos[3] *= 7; 
            $digitos[2] *= 2; 
            $digitos[1] *= 3; 
            
            // Determinar dígito especial según la inicial del RIF
            // Regla introducida por el SENIAT
            switch ($digitos[0]) {
                case 'V':
                    $digitoEspecial = 1;
                    break;
                case 'E':
                    $digitoEspecial = 2;
                    break;
                case 'J':
                    $digitoEspecial = 3;
                    break;
                case 'P':
                    $digitoEspecial = 4;
                    break;
                case 'G':
                    $digitoEspecial = 5;
                    break;
            }
            
            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);
            $residuo = $suma % 11;
            $resta = 11 - $residuo;
            
            $digitoVerificador = ($resta >= 10) ? 0 : $resta;
            
            if ($digitoVerificador != $digitos[9]) {
                $retorno = false;
            }
        }        
        
        return $retorno;
    }

    public function getPersonas() {
        $result = $this->find("all");
        $usuarios = array();
        $usuarios[0] = 'Select';
        
        foreach ($result as $row) {
                $usuarios[$row['Persona']['id']] = $row['Persona']['razon_social'];
        }
        
        return $usuarios;
    }
    
    /**
     * 
     * Obtiene los Id de los clientes del asociado
     * 
     * @param integer $asociadoId
     * @return array
     */
    public function getAsociadoClientes($asociadoId) {
        $sql = 'SELECT p.id as persona_id FROM personas p
                        left outer join persona_personas ac on p.id = ac.persona_hijo_id
                        WHERE persona_padre_id=' . $asociadoId . ' 
        UNION
                        SELECT p.id as persona_id FROM personas p
                        left outer join persona_personas ac on p.id = ac.persona_hijo_id
                        WHERE clasificacion_id=3';
        $result = $this->query($sql);
        $arrClientes = "";
        foreach ($result as $value) {
                $arrClientes[] = $value[0]['persona_id'];
        }

        return $arrClientes;
    }
	
    public function afterSave($created) {
        if ($created) {
            parent::beforeSave(isset($options)?$options:array());
            if (CakeSession::read('UserAuth.UsuarioGrupo.id') == 3) {
                $asociadoCliente = array('AsociadoCliente'=>array(
                    'asociado_id' =>  CakeSession::read('UserAuth.Usuario.persona_id'),
                    'cliente_id' => $this->data['Persona']['id']
                    )
                );

                $asociar = ClassRegistry::init('AsociadoCliente');
                $asociar->save($asociadoCliente);
            }
        }
    }

    public function isUploadedFile($params) {
        echo "<pre>";
        print_r($params);
        echo "</pre>";
        $val = array_shift($params);
        if ((isset($val['error']) && $val['error'] == 0) ||
            (!empty( $val['tmp_name']) && $val['tmp_name'] != 'none')
        ) {
            return is_uploaded_file($val['tmp_name']);
        }
        return false;
    }    
	
    public function getConceptosDisponibles($personaId, $criterio){
        $strSQL = "SELECT Concepto.id, Concepto.nombre "
                . "FROM conceptos AS Concepto "
                . "LEFT JOIN concepto_proveedores AS ConceptoProveedor "
                . "ON ConceptoProveedor.concepto_id = Concepto.id "
                . "AND ConceptoProveedor.proveedor_id = {$personaId} "
                . "WHERE Concepto.activo = 1 "
                . "AND ConceptoProveedor.concepto_id IS NULL ". $criterio;
        
        return $this->query($strSQL);
    }
	
    public function getConceptosAsociados($personaId){
        $strSQL = "SELECT Concepto.id, Concepto.nombre, ConceptoProveedor.id, ConceptoProveedor.proveedor_id, ConceptoProveedor.concepto_id "
                . "FROM conceptos AS Concepto "
                . "INNER JOIN concepto_proveedores AS ConceptoProveedor "
                . "ON ConceptoProveedor.concepto_id = Concepto.id "
                . "WHERE Concepto.activo = 1 "
                . "AND ConceptoProveedor.proveedor_id = {$personaId}";
        
        return $this->query($strSQL);
    }
	
    public function getClientesDisponibles(){
        $strSQL = "SELECT Persona.id, Persona.codigo, Persona.razon_social, CONCAT(Persona.tipo_documento_id, Persona.documento) AS documento "
                . "FROM personas AS Persona "
                . "LEFT JOIN persona_personas AS PersonaPersona "
                . "ON PersonaPersona.persona_hijo_id = Persona.id "
                . "INNER JOIN clasificaciones AS Clasificacion "
                . "ON Clasificacion.id = Persona.clasificacion_id "
                . "WHERE PersonaPersona.persona_hijo_id IS NULL "
                . "AND	Clasificacion.relacion_cliente = 0 "
                . "AND	Clasificacion.relacion_concepto = 0 ";
        
        return $this->query($strSQL);
    }
	
    public function getClientesRelacionadas($personaId){
        $strSQL = "SELECT Persona.id, Persona.codigo, Persona.razon_social, Persona.tipo_documento_id, Persona.documento, "
                . "PersonaPersona.id, PersonaPersona.persona_padre_id "
                . "FROM personas AS Persona "
                . "INNER JOIN persona_personas AS PersonaPersona "
                . "ON PersonaPersona.persona_hijo_id = Persona.id "
                . "INNER JOIN clasificaciones AS Clasificacion "
                . "ON Clasificacion.id = Persona.clasificacion_id "
                . "WHERE PersonaPersona.persona_padre_id = {$personaId} "
                . "AND PersonaPersona.tipo_relacion = 'asociado_clientes'";
        
        return $this->query($strSQL);
    }
    
    public function getAsociadoClientes2($tipo='list', $asociadoId=null){
        $sql="select Cliente.id, Cliente.razon_social from asociado_clientes AsociadoCliente
            inner join personas Asociado on Asociado.id=AsociadoCliente.asociado_id
            inner join personas Cliente on Cliente.id=AsociadoCliente.cliente_id
            where Asociado.id=$asociadoId order by 2";
        $res = $this->query($sql);
        $arr=array();
        if ($tipo=='list'){
            foreach ($res as $row){
                $arr[$row['Cliente']['id']]=$row['Cliente']['razon_social'];
            }
            return $arr;
        }else{
            return $res;
        }
        
        
    }
    
    public function getProveedoresFinalesDisponibles($personaId = null){
        $strSQL = "SELECT Persona.id, Persona.codigo, Persona.razon_social, CONCAT( Persona.tipo_documento_id, Persona.documento ) AS documento "
                . "FROM personas AS Persona "
                . "WHERE Persona.id NOT IN ( "
                . "SELECT persona_hijo_id "
                . "FROM persona_personas PersonaPersona "
                . "WHERE persona_padre_id = " . $personaId . " "
                . "AND tipo_relacion = 'cliente_proveedor')";
        
        return $this->query($strSQL);
    }
	
    public function getProveedoresFinalesRelacionadas($personaId = null){
        $strSQL = "SELECT Persona.id, Persona.codigo, Persona.razon_social, Persona.tipo_documento_id, Persona.documento "
                . "FROM personas AS Persona "
                . "WHERE Persona.id IN ( "
                . "SELECT persona_hijo_id "
                . "FROM persona_personas PersonaPersona "
                . "WHERE persona_padre_id = " . $personaId . " "
                . "AND tipo_relacion = 'cliente_proveedor')";
        
        return $this->query($strSQL);
    }
    
}
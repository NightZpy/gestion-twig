<?php
App::uses('AppModel', 'Model');
/**
 * Instrumento Model
 *
 * @property Asociado $Asociado
 * @property Cliente $Cliente
 * @property Proveedor $Proveedor
 * @property TipoInstrumento $TipoInstrumento
 * @property Banco $Banco
 * @property Cuenta $Cuenta
 * @property Monto $Monto
 */
class Instrumento extends AppModel {

	
	public $actsAs = array('Search.Searchable');
	
	public $filterArgs = array(
		'asociado_id' => array('type' => 'value'),
	);
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'cliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe especificar un cliente',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'proveedor_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe especificar un proveedor',
				'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nro_referencia' => array(
			'numeric' => array(
				'rule' => array('necesitaReferencia'),
				'message' => 'El número de referencia es obligatorio para este tipo de instrumento',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		/*'banco_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cuenta_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Es necesario indicar la cuenta del proveedor',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
		'fecha_carga' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'monto' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe indicar el monto de la transacción',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'asociado_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'estatus_instrumento_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
        'Cobranza',
		'Asociado' => array(
			'className' => 'Persona',
			'foreignKey' => 'asociado_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Persona',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proveedor' => array(
			'className' => 'Persona',
			'foreignKey' => 'proveedor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TipoInstrumento' => array(
			'className' => 'TipoInstrumento',
			'foreignKey' => 'tipo_instrumento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Banco' => array(
			'className' => 'Banco',
			'foreignKey' => 'banco_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cuenta' => array(
			'className' => 'Cuenta',
			'foreignKey' => 'cuenta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getClientesAsociado($asociadoId = null){
		$filtro="";
		if ($asociadoId!="" || $asociadoId>0){
			$filtro=" and ac.asociado_id=" . $asociadoId;
		}
		
		$sql="SELECT p.id, p.razon_social
				FROM `asociado_clientes` ac
				INNER JOIN personas p ON p.id = ac.cliente_id
				WHERE p.clasificacion_id =1 $filtro";
		$result = $this->query($sql);
		$datos[""]="";
		foreach ($result as $row){
			$datos[$row['p']['id']] = $row['p']['razon_social'];
		}
		return $datos;
	}
	
	public function getPersonasConRequerimiento($asociadoId = null){
		$filtro = "";
		if ($asociadoId != null){
			$filtro = " and asociado_id = $asociadoId";
		}
		
		$sql = "select distinct p.id, p.razon_social from personas p
				inner join requerimientos r on r.cliente_id = p.id
				where clasificacion_id=1 $filtro
				order by 2;";
		$result = $this->query($sql);
		$datos[""]="";
		foreach ($result as $row){
			$datos[$row['p']['id']] = $row['p']['razon_social'];
		}
		return $datos;
	}
	
	public function getProveedoresEnFacturas($clienteId = null){
		$filtro = "";
		if ($clienteId != null){
			$filtro = " where cliente_id = $clienteId";
		}
		
		$sql = "select distinct proveedor_id, p.razon_social from facturas f
					left join requerimientos r on r.id=f.requerimiento_id
					INNER JOIN personas p ON p.id = proveedor_id
					$filtro";
		$result = $this->query($sql);
		$datos[""]="";
		foreach ($result as $row){
			$datos[$row['f']['proveedor_id']] = $row['p']['razon_social'];
		}
		return $datos;
	}
	
	public function getCuentasProveedor($proveedorId=null){
		if ($proveedorId!=""){
			$filtro = "where persona_id=$proveedorId";
		}else{
			$filtro="where persona_id=0";
		}
		$sql="SELECT c.id, concat( b.nombre, \" - \", c.numero ) as nombre
				FROM cuentas c
				LEFT JOIN personas p ON c.persona_id = p.id
				LEFT JOIN bancos b ON c.banco_id = b.id $filtro order by 2";
		
		$result = $this->query($sql);
		$datos=array();
		foreach ($result as $row){
			$datos[$row['c']['id']] = $row[0]['nombre'];
		}
		
		return $datos;
	}
	
	public function necesitaReferencia($check){
		$return = true;
		$value = array_values($check);
		$tipoInstrumentoId =  $this->data['Instrumento']['tipo_instrumento_id'];
		if (($tipoInstrumentoId >= 2 and $tipoInstrumentoId <= 3) and !is_numeric($value[0])){
			$return = false;
		}		
		return $return;
	}
	
	public function getTransiciones($GrupoId, $estatusInicial){
		$sql="SELECT estatus_final_id as id, nombre
				FROM transiciones_instrumentos ti
				INNER JOIN acciones a ON ti.accion_id = a.id
				WHERE evento = 'instrumentos' and usuario_grupo_id=$GrupoId and estatus_inicial_id=$estatusInicial";
		$result = $this->query($sql);
		
		return $result;
	}
	
	public function getCamposRequeridos($tipoInstrumentoId){
		$sql="select requiere_banco, requiere_cuenta, requiere_referencia from tipo_instrumentos
				where id=". $tipoInstrumentoId;
		$result = $this->query($sql);
		return $result[0]['tipo_instrumentos'];
	}
	
	public function getInstrumentosConciliados($proveedorId){
		$sql = "select * from instrumentos i
				inner join tipo_instrumentos ti on ti.id = i.tipo_instrumento_id
				left outer join cuentas c on c.id=i.cuenta_id
				left outer join bancos b on b.id=c.banco_id
				where proveedor_id=$proveedorId and recibo_id is null";
		$result = $this->query($sql);
		return $result;
	}
	
	public function setInstrumentoRecibo($instrumentoId, $reciboId){
		$this->id = $instrumentoId;
		
		if ($this->saveField('recibo_id', $reciboId)){
			return true;
		}else{
			return false;
		}
		
		
	}
}

<?php

class Recibo extends AppModel{
	
	public $validate = array(
		'numero' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe especificar un número de recibo',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'asociado_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe elegir a un asociado',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe elegir a un cliente',
				//'allowEmpty' => false,
				'required' => TRUE,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public $belongsTo = array(
		'Asociado' => array(
			'className' => 'Persona',
			'foreignKey' => 'asociado_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Persona',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
	public $hasMany = array('Instrumento','FacturaRecibo');
	
	public $actsAs = array('Search.Searchable');
	
	 public $filterArgs = array(
			'asociado_id' => array('type' => 'value'),
			'cliente_id' => array('type' => 'value'),
			'numero' => array('type' => 'value'),
			);
     
     function getMontosPendientes($facturaId){
         $sql="select 
                    facturas.id,
                    monto_total,
                    (monto_base-monto_retencion_islr+monto_iva-monto_retencion_iva)- coalesce(abono_monto_base,0) as maxdinero_real, 
                    case when monto_base-monto_retencion_islr+monto_iva-monto_retencion_iva-coalesce(abono_monto_base,0)<0 
                        then 0 
                        else monto_base-monto_retencion_islr+monto_iva-monto_retencion_iva-coalesce(abono_monto_base,0) end  as saldo_deudor_monto_base, 
                    monto_retencion_islr-coalesce(abono_monto_islr,0) as saldo_deudor_monto_islr, 
                    monto_retencion_iva-coalesce(abono_monto_iva,0) as saldo_deudor_monto_iva
                from facturas
                left join (select 
                            id, recibo_id, factura_id, 
                            sum(abono_monto_base) as abono_monto_base, 
                            sum(abono_monto_islr) as abono_monto_islr, 
                            sum(abono_monto_iva) as abono_monto_iva 
                from factura_recibos
                GROUP BY id, recibo_id, factura_id) as factura_recibos on facturas.id=factura_recibos.factura_id
                where facturas.id=$facturaId;";
         
         return $this->query($sql);
     }
     
	function getTotalesRecibo(){
		$reciboId = $this->id;
		
		$sql="select *, '0' as monto,'0' as saldo from grupo_tipo_instrumentos gti";
		$result = $this->query($sql);
		
		foreach ($result as $row){
			$datos[$row['gti']['id']]=array('id'=>$row['gti']['id'], 'nombre'=>$row['gti']['nombre'], 'recibido'=>$row[0]['monto'], 'disponible'=>$row[0]['saldo'] );
		}
		
		
		$sql = "select gti.id, gti.nombre, sum(i.monto) as monto, coalesce(montos.monto,0) as usado, sum(i.monto)- coalesce(montos.monto,0) as saldo  from instrumentos i
					inner join tipo_instrumentos ti on ti.id=i.tipo_instrumento_id
					inner join grupo_tipo_instrumentos gti on gti.id=ti.grupo_tipo_instrumento_id
					left outer join (select '$reciboId' as recibo_id, '1' as grupo_instrumento_id, sum(abono_monto_base) as monto from factura_recibos
					where recibo_id=$reciboId
					union
					select '$reciboId' as recibo_id, '2', sum(abono_monto_islr) as monto from factura_recibos
					where recibo_id=$reciboId
					union
					select '$reciboId' as recibo_id, '3', sum(abono_monto_iva) as monto from factura_recibos
					where recibo_id=$reciboId) as montos on montos.recibo_id=i.recibo_id and montos.grupo_instrumento_id=ti.grupo_tipo_instrumento_id
					where i.recibo_id=$reciboId and estatus_instrumento_id in (1,2)
					group by gti.nombre;";
		 //echo $sql;
		$result = $this->query($sql);
		
		foreach ($result as $row){
			$datos[$row['gti']['id']] = array('id'=>$row['gti']['id'],  'nombre'=>$row['gti']['nombre'], 'recibido'=>$row[0]['monto'], 'disponible'=>$row[0]['saldo'] );
		}
		return $datos;
	}
	
	public function getInstrumentos(){
		$sql="select ti.nombre, nro_referencia, b.nombre, cue.numero, monto, ei.nombre, i.*  from instrumentos i
				left join personas cli on i.cliente_id = cli.id
				left join personas pro on i.proveedor_id = pro.id
				left join personas aso on i.asociado_id = aso.id
				left join tipo_instrumentos ti on i.tipo_instrumento_id = ti.id
				left join bancos b on i.banco_id = b.id
				left join cuentas cue on i.cuenta_id = cue.id
				left join estatus_instrumentos ei on i.estatus_instrumento_id = ei.id
				where recibo_id = " . $this->id;
		
		$result = $this->query($sql);
		return $result;
	}
	
	public function getFacturasPendientes($arrayFlt, $page=null){
		$strFiltro = "";
		$intLimit = 5;
		$strLimit = " LIMIT  $intLimit";
		if ($page){
			$offset=$page*$intLimit;
			$strLimit = " LIMIT $offset, $intLimit";
		}
        
        $reciboId=$arrayFlt['recibo_id'];
		unset($arrayFlt['recibo_id']);
		foreach ($arrayFlt as $key=>$valor){			
			if (!is_array($valor)){
				if (@@trim($valor)!=""){
					$strFiltro.= " AND $key = $valor";
				}
			}else{
				if (trim($valor[0])!=""){
					$inicio = $valor[0];
					if (trim($valor[1])==""){
						$fin = $valor[0];
					}else{
						$fin = $valor[1];
					}
						
					$strFiltro.= " AND $key between '$inicio' and '$fin'";
				}	
			}
		}
		
        $sql = "select 
                    facturas.id,
                    numero_factura,
                    facturas.monto_total,
                    facturas.monto_base,
                    facturas.monto_iva,
                    monto_maximo,
                    facturas.monto_retencion_islr,
                    facturas.monto_retencion_iva,
                    factura_recibos . *
                from
                    facturas
                        inner join
                    requerimientos ON facturas.requerimiento_id = requerimientos.id
                        left outer join
                    (select 
                        *
                    from
                        factura_recibos
                    where
                        recibo_id = $reciboId) as factura_recibos ON factura_recibos.factura_id = facturas.id
                where
                    1=1 and pagado<1 $strFiltro $strLimit";
		echo $sql;
		$data = $this->query($sql);

        $sql = "select 
                   (count(*)/$intLimit)-1 as cuenta
                from
                    facturas
                        inner join
                    requerimientos ON facturas.requerimiento_id = requerimientos.id
                        left outer join
                    (select 
                        *
                    from
                        factura_recibos
                    where
                        recibo_id = $reciboId) as factura_recibos ON factura_recibos.factura_id = facturas.id
                where
                    1=1 and pagado<1 $strFiltro";
		//echo $sql;
		$paginado = $this->query($sql);
		
		$result['data'] = $data;
		$result['paginado'] = $paginado;
		return $result;
	}
	
	public function getFacturas(){
		$sql="select 
			fecha_facturacion, numero_factura, monto_total , 
			monto_retencion_islr, monto_retencion_iva, fr.*, f.* from factura_recibos fr
				inner join facturas f on fr.factura_id = f.id
				where recibo_id=" . $this->id . " order by fr.id";
		
		$result = $this->query($sql);
		return $result;
	}
	
	public function setFacturaPagada(){
		$sql="update facturas set pagado=1 
				where monto_maximo=0 and pagado=0";
		
		$result = $this->query($sql);	
	}
    
    public function getEstatusRecibo($reciboId){
        $sql="select sum(monto_maximo) as monto_maximo, sum(monto_total) as monto_total from factura_recibos fr
                inner join facturas f on f.id=fr.factura_id
                where recibo_id=$reciboId;";
        $resultMonto=$this->query($sql);
        
        return $resultMonto[0][0];
        
        
    }
}
?>
